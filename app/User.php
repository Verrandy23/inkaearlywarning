<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Entities\Division;
use App\Entities\Project;
use App\Entities\Role;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username','nip','phone', 'email', 'password','role_id','division_name','project_to_pm'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function get_division()
    {
        return $this->belongsTo(Division::class, 'division_id');
    }

    
    public function get_project()
    {
        return $this->belongsTo(Project::class, 'project_to_pm');
    }

    public function get_role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

}

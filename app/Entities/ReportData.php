<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Project;


class ReportData extends Model
{
    protected $table = "reports";
    protected $fillable = [
        'name', 'description', 'project_id', 'date','file'
    ];

    public function get_project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

}

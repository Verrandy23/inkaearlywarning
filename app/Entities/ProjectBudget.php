<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ProjectBudget extends Model
{
    protected $table = "project_budgets";
    protected $fillable = [
        'rencana', 'realisasi','proyeksi', 'date','project_id'
    ];

    public function get_project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

  
}

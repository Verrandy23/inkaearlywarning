<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Category;
use App\Entities\Project;

class Division extends Model
{
     protected $table = "divisions";
     protected $fillable = [
        'id','name','project_id','start_date','end_date'
    ];

    public function get_project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }



}   
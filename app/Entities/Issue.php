<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Project;

class Issue extends Model
{
    protected $table = "issues";
    protected $fillable = [
        'name', 'issue', 'level', 'action','division_name','project_id','date_issue','status','files','created_by',
        'finished_at','finished_by','master_activity_id', 'sub_activity_id', 'work_activity_id'
    ];

    public function get_project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }
}

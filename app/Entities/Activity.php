<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Division;

class Activity extends Model
{

     protected $table = "activities";
     protected $fillable = [
        'id','name','division_id','start_delivery','end_delivery','status_delivery','picture'
    ];

    public function get_division()
    {
        return $this->belongsTo(Division::class, 'division_id');
    }

    /**
     * Get has many sub activity 
     */
    public function get_sub_activities()
    {
        return $this->hasMany(SubActivity::class, 'activity_id');
    }

}   
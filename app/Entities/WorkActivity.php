<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class WorkActivity extends Model
{
     protected $table = "work_activities";
     protected $fillable = [
        'id','name','sub_activity_id','start_date','end_date','status','status_name','pic','slack'
    ];

    public function get_sub_activity()
    {
        return $this->belongsTo(SubActivity::class, 'sub_activity_id');
    }

  



}   
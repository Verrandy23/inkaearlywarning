<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DetailProject extends Model
{
    protected $table = "detail_projects";
    protected $fillable = [
        'batch', 'start_delivery_date', 'end_delivery_date', 'address_delivery','project_id','payment','denda','term_payment'
    ];

    public function get_project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }
    

}

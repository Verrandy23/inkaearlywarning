<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;


class DivisionAdditional extends Model
{
     protected $table = "division_additional";
     protected $fillable = [
        'name','division_id','information','date','file'
    ];

    



}   
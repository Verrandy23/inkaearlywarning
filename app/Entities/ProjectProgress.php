<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ProjectProgress extends Model
{
    protected $table = "project_progress";
    protected $fillable = [
        'progress_rencana', 'progress_realisasi', 'date','project_id'
    ];

    public function get_project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

  
}

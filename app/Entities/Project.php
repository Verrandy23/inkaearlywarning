<?php

namespace App\Entities;

use App\Entities\Division;
use App\Entities\DetailProject;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
     protected $table = "projects";
     protected $fillable = [
        'id','name','start_date','end_date','progress','description','client'
    ];

    /**
     * Get has many division.
     */
    public function get_divisions()
    {
        return $this->hasMany(Division::class,'project_id');
    }

    /**
     * Get has many through activity.
     */
    public function get_details()
    {
        return $this->hasMany(DetailProject::class,'project_id');
    }


}   
<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\WorkActivity;

class LateHistory extends Model
{
    protected $table = "late_histories";
    protected $fillable = [
        'work_activity_id', 'finished_at', 'count_late'
    ];

}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SubActivity extends Model
{
     protected $table = "sub_activities";
     protected $fillable = [
        'name','activity_id', 'id'
    ];


    public function get_activity()
    {
        return $this->belongsTo(Activity::class, 'activity_id');
    }

    /**
     * Get has many work activity 
     */
    public function get_work_activities()
    {
        return $this->hasMany(WorkActivity::class, 'sub_activity_id');
    }


}   
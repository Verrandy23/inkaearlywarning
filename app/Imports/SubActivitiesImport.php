<?php

namespace App\Imports;

use App\Entities\WorkActivity;
use App\Entities\SubActivity;
use App\Entities\Activity;
use App\Entities\Division;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class SubActivitiesImport implements ToCollection, WithHeadingRow, WithMultipleSheets
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {

        
        $max_activity = Activity::max('id');
        $max_subactivity = SubActivity::max('id');
        $max_workactivity = WorkActivity::max('id');
        
        $division = $rows[0]['divisi'];
        $project = $rows[0]['project'];
        
        $division_wbs = array();
        
       
        foreach($rows as $key => $row){
            $wbs = $rows[$key]['wbs'];
            $start = $rows[$key]['start'];
            $finish = $rows[$key]['finish'];
            $task_name = strtolower(trim($row['task_name'], ' '));
            if($task_name == "engineering"){
                $division_wbs['teknologi'] = $wbs;
            }
            else if($task_name == "procurement"){
                $division_wbs['logistik'] = $wbs;
            }
            else if($task_name == "fabrication"){
                $division_wbs['fabrikasi'] = $wbs;
            }
            else if($task_name == "finishing"){
                $division_wbs['finishing'] = $wbs;
            }
            else if($task_name == "testing and delivery" || $task_name == "testing"){
                $division_wbs['testing'] = $wbs;
            }
            else if($task_name == "delivery"){
                $division_wbs['delivery'] = $wbs;
            }
            else{
                $division_wbs['teknologi'] = null;
                $division_wbs['logistik'] = null;
                $division_wbs['testing'] = null;
                $division_wbs['finishing'] = null;
                $division_wbs['fabrikasi'] = null;
                $division_wbs['delivery'] = null;   
            }
        
        }
        
        foreach($rows as $key => $row){
            
            $wbs = $rows[$key]['wbs'];
            $task_name = trim($rows[$key]['task_name'], ' ');
            $start = $rows[$key]['start'];
            $finish = $rows[$key]['finish'];
            $level1 = 0;
            $level2 = 0;
            $level3 = 0;
            $check = ""; 
            $counter = $key+1;

            if (0 < $counter && $counter < $loop){
                $check = $rows[$counter]['wbs'];
            }
            
            
                
            
            
            
            if(substr($wbs, 0, strlen($divisi_wbs['teknologi'])) === $divisi_wbs['teknologi']) {
                #Divisi teknologi
                $count = substr_count($divisi_wbs['teknologi'], '.');
                $level1 = $count+1;
                $level2 = $count+2;
                $level3 = $count+3;
            
                

                if(substr_count($wbs, '.') == $level1){
                   if(substr($check, 0, strlen($wbs)) !== $wbs ){
                        
                         $idActivity+=1;
                         $this->excelActivity($idActivity, $task_name, $id_teknologi[0]->id);

                          $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);

                         $idWorkActivity+=1;
                         $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                   }
                   else{
                        $idActivity+=1;
                        $this->excelActivity($idActivity, $task_name, $id_teknologi[0]->id);
                   }
                }
                elseif(substr_count($wbs, '.') == $level2 ){
                    if(substr($check, 0, strlen($wbs)) !== $wbs){
                        
                        $idSubActivity+=1;
                        $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                       
                        $idWorkActivity+=1;
                        $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                    }
                    else {
                         $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                    }
                   
                }
                elseif(substr_count($wbs, '.') == $level3){
                    $idWorkActivity+=1;
                    $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                }

            }


             elseif(substr($wbs, 0, strlen($divisi_wbs['finishing'])) === $divisi_wbs['finishing']) {
                #divisi finishing
                $count = substr_count($divisi_wbs['finishing'], '.');
                $level1 = $count+1;
                $level2 = $count+2;
                $level3 = $count+3;

                if(substr_count($wbs, '.') == $level1){
                   if(substr($check, 0, strlen($wbs)) !== $wbs ){

                         $idActivity+=1;
                         $this->excelActivity($idActivity, $task_name, $id_finishing[0]->id);

                          $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);

                         $idWorkActivity+=1;
                         $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                   }
                   else{
                        $idActivity+=1;
                        $this->excelActivity($idActivity, $task_name, $id_finishing[0]->id);
                   }
                }
                elseif(substr_count($wbs, '.') == $level2 ){
                    if(substr($check, 0, strlen($wbs)) !== $wbs){
                        
                        $idSubActivity+=1;
                        $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                       
                        $idWorkActivity+=1;
                        $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                    }
                    else {
                         $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                    }
                   
                }
                elseif(substr_count($wbs, '.') == $level3){
                    $idWorkActivity+=1;
                    $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                }

            }
             elseif(substr($wbs, 0, strlen($divisi_wbs['fabrikasi'])) === $divisi_wbs['fabrikasi']) {
                #Divisi fabrikasi
                $count = substr_count($divisi_wbs['fabrikasi'], '.');
                $level1 = $count+1;
                $level2 = $count+2;
                $level3 = $count+3;

              if(substr_count($wbs, '.') == $level1){
                   if(substr($check, 0, strlen($wbs)) !== $wbs ){

                         $idActivity+=1;
                         $this->excelActivity($idActivity, $task_name, $id_fabrikasi[0]->id);

                          $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);

                         $idWorkActivity+=1;
                         $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                   }
                   else{
                        $idActivity+=1;
                        $this->excelActivity($idActivity, $task_name, $id_fabrikasi[0]->id);
                   }
                }
                elseif(substr_count($wbs, '.') == $level2 ){
                    if(substr($check, 0, strlen($wbs)) !== $wbs){
                        
                        $idSubActivity+=1;
                        $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                       
                        $idWorkActivity+=1;
                        $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                    }
                    else {
                         $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                    }
                   
                }
                elseif(substr_count($wbs, '.') == $level3){
                    $idWorkActivity+=1;
                    $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                }

            }
            elseif(substr($wbs, 0, strlen($divisi_wbs['logistik'])) === $divisi_wbs['logistik']) {
                #Divisi logistik
                $count = substr_count($divisi_wbs['logistik'], '.');
                $level1 = $count+1;
                $level2 = $count+2;
                $level3 = $count+3;

               if(substr_count($wbs, '.') == $level1){
                   if(substr($check, 0, strlen($wbs)) !== $wbs ){

                         $idActivity+=1;
                         $this->excelActivity($idActivity, $task_name, $id_logistik[0]->id);

                          $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);

                         $idWorkActivity+=1;
                         $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                   }
                   else{
                        $idActivity+=1;
                        $this->excelActivity($idActivity, $task_name, $id_logistik[0]->id);
                   }
                }
                elseif(substr_count($wbs, '.') == $level2 ){
                    if(substr($check, 0, strlen($wbs)) !== $wbs){
                        
                        $idSubActivity+=1;
                        $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                       
                        $idWorkActivity+=1;
                        $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                    }
                    else {
                         $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                    }
                   
                }
                elseif(substr_count($wbs, '.') == $level3){
                    $idWorkActivity+=1;
                    $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                }

            }


            elseif((substr($wbs, 0, strlen($divisi_wbs['testing'])) === $divisi_wbs['testing']) and !(substr($wbs, 0, strlen($divisi_wbs['delivery'])) == $divisi_wbs['delivery']) and !($wbs==$divisi_wbs['testing'])){
                
                #Divisi Testing

                $count = substr_count($divisi_wbs['testing'], '.');
                $level1 = $count+1;
                $level2 = $count+2;
                $level3 = $count+3;

               if(substr_count($wbs, '.') == $level1){
                   if(substr($check, 0, strlen($wbs)) !== $wbs ){

                         $idActivity+=1;
                         $this->excelActivity($idActivity, $task_name, $id_testing[0]->id);

                          $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);

                         $idWorkActivity+=1;
                         $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                   }
                   else{
                        $idActivity+=1;
                        $this->excelActivity($idActivity, $task_name, $id_testing[0]->id);
                   }
                }
                elseif(substr_count($wbs, '.') == $level2 ){
                    if(substr($check, 0, strlen($wbs)) !== $wbs){
                        
                        $idSubActivity+=1;
                        $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                       
                        $idWorkActivity+=1;
                        $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                    }
                    else {
                         $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                    }
                   
                }
                elseif(substr_count($wbs, '.') == $level3){
                    $idWorkActivity+=1;
                    $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                }

                
            }
            elseif(substr($wbs, 0, strlen($divisi_wbs['delivery'])) === $divisi_wbs['delivery'])
            {
               #Divisi delivery
                $count = substr_count($divisi_wbs['delivery'], '.');
                $level1 = $count+1;
                $level2 = $count+2;
                $level3 = $count+3;

              if(substr_count($wbs, '.') == $level1){
                   if(substr($check, 0, strlen($wbs)) !== $wbs ){

                         $idActivity+=1;
                         $this->excelActivity($idActivity, $task_name, $id_delivery[0]->id);

                          $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);

                         $idWorkActivity+=1;
                         $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                   }
                   else{
                        $idActivity+=1;
                        $this->excelActivity($idActivity, $task_name, $id_delivery[0]->id);
                   }
                }
                elseif(substr_count($wbs, '.') == $level2 ){
                    if(substr($check, 0, strlen($wbs)) !== $wbs){
                        
                        $idSubActivity+=1;
                        $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                       
                        $idWorkActivity+=1;
                        $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                    }
                    else {
                         $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                    }
                   
                }
                elseif(substr_count($wbs, '.') == $level3){
                    $idWorkActivity+=1;
                    $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                }

            }
        
        
    }
    
    
    
    
}

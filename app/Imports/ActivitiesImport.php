<?php

namespace App\Imports;
use App\Entities\WorkActivity;
use App\Entities\SubActivity;
use App\Entities\Activity;
use App\Entities\Division;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use PhpOffice\PhpSpreadsheet\Shared\Date;


class ActivitiesImport implements ToCollection, WithHeadingRow, WithMultipleSheets
{


    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    
    public $success;
     
    public function collection(Collection $rows)
    {
       

        $max_activity = Activity::max('id');
        $max_subactivity = SubActivity::max('id');
        $max_workactivity = WorkActivity::max('id');
         /** membaca id max untuk increment di kemudan
         */

        $idActivity = $max_activity;
        $idSubActivity = $max_subactivity;
        $idWorkActivity = $max_workactivity;

        #$division = $rows[0]['divisi'];
        $project = $rows[0]['project'];

        
        $id_teknologi = DB::table('divisions')->select('divisions.id')->leftJoin('projects', 'projects.id', '=', 'divisions.project_id')->where('projects.name', $project)->where('divisions.name', 'Teknologi')->get();

        $id_fabrikasi = DB::table('divisions')->select('divisions.id')->leftJoin('projects', 'projects.id', '=', 'divisions.project_id')->where('projects.name', $project)->where('divisions.name', 'Fabrikasi')->get();
        $id_finishing = DB::table('divisions')->select('divisions.id')->leftJoin('projects', 'projects.id', '=', 'divisions.project_id')->where('projects.name', $project)->where('divisions.name', 'Finishing')->get();
       $id_testing = DB::table('divisions')->select('divisions.id')->leftJoin('projects', 'projects.id', '=', 'divisions.project_id')->where('projects.name', $project)->where('divisions.name', 'Testing')->get(); 
       $id_delivery = DB::table('divisions')->select('divisions.id')->leftJoin('projects', 'projects.id', '=', 'divisions.project_id')->where('projects.name', $project)->where('divisions.name', 'Delivery')->get(); 
       $id_logistik= DB::table('divisions')->select('divisions.id')->leftJoin('projects', 'projects.id', '=', 'divisions.project_id')->where('projects.name', $project)->where('divisions.name', 'Logistik')->get(); 

        
       
        $divisi_wbs = array();
        
        /* foreach($rows as $row){
            
           $count_whitespace = substr_count($row['task_name'], '   ');
            
            $task = trim($row['task_name'], '   ');

            if ($task == 'TESTING AND DELIVERY'){
                $divisi_wbs = strval($row['wbs']);
            }

            if (substr($row['wbs'], 0, strlen($divisi_wbs)) === $divisi_wbs) {
                if ($row['task_name']==="DELIVERY"){
                    $sub_division = $row['wbs'];
                    echo $sub_division;
                }

              # code...
             }

          }*/
            
            $loop = 0;    
          foreach ($rows as $key => $row) {
              
            $task_name = strtolower(trim($row['task_name'], ' '));
            
            // mendeteksi jumlah array yang memiliki value
            if($rows[$key]['task_name']){
                $loop +=1;
            }
            
            $wbs = $row['wbs'];
            if ($task_name == "ENGINEERING" || $task_name == "engineering"){
                $divisi_wbs['teknologi'] = $wbs;
            }
            if($task_name == "FABRICATION" || $task_name == "fabrication" ){
                $divisi_wbs['fabrikasi'] = $wbs;
            }
            if($task_name == "FINISHING" || $task_name == "finishing" || $task_name == "finishing process"){
                $divisi_wbs['finishing'] = $wbs;

            }
            if($task_name == "TESTING AND DELIVERY" || $task_name == "testing and delivery" || $task_name == "testing & delivery" || ""){
                $divisi_wbs['testing'] = $wbs;
            }
            if ($task_name == "Delivery" || $task_name == "delivery"){
                $divisi_wbs['delivery'] = $wbs;
            }
            if ($task_name == "LOGISTIC" || $task_name == "PROCUREMENT" || $task_name =="procurement" || $task_name == "procurement_plan"){
                $divisi_wbs['logistik'] = $wbs;
            }

         }
         
            
         for($key=0; $key<$loop; $key++){
            
            $wbs = $rows[$key]['wbs'];
            $task_name = trim($rows[$key]['task_name'], ' ');
            $start = $rows[$key]['start'];
            $finish = $rows[$key]['finish'];
            $level1 = 0;
            $level2 = 0;
            $level3 = 0;
            $check = ""; 
            $counter = $key+1;

            if (0 < $counter && $counter < $loop){
                $check = $rows[$counter]['wbs'];
            }
            
            
                
            
            
            
            if(substr($wbs, 0, strlen($divisi_wbs['teknologi'])) === $divisi_wbs['teknologi']) {
                #Divisi teknologi
                $count = substr_count($divisi_wbs['teknologi'], '.');
                $level1 = $count+1;
                $level2 = $count+2;
                $level3 = $count+3;
            
                

                if(substr_count($wbs, '.') == $level1){
                   if(substr($check, 0, strlen($wbs)) !== $wbs ){
                        
                         $idActivity+=1;
                         $this->excelActivity($idActivity, $task_name, $id_teknologi[0]->id);

                          $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);

                         $idWorkActivity+=1;
                         $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                   }
                   else{
                        $idActivity+=1;
                        $this->excelActivity($idActivity, $task_name, $id_teknologi[0]->id);
                   }
                }
                elseif(substr_count($wbs, '.') == $level2 ){
                    if(substr($check, 0, strlen($wbs)) !== $wbs){
                        
                        $idSubActivity+=1;
                        $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                       
                        $idWorkActivity+=1;
                        $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                    }
                    else {
                         $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                    }
                   
                }
                elseif(substr_count($wbs, '.') == $level3){
                    $idWorkActivity+=1;
                    $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                }

            }


             elseif(substr($wbs, 0, strlen($divisi_wbs['finishing'])) === $divisi_wbs['finishing']) {
                #divisi finishing
                $count = substr_count($divisi_wbs['finishing'], '.');
                $level1 = $count+1;
                $level2 = $count+2;
                $level3 = $count+3;

                if(substr_count($wbs, '.') == $level1){
                   if(substr($check, 0, strlen($wbs)) !== $wbs ){

                         $idActivity+=1;
                         $this->excelActivity($idActivity, $task_name, $id_finishing[0]->id);

                          $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);

                         $idWorkActivity+=1;
                         $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                   }
                   else{
                        $idActivity+=1;
                        $this->excelActivity($idActivity, $task_name, $id_finishing[0]->id);
                   }
                }
                elseif(substr_count($wbs, '.') == $level2 ){
                    if(substr($check, 0, strlen($wbs)) !== $wbs){
                        
                        $idSubActivity+=1;
                        $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                       
                        $idWorkActivity+=1;
                        $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                    }
                    else {
                         $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                    }
                   
                }
                elseif(substr_count($wbs, '.') == $level3){
                    $idWorkActivity+=1;
                    $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                }

            }
             elseif(substr($wbs, 0, strlen($divisi_wbs['fabrikasi'])) === $divisi_wbs['fabrikasi']) {
                #Divisi fabrikasi
                $count = substr_count($divisi_wbs['fabrikasi'], '.');
                $level1 = $count+1;
                $level2 = $count+2;
                $level3 = $count+3;

              if(substr_count($wbs, '.') == $level1){
                   if(substr($check, 0, strlen($wbs)) !== $wbs ){

                         $idActivity+=1;
                         $this->excelActivity($idActivity, $task_name, $id_fabrikasi[0]->id);

                          $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);

                         $idWorkActivity+=1;
                         $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                   }
                   else{
                        $idActivity+=1;
                        $this->excelActivity($idActivity, $task_name, $id_fabrikasi[0]->id);
                   }
                }
                elseif(substr_count($wbs, '.') == $level2 ){
                    if(substr($check, 0, strlen($wbs)) !== $wbs){
                        
                        $idSubActivity+=1;
                        $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                       
                        $idWorkActivity+=1;
                        $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                    }
                    else {
                         $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                    }
                   
                }
                elseif(substr_count($wbs, '.') == $level3){
                    $idWorkActivity+=1;
                    $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                }

            }
            elseif(substr($wbs, 0, strlen($divisi_wbs['logistik'])) === $divisi_wbs['logistik']) {
                #Divisi logistik
                $count = substr_count($divisi_wbs['logistik'], '.');
                $level1 = $count+1;
                $level2 = $count+2;
                $level3 = $count+3;

               if(substr_count($wbs, '.') == $level1){
                   if(substr($check, 0, strlen($wbs)) !== $wbs ){

                         $idActivity+=1;
                         $this->excelActivity($idActivity, $task_name, $id_logistik[0]->id);

                          $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);

                         $idWorkActivity+=1;
                         $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                   }
                   else{
                        $idActivity+=1;
                        $this->excelActivity($idActivity, $task_name, $id_logistik[0]->id);
                   }
                }
                elseif(substr_count($wbs, '.') == $level2 ){
                    if(substr($check, 0, strlen($wbs)) !== $wbs){
                        
                        $idSubActivity+=1;
                        $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                       
                        $idWorkActivity+=1;
                        $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                    }
                    else {
                         $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                    }
                   
                }
                elseif(substr_count($wbs, '.') == $level3){
                    $idWorkActivity+=1;
                    $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                }

            }


            elseif((substr($wbs, 0, strlen($divisi_wbs['testing'])) === $divisi_wbs['testing']) and !(substr($wbs, 0, strlen($divisi_wbs['delivery'])) == $divisi_wbs['delivery']) and !($wbs==$divisi_wbs['testing'])){
                
                #Divisi Testing

                $count = substr_count($divisi_wbs['testing'], '.');
                $level1 = $count+1;
                $level2 = $count+2;
                $level3 = $count+3;

               if(substr_count($wbs, '.') == $level1){
                   if(substr($check, 0, strlen($wbs)) !== $wbs ){

                         $idActivity+=1;
                         $this->excelActivity($idActivity, $task_name, $id_testing[0]->id);

                          $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);

                         $idWorkActivity+=1;
                         $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                   }
                   else{
                        $idActivity+=1;
                        $this->excelActivity($idActivity, $task_name, $id_testing[0]->id);
                   }
                }
                elseif(substr_count($wbs, '.') == $level2 ){
                    if(substr($check, 0, strlen($wbs)) !== $wbs){
                        
                        $idSubActivity+=1;
                        $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                       
                        $idWorkActivity+=1;
                        $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                    }
                    else {
                         $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                    }
                   
                }
                elseif(substr_count($wbs, '.') == $level3){
                    $idWorkActivity+=1;
                    $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                }

                
            }
            elseif(substr($wbs, 0, strlen($divisi_wbs['delivery'])) === $divisi_wbs['delivery'])
            {
               #Divisi delivery
                $count = substr_count($divisi_wbs['delivery'], '.');
                $level1 = $count+1;
                $level2 = $count+2;
                $level3 = $count+3;

              if(substr_count($wbs, '.') == $level1){
                   if(substr($check, 0, strlen($wbs)) !== $wbs ){

                         $idActivity+=1;
                         $this->excelActivity($idActivity, $task_name, $id_delivery[0]->id);

                          $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);

                         $idWorkActivity+=1;
                         $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                   }
                   else{
                        $idActivity+=1;
                        $this->excelActivity($idActivity, $task_name, $id_delivery[0]->id);
                   }
                }
                elseif(substr_count($wbs, '.') == $level2 ){
                    if(substr($check, 0, strlen($wbs)) !== $wbs){
                        
                        $idSubActivity+=1;
                        $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                       
                        $idWorkActivity+=1;
                        $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                    }
                    else {
                         $idSubActivity+=1;
                         $this->excelSubActivity($idSubActivity, $task_name, $idActivity);
                    }
                   
                }
                elseif(substr_count($wbs, '.') == $level3){
                    $idWorkActivity+=1;
                    $this->excelWorkActivity($idWorkActivity, $idSubActivity, $task_name, $start, $finish);
                }

            }
        
        
           
          
            
         }

                 
        
        /*
        foreach($rows as $row){

           $count_whitespace = substr_count($row['task_name'], '   ');
           if ($count_whitespace === 2){
                $WorkActivity = new WorkActivity ([
                    'id'        => $idWorkActivity+=1,
                    'name'      => $row['task_name'],
                    'sub_activity_id' => $idSubActivity,
                    'start_date' => Date::excelToDateTimeObject($start),
                    'end_date'  => Date::excelToDateTimeObject($finish),
                ]);
                $WorkActivity->save();
           }
           elseif( $count_whitespace === 1){
            $SubActivity = new SubActivity([
                'id'    => $idSubActivity+=1,
                'name'  => $row['task_name'],
                'activity_id' => $idActivity,
            ]);
            $SubActivity->save();
           }
           else{
            $Activity = new Activity([
                'id'    => $idActivity+=1,
                'name'  => $row['task_name'],
                'division_id' => $div_id[0]->id,
                
            ]);
            $Activity->save();
           }
        }*/

        

    }

 public function excelActivity($id, $task_name, $division){
         
            $Activity = new Activity([
                'id'    => $id,
                'name'  => $task_name,
                'division_id' => $division,
                
            ]);
            $Activity->save();
       
    }

public function excelSubActivity($id, $task_name, $activity_id){
         $SubActivity = new SubActivity([
                'id'    => $id,
                'name'  => $task_name,
                'activity_id' => $activity_id,
            ]);
            $SubActivity->save();

    }

public function excelWorkActivity($id, $sub_activity_id, $task_name, $start, $finish){
         $WorkActivity = new WorkActivity ([
        'id'        => $id,
        'name'      => $task_name,
        'sub_activity_id' => $sub_activity_id,
        'start_date' => Date::excelToDateTimeObject($start),
        'end_date'  => Date::excelToDateTimeObject($finish),
        ]);
        $WorkActivity->save();
    }

  public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }

    public function __construct(){
        $this->success = false;
       
    }

   /* public function model(array $row)
    {

        return new Activity([
            'name'              => $row[0],
            'division_id'       => $row[1],
        ]);
    }
    */

    
}

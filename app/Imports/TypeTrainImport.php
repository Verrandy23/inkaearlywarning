<?php

namespace App\Imports;

use App\Entities\TypeTrain;
use Maatwebsite\Excel\Concerns\ToModel;

class TypeTrainImport implements ToModel
{

    /**
     * Transform a date value into a Carbon object.
     *
     * @return \Carbon\Carbon|null
     */
    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

        return new TypeTrain([
            'name'              => $row[0],
            'work_activity_id'  => $row[1],
            'start_date'        => $this->transformDate($row[2]),
            'end_date'          => $this->transformDate($row[3]),
        ]);
    }
}

<?php

namespace App\Imports;

use App\Entities\Activity;
use Maatwebsite\Excel\Concerns\ToModel;

class ActivitiesDeliveryImport implements ToModel
{
    /**
     * Transform a date value into a Carbon object.
     *
     * @return \Carbon\Carbon|null
     */
    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        return new Activity([
            'name'              => $row[0],
            'division_id'       => $row[1],
            'start_delivery'    => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[2]),
            'end_delivery'      => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[3]),

        ]);
    }
}

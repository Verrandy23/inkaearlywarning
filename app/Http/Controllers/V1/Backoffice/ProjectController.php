<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\Entities\Activity;
use App\Entities\Division;
use App\Entities\Project;
use App\Entities\DivisionAdditional;
use Illuminate\Http\Request;
use DB;
use Auth;
use SweetAlert;

class ProjectController extends Controller
{
    /**
     * Show the page project.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $project = Project::all();
        return view('backoffice.projects.project', compact('project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required',
            'client'        => 'required',
        ]);

        $project = new Project([
            'name'          => $request->get('name'),
            'start_date'    => $request->get('start_date'),
            'end_date'      => $request->get('end_date'),
            'description'   => $request->get('description'),
            'client'        => $request->get('client'),
        ]);
        $project->save();
        alert()->success('Successfully Add Data.', 'Success!');
        return redirect('/projects');
    }


    /**
     * Show the form for editing project.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);
        return view('backoffice.projects.edit', compact('project'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'          => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required',
        ]);

        $project = Project::find($id);
        $project->name              = $request->get('name');
        $project->description       = $request->get('description');
        $project->start_date        = $request->get('start_date');
        $project->end_date          = $request->get('end_date');
        $project->client            = $request->get('client');
        $project->save();
        alert()->success('Successfully Update Data.', 'Success!');
        return redirect('/projects');
    }

    /**
     * Show detail division
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project            = Project::find($id);    
        return view('backoffice.projects.detail_project', compact('project'));
    }

    /**
     * Show detail division for activity
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showDivision(Request $request)
    {
        $id                 = $request->segment(2);
        $project            = Project::where('id', $id)->first();
        $project_id         = $project->id;
        $project_name       = $project->name;
        $remaining          = $project->end_date;
        $division           = Division::all();
        $division_name      = Auth::user()->division_name;


        /* Main Dashboard Division */
        
        //Conditional for Division
        if ($division_name != "Keproyekan") {

            $progress           = DB::select('SELECT divisions.name,divisions.id, sum(work_activities.status) as jumlah, 
                                COUNT(work_activities.status) as total,work_activities.status as status,
                                activities.name as activity_name,sub_activities.name as sub_name , 
                                work_activities.name as work_name,work_activities.start_date as start_date,
                                work_activities.end_date as end_date,divisions.end_date as akhir FROM projects
                                LEFT JOIN divisions ON projects.id = divisions.project_id
                                LEFT JOIN activities ON divisions.id = activities.division_id
                                LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                where projects.id=' . $project_id . ' AND divisions.name ="' . $division_name . '" 
                                GROUP BY divisions.id');

        //Conditional for Admin & PM
        } else {
            $progress           = DB::select('SELECT divisions.name,divisions.id, sum(work_activities.status) as jumlah, 
                                COUNT(work_activities.status) as total,work_activities.status as status,
                                activities.name as activity_name,sub_activities.name as sub_name , 
                                work_activities.name as work_name,work_activities.start_date as start_date,
                                work_activities.end_date as end_date,divisions.end_date as akhir FROM projects
                                LEFT JOIN divisions ON projects.id = divisions.project_id
                                LEFT JOIN activities ON divisions.id = activities.division_id
                                LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                where projects.id= ' . $project_id . '  GROUP BY divisions.id');
                                // AND divisions.name !="Delivery" 
        }

        // For Division Delivery
        // $progressDelivery = DB::select('SELECT activity_deliveries.id,activity_deliveries.name as activity_name,
        //                                 divisions.name as division_name,
        //                                 activity_deliveries.start_date,activity_deliveries.end_date,
        //                                 activity_deliveries.status,divisions.end_date as akhir,
        //                                 COUNT(activity_deliveries.status) as total,SUM(activity_deliveries.status) AS jumlah 
        //                                 FROM projects LEFT JOIN divisions ON projects.id = divisions.project_id 
        //                                 LEFT JOIN activity_deliveries ON divisions.id = activity_deliveries.division_id 
        //                                 WHERE projects.id = ' . $project_id . ' AND divisions.name = "Delivery" GROUP BY divisions.id');
       
    
        // Calculation Progress Based On Progress Division
        $division_end_date;
        foreach ($progress as $rowtanggal) {
            $division_end_date = $rowtanggal->akhir;
        }

        // $division_end_date;
        // foreach ($progressDelivery as $rowtanggal) {
        //     $division_end_date = $rowtanggal->akhir;
        // }

        $hasil = 0;
        foreach ($progress as $row) {

            if ($row->jumlah != null || $row->total != 0) {
                $jumlah = $row->jumlah;
                $total = $row->total;
                $hasil += $jumlah / $total * 100;
            } else {
                $hasil;
            }
        }

        // $hasil2 = 0;
        // foreach ($progressDelivery as $row) {

        //     if ($row->jumlah != null || $row->total != 0) {
        //         $jumlah = $row->jumlah;
        //         $total = $row->total;
        //         $hasil2 = $jumlah / $total * 100;
        //     } else if (empty($row)) {
        //         $hasil2;
        //     } else {
        //         $hasil2;
        //     }
        // }

        // function Update Data For Progress Project Main Dashboard
        if (Auth::user()->role_id == 1 && Auth::user()->role_id ==5) {
            $hasil_akhir = ($hasil + $hasil2) / 5;
            Project::where('id', $project_id)->update(array('progress' => $hasil_akhir));
        }

        //End Calculation Progress Based On Progress Division

        /* End Main Dashboard Division */


        /* Dashboard For Division Show History Activity */
        if ($division_name != "Keproyekan") {

            $history        = DB::select('SELECT work_activities.id as work_activity_id,divisions.name as division_name, 
                                divisions.id,work_activities.status as status,users.name as user_name,work_activities.id as work_activity_id,
                                activities.name as activity_name,sub_activities.name as sub_name , 
                                work_activities.name as work_name, work_activities.start_date as start_date,
                                work_activities.end_date as end_date, divisions.end_date as akhir FROM projects
                                LEFT JOIN divisions ON projects.id = divisions.project_id
                                LEFT JOIN activities ON divisions.id = activities.division_id
                                LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                LEFT JOIN users ON work_activities.pic = users.id  
                                where projects.id= ' . $project_id . ' AND divisions.name like "' . $division_name . '"
                                AND work_activities.status="0" GROUP BY work_activities.id');

            $historyProses  = DB::select('SELECT work_activities.id as work_activity_id,divisions.name as division_name, divisions.id,work_activities.status as status,
                                activities.name as activity_name,sub_activities.name as sub_name,users.name as user_name, 
                                work_activities.name as work_name, work_activities.start_date as start_date,
                                work_activities.end_date as end_date, divisions.end_date as akhir FROM projects
                                LEFT JOIN divisions ON projects.id = divisions.project_id
                                LEFT JOIN activities ON divisions.id = activities.division_id
                                LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                LEFT JOIN users ON work_activities.pic = users.id  
                                where projects.id= ' . $project_id . ' AND divisions.name like "' . $division_name . '"
                                AND work_activities.status!="1" AND work_activities.status!="0" GROUP BY work_activities.id');

            $historyFinish  = DB::select('SELECT divisions.name as division_name, divisions.id,
                                work_activities.status as status,work_activities.id as work_activity_id,
                                activities.name as activity_name,sub_activities.name as sub_name,users.name as user_name,
                                work_activities.name as work_name, work_activities.start_date as start_date,
                                work_activities.end_date as end_date, divisions.end_date as akhir FROM projects
                                LEFT JOIN divisions ON projects.id = divisions.project_id
                                LEFT JOIN activities ON divisions.id = activities.division_id
                                LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id 
                                LEFT JOIN users ON work_activities.pic = users.id 
                                where projects.id= ' . $project_id . ' AND divisions.name like "' . $division_name . '"
                                AND work_activities.status="1" GROUP BY work_activities.id');
        }
        /* End Dashboard For Division Show History Activity */


        /* Dashboard For Division Show Pie Chart */
        if ($division_name != 'Keproyekan') {
            $piechart = DB::select('SELECT work_activities.status_name as status, COUNT(work_activities.id) as jumlah FROM projects
                                JOIN divisions ON projects.id = divisions.project_id
                                JOIN activities ON divisions.id = activities.division_id
                                JOIN sub_activities ON activities.id = sub_activities.activity_id
                                JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                where divisions.name= "' . $division_name . '" AND projects.id = ' . $project_id . ' GROUP BY work_activities.status_name');
        }
        /* End Dashboard For Division Show Pie Chart */


        /* Dashboard For Admin & PM Show Pie Chart */
        if ($division_name == 'Keproyekan') {

            /* Count for activity*/
            $peichartteknologi = DB::select('SELECT work_activities.status_name as status, COUNT(work_activities.id) as jumlah FROM projects
                            JOIN divisions ON projects.id = divisions.project_id
                            JOIN activities ON divisions.id = activities.division_id
                            JOIN sub_activities ON activities.id = sub_activities.activity_id
                            JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                            where divisions.name= "Teknologi" AND projects.id = ' . $project_id . ' GROUP BY work_activities.status_name');

            $division_id_teknologi = Division::where('project_id', $project_id)->where('name', 'Teknologi')->first();
            $division_id_teknologi_pie  = $division_id_teknologi->id;

            $peichartlogistik = DB::select('SELECT work_activities.status_name as status, COUNT(work_activities.id) as jumlah FROM projects
                            JOIN divisions ON projects.id = divisions.project_id
                            JOIN activities ON divisions.id = activities.division_id
                            JOIN sub_activities ON activities.id = sub_activities.activity_id
                            JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                            where divisions.name= "Logistik" AND projects.id = ' . $project_id . ' GROUP BY work_activities.status_name');

            $division_id_logistik       = Division::where('project_id', $project_id)->where('name', 'Logistik')->first();
            $division_id_logistik_pie   = $division_id_logistik->id;

            $peichartfabrikasi = DB::select('SELECT work_activities.status_name as status, COUNT(work_activities.id) as jumlah FROM projects
                            JOIN divisions ON projects.id = divisions.project_id
                            JOIN activities ON divisions.id = activities.division_id
                            JOIN sub_activities ON activities.id = sub_activities.activity_id
                            JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                            where divisions.name= "Fabrikasi" AND projects.id = ' . $project_id . ' GROUP BY work_activities.status_name');
            $division_id_fabrikasi       = Division::where('project_id', $project_id)->where('name', 'Logistik')->first();
            $division_id_fabrikasi_pie   = $division_id_logistik->id;

            $peichartfinishing = DB::select('SELECT work_activities.status_name as status, COUNT(work_activities.id) as jumlah FROM projects
                            JOIN divisions ON projects.id = divisions.project_id
                            JOIN activities ON divisions.id = activities.division_id
                            JOIN sub_activities ON activities.id = sub_activities.activity_id
                            JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                            where divisions.name= "Finishing" AND projects.id = ' . $project_id . ' GROUP BY work_activities.status_name');
            $division_id_finishing       = Division::where('project_id', $project_id)->where('name', 'Finishing')->first();
            $division_id_finishing_pie   = $division_id_finishing->id;

            $peicharttesting = DB::select('SELECT work_activities.status_name as status, COUNT(work_activities.id) as jumlah FROM projects
                            JOIN divisions ON projects.id = divisions.project_id
                            JOIN activities ON divisions.id = activities.division_id
                            JOIN sub_activities ON activities.id = sub_activities.activity_id
                            JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                            where divisions.name= "Testing" AND projects.id = ' . $project_id . ' GROUP BY work_activities.status_name');
            $division_id_testing       = Division::where('project_id', $project_id)->where('name', 'Testing')->first();
            $division_id_testing_pie   = $division_id_testing->id;

            // $peichartdelivery = DB::select('SELECT activity_deliveries.status_name as status, 
            //                 COUNT(activity_deliveries.id) as jumlah FROM projects
            //                 JOIN divisions ON projects.id = divisions.project_id
            //                 JOIN activity_deliveries ON divisions.id = activity_deliveries.division_id
            //                 where divisions.name= "Delivery" AND projects.id = ' . $project_id . ' GROUP BY activity_deliveries.status_name');
            // $division_id_delivery       = Division::where('project_id', $project_id)->where('name', 'Delivery')->first();
            // $division_id_delivery_pie   = $division_id_delivery->id;
        }
        /* End Dashboard For Admin & PM Show Pie Chart */


        /*  Dashboard For Division Fabrikasi Show Activity Based On Status */

            $fabrikasiActivity   = DB::select('SELECT activities.id,activities.name FROM projects 
                                    JOIN divisions ON projects.id = divisions.project_id 
                                    JOIN activities ON divisions.id = activities.division_id 
                                    WHERE projects.id = ' . $project_id . ' AND divisions.name = "Fabrikasi" 
                                    AND activities.name REGEXP "Carbody|Bogie"');


            if ($request->has('search')) {
            $filter         = $request->search;
            $fabrikasiName  = Activity::where('id',$filter)->select('name')->get();
            foreach($fabrikasiName as $row){
                $fabrikasiName = $row->name;
            }
            
            $fabrikasi      = DB::select('SELECT sub_activities.name as sub_name,COUNT(case work_activities.status 
                                    WHEN 1 then 1 else null end) as total_finish, 
                                    COUNT(work_activities.id) as total,work_activities.status FROM projects 
                                    LEFT JOIN divisions ON projects.id = divisions.project_id
                                    LEFT JOIN activities ON divisions.id = activities.division_id
                                    LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                    LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                    WHERE activities.id = ' . $filter . ' GROUP BY sub_activities.id');
            } else {
            
            $fabrikasiName   = "Carbody Assy";
            $fabrikasi       = DB::select('SELECT sub_activities.name as sub_name,COUNT(case work_activities.status 
                                    WHEN 1 then 1 else null end) as total_finish, 
                                    COUNT(work_activities.id) as total,work_activities.status FROM projects 
                                    LEFT JOIN divisions ON projects.id = divisions.project_id
                                    LEFT JOIN activities ON divisions.id = activities.division_id
                                    LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                    LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                    WHERE activities.name like "%Carbody%" and divisions.name = "Fabrikasi" 
                                    and projects.id = ' . $project_id . ' GROUP BY sub_activities.id');
            }


        /*  End Dashboard For Division Fabrikasi Show Activity Based On Status */

        /*  Dashboard For Division Finishing Show Activity Based On Status */
            
            // Condition for project 4 TS KRDE (Conditional Case)
            if($project_id == 2) {

                $taktActivity = DB::select('SELECT activities.id,activities.name FROM projects 
                                    JOIN divisions ON projects.id = divisions.project_id 
                                    JOIN activities ON divisions.id = activities.division_id 
                                    WHERE projects.id = ' . $project_id . ' AND divisions.name = "Finishing" 
                                    AND activities.name like "%Takt%"');

                if ($request->has('search')) {
                    $filter         = $request->search;
                    $taktName       = Activity::where('id',$filter)->select('name')->get();
                    foreach($taktName as $row){
                        $taktName = $row->name;
                    }

                    $takt         = DB::select('SELECT sub_activities.name as sub_name,COUNT(case work_activities.status 
                                        WHEN 1 then 1 else null end) as total_finish, 
                                        COUNT(work_activities.id) as total,work_activities.status FROM projects 
                                        LEFT JOIN divisions ON projects.id = divisions.project_id
                                        LEFT JOIN activities ON divisions.id = activities.division_id
                                        LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                        LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                        WHERE activities.id = ' . $filter . ' GROUP BY sub_activities.id');
                } else {
                    $taktName   = "Takt 1";
                    $takt       = DB::select('SELECT sub_activities.name as sub_name,COUNT(case work_activities.status 
                                        WHEN 1 then 1 else null end) as total_finish, 
                                        COUNT(work_activities.id) as total,work_activities.status FROM projects 
                                        LEFT JOIN divisions ON projects.id = divisions.project_id
                                        LEFT JOIN activities ON divisions.id = activities.division_id
                                        LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                        LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                        WHERE sub_activities.name like "%Takt 1%" and divisions.name = "Finishing" 
                                        and projects.id = ' . $project_id . ' GROUP BY sub_activities.id');
                }
            
            // for all projects
            } else {

                $taktActivity = DB::select('SELECT activities.id,activities.name FROM projects 
                                    JOIN divisions ON projects.id = divisions.project_id 
                                    JOIN activities ON divisions.id = activities.division_id 
                                    WHERE projects.id = ' . $project_id . ' AND divisions.name = "Finishing" 
                                    AND activities.name like "%Takt%"');

                if ($request->has('search')) {
                    $filter         = $request->search;
                    $taktName       = Activity::where('id',$filter)->select('name')->get();
                    foreach($taktName as $row){
                        $taktName = $row->name;
                    }
                    $takt         = DB::select('SELECT sub_activities.name as sub_name,COUNT(case work_activities.status 
                                        WHEN 1 then 1 else null end) 
                                        as total_finish, COUNT(work_activities.id) as total,work_activities.status FROM projects 
                                        LEFT JOIN divisions ON projects.id = divisions.project_id
                                        LEFT JOIN activities ON divisions.id = activities.division_id
                                        LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                        LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                        WHERE activities.id = ' . $filter . ' GROUP BY sub_activities.id');
                } else {
                    $taktName   = "Takt 1";
                    $takt       = DB::select('SELECT sub_activities.name as sub_name,COUNT(case work_activities.status 
                                        WHEN 1 then 1 else null end) 
                                        as total_finish, COUNT(work_activities.id) as total,work_activities.status FROM projects 
                                        LEFT JOIN divisions ON projects.id = divisions.project_id
                                        LEFT JOIN activities ON divisions.id = activities.division_id
                                        LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                        LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                        WHERE activities.name like "%Takt 1%" and divisions.name = "Finishing" 
                                        and projects.id = ' . $project_id . ' GROUP BY sub_activities.id');
                }
     
            }

        /*  End Dashboard For Division Finishing Show Activity Based On Status */

        /*  Dashboard Picture For Division Fabrikasi & Finishing */

        // Division Page
        $progressPicture      = DB::select('SELECT activities.id,activities.name as activity_name,activities.picture FROM projects
                                LEFT JOIN divisions ON projects.id = divisions.project_id
                                LEFT JOIN activities ON divisions.id = activities.division_id
                                where projects.id= ' . $project_id . ' AND divisions.name like "' . $division_name . '"');

        // Admin Page
        $progressFabrikasiPictureAdmin = DB::select('SELECT activities.name as activity_name,activities.picture FROM projects
                                LEFT JOIN divisions ON projects.id = divisions.project_id
                                LEFT JOIN activities ON divisions.id = activities.division_id
                                where projects.id= ' . $project_id . ' AND divisions.name = "Fabrikasi"');

        $progressFinishingPictureAdmin = DB::select('SELECT activities.name as activity_name,activities.picture FROM projects
                                LEFT JOIN divisions ON projects.id = divisions.project_id
                                LEFT JOIN activities ON divisions.id = activities.division_id
                                where projects.id= ' . $project_id . ' AND divisions.name = "Finishing"');

        /* End Dashboard Picture For Division Fabrikasi & Finishing */


        /*  Dashboard Additional For Division */

            $division               = Division::where('id', $id)->first();
            $id_division            = $division->id;
            $division_additional    = DivisionAdditional::where('division_id',$id_division)->get();

        /*  End Dashboard Additional For Division */

        

       

        /* Conditional For Division */
        if ($division_name != 'Keproyekan'){
            return view('backoffice.division.dashboard_division', compact(
                'project','project_name',
                'progress',
                'progressPicture',
                'taktActivity','taktName','takt',
                'fabrikasiActivity','fabrikasiName','fabrikasi',
                //'bogie',
                'project_id',
                //'progressDelivery',
                'history',
                'historyProses',
                'historyFinish',
                'division',
                'remaining',
                'hasil',
                //'hasil2',
                'division_end_date',
                'piechart',
                'division_additional','id_division'
            ));

        /* Conditional For Admin */
        } else {
            return view('backoffice.division.dashboard_division', compact(
                'project','project_name',
                'progress','progressPicture',
                'progressFabrikasiPictureAdmin',
                'progressFinishingPictureAdmin',
                'project_id',
                //'progressDelivery',
                'division_id_teknologi_pie',
                'division_id_logistik_pie',
                'division_id_fabrikasi_pie',
                'division_id_finishing_pie',
                'division_id_testing_pie',
                //'division_id_delivery_pie',
                'hasil',
               // 'hasil2',
                'division_end_date',
                'peichartteknologi',
                'peichartlogistik',
                //'peichartdelivery',
                'peichartfabrikasi',
                'peichartfinishing',
                'peicharttesting',
                'division_additional','id_division'

            ));
        }
    }


    /**
     * Show detail activity for pie chart for admin
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showDetailPieChartAdmin(Request $request)
    {
        $id                 = $request->segment(2);
        $project            = Project::where('id', $id)->first();
        $project_id         = $project->id;
        $status_name        = $request->segment(3);
        $division_id        = $request->segment(4);


        $detailPieChart    = DB::select('SELECT projects.name as project_name,work_activities.pic,
                        divisions.name as division_name,activities.name as activity_name,sub_activities.name as sub_name,
                        work_activities.name as work_name,work_activities.status_name as status FROM projects
                        LEFT JOIN divisions ON projects.id = divisions.project_id
                        LEFT JOIN activities ON divisions.id = activities.division_id
                        LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                        LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                        where divisions.id= ' . $division_id . ' AND projects.id = ' . $project_id . ' 
                        AND work_activities.status_name = "' . $status_name . '"');

        return view('backoffice.pie_chart.page_detail_pie_admin', compact('detailPieChart'));
    }

    /**
     * Show detail activity pie chart for division
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showDetailPieChartDivision(Request $request)
    {
        $division_name      = Auth::user()->division_name;
        $id                 = $request->segment(2);
        $project            = Project::where('id', $id)->first();
        $project_id         = $project->id;
        $status_name        = $request->segment(3);
        $division_id        = $request->segment(4);

        $detailPieChartDivision = DB::select('SELECT projects.name as project_name,
                        divisions.name as division_name,work_activities.pic,work_activities.pic,
                        activities.name as activity_name,sub_activities.name as sub_name,
                        work_activities.name as work_name,work_activities.status_name as status FROM projects
                        LEFT JOIN divisions ON projects.id = divisions.project_id
                        LEFT JOIN activities ON divisions.id = activities.division_id
                        LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                        LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                        where divisions.name= "' . $division_name . '" AND projects.id = ' . $project_id . ' 
                        AND work_activities.status_name = "' . $status_name . '"');


        return view('backoffice.pie_chart.page_detail_pie_division', compact('detailPieChartDivision'));
    }


    /** 
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $project = Project::find($id);
        $project->delete();
        alert()->success('Successfully Remove Data.', 'Success!');
        return redirect('/projects');
    }
}

<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\Entities\Activity;
use App\Entities\WorkActivity;
use App\Entities\SubActivity;
use Illuminate\Http\Request;
use PDF;
use DB;

class MasterDataController extends Controller
{
    /**
     * master activity page all
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $activities = Activity::with('get_division')->get();
        return view('backoffice.masters.master_activity', compact('activities'));
    }

    /**
     * master sub activity page all
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showSubActivity(Request $request)
    {
        $sub_activities = SubActivity::with('get_activity')->get();
        return view('backoffice.masters.master_sub_activity', compact('sub_activities'));
    }


    /**
     * master sub activity page all
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showWorkActivity(Request $request)
    {
        $work_activities = WorkActivity::with('get_sub_activity')->get();
        return view('backoffice.masters.master_work_activity', compact('work_activities'));
    }

    public function cetak_laporan_activity()
    {
        $activities = Activity::with('get_division')->get();
        ini_set('memory_limit', '8192M');
        $pdf = PDF::loadview('backoffice.masters.report_master_activity', ['activities'=>$activities]);
        set_time_limit(600);
        return $pdf->download('report-master-activity.pdf');

    }

    public function cetak_laporan_sub_activity()
    {
        $sub_activities = SubActivity::with('get_activity')->get();
        ini_set('memory_limit', '8192M');
        $pdf = PDF::loadview('backoffice.masters.report_master_sub_activity', ['sub_activities'=>$sub_activities]);
        set_time_limit(600);
        return $pdf->download('report-master-sub-activity.pdf');

    }

    public function cetak_laporan_work_activity()
    {
        $work_activities = WorkActivity::with('get_sub_activity')->get();
        ini_set('memory_limit', '8192M');
        $pdf = PDF::loadview('backoffice.masters.report_master_work_activity', ['work_activities'=>$work_activities]);
        set_time_limit(600);
        return $pdf->download('report-master-work-activity.pdf');

    }
}

<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Entities\Division;
use App\Entities\Role;
use App\Entities\Project;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Show the page registration.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::with(['get_role'])->orderBy('users.id','DESC')->get();
        return view('backoffice.users.data_users', compact('user'));
    }

    /**
     * Show the form registration.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showFormRegister()
    {
        $project    = Project::all();
        $role       = Role::all();
        $division   = Division::all();
        return view('backoffice.users.register', compact('role','division','project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required', 'string', 'max:255',
            'username'      => 'required', 'string', 'max:255',
            'nip'           => 'required', 'max:255', 'unique:users',
            'phone'         => 'required', 'max:15',
            'email'         => 'required', 'string', 'email', 'max:255', 'unique:users',
            'password'      => 'required', 'string', 'min:3',
            'division_name' => 'required',
            'role_id'       => 'required',
        ]);

        $user = new User([
            'name'          => $request->get('name'),
            'username'      => $request->get('username'),
            'nip'           => $request->get('nip'),
            'email'         => $request->get('email'),
            'phone'         => $request->get('phone'),
            'division_name' => $request->get('division_name'),
            'password'      => Hash::make($request->get('password')),
            'role_id'       => $request->get('role_id'),
            'project_to_pm' => $request->get('project_to_pm'),
        ]);
        $user->save();
        alert()->success('Successfully Add Data.', 'Success!');
        return redirect('/auths');
    }

    /**
     * Show the form for editing user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user       = User::find($id);
        $role       = Role::all();
        $project    = Project::all();
        $division   = Division::all();
        return view('backoffice.users.edit', compact('user','role','division','project'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if(empty($request->password)){
            $user->name         = $request->get('name');
            $user->username     = $request->get('username');
            $user->nip           = $request->get('nip');
            $user->email         = $request->get('email');
            $user->phone         = $request->get('phone');
            $user->division_name = $request->get('division_name');
            $user->role_id       = $request->get('role_id');
            $user->project_to_pm = $request->get('project_to_pm');
            $user->save();
        } else {
            $user->name         = $request->get('name');
            $user->username     = $request->get('username');
            $user->nip           = $request->get('nip');
            $user->email         = $request->get('email');
            $user->phone         = $request->get('phone');
            $user->division_name = $request->get('division_name');
            $user->password      = Hash::make($request->get('password'));
            $user->role_id       = $request->get('role_id');
            $user->project_to_pm = $request->get('project_to_pm');
            $user->save();
        }
        alert()->success('Successfully Update Data.', 'Success!');
        return redirect('/auths');
    }


    /** 
     * Remove the specified resou   rce from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        alert()->success('Successfully Remove Data.', 'Success!');
        return redirect('/auths');
    }


}

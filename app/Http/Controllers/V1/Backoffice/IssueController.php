<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\Entities\Project;
use App\Entities\Issue;
use App\Entities\Activity;
use App\Entities\Division;
use Illuminate\Http\Request;
use DB;
use Auth;
use PDF;


class IssueController extends Controller
{

    /**
     * Show the form for editing issue.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project = Project::all();
        if (Auth::user()->project_to_pm == null) {
            $teknologi  = Issue::with('get_project')->join('users','users.id','issues.created_by')
            ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                                ->where('issues.division_name', "Teknologi")
                                ->select('issues.*','users.name as user_name',  'activities.name as master_activity')->get();
            $logistik   = Issue::with('get_project')->join('users','users.id','issues.created_by')
            ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                                ->where('issues.division_name', "Logistik")
                                ->select('issues.*','users.name as user_name',  'activities.name as master_activity')->get();
            $fabrikasi  = Issue::with('get_project')->join('users','users.id','issues.created_by')
            ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                                ->where('issues.division_name', "Fabrikasi")
                                ->select('issues.*','users.name as user_name',  'activities.name as master_activity')->get();
            $finishing  = Issue::with('get_project')->join('users','users.id','issues.created_by')
            ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                                ->where('issues.division_name', "Finishing")
                                ->select('issues.*','users.name as user_name',  'activities.name as master_activity')->get();
            $testing    = Issue::with('get_project')->join('users','users.id','issues.created_by')
            ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                                ->where('issues.division_name', "Testing")
                                ->select('issues.*','users.name as user_name',  'activities.name as master_activity')->get();
            $delivery   = Issue::with('get_project')->join('users','users.id','issues.created_by')
            ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                                ->where('issues.division_name', "Delivery")
                                ->select('issues.*','users.name as user_name',  'activities.name as master_activity')->get();
        } else {
            //condition for pm
            $id     = Auth::user()->project_to_pm;
            $teknologi  = Issue::join('projects','projects.id','issues.project_id','activities','activities.id', 'issues.master_activity.id')
                            ->leftJoin('users','users.id','issues.created_by')
                            ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                            ->where('issues.division_name', "Teknologi")
                            ->where('issues.project_id',$id)
                            ->select('issues.*','users.name as user_name' ,'activities.name as master_activity')->get();
            $logistik   = Issue::join('projects','projects.id','issues.project_id')
                            ->leftJoin('users','users.id','issues.created_by')
                            ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                            ->where('issues.division_name', "Logistik")
                            ->where('issues.project_id',$id)
                            ->select('issues.*','users.name as user_name','activities.name as master_activity')->get();
            $fabrikasi  = Issue::join('projects','projects.id','issues.project_id')
                            ->leftJoin('users','users.id','issues.created_by')
                            ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                            ->where('issues.division_name', "Fabrikasi")
                            ->where('issues.project_id',$id)
                            ->select('issues.*','users.name as user_name', 'activities.name as master_activity')->get();
            $finishing  = Issue::join('projects','projects.id','issues.project_id')
                            ->leftJoin('users','users.id','issues.created_by')
                            ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                            ->where('issues.division_name', "Finishing")
                            ->where('issues.project_id',$id)
                            ->select('issues.*','users.name as user_name', 'activities.name as master_activity')->get();
            $testing    = Issue::join('projects','projects.id','issues.project_id')
                            ->leftJoin('users','users.id','issues.created_by')
                            ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                            ->where('issues.division_name', "Testing")
                            ->where('issues.project_id',$id)
                            ->select('issues.*','users.name as user_name', 'activities.name as master_activity')->get();
            $delivery   = Issue::join('projects','projects.id','issues.project_id')
                            ->leftJoin('users','users.id','issues.created_by')
                            ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                            ->where('issues.division_name', "Delivery")
                            ->where('issues.project_id',$id)
                            ->select('issues.*','users.name as user_name', 'activities.name as master_activity')->get();

        }
       
        return view('backoffice.issues.issue', compact('teknologi','logistik','fabrikasi','finishing','testing','delivery','project'));
    }





    /**
     * Show the form for editing issue.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create_issue()
    {
        $project = Project::all();
        $master_activity = Activity::all();
        return view('backoffice.issues.create_issue', compact('project', 'master_activity'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required',
            'issue'         => 'required',
            'status'        => 'required',
            'project_id'    => 'required',
            'division_name' => 'required',
            'date_issue'    => 'required',    
        ]);


        if (!empty($request->file_data)){
            
        $file = time() . '.' . $request->file_data->getClientOriginalExtension();
        $request->file_data->move(public_path('issue'), $file);

            $issue = new Issue([
                'name'              => $request->get('name'),
                'issue'             => $request->get('issue'),
                'level'             => $request->get('level'),
                'project_id'        => $request->get('project_id'),
                'division_name'     => $request->get('division_name'),
                'date_issue'        => $request->get('date_issue'),
                'end_date_issue'    => $request->get('end_date_issue'),
                'status'            => $request->get('status'),
                'created_by'        => $request->get('created_by'),
                'master_activity_id'=> $request->get('master_activity'),
                'files'             => $file,
            ]);
        } else {
            $issue = new Issue([
                'name'              => $request->get('name'),
                'issue'             => $request->get('issue'),
                'level'             => $request->get('level'),
                'project_id'        => $request->get('project_id'),
                'division_name'     => $request->get('division_name'),
                'date_issue'        => $request->get('date_issue'),
                'end_date_issue'    => $request->get('end_date_issue'),
                'status'            => $request->get('status'),
                'created_by'        => $request->get('created_by'),
                'master_activity_id'=> $request->get('master_activity'),
            ]);
        }
        $issue->save();
        alert()->success('Successfully Add Data.', 'Success!');
        return redirect('/issues');
    }
   
    /**
     * Show the form for editing issue.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $issue      = Issue::find($id);
        $division   = Division::all();
        $project    = Project::all();
        $master_activity = Activity::all();
        return view('backoffice.issues.edit', compact('issue','project','division','master_activity'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $issue = Issue::find($id);
        if(Auth::user()->id != $issue->created_by){
            $finished_at = date("Y-m-d");
            $issue->status          = $request->get('status');
            $issue->action          = $request->get('action');
            $issue->finished_by     = $request->get('finished_by');
            $issue->finished_at     = $finished_at;
            $issue->save();
            alert()->success('Successfully Update Data.', 'Success!');            
            return redirect('/issues');

        }
        elseif (empty($request->image)) {
            $issue->name            = $request->get('name');
            $issue->issue           = $request->get('issue');
            $issue->level           = $request->get('level');
            $issue->action          = $request->get('action');
            $issue->division_name   = $request->get('division_name');
            $issue->project_id      = $request->get('project_id');
            $issue->status          = $request->get('status');
            $issue->save();
            alert()->success('Successfully Update Data.', 'Success!');
            return redirect('/issues');

        } else {
                
            $file = time() . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('issue'), $file);

            $issue->name            = $request->get('name');
            $issue->issue           = $request->get('issue');
            $issue->level           = $request->get('level');
            $issue->action          = $request->get('action');
            $issue->division_name   = $request->get('division_name');
            $issue->project_id      = $request->get('project_id');
            $issue->status          = $request->get('status');
            $issue->files           = $file;
            $issue->save();
            alert()->success('Successfully Update Data.', 'Success!');
            return redirect('/issues');            
        }
    }

    
    /**
     * Show the detail page for issue.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id             = $request->segment(2);
        $issue          = Issue::where('id', $id)->first();
        $issue_id       = $issue->id;
        $issue          = DB::select('SELECT issues.*,projects.name as project_name, 
                                pic.name as pic_name,finished.name as finished_name
                                FROM issues 
                                left JOIN users as pic ON pic.id = issues.created_by 
                                left JOIN users as finished ON finished.id = issues.finished_by
                                left JOIN projects ON projects.id = issues.project_id
                                where issues.id = '.$issue_id.'');
        return view('backoffice.issues.detail_issue', compact('issue'));
    }

    /** 
     * Remove the specified resou   rce from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $issue = Issue::find($id);
        $issue->delete();
        alert()->success('Successfully Remove Data.', 'Success!');
        return redirect('/issues');
    }
    public function get_master_activity($id){
        $getstamp = DB::table('activities')->where("division_id", '=', $id)->get();

        return response()->json(array('master_activity' => $getstamp));

    }
    
    public function get_sub_activity($id)
    {
        $getstamp = DB::table('sub_activities')->where("activity_id", '=', $id)->get();

        return response()->json(array('sub_activity' => $getstamp));


    }

    public function get_divisions($id){
        $getstamp = DB::table('divisions')->where("project_id", '=', $id)->get();

        return response()->json(array('divisions' => $getstamp));
    }

    public function get_work_activity($id){
        $getstamp = DB::table('work_activities')->where("sub_activity_id", '=', $id)->get();

        return response()->json(array('work_activity' => $getstamp));
    }

    
    public function test(){
        $teknologi  = Issue::join('projects','projects.id','issues.project_id')
                                ->leftJoin('users','users.id','issues.created_by')
                                ->leftjoin('activities', 'activities.id', 'issues.master_activity_id')
                            
                                ->where('issues.division_name', "Teknologi")
                                ->select('issues.*','users.name as user_name', 'activities.name as master_activity')->get();

        return response()->json(array('teknologi'=>$teknologi));
    }
    
    public function cetak_laporan_isu_teknologi(Request $request)
    {
        $name_project = $request->get('project');
        $status = $request->get('status');
        $level = $request->get('level');

        

         if($name_project != null and $status != null and $level != null)
        {

            $teknologi  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Teknologi " and projects.name=:name_project and issues.level=:level and issues.status=:status', array('name_project' => $name_project, 'level'=>$level, 'status'=> $status));
        }

        elseif ($name_project != null or $status != null or $level != null ){
           $teknologi =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Teknologi"'.(is_null($name_project) ? '' : ' AND projects.name="'.$name_project.'"').(is_null($status) ? '' : ' AND issues.status = "'.$status.'"').(is_null($level) ? '' : ' AND issues.level = "'.$level.'"'));
        }

        else{
        $teknologi  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Teknologi"');
        
        }
        
        $pdf = PDF::loadview('backoffice.issues.report_isu_teknologi', ['teknologi'=>$teknologi])->setPaper('a4', 'landscape');
        return $pdf->download('laporan-isu-teknologi.pdf');
    }

    public function cetak_laporan_isu_logistik(Request $request)
    {
        $name_project = "";
        $status = "";
        $level = "";

        if($request->has('status') || $request->has('project') || $request->has('level')){
            $name_project = $request->get('project');
            $status = $request->get('status');
            $level = $request->get('level');
        }
        else{

        $name_project = $request->get('project1');
        $status = $request->get('status1');
        $level = $request->get('level1');
        
        }
            
        

        
         if($name_project != null and $status != null and $level != null)
        {

            $logistik  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Logistik " and projects.name=:name_project and issues.level=:level and issues.status=:status', array('name_project' => $name_project, 'level'=>$level, 'status'=> $status));
        }

        elseif ($name_project != null or $status != null or $level != null ){

            $logistik  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Logistik"'.(is_null($name_project) ? '' : ' AND projects.name="'.$name_project.'"').(is_null($status) ? '' : ' AND issues.status = "'.$status.'"').(is_null($level) ? '' : ' AND issues.level = "'.$level.'"'));
          /*
           if ($name_project != null){
                  $logistik  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Logistik " and projects.name=:name_project', array('name_project' => $name_project));
           }
           elseif($status != null )
           {
                  $logistik  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Logistik " and issues.status = :status ', array('status' => $status));
           }
           elseif($level != null){
              $logistik  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Logistik " and issues.level=:level', array('level' => $level));
           }
           */
        }

        else{
        $logistik  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Logistik"');
        
        }

        $pdf = PDF::loadview('backoffice.issues.report_isu_logistik', ['logistik'=>$logistik])->setPaper('a4', 'landscape');
        return $pdf->download('laporan-isu-logistik.pdf');
    }

    public function cetak_laporan_isu_fabrikasi(Request $request)
    {
        if($request->has('status') || $request->has('project') || $request->has('level')){
            $name_project = $request->get('project');
            $status = $request->get('status');
            $level = $request->get('level');
        }
        else{
       $name_project = $request->get('project2');
        $status = $request->get('status2');
        $level = $request->get('level2');
        //dd($level);
    }
        
         if($name_project != null and $status != null and $level != null)
        {

            $fabrikasi  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Fabrikasi " and projects.name=:name_project and issues.level=:level and issues.status=:status', array('name_project' => $name_project, 'level'=>$level, 'status'=> $status));
        }

        elseif ($name_project != null or $status != null or $level != null ){
          
           $fabrikasi =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Fabrikasi"'.(is_null($name_project) ? '' : ' AND projects.name="'.$name_project.'"').(is_null($status) ? '' : ' AND issues.status = "'.$status.'"').(is_null($level) ? '' : ' AND issues.level = "'.$level.'"'));
        }

        else{
        $fabrikasi  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Fabrikasi"');
        
        }
        $pdf = PDF::loadview('backoffice.issues.report_isu_fabrikasi', ['fabrikasi'=>$fabrikasi])->setPaper('a4', 'landscape');
        return $pdf->download('laporan-isu-fabrikasi.pdf');
    }

    public function cetak_laporan_isu_finishing(Request $request)
    {
        if($request->has('status') || $request->has('project') || $request->has('level')){
            $name_project = $request->get('project');
            $status = $request->get('status');
            $level = $request->get('level');
        }
        else{

        $name_project = $request->get('project3');
        $status = $request->get('status3');
        $level = $request->get('level3');
    }
        
         if($name_project != null and $status != null and $level != null)
        {

            $finishing  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Finishing " and projects.name=:name_project and issues.level=:level and issues.status=:status', array('name_project' => $name_project, 'level'=>$level, 'status'=> $status));
        }

        elseif ($name_project != null or $status != null or $level != null ){
          
           $finishing =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Finishing"'.(is_null($name_project) ? '' : ' AND projects.name="'.$name_project.'"').(is_null($status) ? '' : ' AND issues.status = "'.$status.'"').(is_null($level) ? '' : ' AND issues.level = "'.$level.'"'));
        }

        else{
        $finishing  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Finishing"');
        
        }
        $pdf = PDF::loadview('backoffice.issues.report_isu_finishing', ['finishing'=>$finishing])->setPaper('a4', 'landscape');
        return $pdf->download('laporan-isu-finishing.pdf');
    }

    public function cetak_laporan_isu_testing(Request $request)
    {
        if($request->has('status') || $request->has('project') || $request->has('level')){
            $name_project = $request->get('project');
            $status = $request->get('status');
            $level = $request->get('level');
        }
        else{

        $name_project = $request->get('project4');
        $status = $request->get('status4');
        $level = $request->get('level4');
    }

         if($name_project != null and $status != null and $level != null)
        {

            $testing  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Testing " and projects.name=:name_project and issues.level=:level and issues.status=:status', array('name_project' => $name_project, 'level'=>$level, 'status'=> $status));
        }

        elseif ($name_project != null or $status != null or $level != null ){
           $testing =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Testing"'.(is_null($name_project) ? '' : ' AND projects.name="'.$name_project.'"').(is_null($status) ? '' : ' AND issues.status = "'.$status.'"').(is_null($level) ? '' : ' AND issues.level = "'.$level.'"'));
        }

        else{
        $testing  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Testing"');
        
        }

        $pdf = PDF::loadview('backoffice.issues.report_isu_testing', ['testing'=>$testing])->setPaper('a4', 'landscape');
        return $pdf->download('laporan-isu-testing.pdf');
    }

    public function cetak_laporan_isu_delivery(Request $request){
          $delivery  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Delivery"'.(is_null($name_project) ? '' : ' AND projects.name="'.$name_project.'"').(is_null($status) ? '' : ' AND issues.status = "'.$status.'"').(is_null($level) ? '' : ' AND issues.level = "'.$level.'"'));
        $pdf = PDF::loadview('backoffice.issues.report_isu_testing', ['delivery'=>$delivery])->setPaper('a4', 'landscape');
        return $pdf->download('laporan-isu-testing.pdf');
    }



}
/*
if($name_project != null and $status != null and $level != null)
        {

            $teknologi  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Teknologi " and projects.name=:name_project and issues.level=:level and issues.status=:status', array('name_project' => $name_project, 'level'=>$level, 'status'=> $status));
        }

        elseif ($name_project != null or $status != null or $level != null ){
          
           if ($name_project != null){
                  $teknologi  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Teknologi " and projects.name=:name_project', array('name_project' => $name_project));
           }
           elseif($status != null )
           {
                  $teknologi  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Teknologi " and issues.status = :status ', array('status' => $status));
           }
           elseif($level != null){
              $teknologi  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Teknologi " and issues.level=:level', array('level' => $level));
           }
        }

        else{
        $teknologi  =  DB::select('SELECT issues.*,projects.name as project_name, 
                            pic.name as pic_name,finished.name as finished_name,
                            activities.name as master_activity FROM issues 
                            left JOIN users as pic ON pic.id = issues.created_by 
                            left JOIN users as finished ON finished.id = issues.finished_by
                            left JOIN projects ON projects.id = issues.project_id
                            left JOIN activities ON activities.id = issues.master_activity_id
                            where issues.division_name = "Teknologi"');
        
        }

        */
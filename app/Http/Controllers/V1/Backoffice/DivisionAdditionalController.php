<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\Entities\DivisionAdditional;
use Illuminate\Http\Request;
use DB;
use Auth;

class DivisionAdditionalController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required',
            'information'   => 'required',
            'file'         => 'required|mimes:jpeg,png,jpg,gif,svg,pdf,xlsx|max:20048',
            'division_id'   => 'required',
        ]);

        $file = time() . '.' . $request->file->getClientOriginalExtension();
        $request->file->move(public_path('Division'), $file);

        $division_additional = new DivisionAdditional([
            'name'          => $request->get('name'),
            'file'          => $file,
            'information'   => $request->get('information'),
            'division_id'   => $request->get('division_id'),
            'date'          => $request->get('date'),
        ]);
        $division_additional->save();
        alert()->success('Successfully Add Data.', 'Success!');
        return redirect()->back();
    }


    /**
     * Show the form for editing project.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data  = DivisionAdditional::find($id);
        return view('backoffice.division_additional.edit', compact('data'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $url = $request->get('url');

        $request->validate([
            'name'          => 'required',
            'division_id'   => 'required',
            'date'          => 'required',
        ]);

        
        $division_additional = DivisionAdditional::find($id);

        if (empty($request->file)) {
            $division_additional->name          = $request->get('name');
            $division_additional->information   = $request->get('information');
            $division_additional->date          = $request->get('date');
            $division_additional->division_id   = $request->get('division_id');  
            $division_additional->save();
            alert()->success('Successfully Update Data.', 'Success!');
            return redirect($url);

        } else {
            $file = time() . '.' . $request->file->getClientOriginalExtension();
            $request->files->move(public_path('Division'), $file);

            $division_additional->name          = $request->get('name');
            $division_additional->file          = $file;
            $division_additional->information   = $request->get('information');
            $division_additional->date          = $request->get('date');
            $division_additional->division_id    = $request->get('division_id');  
            $division_additional->save();
            alert()->success('Successfully Update Data.', 'Success!');
            return redirect($url);

        } 


    }

    /** 
     * Remove the specified resou   rce from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $division_additional = DivisionAdditional::find($id);
        $division_additional->delete();
        alert()->success('Successfully Remove Data.', 'Success!');
        return redirect()->back();
    }
}

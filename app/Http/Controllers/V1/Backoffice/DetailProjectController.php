<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\Entities\DetailProject;
use Illuminate\Http\Request;

class DetailProjectController extends Controller
{

    /**
     * Show the detail project.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $detail_project = DetailProject::all();
        return view('backoffice.projects.detail_project', compact('detail_project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'batch'                 => 'required',
            'project_id'            => 'required',
            'start_delivery_date'   => 'required',
            'end_delivery_date'     => 'required',
        ]);

        $detail_project = new DetailProject([
            'term_payment'          => $request->get('term_payment'),
            'payment'               => $request->get('payment'),
            'denda'                 => $request->get('denda'),
            'batch'                 => $request->get('batch'),
            'project_id'            => $request->get('project_id'),
            'start_delivery_date'   => $request->get('start_delivery_date'),
            'end_delivery_date'     => $request->get('end_delivery_date'),
            'address_delivery'      => $request->get('address_delivery'),
        ]);
        $detail_project->save();
        alert()->success('Successfully Add Data.', 'Success!');
        return redirect()->back();
    }

    /**
     * Show the form for editing detail project.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail_project = DetailProject::find($id);
        return view('backoffice.projects.edit_detail', compact('detail_project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $url = $request->get('url');

        $request->validate([
            'batch'                 => 'required',
            'project_id'            => 'required',
            'start_delivery_date'   => 'required',
            'end_delivery_date'     => 'required',
        ]);


        $detail_project = DetailProject::find($id);
        $detail_project->batch                  = $request->get('batch');
        $detail_project->project_id             = $request->get('project_id');
        $detail_project->start_delivery_date    = $request->get('start_delivery_date');
        $detail_project->end_delivery_date      = $request->get('end_delivery_date');
        $detail_project->save();
        alert()->success('Successfully Update Data.', 'Success!');
        return redirect($url);
    }


    /** 
     * Remove the specified resou   rce from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail_project = DetailProject::find($id);
        $detail_project->delete();
        alert()->success('Successfully Remove Data', 'Success!');
        return redirect()->back();
    }
}

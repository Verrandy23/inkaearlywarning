<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\Entities\Project;
use App\Entities\Issue;
use App\Entities\ProjectProgress;
use App\Entities\DetailProject;
use App\Entities\ProjectBudget;
use App\Entities\ReportData;
use Illuminate\Http\Request;
use DB;
use Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Closure as callback
            if(!Auth::check()) {
                return 'no';
            }
    
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
         $id = Auth::user()->project_to_pm;
        if ($request->has('search')) {
                $project            = DB::select('SELECT projects.id ,projects.name,datediff(projects.end_date,current_date()) as remaining,
                                            max(project_progress.progress_realisasi) as progress,max(project_progress.progress_rencana) as progress_rencana FROM projects 
                                            JOIN project_progress ON projects.id = project_progress.project_id
                                            GROUP BY projects.name ORDER BY projects.id ASC');
                $report             = ReportData::join('projects', 'projects.id', '=', 'reports.project_id')
                                        ->select('reports.*','projects.name as project_name')
                                        ->get();                
                $project_progress   = ProjectProgress::with('get_project')->where('project_id',$request->search)->get();
                $project_progress1 = DB::select('select projects.id as project_id, count(work_activities.status) as total_aktivitas, sum(work_activities.status) as total_jumlah from work_activities left join sub_activities on sub_activities.id = work_activities.sub_activity_id left join activities on activities.id = sub_activities.activity_id left join divisions on divisions.id = activities.division_id left JOIN projects on projects.id = divisions.project_id GROUP BY projects.id ORDER BY projects.id ASC ');
                $monthly_project    = DB::select('SELECT DISTINCT(project_id),projects.name as project_name FROM `project_progress` 
                                        JOIN projects ON projects.id = project_progress.project_id');
                $projectData        = ProjectProgress::join('projects', 'projects.id', '=', 'project_progress.project_id')
                                    ->where('project_id',$request->search)
                                    ->select('projects.name')
                                    ->first();
                $project_name       = $projectData->name;
                $count_issue_open   = Issue::where('status','open')->count();
                $count_issue_closed = Issue::where('status','closed')->count();
                $issues             = DB::select('SELECT level, COUNT(level) as total, status from issues GROUP by level,status DESC');


                return view('backoffice.dashboard.dashboard', compact('project', 'report','project_progress','monthly_project','project_name',
                            'count_issue_open','count_issue_closed','issues', 'project_progress1'));
           
        } 
        
        else {
            if (Auth::user()->project_to_pm == null) {
                $project            = DB::select('SELECT projects.id ,projects.name,datediff(projects.end_date,current_date()) as remaining,
                                            max(project_progress.progress_realisasi) as progress,max(project_progress.progress_rencana) as progress_rencana FROM projects 
                                            JOIN project_progress ON projects.id = project_progress.project_id
                                            GROUP BY projects.name ORDER BY projects.id ASC');
                $report             = ReportData::join('projects', 'projects.id', '=', 'reports.project_id')
                                    ->select('reports.*','projects.name as project_name')
                                    ->get();  
                $project_progress   = ProjectProgress::join('projects', 'projects.id', '=', 'project_progress.project_id')
                                                   ->where('projects.id',$id)->get();
             $project_progress1 = DB::select('select projects.id as project_id, count(work_activities.status) as total_aktivitas, sum(work_activities.status) as total_jumlah from work_activities left join sub_activities on sub_activities.id = work_activities.sub_activity_id left join activities on activities.id = sub_activities.activity_id left join divisions on divisions.id = activities.division_id left JOIN projects on projects.id = divisions.project_id GROUP BY projects.id ORDER BY projects.id ASC ');

                $monthly_project    = DB::select('SELECT DISTINCT(project_id),projects.name as project_name FROM `project_progress` 
                                                JOIN projects ON projects.id = project_progress.project_id');
                $projectData        = ProjectProgress::join('projects', 'projects.id', '=', 'project_progress.project_id')
                                    ->select('projects.name')
                                    ->where('projects.id',1)
                                    ->first();
                $project_name       = $projectData->name;
                
                $count_issue_open   = Issue::where('status','open')->count();
                $count_issue_closed = Issue::where('status','closed')->count();
                $issues             = DB::select('SELECT level, COUNT(level) as total, status from issues GROUP by level,status DESC');
                return view('backoffice.dashboard.dashboard', compact('project', 'report','project_progress','monthly_project','project_name',
                            'count_issue_open','count_issue_closed','issues',  'project_progress1'));
            } else {
                //condition for PM
                $id                 = Auth::user()->project_to_pm;
                $project            = DB::select('SELECT projects.id, projects.name,datediff(projects.end_date,current_date()) as remaining,
                                            max(project_progress.progress_realisasi) as progress,max(project_progress.progress_rencana) as progress_rencana FROM projects 
                                            JOIN project_progress ON projects.id = project_progress.project_id where projects.id = "'.$id.'"
                                            GROUP BY projects.name ORDER BY projects.id ASC');
                $report             = ReportData::join('projects', 'projects.id', '=', 'reports.project_id')
                                    ->select('reports.*','projects.name as project_name')
                                    ->where('projects.id',$id)
                                    ->get();         
               $project_progress   = ProjectProgress::join('projects', 'projects.id', '=', 'project_progress.project_id')
                                                   ->where('projects.id',$id)->get();

                 $project_progress1 = DB::select('select projects.id as project_id, count(work_activities.status) as total_aktivitas, sum(work_activities.status) as total_jumlah from work_activities left join sub_activities on sub_activities.id = work_activities.sub_activity_id left join activities on activities.id = sub_activities.activity_id left join divisions on divisions.id = activities.division_id left JOIN projects on projects.id = divisions.project_id GROUP BY projects.id ORDER BY projects.id ASC ');

                $monthly_project    = DB::select('SELECT DISTINCT(project_id),projects.name as project_name FROM `project_progress` 
                                    JOIN projects ON projects.id = project_progress.project_id');
                $projectData        = ProjectProgress::join('projects', 'projects.id', '=', 'project_progress.project_id')
                                                    ->where('projects.id',$id)
                                                    ->select('projects.name')
                                                    ->first();
                $project_name       = $projectData->name;
                $count_issue_open   = Issue::where('status','open')->count();
                $count_issue_closed = Issue::where('status','closed')->count();
                $issues             = DB::select('SELECT level, COUNT(level) as total, status from issues GROUP by level,status DESC');
                return view('backoffice.dashboard.dashboard', compact('project', 'report','project_progress','monthly_project','project_name',
                            'count_issue_open','count_issue_closed','issues', 'project_progress1'));
            }
        }
        
    }

       /**
     * Show the main application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showDashboard(Request $request)
    {

            $project            = DB::select('SELECT projects.id,projects.name,max(project_progress.progress_realisasi) as progress,
                                        max(project_progress.progress_rencana) as progress_rencana FROM projects 
                                        JOIN project_progress ON projects.id = project_progress.project_id 
                                        GROUP BY projects.name ORDER BY projects.id ASC');
             $project_progress1 = DB::select('select projects.id as project_id, count(work_activities.status) as total_aktivitas, sum(work_activities.status) as total_jumlah from work_activities left join sub_activities on sub_activities.id = work_activities.sub_activity_id left join activities on activities.id = sub_activities.activity_id left join divisions on divisions.id = activities.division_id left JOIN projects on projects.id = divisions.project_id GROUP BY projects.id ORDER BY projects.id ASC ');

            $project_budget     = DB::select('SELECT projects.id,projects.name,max(project_budgets.rencana) as rencana,
                                        max(project_budgets.realisasi) as realisasi,max(project_budgets.proyeksi) as proyeksi, 
                                        max(project_budgets.date) as date FROM projects 
                                        JOIN project_budgets ON projects.id = project_budgets.project_id
                                        GROUP BY projects.name ORDER BY projects.id ASC');
            
            $project_date       = DB::select('select max(date) as date from project_budgets');

            $total_realisasi = DB::select('SELECT max(realisasi) as total from project_budgets');
            
            $total_proyeksi = DB::select('SELECT max(proyeksi) as total from project_budgets');

            $total_budget = DB::select('SELECT max(rencana) as total from project_budgets');

            $issue = DB::select('SELECT count(*) as total_issues, project_id, (select count(a.status) from issues as a where a.status="open" and a.project_id=issues.project_id) as open_issue, project.name FROM issues left JOIN projects as project on project.id = issues.project_id group by issues.project_id');

           
            
            return view('backoffice.dashboard.main_dashboard', compact('project', 'project_budget','project_date', 'issue', 'total_realisasi', 'total_proyeksi', 'total_budget', 'project_progress1'));
        
        
    }

    /**
     * Show the detail project.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(Request $request)
    {
        
        $id                 = $request->segment(2);
        $project            = Project::where('id',$id)->first();
        $detail_project     = DetailProject::where('project_id',$id)->first();
        return view('backoffice.dashboard.detail_project', compact('project','detail_project'));
    }

    public function showProgress(Request $request)
    {}

    /**
     * Show the import activity admin.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function import()
    {
        return view('backoffice.masters.import_activity');
    }
}

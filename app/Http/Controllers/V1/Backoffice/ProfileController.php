<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
 
    /**
     * Show the form for editing user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backoffice.users.edit_profile');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->get('id');
        if(empty($request->picture) && empty($request->password)){

            $user               =  User::where('id', $id)->first();
            $user->name         = $request->get('name');
            $user->username     = $request->get('username');
            $user->nip          = $request->get('nip');
            $user->email        = $request->get('email');
            $user->phone        = $request->get('phone');
            $user->save();
        }  else if(empty($request->password)){

            $imageName = time() . '.' . $request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('assets/stisla/img/profile'), $imageName);

            $user               =  User::where('id', $id)->first();
            $user->name         = $request->get('name');
            $user->username     = $request->get('username');
            $user->nip          = $request->get('nip');
            $user->email        = $request->get('email');
            $user->phone        = $request->get('phone');
            $user->picture      = $imageName;
            $user->save();
        }   else if(empty($request->picture)){
            
            $user               =  User::where('id', $id)->first();
            $user->name         = $request->get('name');
            $user->username     = $request->get('username');
            $user->nip          = $request->get('nip');
            $user->email        = $request->get('email');
            $user->phone        = $request->get('phone');
            $user->password     = Hash::make($request->get('password'));            
            $user->save();
        }   else {

            $imageName = time() . '.' . $request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('assets/stisla/img/profile'), $imageName);

            $user               =  User::where('id', $id)->first();
            $user->name         = $request->get('name');
            $user->username     = $request->get('username');
            $user->nip          = $request->get('nip');
            $user->email        = $request->get('email');
            $user->phone        = $request->get('phone');
            $user->picture      = $imageName;
            $user->password     = Hash::make($request->get('password'));
            $user->save();
        }
        return redirect('/profiles')->with('success', 'Successfully update data');
    }

}

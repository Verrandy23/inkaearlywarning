<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\Entities\Project;
use App\Entities\ProjectProgress;
use Illuminate\Http\Request;
use DB;

class ProjectProgressController extends Controller
{

    /**
     * Show the form for editing issue.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project           = Project::all();
        $project_progress  = ProjectProgress::with('get_project')->get();
        return view('backoffice.project_progress.project_progress', compact('project_progress','project'));
    }


    /**
     * Show the form for editing issue.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create_progress()
    {
        $project           = Project::all();
        return view('backoffice.project_progress.create', compact('project'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'progress_rencana'      => 'required|numeric|between:0,100',
            'progress_realisasi'    => 'required|numeric|between:0,100',
            'project_id'            => 'required',
            'date'                  => 'required',
        ]);

        $project_progress = new ProjectProgress([
            'progress_rencana'      => $request->get('progress_rencana'),
            'progress_realisasi'    => $request->get('progress_realisasi'),
            'project_id'            => $request->get('project_id'),
            'date'                  => $request->get('date'),

        ]);
        $project_progress->save();
        alert()->success('Successfully Add Data.', 'Success!');
        return redirect('/projects_progress');
    }
   
    /**
     * Show the form for editing issue.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project_progress   = ProjectProgress::find($id);
        $project            = Project::all();
        return view('backoffice.project_progress.edit', compact('project_progress','project'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project_progress   = ProjectProgress::find($id);
        $project_progress->progress_rencana     = $request->get('progress_rencana');
        $project_progress->progress_realisasi   = $request->get('progress_realisasi');
        $project_progress->project_id           = $request->get('project_id');
        $project_progress->date                 = $request->get('date');
        $project_progress->save();
        alert()->success('Successfully Update Data.', 'Success!');
        return redirect('/projects_progress');
        
    }

    
   /** 
     * Remove the specified resou   rce from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $project_progress = ProjectProgress::find($id);
        $project_progress->delete();
        alert()->success('Successfully Remove Data.', 'Success!');
        return redirect('/projects_progress');
    }


}

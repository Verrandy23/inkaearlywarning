<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Entities\Project;
use Auth;

class HistoryController extends Controller
{
    /**
     * Late page history
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id     = Auth::user()->project_to_pm;
        if (Auth::user()->project_to_pm == null) {
            $teknologi = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->join('late_histories','work_activities.id','late_histories.work_activity_id')
                                ->where('divisions.name','Teknologi')
                                ->select('divisions.name as division_name','projects.name as project_name',
                                        'activities.name as activity_name','sub_activities.name as sub_name',
                                        'work_activities.name as work_name','late_histories.finished_at',
                                        'late_histories.count_late')
                                ->orderByRaw('late_histories.id ASC')->get();
        } else {
            $teknologi = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->join('late_histories','work_activities.id','late_histories.work_activity_id')
                                ->where('divisions.name','Teknologi')
                                ->where('projects.id',$id)
                                ->select('divisions.name as division_name','projects.name as project_name',
                                        'activities.name as activity_name','sub_activities.name as sub_name',
                                        'work_activities.name as work_name','late_histories.finished_at',
                                        'late_histories.count_late')
                                ->orderByRaw('late_histories.id ASC')->get();
        }

        if (Auth::user()->project_to_pm == null) {
            $logistik = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->join('late_histories','work_activities.id','late_histories.work_activity_id')
                                ->where('divisions.name','Logistik')
                                ->select('divisions.name as division_name','projects.name as project_name',
                                        'activities.name as activity_name','sub_activities.name as sub_name',
                                        'work_activities.name as work_name','late_histories.finished_at',
                                        'late_histories.count_late')
                                ->orderByRaw('late_histories.id ASC')->get();
        } else {
            $logistik = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->join('late_histories','work_activities.id','late_histories.work_activity_id')
                                ->where('divisions.name','Logistik')
                                ->where('projects.id',$id)
                                ->select('divisions.name as division_name','projects.name as project_name',
                                        'activities.name as activity_name','sub_activities.name as sub_name',
                                        'work_activities.name as work_name','late_histories.finished_at',
                                        'late_histories.count_late')
                                ->orderByRaw('late_histories.id ASC')->get();
        } 

        if (Auth::user()->project_to_pm == null) {

            $fabrikasi = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->join('late_histories','work_activities.id','late_histories.work_activity_id')
                                ->where('divisions.name','Fabrikasi')
                                ->select('divisions.name as division_name','projects.name as project_name',
                                        'activities.name as activity_name','sub_activities.name as sub_name',
                                        'work_activities.name as work_name','late_histories.finished_at',
                                        'late_histories.count_late')
                                ->orderByRaw('late_histories.id ASC')->get();
        } else {
            $fabrikasi = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->join('late_histories','work_activities.id','late_histories.work_activity_id')
                                ->where('divisions.name','Fabrikasi')
                                ->where('projects.id',$id)
                                ->select('divisions.name as division_name','projects.name as project_name',
                                        'activities.name as activity_name','sub_activities.name as sub_name',
                                        'work_activities.name as work_name','late_histories.finished_at',
                                        'late_histories.count_late')
                                ->orderByRaw('late_histories.id ASC')->get();
        }

        if (Auth::user()->project_to_pm == null) {

            $finishing = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->join('late_histories','work_activities.id','late_histories.work_activity_id')
                                ->where('divisions.name','Finishing')
                                ->select('divisions.name as division_name','projects.name as project_name',
                                        'activities.name as activity_name','sub_activities.name as sub_name',
                                        'work_activities.name as work_name','late_histories.finished_at',
                                        'late_histories.count_late')
                                ->orderByRaw('late_histories.id ASC')->get();
        } else {
            $finishing = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->join('late_histories','work_activities.id','late_histories.work_activity_id')
                                ->where('divisions.name','Finishing')
                                ->where('projects.id',$id)
                                ->select('divisions.name as division_name','projects.name as project_name',
                                        'activities.name as activity_name','sub_activities.name as sub_name',
                                        'work_activities.name as work_name','late_histories.finished_at',
                                        'late_histories.count_late')
                                ->orderByRaw('late_histories.id ASC')->get();
        }

        if (Auth::user()->project_to_pm == null) {

            $testing = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->join('late_histories','work_activities.id','late_histories.work_activity_id')
                                ->where('divisions.name','Finishing')
                                ->select('divisions.name as division_name','projects.name as project_name',
                                        'activities.name as activity_name','sub_activities.name as sub_name',
                                        'work_activities.name as work_name','late_histories.finished_at',
                                        'late_histories.count_late')
                                ->orderByRaw('late_histories.id ASC')->get();
        } else {
            $testing = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->join('late_histories','work_activities.id','late_histories.work_activity_id')
                            ->where('divisions.name','Testing')
                            ->where('projects.id',$id)
                            ->select('divisions.name as division_name','projects.name as project_name',
                                    'activities.name as activity_name','sub_activities.name as sub_name',
                                    'work_activities.name as work_name','late_histories.finished_at',
                                    'late_histories.count_late')
                            ->orderByRaw('late_histories.id ASC')->get();
        }
      
        return view('backoffice.histories.history', compact('logistik', 'fabrikasi', 'finishing', 'testing', 'teknologi'));
    }
}
<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\Entities\MasterDivision;
use App\Entities\Division;
use Illuminate\Http\Request;
use DB;

class DivisionController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'        => 'required',
            'project_id'  => 'required',
            'start_date'  => 'required',
            'end_date'    => 'required',
        ]);

        $division = new Division([
            'name'          => $request->get('name'),
            'project_id'    => $request->get('project_id'),
            'start_date'    => $request->get('start_date'),
            'end_date'      => $request->get('end_date'),
        ]);
        $division->save();
        alert()->success('Successfully Add Data.', 'Success!');
        return redirect()->back();
    }

    /**
     * Show the form for editing project.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $id                 = $request->segment(2);
        $division           = Division::find($id);
        $project            = Division::with('get_project')->where('id',$id)->first();
        return view('backoffice.projects.edit_division', compact('division','project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $url = $request->get('url');

        $request->validate([
            'name'          => 'required',
            'project_id'    => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required',
        ]);

        
        $division = Division::find($id);
        $division->name         = $request->get('name');
        $division->project_id   = $request->get('project_id');
        $division->start_date   = $request->get('start_date');
        $division->end_date     = $request->get('end_date');
        $division->save();
        alert()->success('Successfully Update Data.', 'Success!');
        return redirect($url);
    }



    /**
     * Show trainset for general
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id                 = $request->segment(2);
        $division           = Division::where('id', $id)->first();
        $division_id        = $division->id;
        $project            = DB::select('SELECT distinct(projects.name) as project_name FROM projects
                            join divisions ON projects.id = divisions.project_id
                            where divisions.id = ' . $division_id . '');

        $progress = DB::select('SELECT activities.name, activities.id, sum(work_activities.status) as jumlah, 
                                COUNT(work_activities.status) as total FROM projects
                                LEFT JOIN divisions ON projects.id = divisions.project_id
                                LEFT JOIN activities ON divisions.id = activities.division_id
                                LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                where divisions.id= ' . $division_id . ' 
                                GROUP BY activities.id
                                '); // AND divisions.name != "Delivery"


        return view('backoffice.activities.activity', compact('project', 'division', 'progress', 'division_id','id'));
    }


    /**
     * Show detail activity for delivery
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showDelivery(Request $request)
    {
        $id                 = $request->segment(2);
        $project            = Division::with('get_project')->first();
        $division           = Division::where('id', $id)->first();
        $division_id        = $division->id;

        $progress = DB::select('SELECT activities.name, activities.id,activities.start_delivery,activities.end_delivery,
                                sum(activities.status_delivery) as jumlah,activities.status_delivery,
                                COUNT(activities.status_delivery) as total FROM projects
                                LEFT JOIN divisions ON projects.id = divisions.project_id
                                LEFT JOIN activities ON divisions.id = activities.division_id
                                where divisions.id= ' . $division_id . ' AND divisions.name = "Delivery" GROUP BY activities.id
                                ');

        return view('backoffice.activities.activity_delivery', compact('project', 'division', 'progress', 'division_id'));
    }

    /** 
     * Remove the specified resou   rce from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $division = Division::find($id);
        $division->delete();
        alert()->success('Successfully Remove Data.', 'Success!');
        return redirect()->back();    

    }

 
}

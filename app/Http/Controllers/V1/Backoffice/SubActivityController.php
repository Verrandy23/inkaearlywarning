<?php

    namespace App\Http\Controllers\V1\Backoffice;

    use App\Http\Controllers\Controller;
    use App\User;
    use App\Entities\Project;
    use App\Entities\SubActivity;
    use App\Imports\SubActivitiesImport;
    use Illuminate\Http\Request;
    use DB;
    use Excel;
    use SweetAlert;

    class SubActivityController extends Controller
    {

        public function storeExcelSubActivity(Request $request)
        {
            //VALIDASI
            $this->validate($request, [
                'file' => 'required|mimes:xls,xlsx'
            ]);

            if ($request->hasFile('file')) {
                $file = $request->file('file'); //GET FILE
                Excel::import(new SubActivitiesImport, $file); //IMPORT FILE
                alert()->success('Successfully Add Data.', 'Success!'); 
                return redirect()->back();
            }
            return redirect()->back()->with(['error' => 'Please choose file before']);
        }

        public function show(Request $request)
        {
            $pic                = User::pluck('name','id')->toArray();
            $id                 = $request->segment(2);
            $sub_activity       = SubActivity::where('id', $id)->first();
            $sub_activity_id    = $sub_activity->id;
            $general            = DB::select('SELECT projects.name AS project_name,projects.id as project_id,
                                divisions.name as division_name,divisions.id as division_id,activities.id as activity_id,
                                work_activities.start_date,work_activities.end_date,work_activities.status FROM projects
                                join divisions ON projects.id = divisions.project_id
                                join activities ON divisions.id = activities.division_id
                                join sub_activities ON activities.id = sub_activities.activity_id
                                join work_activities ON sub_activities.id = work_activities.sub_activity_id
                                where sub_activities.id =  ' . $sub_activity_id . ' GROUP BY projects.id');
                                
            $progress           = DB::select('SELECT work_activities.*,users.name as pic_name,IF(work_activities.slack>0, 
                                        DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                        DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late,
                                        sum(work_activities.status) as jumlah,COUNT(work_activities.status) as total FROM projects
                                        LEFT JOIN divisions ON projects.id = divisions.project_id
                                        LEFT JOIN activities ON divisions.id = activities.division_id
                                        LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                        LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                        LEFT JOIN users ON work_activities.pic = users.id
                                        where sub_activities.id= ' . $sub_activity_id . ' 
                                        GROUP BY work_activities.id
                                    '); // AND divisions.name != "Delivery" 


            return view('backoffice.work_activities.work_activity', compact(
                'general',
                'sub_activity_id',
                'sub_activity',
                'progress',
                'pic'
            ));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $request->validate([
                'name'          => 'required',
                'activity_id'   => 'required',
            ]);
             $max = SubActivity::max('id');
             $id = $max+1;
            $activity = new SubActivity([
                'id'            => $id,
                'name'          => $request->get('name'),
                'activity_id'   => $request->get('activity_id')
            ]);
            
            $activity->save();
            alert()->success('Successfully Add Data.', 'Success!');
            return redirect()->back();
        }


        /**
         * Show the form for editing activity.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $sub_activity = SubActivity::find($id);
            return view('backoffice.sub_activities.edit', compact('sub_activity'));
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $url = $request->get('url');
            $request->validate([
                'name' => 'required',
            ]);

            $sub_activity = SubActivity::find($id);
            $sub_activity->name = $request->get('name');
            $sub_activity->save();
            alert()->success('Successfully Update Data.', 'Success!');
            return redirect($url);
        }

       

        /** 
         * Remove the specified resou   rce from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $sub_activity = SubActivity::find($id);
            $sub_activity->delete();
            alert()->success('Successfully Remove Data.', 'Success!');
            return redirect()->back();
        }

    }

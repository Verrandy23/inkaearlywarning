<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Entities\WorkActivity;
use App\Entities\Project;
use Auth;
use PDF;

class LateHistoryController extends Controller
{
    /**
     * Late page history
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id     = Auth::user()->project_to_pm;
        $slack  = DB::Table('work_activities')->select('work_activities.slack')->get();  
        if (Auth::user()->project_to_pm == null) {
            $teknologi = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('divisions.name','Teknologi')
                                ->where('work_activities.status','!=',1)
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                        'projects.name as project_name','activities.name as activity_name',
                                        'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                        'work_activities.end_date',
                                        DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                                DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();
        } else {
            $teknologi = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('divisions.name','Teknologi')
                                ->where('work_activities.status','!=',1)
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->where('projects.id',$id)
                                ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                        'projects.name as project_name','activities.name as activity_name',
                                        'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                        'work_activities.end_date',
                                        DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                                DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();
        }

        if (Auth::user()->project_to_pm == null) {
            $logistik = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('divisions.name','Logistik')
                                ->where('work_activities.status','!=',1)
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                        'projects.name as project_name','activities.name as activity_name',
                                        'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                        'work_activities.end_date',
                                        DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                                DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();
        } else {
            $logistik = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('divisions.name','Logistik')
                                ->where('work_activities.status','!=',1)
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->where('projects.id',$id)
                                ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                        'projects.name as project_name','activities.name as activity_name',
                                        'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                        'work_activities.end_date',
                                        DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                                DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();
        } 

        if (Auth::user()->project_to_pm == null) {

            $fabrikasi = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('divisions.name','Fabrikasi')
                                ->where('work_activities.status','!=',1)
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                        'projects.name as project_name','activities.name as activity_name',
                                        'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                        'work_activities.end_date',
                                        DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                                DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();
        } else {
            $fabrikasi = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('divisions.name','Fabrikasi')
                                ->where('work_activities.status','!=',1)
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->where('projects.id',$id)
                                ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                        'projects.name as project_name','activities.name as activity_name',
                                        'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                        'work_activities.end_date',
                                        DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                                DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();
        }

        if (Auth::user()->project_to_pm == null) {

            $finishing = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('divisions.name','Finishing')
                                ->where('work_activities.status','!=',1)
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                        'projects.name as project_name','activities.name as activity_name',
                                        'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                        'work_activities.end_date',
                                        DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                                DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();
        } else {
            $finishing = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('divisions.name','Finishing')
                                ->where('work_activities.status','!=',1)
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->where('projects.id',$id)
                                ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                        'projects.name as project_name','activities.name as activity_name',
                                        'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                        'work_activities.end_date',
                                        DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                                DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();
        }

        if (Auth::user()->project_to_pm == null) {
            $testing = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Testing')
                            ->where('work_activities.status','!=',1)
                            ->where('work_activities.end_date','<=',Carbon::today())
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date',
                                    DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                            ->having('late','>',0)
                            ->orderBy('work_activities.id')->get();
        } else {
            $testing = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Testing')
                            ->where('work_activities.status','!=',1)
                            ->where('work_activities.end_date','<=',Carbon::today())
                            ->where('projects.id',$id)
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date',
                                    DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                            ->having('late','>',0)
                            ->orderBy('work_activities.id')->get();
        }

        if (Auth::user()->project_to_pm == null) {
                $delivery = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Delivery')
                            ->where('work_activities.status','!=',1)
                            ->where('work_activities.end_date','<=',Carbon::today())
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date',
                                    DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                            ->having('late','>',0)
                            ->orderBy('work_activities.id')->get();

        } else {
                $delivery = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Delivery')
                            ->where('work_activities.status','!=',1)
                            ->where('work_activities.end_date','<=',Carbon::today())
                            ->where('projects.id',$id)
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date',
                                    DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                            ->having('late','>',0)
                            ->orderBy('work_activities.id')->get();
        }

        return view('backoffice.histories.late', compact('logistik', 'fabrikasi', 'finishing', 'testing', 'teknologi','delivery'));
    }

    public function cetak_laporan_late_teknologi(Request $request)
    {
        if($request->has('filter0') || $request->has('filter1')){
            $name_project = $request->get('filter0');
            $activity = $request->get('filter1'); 
        }
        $name_project = $request->get('filter0');
        $activity = $request->get('filter1'); 
        $teknologi = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('divisions.name','Teknologi')
                                ->whereRaw('work_activities.status != 1 '.(is_null($name_project) ? '' : ' and projects.name="'.$name_project.'"').(is_null($activity) ? '' : ' and activities.name="'.$activity.'"'))
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                        'projects.name as project_name','activities.name as activity_name',
                                        'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                        'work_activities.end_date',
                                        DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                                DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();
        ini_set('memory_limit', '8192M');
        $pdf = PDF::loadview('backoffice.histories.report_latehistories_teknologi', ['teknologi'=>$teknologi])->setPaper('a4', 'landscape');
        set_time_limit(600);
        return $pdf->download('laporan-late-history-teknologi.pdf');
    }

    public function cetak_laporan_late_logistik(Request $request)
    {
        if($request->has('filter0') || $request->has('filter1')){
            $name_project = $request->get('filter0');
            $activity = $request->get('filter1'); 
        }
        else{
        $name_project = $request->get('logistik0');
        $activity = $request->get('logistik1');
         }
        $logistik = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('divisions.name','Logistik')->where('projects.id','!=',1)
                                ->whereRaw('work_activities.status != 1 '.(is_null($name_project) ? '' : ' and projects.name="'.$name_project.'"').(is_null($activity) ? '' : ' and activities.name="'.$activity.'"'))
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                        'projects.name as project_name','activities.name as activity_name',
                                        'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                        'work_activities.end_date','work_activities.slack',
                                        DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                                DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();
         ini_set('memory_limit', '8192M');
        $pdf = PDF::loadview('backoffice.histories.report_latehistories_logistik', ['logistik'=>$logistik])->setPaper('a4', 'landscape');
        set_time_limit(600);
        return $pdf->download('laporan-late-history-logistik.pdf');
    }

    public function cetak_laporan_late_fabrikasi(Request $request)
    {
        if($request->has('filter0') || $request->has('filter1')){
            $name_project = $request->get('filter0');
            $activity = $request->get('filter1'); 
        }
        else{
        $name_project = $request->get('fabrikasi0');
        $activity = $request->get('fabrikasi1');
         }

        $fabrikasi = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('divisions.name','Fabrikasi')
                                ->whereRaw('work_activities.status != 1 '.(is_null($name_project) ? '' : ' and projects.name="'.$name_project.'"').(is_null($activity) ? '' : ' and activities.name="'.$activity.'"'))
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                        'projects.name as project_name','activities.name as activity_name',
                                        'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                        'work_activities.end_date',
                                        DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                                DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();
       ini_set('memory_limit', '8192M');
        $pdf = PDF::loadview('backoffice.histories.report_latehistories_fabrikasi', ['fabrikasi'=>$fabrikasi])->setPaper('a4', 'landscape');
        set_time_limit(600);
        return $pdf->download('laporan-late-history-fabrikasi.pdf');
    }

    public function cetak_laporan_late_finishing(Request $request)
    {
        if($request->has('filter0') || $request->has('filter1')){
            $name_project = $request->get('filter0');
            $activity = $request->get('filter1'); 
        }
        else{
        $name_project = $request->get('finishing0');
        $activity = $request->get('finishing1');
         }
        $finishing = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('divisions.name','Finishing')
                                ->whereRaw('work_activities.status != 1 '.(is_null($name_project) ? '' : ' and projects.name="'.$name_project.'"').(is_null($activity) ? '' : ' and activities.name="'.$activity.'"'))
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                        'projects.name as project_name','activities.name as activity_name',
                                        'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                        'work_activities.end_date',
                                        DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                                DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();
        
        ini_set('memory_limit', '8192M');
        $pdf = PDF::loadview('backoffice.histories.report_latehistories_finishing', ['finishing'=>$finishing])->setPaper('a4', 'landscape');
        set_time_limit(600);
        return $pdf->download('laporan-LateHistories.pdf');
    }

    public function cetak_laporan_late_testing(Request $request)
    {
        if($request->has('filter0') || $request->has('filter1')){
            $name_project = $request->get('filter0');
            $activity = $request->get('filter1'); 
        }
        
        else {
        $name_project = $request->get('testing0');
        $activity = $request->get('testing1');
         }

        $testing = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Testing')
                            ->whereRaw('work_activities.status != 1 '.(is_null($name_project) ? '' : ' and projects.name="'.$name_project.'"').(is_null($activity) ? '' : ' and activities.name="'.$activity.'"'))
                            ->where('work_activities.end_date','<=',Carbon::today())
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date',
                                    DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                            ->having('late','>',0)
                            ->orderBy('work_activities.id')->get();
        ini_set('memory_limit', '8192M');
        $pdf = PDF::loadview('backoffice.histories.report_latehistories_testing', ['testing'=>$testing])->setPaper('a4', 'landscape');
        set_time_limit(600);
        return $pdf->download('laporan-late-history-testing.pdf');
    }

    public function cetak_laporan_late_delivery(Request $request)
    {
         if($request->has('filter0') || $request->has('filter1')){
            $name_project = $request->get('filter0');
            $activity = $request->get('filter1'); 
        }
        else{
        $name_project = $request->get('delivery0');
        $activity = $request->get('delivery1');
         }

        $delivery =  Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Delivery')
                            ->whereRaw('work_activities.status != 1 '.(is_null($name_project) ? '' : ' and projects.name="'.$name_project.'"').(is_null($activity) ? '' : ' and activities.name="'.$activity.'"'))
                            ->where('work_activities.status','!=',1)
                            ->where('work_activities.end_date','<=',Carbon::today())
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date',
                                    DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                            ->having('late','>',0)
                            ->orderBy('work_activities.id')->get();
        ini_set('memory_limit', '8192M');
        $pdf = PDF::loadview('backoffice.histories.report_latehistories_delivery', ['delivery'=>$delivery])->setPaper('a4', 'landscape');
        set_time_limit(600);
        return $pdf->download('laporan-LateHistories.pdf');
    }



    

}

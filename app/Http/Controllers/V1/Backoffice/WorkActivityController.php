<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\User;
use App\Entities\WorkActivity;
use App\Entities\LateHistory;
use App\Imports\WorkActivitiesImport;
use Illuminate\Http\Request;
use Excel;
use DateTime;
use DB;

class WorkActivityController extends Controller
{

    public function storeExcelWorkActivity(Request $request)
    {
        //VALIDASI
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new WorkActivitiesImport, $file); //IMPORT FILE
            alert()->success('Successfully Add Data.', 'Success!'); 
            return redirect()->back(); 
        }
        return redirect('/imports')->with(['error' => 'Please choose file before']);
    }

    public function store(Request $request)
    {
        
        $request->validate([
            'name'              => 'required',
            'sub_activity_id'   => 'required',
            'start_date'        => 'required',
            'end_date'          => 'required',
            'slack'             => 'required',
        ]);
        $max = WorkActivity::max('id');
        $id = $max+1;
        $activity = new WorkActivity([
            'id' => $id,
            'name'              => $request->get('name'),
            'sub_activity_id'   => $request->get('sub_activity_id'),
            'start_date'        => $request->get('start_date'),
            'end_date'          => $request->get('end_date'),
            'slack'             => $request->get('slack'),
            'pic'               => $request->get('pic')
        ]);
        $activity->save();
        alert()->success('Successfully Add Data.', 'Success!'); 
        return redirect()->back();
    }

    /** 
     * update checklist type work activity
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateTask(Request $request, $id)
    {
        
        $work_activity              = new WorkActivity();    
        $work_activity              = WorkActivity::where('id', $id)->first();
        $work_activity->status      = $request->get('status');
        $work_activity->status_name = $request->get('status_name');
        $work_activity->save();


        $data = $work_activity->status;
        
        
            
       

        if($request->get('status_name') == "finish" && stristr($request->get('late'), '-') === false){
            try {     
                    $late_history                   = new LateHistory();
                    $late_history->work_activity_id = $work_activity->id;
                    $late_history->finished_at      = date("Y/m/d");
                    $late_history->count_late       = $request->get('late');
                    $late_history->save();

                    DB::commit();

            } catch(\Exception $e){
                DB::rollback();
                return redirect()->back() 
                ->with('success','Something Went Wrong!');
            }
        }

        /*
        alert()->success('Successfully Update Data.', 'Success!'); 
        return redirect()->back(); 
        */
        return response()->json(['success'=> 'true', 'data' => $data]);

        
    }

    /**
     * Show the form for editing activity.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pic           = User::pluck('name','id')->toArray();
        $work_activity = WorkActivity::find($id);
        return view('backoffice.work_activities.edit', compact('work_activity','pic'));
    }

    /**
     * Show the form for editing pic.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pic           = User::pluck('name','id')->toArray();
        $work_activity = WorkActivity::find($id);
        return view('backoffice.work_activities.edit_pic', compact('work_activity','pic'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hasil = 0;
        $url = $request->get('url');
        $request->validate([
            'name'          => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required',
        ]);

        $work_activity  = WorkActivity::where('id', $id)->first();
        $end_date       = $work_activity->end_date;
        $slack          = $work_activity->slack;


        if($request->get('end_date') > $end_date) {

            $earlier        = new DateTime($end_date);
            $later          = new DateTime($request->get('end_date'));
            $diff           = $later->diff($earlier)->format("%a");
            $hasil          = $diff + $slack;
        }
        else
        {

            $earlier        = new DateTime($end_date);
            $later          = new DateTime($request->get('end_date'));
            $diff           = $later->diff($earlier)->format("%a");
            $hasil          = $slack - $diff;
        }

        if(empty($slack)){
            WorkActivity::where('id',$id)->update([
                'name'          => $request->get('name'),
                'start_date'    => $request->get('start_date'),
                'end_date'      => $request->get('end_date'),
                'slack'         => $request->get('slack'),
  
            ]);
        } else {
            WorkActivity::where('id',$id)->update([
                'name'          => $request->get('name'),
                'start_date'    => $request->get('start_date'),
                'end_date'      => $request->get('end_date'),
                'slack'         => $hasil,
  
            ]);
        }
    
        alert()->success('Successfully Update Data.', 'Success!'); 
        return redirect($url);    
    }

    /**
     * Update pic work activity
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePic(Request $request)
    {
        $url = $request->get('url');
        
        $id = $request->get('work_activity_id');
        $work_activity      = WorkActivity::where('id', $id)->first();
        $work_activity->pic = $request->get('pic');
        $work_activity->save();
        alert()->success('Successfully Update Data.', 'Success!'); 
        return redirect($url);    
    }

    /** 
     * Remove the specified resou   rce from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $work_activity = WorkActivity::find($id);
        $work_activity->delete();
        alert()->success('Successfully Remove Data.', 'Success!'); 
        return redirect()->back();
    }

}

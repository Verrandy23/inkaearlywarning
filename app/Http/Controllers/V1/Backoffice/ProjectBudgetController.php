<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\Entities\Project;
use App\Entities\ProjectBudget;
use Illuminate\Http\Request;
use DB;

class ProjectBudgetController extends Controller
{

    /**
     * Show the form for editing issue.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project           = Project::all();
        $project_budget    = ProjectBudget::with('get_project')->get();
        return view('backoffice.project_budget.project_budget', compact('project_budget','project'));
    }


    /**
     * Show the form for editing issue.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create_budget()
    {
        $project           = Project::all();
        return view('backoffice.project_budget.create', compact('project'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'rencana'      => 'required',
            'realisasi'    => 'required',
            'proyeksi'     => 'required',
            'project_id'   => 'required',
            'date'         => 'required',
        ]);

        $project_budget = new ProjectBudget([
            'rencana'      => $request->get('rencana'),
            'realisasi'    => $request->get('realisasi'),
            'proyeksi'     => $request->get('proyeksi' ),
            'project_id'   => $request->get('project_id'),
            'date'         => $request->get('date'),

        ]);
        $project_budget->save();
        alert()->success('Successfully Add Data.', 'Success!');
        return redirect('/projects_budget');
    }
   
    /**
     * Show the form for editing issue.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project_budget     = ProjectBudget::find($id);
        $project            = Project::all();
        return view('backoffice.project_budget.edit', compact('project_budget','project'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project_budget   = ProjectBudget::find($id);
        $project_budget->rencana     = $request->get('rencana');
        $project_budget->realisasi   = $request->get('realisasi');
        $project_budget->proyeksi    = $request->get('proyeksi'); 
        $project_budget->project_id  = $request->get('project_id');
        $project_budget->date        = $request->get('date');
        $project_budget->save();
        alert()->success('Successfully Update Data.', 'Success!');
        return redirect('/projects_budget');
        
    }

    
   /** 
     * Remove the specified resou   rce from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $project_budget = ProjectBudget::find($id);
        $project_budget->delete();
        alert()->success('Successfully Remove Data.', 'Success!');
        return redirect('/projects_budget');
    }


}

<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\Entities\Activity;
use Illuminate\Http\Request;
use DB;
use App\Imports\ActivitiesImport;
use App\Imports\SubActivitiesImport;
use App\Imports\ActivitiesDeliveryImport;
use Excel;
use Auth;
use SweetAlert;


class ActivityController extends Controller
{

    /**
     * Show work activity for general
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(Request $request)
    {
        $id            = $request->segment(2);
        $activity      = Activity::where('id', $id)->first();
        $activity_id   = $activity->id;
        $general       = DB::select('SELECT distinct(projects.name) as project_name,
                        projects.id as project_id,divisions.name as division_name,
                        divisions.id as division_id FROM projects
                        join divisions ON projects.id = divisions.project_id
                        join activities ON divisions.id = activities.division_id
                        where activities.id = ' . $activity_id . '');
                        
        $progress = DB::select('SELECT sub_activities.name, sub_activities.id, 
                                sum(work_activities.status) as jumlah,COUNT(work_activities.status) as total FROM projects
                                LEFT JOIN divisions ON projects.id = divisions.project_id
                                LEFT JOIN activities ON divisions.id = activities.division_id
                                LEFT JOIN sub_activities ON activities.id = sub_activities.activity_id
                                LEFT JOIN work_activities ON sub_activities.id = work_activities.sub_activity_id
                                where activities.id= ' . $activity_id . ' GROUP BY sub_activities.id
                                '); // AND divisions.name != "Delivery"


        return view('backoffice.sub_activities.sub_activity', compact(
            'general',
            'activity_id',
            'activity',
            'progress'
        ));
    }


    /**
     * Show sub activity for division delivery 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showDelivery(Request $request)
    {
        $id            = $request->segment(2);
        $activity      = Activity::where('id', $id)->first();
        $activity_id   = $activity->id;
        $general       = DB::select('SELECT distinct(projects.name) as project_name,divisions.name as division_name,
                        divisions.id as division_id, projects.id as project_id FROM projects
                        join divisions ON projects.id = divisions.project_id
                        join activities ON divisions.id = activities.division_id
                        where activities.id = ' . $activity_id . '');

        $progress = DB::select('SELECT projects.id as project_id,activities.id as activity_id,
                                activities.name, trainsets.id,activities.start_delivery,activities.end_delivery,
                                sum(activities.status_delivery) as jumlah,activities.status, 
                                COUNT(activities.status_delivery) as total FROM projects
                                LEFT JOIN divisions ON projects.id = divisions.project_id
                                LEFT JOIN activities ON divisions.id = activities.division_id
                                where activities.id= ' . $activity_id . ' AND divisions.name = "Delivery" GROUP BY activities.id
                                ');

        return view('backoffice.trainsets.trainset_delivery', compact(
            'general',
            'activity_id',
            'activity',
            'progress'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required',
            'division_id'   => 'required',
        ]);
        $max_activity = Activity::max('id');
        $activity = new Activity([
            'id' => $max_activity+1,
            'name'          => $request->get('name'),
            'division_id'   => $request->get('division_id')
        ]);
        $activity->save();
        alert()->success('Successfully Add Data.', 'Success!');
        return redirect()->back();
    }


    public function storeExcelActivity(Request $request)
    {
        //VALIDASI
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new ActivitiesImport, $file); //IMPORT FILE 
            alert()->success('Successfully Add Data.', 'Success!');
            return redirect()->back();
        }
        return redirect()->back()->with('error','Please choose file before');
    }

    public function storeExcelActivityDelivery(Request $request)
    {
        //VALIDASI
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new ActivitiesDeliveryImport, $file); //IMPORT FILE 
            alert()->success('Successfully Add Data.', 'Success!');
            return redirect()->back();
        }
        return redirect()->back()->with('error', 'Please choose file before');
    }

    /**
     * Show the form for editing activity.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activity = Activity::find($id);
        return view('backoffice.activities.edit', compact('activity'));
    }

    /** 
     * update checklist type work activity
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateTaskDelivery(Request $request)
    {
        $id = $request->get('idwork');
        $activity = Activity::where('id', $id)->first();
        $activity->status_delivery = $request->get('cek');
        $activity->save();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $url = $request->get('url');
       
        if(Auth::user()->role_id == 1) {
            $request->validate([
                'name' => 'required',
            ]);
            $activity = Activity::find($id);
            $activity->name = $request->get('name');
            $activity->save();
        } else if(Auth::user()->role_id == 5) {
            $file = time() . '.' . $request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('Activity'), $file);

            $activity = Activity::find($id);
            $activity->picture = $file;
            $activity->save();
        }

        alert()->success('Successfully Update Data.', 'Success!');
        return redirect($url);
    }

    /** 
     * Remove the specified resou   rce from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity = Activity::find($id);
        $activity->delete();
        alert()->success('Successfully Remove Data.', 'Success!');
        return redirect()->back();
    }
    
      public function storeExcelActivityWithDivision(Request $request)
    {
        //VALIDASI
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new SubActivitiesImport, $file); //IMPORT FILE 
            alert()->success('Successfully Add Data.', 'Success!');
            return redirect()->back();
        }
        return redirect()->back()->with('error','Please choose file before');
    }

}

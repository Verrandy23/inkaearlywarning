<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Project;
use DB;
use Carbon\Carbon;
use Auth;


class NotificationController extends Controller
{

    /**
     * Show finish activities
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->project_to_pm;
        if (Auth::user()->project_to_pm == null) {
   
            $teknologi  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Teknologi')
                            ->where('work_activities.status','=',1)
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status')
                            ->orderBy('work_activities.id')->get();


        } else {
            $teknologi  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Teknologi')
                            ->where('work_activities.status','=',1)
                            ->where('projects.id',$id)
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status')
                            ->orderBy('work_activities.id')->get();
        }


        if (Auth::user()->project_to_pm == null) {
            $logistik  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Logistik')
                            ->where('work_activities.status','=',1)
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status')
                            ->orderBy('work_activities.id')->get();
        } else {
            $logistik  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Logistik')
                            ->where('work_activities.status','=',1)
                            ->where('projects.id',$id)
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status')
                            ->orderBy('work_activities.id')->get();
        }

        if (Auth::user()->project_to_pm == null) {
            $fabrikasi = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Fabrikasi')
                            ->where('work_activities.status','=',1)
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status')
                            ->orderBy('work_activities.id')->get();
        } else {
            $fabrikasi  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Fabrikasi')
                            ->where('work_activities.status','=',1)
                            ->where('projects.id',$id)
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status')
                            ->orderBy('work_activities.id')->get();
        }

        if (Auth::user()->project_to_pm == null) {
            $finishing = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Finishing')
                            ->where('work_activities.status','=',1)
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status')
                            ->orderBy('work_activities.id')->get();
        } else {
            $finishing  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Finishing')
                            ->where('work_activities.status','=',1)
                            ->where('projects.id',$id)
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status')
                            ->orderBy('work_activities.id')->get();
        }

        if (Auth::user()->project_to_pm == null) {
            $testing   = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Testing')
                            ->where('work_activities.status','=',1)
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status')
                            ->orderBy('work_activities.id')->get();
        } else {
            $testing    = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Testing')
                            ->where('work_activities.status','=',1)
                            ->where('projects.id',$id)
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status')
                            ->orderBy('work_activities.id')->get();
        }
        if (Auth::user()->project_to_pm == null) {
            
            $delivery = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activity_deliveries','divisions.id','activity_deliveries.division_id')
                                ->where('divisions.name','Delivery')
                                ->where('activity_deliveries.status','=',1)
                                ->select('projects.name as project_name','activity_deliveries.name as activity_name',
                                        'activity_deliveries.slack','activity_deliveries.end_date','activity_deliveries.status')
                                ->orderBy('activity_deliveries.id')->get();

        } else {
            $delivery = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activity_deliveries','divisions.id','activity_deliveries.division_id')
                                ->where('projects.id',$id)
                                ->where('divisions.name','Delivery')
                                ->where('activity_deliveries.status','=',1)
                                ->select('projects.name as project_name','activity_deliveries.name as activity_name',
                                        'activity_deliveries.slack','activity_deliveries.end_date','activity_deliveries.status')
                                ->orderBy('activity_deliveries.id')->get();
        }

        return view('backoffice.notifications.finish_all', compact('teknologi', 'logistik', 'fabrikasi', 'finishing', 'testing', 'delivery'));
    }

    /**
     * Show warning activities
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showWarning()
    {
        $id = Auth::user()->project_to_pm;
        if (Auth::user()->project_to_pm == null) {
            $teknologi  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Teknologi')
                            ->where('work_activities.status','!=',1)
                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status',
                                    DB::raw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                            work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date)) as remaining'))
                            ->orderBy('work_activities.id')->get();
        } else {
            $teknologi  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Teknologi')
                            ->where('work_activities.status','!=',1)
                            ->where('projects.id',$id)
                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status',
                                    DB::raw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                            work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date)) as remaining'))
                            ->orderBy('work_activities.id')->get();
        
        }

        if (Auth::user()->project_to_pm == null) {
            $logistik = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Logistik')
                            ->where('work_activities.status','!=',1)
                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status',
                                    DB::raw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                            work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date)) as remaining'))
                            ->orderBy('work_activities.id')->get();
                            
        } else {
         $logistik = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Logistik')
                            ->where('work_activities.status','!=',1)
                            ->where('projects.id',$id)
                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status',
                                    DB::raw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                            work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date)) as remaining'))
                            ->orderBy('work_activities.id')->get();
        }

        if (Auth::user()->project_to_pm == null) {
            $fabrikasi  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Fabrikasi')
                            ->where('work_activities.status','!=',1)
                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status',
                                    DB::raw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                            work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date)) as remaining'))
                            ->orderBy('work_activities.id')->get();
        
        } else {
            $fabrikasi  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Fabrikasi')
                            ->where('work_activities.status','!=',1)
                            ->where('projects.id',$id)
                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status',
                                    DB::raw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                            work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date)) as remaining'))
                            ->orderBy('work_activities.id')->get();
        }

        if (Auth::user()->project_to_pm == null) {
            $finishing  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Finishing')
                            ->where('work_activities.status','!=',1)
                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status',
                                    DB::raw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                            work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date)) as remaining'))
                            ->orderBy('work_activities.id')->get();
        } else {
           $finishing   = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Finishing')
                            ->where('work_activities.status','!=',1)
                            ->where('projects.id',$id)
                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status',
                                    DB::raw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                            work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date)) as remaining'))
                            ->orderBy('work_activities.id')->get();
        }

        if (Auth::user()->project_to_pm == null) {
            $testing    = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Testing')
                            ->where('work_activities.status','!=',1)
                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status',
                                    DB::raw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                            work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date)) as remaining'))
                            ->orderBy('work_activities.id')->get();
        } else {
            $testing    = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                            ->leftJoin('activities','divisions.id','activities.division_id')
                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                            ->where('divisions.name','Testing')
                            ->where('work_activities.status','!=',1)
                            ->where('projects.id',$id)
                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                            ->select('work_activities.id as work_activity_id','divisions.name as division_name',
                                    'projects.name as project_name','activities.name as activity_name',
                                    'sub_activities.name as sub_name','work_activities.slack','work_activities.name as work_name',
                                    'work_activities.end_date','work_activities.status',
                                    DB::raw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                            work_activities.end_date + INTERVAL work_activities.slack DAY), 
                                            TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date)) as remaining'))
                            ->orderBy('work_activities.id')->get();
        }

        if (Auth::user()->project_to_pm == null) {
                
            $delivery = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activity_deliveries','divisions.id','activity_deliveries.division_id')
                                ->where('divisions.name','Delivery')
                                ->where('activity_deliveries.status','!=',1)
                                ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,activity_deliveries.end_date) >= 1')
                                ->whereRaw('IF(activity_deliveries.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                            activity_deliveries.end_date + INTERVAL activity_deliveries.slack DAY) <=10 , 
                                            TIMESTAMPDIFF(DAY,CURRENT_DATE ,activity_deliveries.end_date) <= 10)')
                                ->select('projects.name as project_name','activity_deliveries.name as activity_name',
                                        'activity_deliveries.slack','activity_deliveries.end_date','activity_deliveries.status',
                                        DB::raw('IF(activity_deliveries.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                        activity_deliveries.end_date + INTERVAL activity_deliveries.slack DAY), 
                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,activity_deliveries.end_date)) as remaining'))
                                ->orderBy('activity_deliveries.id')->get();

           
        } else {
            $delivery = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activity_deliveries','divisions.id','activity_deliveries.division_id')
                                ->where('divisions.name','Delivery')
                                ->where('activity_deliveries.status','!=',1)
                                ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,activity_deliveries.end_date) >= 1')
                                ->whereRaw('IF(activity_deliveries.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                            activity_deliveries.end_date + INTERVAL activity_deliveries.slack DAY) <=10 , 
                                            TIMESTAMPDIFF(DAY,CURRENT_DATE ,activity_deliveries.end_date) <= 10)')
                                ->select('projects.name as project_name','activity_deliveries.name as activity_name',
                                        'activity_deliveries.slack','activity_deliveries.end_date','activity_deliveries.status',
                                        DB::raw('IF(activity_deliveries.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                        activity_deliveries.end_date + INTERVAL activity_deliveries.slack DAY), 
                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,activity_deliveries.end_date)) as remaining'))
                                ->orderBy('activity_deliveries.id')->get();
        }
        return view('backoffice.notifications.warning_all', compact('teknologi', 'logistik', 'fabrikasi', 'finishing', 'testing','delivery'));
    }

    /**
     * show notification late on Navbar
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        if(Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4){
            $late_admin_navbar         = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                        ->leftJoin('activities','divisions.id','activities.division_id')
                                        ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                        ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                        ->where('work_activities.status','!=',1)
                                        ->where('work_activities.end_date','<=',Carbon::today())
                                        ->select('projects.name as project_name','divisions.name as division_name',
                                                'activities.name as activity_name',
                                                'sub_activities.name as sub_name','work_activities.slack',
                                                'work_activities.name as work_name','work_activities.end_date',
                                                DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + 
                                                        INTERVAL work_activities.slack DAY), 
                                                        DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                        ->having('late','>',0)
                                        ->orderByRaw('work_activities.id DESC LIMIT 10')->get();
            
        } else if (Auth::user()->role_id == 4) {
            $id = Auth::user()->project_to_pm;
            $late_pm_navbar         = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                    ->leftJoin('activities','divisions.id','activities.division_id')
                                    ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                    ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                    ->where('projects.id',$id)
                                    ->where('work_activities.status','!=',1)
                                    ->where('work_activities.end_date','<=',Carbon::today())
                                    ->select('projects.name as project_name','divisions.name as division_name',
                                            'activities.name as activity_name',
                                            'sub_activities.name as sub_name','work_activities.slack',
                                            'work_activities.name as work_name','work_activities.end_date',
                                            DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + 
                                                    INTERVAL work_activities.slack DAY), 
                                                    DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                    ->having('late','>',0)
                                    ->orderByRaw('work_activities.id DESC LIMIT 10')->get();
           
        } else {
            $division_name          = Auth::user()->division_name;
            $late_division_navbar   = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                    ->leftJoin('activities','divisions.id','activities.division_id')
                                    ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                    ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                    ->where('divisions.name',$division_name)
                                    ->where('work_activities.status','!=',1)
                                    ->where('work_activities.end_date','<=',Carbon::today())
                                    ->select('projects.name as project_name','activities.name as activity_name',
                                            'sub_activities.name as sub_name','work_activities.slack',
                                            'work_activities.name as work_name','work_activities.end_date',
                                            DB::raw('IF(work_activities.slack>0, DATEDIFF(CURRENT_DATE ,work_activities.end_date + 
                                                    INTERVAL work_activities.slack DAY), 
                                                    DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                    ->having('late','>',0)
                                    ->orderByRaw('work_activities.id DESC LIMIT 10')->get();
    
        }

        if (Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4) {
            return view('backoffice.notifications.notif_navbar', compact('late_admin_navbar'));
        } else if (Auth::user()->role_id == 4) {
            return view('backoffice.notifications.notif_navbar', compact('late_pm_navbar'));

        } else {
            return view('backoffice.notifications.notif_navbar', compact('late_division_navbar'));
        }
        
       
    }

    /**
     * Count warning
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function countWarning()
    {
        if(Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4){

            $count_warning = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                            ->leftJoin('activities','divisions.id','activities.division_id')
                                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                            ->where('work_activities.status','!=',1)
                                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                                            ->selectRaw('COUNT(work_activities.id) as id')
                                            ->get();

        } else if (Auth::user()->role_id == 4){
            $id = Auth::user()->project_to_pm;
            $count_warning_pm   = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                            ->leftJoin('activities','divisions.id','activities.division_id')
                                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                            ->where('divisions.name','Logistik')
                                            ->where('work_activities.status','!=',1)
                                            ->where('projects.id',$id)
                                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                                            ->selectRaw('COUNT(work_activities.id) as id')
                                            ->get();
        } else {
            $division_name          = Auth::user()->division_name;
            $count_warning_division = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                            ->leftJoin('activities','divisions.id','activities.division_id')
                                            ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                            ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                            ->where('divisions.name','Logistik')
                                            ->where('work_activities.status','!=',1)
                                            ->where('divisions.name',$division_name)
                                            ->whereRaw('TIMESTAMPDIFF(DAY, CURRENT_DATE,work_activities.end_date) >= 1')
                                            ->whereRaw('IF(work_activities.slack>0, TIMESTAMPDIFF(DAY,CURRENT_DATE ,
                                                        work_activities.end_date + INTERVAL work_activities.slack DAY) <=10 , 
                                                        TIMESTAMPDIFF(DAY,CURRENT_DATE ,work_activities.end_date) <= 10)')
                                            ->selectRaw('COUNT(work_activities.id) as id')
                                            ->get();
        }

        if (Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4) {
            return view('backoffice.notifications.count_warning', compact('count_warning'));
        } else if (Auth::user()->role_id == 4) {
            return view('backoffice.notifications.count_warning', compact('count_warning_pm'));
        } else {
            return view('backoffice.notifications.count_warning', compact('count_warning_division'));
        }


    }

    /**
     * Count late Admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function countLate()
    {
        if(Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4){
            $late_admin = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('work_activities.status','!=',1)
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->select('work_activities.id as work_activity_id',DB::raw('IF(work_activities.slack>0, 
                                        DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY),
                                        DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();

            $count_late_admin = 0;
            foreach ($late_admin as $index => $row) {
                $count_late_admin++;
            }

        } else if (Auth::user()->role_id == 4) {
            $id         = Auth::user()->project_to_pm;
            $late_pm    = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('projects.id',$id)
                                ->where('work_activities.status','!=',1)
                                ->where('work_activities.end_date','<=',Carbon::today())
                                ->select('work_activities.id as work_activity_id',DB::raw('IF(work_activities.slack>0, 
                                        DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY),
                                        DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                ->having('late','>',0)
                                ->orderBy('work_activities.id')->get();

            $count_late_pm = 0;
            foreach ($late_pm as $index => $row) {
                $count_late_pm++;
            }                     
        } else {
            $division_name  = Auth::user()->division_name;
            $late_division  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                    ->leftJoin('activities','divisions.id','activities.division_id')
                                    ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                    ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                    ->where('divisions.name',$division_name)
                                    ->where('work_activities.status','!=',1)
                                    ->where('work_activities.end_date','<=',Carbon::today())
                                    ->select('work_activities.id as work_activity_id',DB::raw('IF(work_activities.slack>0, 
                                            DATEDIFF(CURRENT_DATE ,work_activities.end_date + INTERVAL work_activities.slack DAY),
                                            DATEDIFF(CURRENT_DATE ,work_activities.end_date)) as late'))
                                    ->having('late','>',0)
                                    ->orderBy('work_activities.id')->get();
            
            $count_late_division = 0;
            foreach ($late_division as $index => $row) {
                $count_late_division++;
            }                     
                                
        }

        if (Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4) {
            return view('backoffice.notifications.count_late', compact('count_late_admin'));
        } else if (Auth::user()->role_id == 4) {
            return view('backoffice.notifications.count_late', compact('count_late_pm'));

        } else {
            return view('backoffice.notifications.count_late', compact('count_late_division'));
        }
    }

      /**
     * Count late Admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function countFinish()
    {
        if(Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4){
            $finish_admin = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('work_activities.status',1)
                                ->orderBy('work_activities.id')->get();

            $count_finish_admin = 0;
            foreach ($finish_admin as $index => $row) {
                $count_finish_admin++;
            }

        } else if (Auth::user()->role_id == 4) {
            $id         = Auth::user()->project_to_pm;
            $finish_pm  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                ->leftJoin('activities','divisions.id','activities.division_id')
                                ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                ->where('projects.id',$id)
                                ->where('work_activities.status',1)
                                ->orderBy('work_activities.id')->get();

            $count_finish_pm = 0;
            foreach ($finish_pm as $index => $row) {
                $count_finish_pm++;
            }                     
        } else {
            $division_name  = Auth::user()->division_name;
            $finish_division  = Project::leftJoin('divisions', 'projects.id', '=', 'divisions.project_id')
                                    ->leftJoin('activities','divisions.id','activities.division_id')
                                    ->leftJoin('sub_activities','activities.id','sub_activities.activity_id')
                                    ->leftJoin('work_activities','sub_activities.id','work_activities.sub_activity_id')
                                    ->where('divisions.name',$division_name)
                                    ->where('work_activities.status',1)
                                    ->orderBy('work_activities.id')->get();
            
            $count_finish_division = 0;
            foreach ($finish_division as $index => $row) {
                $count_finish_division++;
            }                     
                                
        }

        if (Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4) {
            return view('backoffice.notifications.count_finish', compact('count_finish_admin'));
        } else if (Auth::user()->role_id == 4) {
            return view('backoffice.notifications.count_finish', compact('count_finish_pm'));

        } else {
            return view('backoffice.notifications.count_finish', compact('count_finish_division'));
        }
    }

   
    /**
     * Count Issue
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function countIssue()
    {
        if(Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4){
            $count_issue            = DB::select('SELECT COUNT(issues.id) as id FROM issues where status="open"');
        } else if (Auth::user()->role_id == 4) {
            $id = Auth::user()->project_to_pm;
            $count_issue_pm         = DB::select('SELECT COUNT(issues.id) as id FROM issues WHERE status="open" and project_id = ' . $id . '');
      
        } else {
            $division_name          = Auth::user()->division_name;
            $count_issue_division   = DB::select('SELECT COUNT(issues.id) as id FROM issues where division_name= "' . $division_name . '" and status="open"');
        }

        if (Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4) {
            return view('backoffice.notifications.count_issue', compact('count_issue'));
        } else if (Auth::user()->role_id == 4) {
            return view('backoffice.notifications.count_issue', compact('count_issue_pm'));
        } else {
            return view('backoffice.notifications.count_issue', compact('count_issue_division'));
        }    
    }
}

  


<?php

namespace App\Http\Controllers\V1\Backoffice;

use App\Http\Controllers\Controller;
use App\Entities\ReportData;
use App\Entities\Project;
use Illuminate\Http\Request;
use DB;
use Auth;

class ReportController extends Controller
{
    /**
     * Show the page project.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $report  = ReportData::with('get_project')->get();
        $project = Project::all();
        return view('backoffice.reports.report', compact('report','project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required',
            'project_id'    => 'required',
            'date'          => 'required',
        ]);

        $file = time() . '.' . $request->file->getClientOriginalExtension();
        $request->file->move(public_path('laporan'), $file);

       try{ $report = new ReportData([
            'name'          => $request->get('name'),
            'file'          => $file,
            'description'   => $request->get('description'),
            'project_id'    => $request->get('project_id'),
            'date'          => $request->get('date'),
        ]);
        $report->save();
        alert()->success('Successfully Add Data.', 'Success!');
        return redirect('/reports');
       }
       catch(Exception $e){
        
       }
    }


    /**
     * Show the form for editing project.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data  = ReportData::find($id);
        $project = Project::all();
        return view('backoffice.reports.edit', compact('data','project'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
        'name'          => 'required',
        'project_id'    => 'required',
        'date'          => 'required',
        ]);

        
        $report = ReportData::find($id);

        if (empty($request->file)) {
            $report->name          = $request->get('name');
            $report->description   = $request->get('description');
            $report->date          = $request->get('date');
            $report->project_id    = $request->get('project_id');  
            $report->save();
            alert()->success('Successfully Update Data.', 'Success!');
            return redirect('/reports');

        } else {
            $file = time() . '.' . $request->file->getClientOriginalExtension();
            $request->file->move(public_path('laporan'), $file);

            $report->name          = $request->get('name');
            $report->file          = $file;
            $report->description   = $request->get('description');
            $report->date          = $request->get('date');
            $report->project_id    = $request->get('project_id');  
            $report->save();
            alert()->success('Successfully Update Data.', 'Success!');
            return redirect('/reports');

        } 


    }

    /** 
     * Remove the specified resou   rce from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $report = ReportData::find($id);
        $report->delete();
        alert()->success('Successfully Remove Data.', 'Success!');
        return redirect('/reports');
    }
}

@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC"></h1>
    <div class="section-header-breadcrumb">
        <a class="btn btn-primary btn-sm" href="{{ URL::previous() }}"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <h6 class="titleC text-center">Form Edit Project Budget Bulanan</h6><br>
            <form method="POST" action="{{ route('projects_budget.update',$project_budget->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Budget Rencana %') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="number" min="0" oninvalid="setCustomValidity('Nilai tidak boleh kosong,nilai minimal 0 tidak boleh menggunakan spasi atau . ')" value="{{$project_budget->rencana}}" class="form-control @error('name') is-invalid @enderror" name="rencana" required>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Budget Realisasi %') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="number" min="0" value="{{$project_budget->realisasi}}" class="form-control @error('name') is-invalid @enderror" name="realisasi" required>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Budget Proyeksi %') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" value="{{$project_budget->realisasi}}" class="form-control @error('name') is-invalid @enderror" name="proyeksi" required>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>


                <div class="form-group row">
                    <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('Project') }}</label>
                    <div class="col-md-6">
                        <select class="form-control @error('project_id') is-invalid @enderror" name="project_id">
                            <?php if (empty($project_budget->project_id)) { ?>
                                <option value="" selected>Pilih Project</option>
                            <?php } else { ?>
                                @foreach($project as $row)
                                <option value="{{$row->id}}" {{ $project_budget->get_project->id == $row->id ? 'selected' : '' }}>{{$row->name}}</option>
                                @endforeach
                            <?php } ?>
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="date" value="{{$project_budget->date}}" class="form-control @error('date') is-invalid @enderror" name="date" required>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Update') }}
                        </button>
                    </div>
                </div>
                <br>
                <span class="text-info">* Data tidak boleh kosong, nilai yang dimasukkan minimal 0 , tidak boleh menggunakan tambahan karakter(.)</span>
            </form>

        </div>
    </div>
</div>

@endsection
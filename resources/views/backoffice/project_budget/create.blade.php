@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC"></h1>
    <div class="section-header-breadcrumb">
        <a class="btn btn-primary btn-sm" href="{{ URL::previous() }}"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <h6 class="titleC text-center">Form Edit Project Budget Bulanan</h6><br>
                <form method="post" id="form" action="{{ route('projects_budget.store') }}">
                    @csrf

                    <div class="form-group">
                        <label>Budget Rencana %</label>
                        <input required type="number" data-error="input budget rencana" min="0" value="0" name="rencana" class="form-control" placeholder="Masukkan budget rencana">
                    </div>

                    <div class="form-group">
                        <label>Budget Realisasi %</label>
                        <input required type="number" min="0" data-error="input budget realisasi" value="0" name="realisasi" class="form-control" placeholder="Masukkan budget realisasi">
                    </div>

                    <div class="form-group">
                        <label>Budget Proyeksi %</label>
                        <input required type="number" data-error="input budget proyeksi" min="0" value="0" name="proyeksi" class="form-control" placeholder="Masukkan budget realisasi">
                    </div>

                    <div class="form-group">
                        <label>Project</label>
                        <select id="project_id" class="form-control @error('project_id') is-invalid @enderror" data-error="input project" name="project_id" required>
                        <option value="" selected>Pilih Project</option>
                        @foreach($project as $row)
                        <option value="{{$row->id}}">{{$row->name}}</option>
                        @endforeach
                    </select>
                    </div>

                    <div class="form-group">
                        <label>Date</label>
                        <input required type="date" data-error="input tanggal" name="date" class="form-control">
                    </div>

                    <div class="form-group">
                        <div>
                            <button type="button" id="btn_save" class="btn btn-primary">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>
                    <br>
                <span class="text-info">* Data tidak boleh kosong, nilai yang dimasukkan minimal 0 , tidak boleh menggunakan tambahan karakter(.)</span>
                </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

    var $input = document.querySelectorAll('#form input.form-control');

    for (let i = 0; i<$input.length; i++){
        if($input[i].type === "number"){
            $input[i].addEventListener('keyup', event=>{
                event.target.value = event.target.value.split(/[^0-9]/).join('');
            });
        }
       
    }

    document.getElementById('btn_save').addEventListener('click', event=>{
    let $select = document.querySelectorAll('#form select');


    let $errors = []
    let error = false;
    // chek input empty
    for(let i=0; i<$input.length; i++){

        // check input if emtpy
        if($input[i].value == ""){
            var $input_error = $input[i].dataset.error;
            if ($input_error !== undefined){
                $errors.push($input_error);
                error = true;
            }
        }


    }

    //check select empty
    for(let i=0; i<$select.length; i++){
        const $error_name = $select[i].dataset.error;

        if($select[i].value == ""){
            if($error_name !== undefined){
                $errors.push($error_name);
                error =true;
            }
        }
    }

    var text_error = "";

    for(let i=0; i<$errors.length; i++){
        text_error+= "<p class='text-danger'> form " + $errors[i] + " tidak boleh kosong</p>";
    }

    var div_error = document.createElement("div");
    div_error.innerHTML = text_error;
    if (error){
        swal({
            title: 'Error !',
            content: div_error,
            icon: 'error',
        });
    }
    else{
        document.querySelector('#form').submit();
    }
});
    })
     
</script>
@endsection
@extends('layouts.backend.masterPage')
@section('content')

<div class="section-header">
    <h1 class="titleC text-center">Detail Project </h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
            <a href="/backoffice"><strong> <i class="fas fa-tachometer-alt"></i> Main Dashboard</strong></a>
        </div><br><br>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <h6 class="color-warning"> Project {{$project->name}}</h6>
            <h6 class="color-success"> <i class="far fa-calendar color-success"></i> Contact Date : <?php
                                                                                                    $date = date_create($project->start_date);
                                                                                                    echo date_format($date, "j F Y");
                                                                                                    ?> - <?php
                                                                                                            $date = date_create($project->end_date);
                                                                                                            echo date_format($date, "j F Y");
                                                                                                            ?> </h6>
            <b class="color-primary">Client : {{$project->client}}</b>
            <p>{{$project->description}}</p>

            <hr>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="table3">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @endif
                    <thead style="background-color: #718093">
                        <tr>
                            <th style="color:white" width="10%">Term Of Payment</th>
                            <th style="color:white" width="10%">Payment Method</th>
                            <th style="color:white" width="20%">Denda</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($detail_project->id))
                        <tr>
                            <td>{{$detail_project->term_payment}}</td>
                            <td>{{$detail_project->payment}}</td>
                            <td>{{$detail_project->denda}}</td>


                        </tr>
                        @else
                        <tr>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <hr>
            <h6 class="titleC text-center color-primary">Pengiriman</h4><br>
                <div class="table-responsive">
                    <table class="table col-lg-12 zero-configuration">
                        @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div><br />
                        @endif
                        <thead style="background-color: #e1b12c">
                            <tr>
                                <th style="color:white" width="10%">Batch</th>
                                <th style="color:white" width="30%">Mulai Tanggal Pengiriman</th>
                                <th style="color:white" width="30%">Akhir Tanggal Pengiriman</th>
                                <th style="color:white" width="30%">Lokasi Pengiriman</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($project->get_details as $row)
                            <tr>
                                <td>{{$row->batch}}</td>
                                <td>{{$row->start_delivery_date}}</td>
                                <td>{{$row->end_delivery_date}}</td>
                                <td>{{$row->address_delivery}}</td>


                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

        </div>
    </div>
</div>
@endsection
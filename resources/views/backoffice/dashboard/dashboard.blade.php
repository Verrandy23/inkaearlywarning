@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1>Early Warning Monitoring</h1>
</div>
<div class="row">
    @foreach($project as $row)
    <?php $random = mt_rand(1, 5);
        $gradient = 'gradient-' . $random;

        $total = "";
         foreach ($project_progress1 as $row1) {
                                # code...
            if($row1->project_id == $row->id){

                $total = ($row1->total_jumlah)/($row1->total_aktivitas)*100;
                
            }
            }
        ?>
    @if (!empty($row->progress))
    <div class="col-lg-12 col-md-6 col-sm-12 mb-30">
        <div class="bg-white pd-20  height-100-p shadow">
            <div class="row">
                <div class="col-lg-8">
                    <div class="project-info clearfix">
                        <div class="project-info-left">
                            <p class="titleC">{{$row->name}}</p>
                            
                        </div>
                        <div class="project-info-right">
                            <div class="row">
                                <a href="/notifications" style="margin-top:15px;" class="icon box-shadow bg-light-green text-white">
                                <i class="fa fa-check"></i>
                                </a>
                                <a href="/warning_notifications" style="margin-top:15px" class="icon box-shadow bg-light-yellow text-white">
                                <i class="fa fa-exclamation-triangle"></i>
                                </a>
                                <a href="/late_history" style="margin-top:15px;margin-right:15px" class="icon box-shadow bg-light-orange text-white">
                                <i class="fa fa-bell"></i>
                                </a>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="project-info-progress">
                        <div class="row clearfix">
                            <div class="col-sm-6 text-muted weight-500">Progress Rencana</div>
                            <div class="col-sm-6 text-right"><b><?php echo number_format((float) $row->progress_rencana, 2, '.', ''); ?> %</b></div>
                        </div>
                        <div class="progress" style="height: 15px;">
                            <div class="progress-bar bg-light-success progress-bar-striped progress-bar-animated" role="progressbar" style="width:{{$row->progress_rencana}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                        <div class="row clearfix" style="margin-top:2%">
                            <div class="col-sm-6 text-muted weight-500">Progress Aktual</div>
                            <div class="col-sm-6 text-right"><b> <?php
                                echo number_format((float) $total, 2, '.', ''); 
                                
                             ?> %</b></div>
                        </div>
                        <div class="progress" style="height: 15px;">
                            <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width:<?php echo $total; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                        <br>
                        <a href="/dashboard_divisions/{{$row->id}}">
                        <button class="btn btn-icon btn-sm btn-info" type="button"> Show Division
                        <i class="fa fa-arrow-right"></i>
                        </button>
                        </a>
                        <div class="project-info-right">
                            @if (strpos($row->remaining, '-') !== false)
                            <strong class="badge badge-danger"><?php echo substr($row->remaining, 1) ?> hari terlambat</strong>
                            @else
                            <strong class="badge badge-success">{{$row->remaining}} hari tersisa</strong>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row" style="margin-top:5%">
                        <div class="col-6 col-6">
                            <div class="wizard-steps">
                                <div class="wizard-step">
                                    <div class="wizard-step-icon">
                                        <button class="btn btn-sm mb-1 btn-primary" style="font-size:25px;width:60px;height:50px">
                                            <a href="{{ route('dashboards.show', $row->id)}}">
                                                <div class="icon text-white">
                                                    <i class="fa fa-train"></i>
                                                </div>
                                            </a>
                                        </button>
                                    </div>
                                    <div class="wizard-step-label" style="margin-top:10px">
                                        Project Information
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-6">
                            <div class="wizard-steps">
                                <div class="wizard-step">
                                    <div class="wizard-step-icon">
                                        <button class="btn btn-sm mb-1 btn-success" style="font-size:25px;width:60px;height:50px">
                                            <a href="{{ route('dashboards.show', $row->id)}}">
                                                <div class="icon text-white">
                                                    <i class="fa fa-info"></i>
                                                </div>
                                            </a>
                                        </button>
                                    </div>
                                    <div class="wizard-step-label" style="margin-top:10px">
                                        Issue<br> Information
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- If conditional null -->
    @else
    <div class="col-lg-8 col-md-6 col-sm-12 mb-30">
        <div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
            <div class="project-info clearfix">
                <div class="project-info-left">
                    <p class="titleC">{{$row->name}}</p>
                </div>
                <div class="project-info-right">
                    Data Not Completed
                </div>
            </div>
            <div class="project-info-progress">
                <div class="row clearfix">
                    <div class="col-sm-6 text-muted weight-500">Progress</div>
                    <div class="col-sm-6 text-right weight-500 font-14 text-muted">0</div>
                </div>
                <div class="progress" style="height: 10px;">
                    <div class="progress-bar bg-blue progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endforeach
</div>
<!-- <div id="container"></div> -->
<div class="card">
    <div class="card-body">
        <div class="section-title">Monthly Project Progress</div>
        @if(Auth::user()->role_id == 4)
        @else
        <div class="form-group">
            <form action="/backoffice" method="GET">
                <div class="col-md-12">
                    <div class="input-group">
                        <select class="custom-select" id="inputGroupSelect04" name="search">
                            <option value="" selected> Pilih Projek</option>
                            @foreach($monthly_project as $row)
                            <option value="{{$row->project_id}}">{{$row->project_name}}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @endif
        <div id="container2"></div>
        <table id="datatable" style="display:none">
            <thead>
                <tr>
                    <th></th>
                    <th>Rencana</th>
                    <th>Realisasi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($project_progress as $row)
                <tr>
                    <th>
                        <?php
                            $date = date_create($row->date);
                            echo date_format($date, "F Y");
                            ?>
                    </th>
                    <td>{{$row->progress_rencana}}</td>
                    <td>{{$row->progress_realisasi}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="card">
    <div class="card-body">
    <div class="section-title"> Project Issues</div><hr>

        <div class="row">
            <div class="col-md-6">
                <figure class='open_close'>
                    <canvas id='myChart'></canvas>
                </figure>
            </div>
            <div class="col-md-6">
                <figure class='open_close'>
                    <canvas id='myMajorMinor'></canvas>
                </figure>
            </div>
        </div>
    </div>
</div>
<hr>
<div id="container">
    <div class="card">
        <div class="card-body">
            <div class="container">
                <h3 class="titleC text-center">Laporan Project Bulanan</h3>
                <br>
                <table>
                    <tr>
                        <label style="margin-left:1%" for="filter">Filter By Project</label>
                        <th style="width:200px" class="filterhead"></th>
                        <th class="filterhead none"></th>
                        <th class="filterhead none"></th>
                        <th class="filterhead none"></th>
                        <label style="margin-left:11%" for="filter">Filter By Bulan</label>
                        <th class="filterhead"></th>
                    </tr>
                </table>
                <br>
                <div class="table-responsive">
                    <table class="table col-lg-12 zero-configuration" id="exampledr">
                        <thead>
                            <tr>
                                <th width="20%">Project</th>
                                <th width="20%">Name</th>
                                <th width="40%">File</th>
                                <th width="20%">Date</th>
                                <th class="none">Date Filter</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($report as $row)
                            <tr>
                                <td>{{$row->get_project->name}}</td>
                                <td>{{$row->name}}</td>
                                <td>
                                    <a href="/laporan/{{$row->file}}" target="_blank"><img width="100%" src="{{ asset('cover.png')}}" alt="...">
                                    </a>
                                </td>
                                <td>
                                    <?php
                                        $date = date_create($row->date);
                                        echo date_format($date, "j F Y");
                                        ?>
                                </td>
                                <td class="none">
                                    <?php
                                        date_create($row->date);
                                        $string =  date_format($date, "j F Y");
                                        if (stristr($string, 'January') !== FALSE) {
                                            echo 'January';
                                        } else if (stristr($string, 'February') !== FALSE) {
                                            echo 'February';
                                        } else if (stristr($string, 'March') !== FALSE) {
                                            echo 'March';
                                        } else if (stristr($string, 'April') !== FALSE) {
                                            echo 'April';
                                        } else if (stristr($string, 'May') !== FALSE) {
                                            echo 'May';
                                        } else if (stristr($string, 'June') !== FALSE) {
                                            echo 'June';
                                        } else if (stristr($string, 'July') !== FALSE) {
                                            echo 'July';
                                        } else if (stristr($string, 'August') !== FALSE) {
                                            echo 'August';
                                        } else if (stristr($string, 'September') !== FALSE) {
                                            echo 'September';
                                        } else if (stristr($string, 'October') !== FALSE) {
                                            echo 'October';
                                        } else if (stristr($string, 'November') !== FALSE) {
                                            echo 'November';
                                        } else if (stristr($string, 'December') !== FALSE) {
                                            echo 'December';
                                        }
                                        ?>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('assets/stisla/js/Chart.js')}}"></script>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
    $(function() {
      $('#container2').highcharts({
      chart: {
          type: 'column',
          options3d: {
              enabled: true,
              alpha: 15,
              beta: 15,
              viewDistance: 25,
              depth: 40
          }
      },
    
      title: {
        text:  <?php echo "'" . $project_name . "'"?>
      },
    
      xAxis: {
          categories: [ <?php foreach ($project_progress as $row) {
              $date = date_create($row->date);
        echo '"'.date_format($date, "F Y").'",';
        }         ?> ],
          labels: {
              skew3d: true,
              style: {
                  fontSize: '16px'
              }
          }
      },
    
      yAxis: {
          allowDecimals: false,
          min: 0,
          title: {
              text: 'Number of percentage %',
              skew3d: true
          }
      },
    
      tooltip: {
          headerFormat: '<b>{point.key}</b><br>',
          pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}'
      },
    
      plotOptions: {
          column: {
              stacking: 'normal',
              depth: 40
          }
      },
    
        
      series: [
      {
          name: 'Progress Rencana %',
          color: '#eccc68',
          data: [<?php foreach ($project_progress as $value) {
        echo $value->progress_rencana.",";
        } 
        ?>],
          stack: 'male'
      },{
          name: 'Progress Realisasi %',
          color: '#2ed573',
          data: [<?php foreach ($project_progress as $value) {
        echo $value->progress_realisasi.",";
        } 
        ?>],
          stack: 'female'
      }
      ]
        });
      });

    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
    
            labels: ["Open", "Closed"],
            datasets: [{
                label: '',
                data: [
                <?php echo $count_issue_open?>
                , 
                <?php echo $count_issue_closed ?>
                ],
                backgroundColor: [
                'rgba(255, 99, 132, 0.9)',
                'rgba(54, 162, 235, 0.9)'
                ],
                borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1,
            }]
        },
        options: {
            title:{
    
                display: true,
                text:'Rekap All Issue By Status',
            }
        }
    });

    var ctx = document.getElementById("myMajorMinor").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [ <?php foreach ($issues as $value) {
        echo '"'.$value->status.'-'.$value->level.'",';
        }         ?> ],
            datasets: [{
                label: '',
                data: [
                   <?php foreach ($issues as $value) {
        echo $value->total.",";
        } 
        ?>
                ],
                backgroundColor: [
                '#f56767',
                '#41ccba',
                '#feca57',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 99, 132,1)',
                'rgba(54, 162, 235, 1)'
                ],
                borderColor: [
                '#f56767',
                '#41ccba',
                '#feca57',
                'rgba(54, 162, 235, 1)',
                 'rgba(255, 99, 132,1)',
                'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            },
            title:{
    
                display: true,
                text:' Rekap All Issue By Level',
            }
        }
    });
    
</script>

@endsection
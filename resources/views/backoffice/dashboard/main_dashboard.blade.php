@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="section-title">Main Dashboard</h1>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <div class="row">
                <figure class="highcharts-figure">
                    <div id="container"></div>
                    <p class="highcharts-description">
                        Grafik untuk menampilkan progess rencana dan progress realisasi project yang sedang berjalan berdasarkan bulan.
                    </p>
                </figure>

                <figure class="highcharts-figure">
                  <div id="container2"></div>
                  <p class="highcharts-description">
                    Grafik untuk menampilkan budget, realisasi dan proyeksi untuk total budget project yang sedang berjalan berdasarkan bulan.

                  </p>
                </figure>

              <figure class="highcharts-figure" style="width:100% !important;height:100% !important;">
                  

                  <br>
                  <div id="container3"></div>
                  <p class="highcharts-description" align="center">
                    Grafik untuk menampilkan issue open berdasarkan project

                  </p>
              </figure>
      
              <figure class="highcharts-figure">
                  <div id="containers"></div>
                  <p class="highcharts-description">
                    Grafik untuk menampilkan budget, realisasi dan proyeksi untuk total budget project yang sedang berjalan berdasarkan bulan.

                  </p>
                </figure>
          
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
      $('#container').highcharts({
      chart: {
          type: 'column',
          options3d: {
              enabled: true,
              alpha: 15,
              beta: 15,
              viewDistance: 25,
              depth: 40
          }
      },
    
      title: {
        text: 'Project Progress - <?php foreach ($project_date as $row) {
        $date = date_create($row->date);
        echo date_format($date, "F Y");
        }         ?>'
      },
    
      xAxis: {
          categories: [ <?php foreach ($project as $row) {
        echo '"'.$row->name.'",';
        }         ?> ],
          labels: {
              skew3d: true,
              style: {
                  fontSize: '16px'
              }
          }
      },
    
      yAxis: {
          allowDecimals: false,
          min: 0,
          title: {
              text: 'Number of fruits',
              skew3d: true
          }
      },
    
      tooltip: {
          headerFormat: '<b>{point.key}</b><br>',
          pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}'
      },
    
      plotOptions: {
          column: {
              stacking: 'normal',
              depth: 40
          },
          
      },
    
      series: [
      {
          name: 'Progress Rencana',
          color: '#eccc68',
          data: [<?php foreach ($project as $value) {
        echo $value->progress_rencana.",";
        } 
        ?>],
          stack: 'male'
      },{
          name: 'Progress Realisasi',
          color: '#2ed573',
          data: [<?php foreach ($project_progress1 as $value) {

            $total = ($value->total_jumlah)/($value->total_aktivitas)*100;
       echo number_format((float) $total, 2, '.', '').','; 
        } 
        ?>],
          stack: 'female'
      }
      ]
        });
      });
    
</script>

<script type="text/javascript">
//project budget

$(function(){
  $('#container2').highcharts({
     title: {
        text: 'Project Budget - <?php foreach ($project_date as $row) {
        $date = date_create($row->date);
        echo date_format($date, "F Y");
        }         ?>'
    },
    xAxis: {
        categories: [<?php foreach ($project as $row) {
        echo '"'.$row->name.'",';
        }         ?> ]
    },
     yAxis: {
        min: 0,
        title: {
            text: 'Budget (juta)',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' juta'
    },
     plotOptions: {
        series: {
            dataLabels: {
                enabled: true
            }
        }
    },
    labels: {
      
    },
    series: [{

        type: 'column',
        name: 'Budget',
        color: '#f5c181',
        data: [<?php foreach ($project_budget as $value) {
        echo $value->rencana.",";
        } 
        ?>]
    }, {
        type: 'column',
        name: 'Realisasi',
        color: '#3dc4d0',
        data: [<?php foreach ($project_budget as $value) {
        echo $value->realisasi.",";
        } 
        ?>]
    }, {
        type: 'column',
        name: 'Proyeksi',
        color: '#b2d430',
        data: [<?php 
        foreach ($project_budget as $value) {
        echo $value->proyeksi.",";
        } 
        ?>]
    }, 
     {
        type: 'pie',
        name: 'Total Projects Budget',
        data: [{
            name: 'Realisasi',
            y: <?php foreach ($total_realisasi as $row){
              echo $row->total;
            } ?>,
            color: '#3dc4d0' // Jane's color
        }, {
            name: 'Proyeksi',
            y: <?php foreach($total_proyeksi as $row) {
              echo $row->total;
            } ?>,
            color: '#b2d430' // John's color
        }, {
            name: 'Budget',
            y: <?php foreach($total_budget as $row) {
              echo $row->total;
            }?>,
            color: '#f5c181' // Joe's color
        }],
        center: [100, 80],
        size: 0,
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }]
  });
})

</script>

<script>
    $(function() {
      $('#container3').highcharts({
      chart: {
          type: 'column',
          options3d: {
              enabled: true,
              alpha: 16,
              beta: 8,
              viewDistance: 25,
              depth: 47
          },

      },
    
      title: {
        text: 'Open issue Project'
      },
    
      xAxis: {
          categories: [<?php foreach($issue as $row){
            echo "'".$row->name."',";
          }?>],
          labels: {
              skew3d: true,
              style: {
                  fontSize: '16px'
              }
          }
      },
    
      yAxis: {
          allowDecimals: false,
          min: 0,
          title: {
              text: 'number of total issue',
              skew3d: true
          }
      },
    
      tooltip: {
          headerFormat: '<b>{point.key}</b><br>',
          pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}'
      },
    
      plotOptions: {
          column: {
              stacking: 'normal',
              depth: 40
          },
          
      },
    
      series: [
      {
          name: 'Total Issue',
          color: '#22eaaa',
          data: [<?php foreach($issue as $row){
            echo $row->total_issues.',';
          } 
            ?>],
          stack: 'male'
      },{
          name: 'Total Open Issue',
          color: '#ee5a5a',
          data: [<?php foreach($issue as $row){
            echo $row->open_issue.',';
          } 
            ?>],
          stack: 'female'
      }
      ]
        });
      });




    
</script>


<script>
/*
      // Dashboard budgets
      $(function() {
      $('#containe').highcharts({
 chart: {
        type: 'bar'
    },
    title: {
        text: 'Project Budget - <?php foreach ($project_date as $row) {
        $date = date_create($row->date);
        echo date_format($date, "F Y");
        }         ?>'
    },
    xAxis: {
        categories: [<?php foreach ($project as $row) {
        echo '"'.$row->name.'",';
        }         ?>],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Budget (juta)',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' juta'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Budget',
        data: [<?php foreach ($project_budget as $value) {
        echo $value->rencana.",";
        } 
        ?>]
    }, {
        name: 'Realisasi',
        data: [<?php foreach ($project_budget as $value) {
        echo $value->realisasi.",";
        } 
        ?>]
    }, {
        name: 'Proyeksi',
        data: [<?php foreach ($project_budget as $value) {
        echo $value->proyeksi.",";
        } 
        ?>]
    }]
});
});
*/
</script>

@endsection
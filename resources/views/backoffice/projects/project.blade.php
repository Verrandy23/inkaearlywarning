@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">Master Data Project</h1>
    <div style="margin-top:2%" class="section-header-breadcrumb">
        <div class="bottom">
                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                     + Add Project
                    </button>
        </div>
    </div>
</div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="false" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Project</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                     <form class="form-class" id="form" method="post" action="{{ route('projects.store') }}">
                @csrf
                <div class="form-group">
                    <label>Name</label>
                    <input required type="text" name="name" data-form="input nama project" placeholder="Input project name" class="form-control">
                    <span class="invalid-feedback" name="name_validation"></span>
                </div>

                <div class="form-group">
                    <label>Description</label>
                    <textarea name="description" class="form-control" data-form="input description" cols="50" placeholder="Input project Description"></textarea>
                </div>

                <div class="form-group">
                    <label>Start Date</label>
                    <input required type="date" data-form="input tanggal awal" name="start_date" id='start' class="form-control">
                </div>

                <div class="form-group">
                    <label>End Date</label>
                    <input required type="date" data-form="input tanggal selesai" name="end_date" id='end' class="form-control">
                </div>

                <div class="form-group">
                    <label>Client</label>
                    <input required type="text" data-form="input client" name="client" class="form-control">
                </div>

            </form>
                </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="save" class="btn btn-primary">Save changes</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>


<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <!-- Form Modal Project -->
            
            <!-- End Form Modal TypeTrain -->

            <div class="col-md-8">
                <p></p>
            </div>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="table1">
                    @include('sweet::alert')

                    @if ($errors->any())
                    <div class="col-md-10">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                    <thead>
                        <tr>
                            <th width="10%">No.</th>
                            <th width="10%">Name</th>
                            <th width="20%">Description</th>
                            <th width="10%">Start Date</th>
                            <th width="10%">End Date</th>
                            <th width="10%">Client</th>
                            <th width="40%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($project as $index => $row)
                        <tr>
                            <td>{{$index+1}}.</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->description}}</td>
                            <td>
                                <?php
                                $date = date_create($row->start_date);
                                echo date_format($date, "j F Y");
                                ?>
                            </td>
                            <td>
                                <?php
                                $date = date_create($row->end_date);
                                echo date_format($date, "j F Y");
                                ?>
                            </td>
                            <td>{{$row->client}}</td>
                            <td>
                                <form action="{{ route('projects.destroy', $row->id)}}" method="post" id="delete{{$index}}">
                                    @csrf
                                    @method('DELETE')

                                    <a>
                                        <button name="button" class="btn btn-icon btn-danger" type="button">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </a>

                                    <a href="{{ route('projects.edit', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </a>

                                    <a href="{{ route('projects.show', $row->id)}}">
                                        <button class="btn btn-icon btn-primary" type="button">
                                            <i class="fa fa-arrow-right"></i> Detail Project
                                        </button>
                                    </a>
                                </form>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- for start and end date validation -->
<script type="text/javascript">
$(document).ready(function(){
    /*
    var $start = ''
    var $end = ''
    $('#start').on('change', function(){
        $start = new Date(String($('#start').val()));
    });
    
    $('#end').on('change', function() {
        $end = new Date(String($('#end').val()));
        if ($start>$end){
        Swal.fire({
            icon: 'info',
            title: 'Invalid Waktu',
            text: 'Check start dan end datenya'
        });
        $('#end').val('yy-mm-dd');
        }
    });
   */
    var $start_date = "";
    var $end_date = "";
    
    document.getElementsByName('start_date')[0].addEventListener('change',function(){
       $start_date = new Date(String(this.value));
    });  
    
    document.getElementsByName('end_date')[0].addEventListener('change', function(){
        $end_date = new Date(String(this.value));
        
        if ($start_date > $end_date){
            swal({
                title: "Info !",
                text: "Please check start date & end date",
                icon: "info",
                button: "yes, i'am Understand",
            });
            
             document.getElementsByName('end_date')[0].value = "yyyy-mm-dd";
        }
       
        
    })
    
    // validation form
    document.getElementById('save').addEventListener('click', event=>{
    var $input = document.querySelectorAll('#form input');
    var $select = document.querySelectorAll('#form select');


    var $errors = []
    var error = false;
    // chek input empty
    for(let i=0; i<$input.length; i++){

        // check input if emtpy
        if($input[i].value == ""){
            var $input_error = $input[i].dataset.form;
            if ($input_error !== undefined){
                $errors.push($input_error);
                error = true;
            }
        }


    }

    //check select empty
    for(let i=0; i<$select.length; i++){
        const $error_name = $select[i].dataset.form;

        if($select[i].value == ""){
            if($error_name !== undefined){
                $errors.push($error_name);
                error =true;
            }
        }
    }

    var text_error = "";

    for(let i=0; i<$errors.length; i++){
        text_error+= "<p class='text-danger'> form " + $errors[i] + " tidak boleh kosong</p>";
    }

    var div_error = document.createElement("div");
    div_error.innerHTML = text_error;
    if (error){
        swal({
            title: 'Error !',
            content: div_error,
            icon: 'error',
        });
    }
    else{
        document.querySelector('#form').submit();
    }
});



// hapus
 var btn = document.getElementsByName('button');
   

    for (let i=0; i<btn.length; i++){
        

        document.getElementsByName('button')[i].addEventListener('click', function(){
            var id = i;
            
            swal({
                title: "are you sure for deleting this data ?",
                icon: "warning",
                buttons: true,
                dangerMode: true 
            })
            .then((willDelete)=>{
                if(willDelete){
                    document.getElementById('delete' + id).submit();  
                }
            });
        });
    }    

    })


   
</script>
@endsection
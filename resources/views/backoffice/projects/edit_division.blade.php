@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="title">{{$division->name}}</h1>
    <div class="section-header-breadcrumb">
        <a class="btn btn-primary btn-sm" href="{{ URL::previous() }}"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-success">
            {{ session('error') }}
        </div>
        @endif
        <h4 class="titleC text-center">Form Edit Division</h4><br>
        <div class="basic-form">
            <form class="form-horizontal" method="post" action="{{ route('divisions.update',$division->id) }}">
                @method('PATCH')
                @csrf
                @if ($errors->any())
                <div class="col-md-10">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Name <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <select class="form-control" name="name" required>
                            <option value="Teknologi" {{$division->name == 'Teknologi' ? 'selected' : ''}}>Teknologi</option>
                            <option value="Logistik" {{$division->name == 'Logistik' ? 'selected' : ''}}>Logistik</option>
                            <option value="Fabrikasi" {{$division->name == 'Fabrikasi' ? 'selected' : ''}}>Fabrikasi</option>
                            <option value="Finishing" {{$division->name == 'Finishing' ? 'selected' : ''}}>Finishing</option>
                            <option value="Testing" {{$division->name == 'Testing' ? 'selected' : ''}}>Testing</option>
                            <option value="Delivery" {{$division->name == 'Delivery' ? 'selected' : ''}}>Delivery</option>
                        </select>
                    </div><br>
                    <input type="hidden" name="project_id" value="{{$project->get_project->id}}">

                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Start Date <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" name="start_date" value="{{$division->start_date}}">
                    </div><br>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">End Date <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" name="end_date" value="{{$division->end_date}}">
                    </div><br>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <p></p>
                    </div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn mb-1 btn-primary">Update</span>
                        </button>
                    </div>
                    <input type="hidden" value="{{ URL::previous() }}" name="url">
                </div>

            </form>
        </div>
    </div>
</div>
<!-- #/ container -->
@endsection
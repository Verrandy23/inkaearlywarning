@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">{{$project->name}}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
            <a href="/projects"><strong> Home</strong></a>
        </div><br><br>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        @include('sweet::alert')

        @if (session('error'))
        <div class="alert alert-success">
            {{ session('error') }}
        </div>
        @endif
        <div class="col-lg-12">
            <h6 class="titleC text-center">Data divisions</h6>
            <!-- modal division -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="false" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Division</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                      <form class="class" id="form" method="post" action="{{ route('divisions.store') }}">
                @csrf
                <div class="form-group">
                    <label>Name</label>
                    <select class="form-control" data-form="input nama division" name="name" required>
                        <option value="" selected>Pilih Divisi</option>
                        <option value="Teknologi">Teknologi</option>
                        <option value="Logistik">Logistik</option>
                        <option value="Fabrikasi">Fabrikasi</option>
                        <option value="Finishing">Finishing</option>
                        <option value="Testing">Testing</option>
                        <option value="Delivery">Delivery</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Start Date</label>
                    <input type="date" data-form="input tanggal mulai" class="form-control" name="start_date">
                </div>
                <div class="form-group">
                    <label>End Date</label>
                    <input type="date" data-form="input tanggal selesai" class="form-control" name="end_date">
                </div>
                <input type="hidden" class="form-control" name="project_id" value="{{$project->id}}">
            </form>
                </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="save" class="btn btn-primary">Save changes</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
           
            <!-- End Form Modal Division -->

            <div class="col-md-8">
                <p></p>
            </div>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="table1">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>


                        <div class="bottom text-right">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            + Add Division
                                </button>
                        </div>
                        @foreach($project->get_divisions as $index => $row)
                        <tr>
                            <td>{{$index+1}}.</td>
                            <td>{{$row->name}}</td>
                            <td>
                                <?php
                                $date = date_create($row->start_date);
                                echo date_format($date, "j F Y");
                                ?>
                            </td>
                            <td>
                                <?php
                                $date = date_create($row->end_date);
                                echo date_format($date, "j F Y");
                                ?>
                            </td>
                            <td>
                                <form id="delete{{$index}}" action="{{ route('divisions.destroy', $row->id)}}" method="post" id="confirm_delete">
                                    @csrf
                                    @method('DELETE')

                                    <a>
                                        <button name="button_division" class="btn btn-icon btn-danger" type="button">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </a>

                                    <a href="{{ route('divisions.edit', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </a>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="card">
    <div class="card-body">
        <div class="col-lg-12">
            <h6 class="titleC text-center">Data Detail Project</h6>
                
                <!-- Form Modal Project -->
                <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="false" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Data Detail Project</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>

              <div class="modal-body">      
            <form class="" id="form_detail_project" method="post" action="{{ route('detail_projects.store') }}">
                @csrf
                <div class="form-group">
                    <label>Term Of Payment</label>
                    <textarea class="form-control" data-form="input term payment" name="term_payment"></textarea>
                </div>
                <div class="form-group">
                    <label>Metode Pembayaran</label>
                    <input type="text" class="form-control" data-form="input metode pembayaran" name="payment">
                </div>
                <div class="form-group">
                    <label>Denda</label>
                    <input type="number" min="0" class="form-control" data-form="input denda" name="denda">
                </div>
                <div class="form-group">
                    <label>Batch</label>
                    <input type="text" class="form-control" data-form="input batch" name="batch">
                </div>
                <div class="form-group">
                    <label>Start Date Delivery</label>
                    <input type="date" class="form-control" data-form="input tanggal mulai" name="start_delivery_date">
                </div>
                <div class="form-group">
                    <label>End Date Delivery</label>
                    <input type="date" class="form-control" data-form="input tanggal selesai" name="end_delivery_date">
                </div>
                <div class="form-group">
                    <label>Delivery Address</label>
                    <textarea class="form-control" data-form="input delivery address" name="address_delivery" rows="5"></textarea>
                </div>
               
                <input type="hidden" class="form-control" name="project_id" value="{{$project->id}}">

                </form>
            </div>  
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="save_detail_project" class="btn btn-primary">Save changes</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>


            <!-- End Form Modal Detail Project -->

            <div class="col-md-8">
                <p></p>
            </div>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="table3">
                    <thead>
                        <tr>
                            <th width="20%">Term Of Payment</th>
                            <th width="5%">Payment</th>
                            <th width="10%">Denda</th>
                            <th width="5%">Batch</th>
                            <th width="15%">Start Date Delivery</th>
                            <th width="15%">End Date Delivery</th>
                            <th width="15%">Address Delivery</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <div class="bottom text-right">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1">
                            + Add Detail Project
                                </button>
                        </div>
                        @foreach($project->get_details as $index => $row)
                        <tr>
                            <td>{{$row->term_payment}}</td>
                            <td>{{$row->payment}}</td>
                            <td>{{$row->denda}}</td>
                            <td>{{$row->batch}}</td>
                            <td>
                                <?php
                                $date = date_create($row->start_delivery_date);
                                echo date_format($date, "j F Y");
                                ?>
                            </td>
                            <td>
                                <?php
                                $date = date_create($row->end_delivery_date);
                                echo date_format($date, "j F Y");
                                ?>
                            </td>
                            <td>{{$row->address_delivery}}</td>
                            <td>
                                <form id="delete_detail{{$index}}" action="{{ route('detail_projects.destroy', $row->id)}}" method="post" id="confirm_delete">
                                    @csrf
                                    @method('DELETE')

                                    <a>
                                        <button name="button_detail_project" class="btn btn-icon btn-danger" type="button">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </a>

                                    <a href="{{ route('detail_projects.edit', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </a>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var $start_date = "";
    var $end_date = "";
    
    document.getElementsByName('start_date')[0].addEventListener('change',function(){
       $start_date = new Date(String(this.value));
    });  
    
    document.getElementsByName('end_date')[0].addEventListener('change', function(){
        $end_date = new Date(String(this.value));
        
        if ($start_date > $end_date){
            swal({
                title: "Info !",
                text: "Please check start date & end date",
                icon: "info",
                button: "yes, i'am Understand",
            });
            
             document.getElementsByName('end_date')[0].value = "yyyy-mm-dd";
        }
       
        
    })
    
    // validation form
    document.getElementById('save').addEventListener('click', event=>{
    var $input = document.querySelectorAll('#form input');
    var $select = document.querySelectorAll('#form select');


    var $errors = []
    var error = false;
    // chek input empty
    for(let i=0; i<$input.length; i++){

        // check input if emtpy
        if($input[i].value == ""){
            var $input_error = $input[i].dataset.form;
            if ($input_error !== undefined){
                $errors.push($input_error);
                error = true;
            }
        }


    }

    //check select empty
    for(let i=0; i<$select.length; i++){
        const $error_name = $select[i].dataset.form;

        if($select[i].value == ""){
            if($error_name !== undefined){
                $errors.push($error_name);
                error =true;
            }
        }
    }

    var text_error = "";

    for(let i=0; i<$errors.length; i++){
        text_error+= "<p class='text-danger'> form " + $errors[i] + " tidak boleh kosong</p>";
    }

    var div_error = document.createElement("div");
    div_error.innerHTML = text_error;
    if (error){
        swal({
            title: 'Error !',
            content: div_error,
            icon: 'error',
        });
    }
    else{
        document.querySelector('#form').submit();
    }
});



// hapus division
 var button_detail_project = document.getElementsByName('button_detail_project');
   

    for (let i=0; i<button_detail_project.length; i++){
        

        document.getElementsByName('button_detail_project')[i].addEventListener('click', function(){
            var id = i;
            
            swal({
                title: "are you sure for deleting this data ?",
                icon: "warning",
                buttons: true,
                dangerMode: true 
            })
            .then((willDelete)=>{
                if(willDelete){
                    document.getElementById('delete_detail' + id).submit();  
                }
            });
        });
    }    

// hapus detail project

var button_division = document.getElementsByName('button_division');
   

    for (let i=0; i<button_division.length; i++){
        

        document.getElementsByName('button_division')[i].addEventListener('click', function(){
            var id = i;
            
            swal({
                title: "are you sure for deleting this data ?",
                icon: "warning",
                buttons: true,
                dangerMode: true 
            })
            .then((willDelete)=>{
                if(willDelete){
                    document.getElementById('delete' + id).submit();  
                }
            });
        });
    }    


  // form input detail project
   // validation form
    document.getElementById('save_detail_project').addEventListener('click', event=>{
    var $input1 = document.querySelectorAll('#form_detail_project input');
    var $select1 = document.querySelectorAll('#form_detail_project select');

    var $textarea1 = document.querySelectorAll('#form_detail_project textarea');

    var $errors = []
    var error = false;
    // chek input empty
    for(let i=0; i<$input1.length; i++){

        // check input if emtpy
        if($input1[i].value == ""){
            var $input_error = $input1[i].dataset.form;
            if ($input_error !== undefined){
                $errors.push($input_error);
                error = true;
            }
        }

        //vali dation start date
        var $start_date = "";
        var $end_date = "";
      

    }

    //check select empty
    for(let i=0; i<$select1.length; i++){
        const $error_name = $select1[i].dataset.form;

        if($select1[i].value == ""){
            if($error_name !== undefined){
                $errors.push($error_name);
                error =true;
            }
        }
    }


 for(let i=0; i<$textarea1.length; i++){
        const $error_name = $textarea1[i].dataset.form;

        if($textarea1[i].value == ""){
            if($error_name !== undefined){
                $errors.push($error_name);
                error=true;
            }
        }
    }



    var text_error = "";

    for(let i=0; i<$errors.length; i++){
        text_error+= "<p class='text-danger'> form " + $errors[i] + " tidak boleh kosong</p>";
    }

    var div_error = document.createElement("div");
    div_error.innerHTML = text_error;
    if (error){
        swal({
            title: 'Error !',
            content: div_error,
            icon: 'error',
        });
    }
    else{
        document.querySelector('#form_detail_project').submit();
    }
});


// validation delivery date

var $start_delivery_date = "";
var $end_delivery_date = "";

document.getElementsByName('start_delivery_date')[0].addEventListener('change', function(){
    $start_delivery_date = new Date(String(this.value));
});

document.getElementsByName('end_delivery_date')[0].addEventListener('change', function(){
    $end_delivery_date = new Date(String(this.value));
    
    if($start_delivery_date > $end_delivery_date){
        swal({
            title: 'Info !',
            text: 'Invalid start date & end date',
            icon: 'warning'
        });
        this.value = "";
    }
})

</script>

@endsection
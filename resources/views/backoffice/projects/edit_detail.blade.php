@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="title">Edit Data Detail Project</h1>
    <div class="section-header-breadcrumb">
        <a class="btn btn-primary btn-sm" href="{{ URL::previous() }}"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <h4 class="titleC text-center">Form Edit Detail Project</h4><br>
        <div class="basic-form">
            <form class="form-horizontal" method="post" action="{{ route('detail_projects.update',$detail_project->id) }}">
                @method('PATCH')
                @csrf
                @if ($errors->any())
                <div class="col-md-10">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label"> Batch <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="batch" value="{{$detail_project->batch}}">
                    </div><br>

                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Start Date Delivery<span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" name="start_delivery_date" value="{{$detail_project->start_delivery_date}}">
                    </div><br>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">End Date Delivery<span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" name="end_delivery_date" value="{{$detail_project->end_delivery_date}}">
                    </div><br>
                </div>
                <input type="hidden" name="project_id" value="{{$detail_project->get_project->id}}">

                <div class="form-group row">
                    <div class="col-sm-3">
                        <p></p>
                    </div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn mb-1 btn-primary">Update</span>
                        </button>
                    </div>
                    <input type="hidden" value="{{ URL::previous() }}" name="url">
                </div>

            </form>
        </div>
    </div>
</div>
<!-- #/ container -->
@endsection
@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="title">{{$project->name}}</h1>
    <div class="section-header-breadcrumb">
        <a class="btn btn-primary btn-sm" href="{{ URL::previous() }}"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <h4 class="titleC text-center">Form Edit Project</h4><br>
        <div class="basic-form">
            <form class="form-horizontal" method="post" action="{{ route('projects.update',$project->id) }}">
                @method('PATCH')
                @csrf
                @if ($errors->any())
                <div class="col-md-10">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Name <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" placeholder="Input project name" name="name" value="{{$project->name}}">
                    </div><br>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Description
                    </label>
                    <div class="col-sm-9">
                        <textarea name="description" placeholder="Input project description" class="form-control">{{$project->description}}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Start Date <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" name="start_date" value="{{$project->start_date}}">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">End Date <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" name="end_date" value="{{$project->end_date}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Client <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" placeholder="Input Client name" name="client" value="{{$project->client}}">
                    </div><br>
                </div>
      
                <div class="form-group row">
                    <div class="col-sm-3">
                        <p></p>
                    </div>
                    <div class="col-sm-9">
                        <button type="button" name="submit" class="btn mb-1 btn-primary">Update</span>
                        </button>
                    </div>

                </div>

            </form>
        </div>
    </div>
</div>
<!-- Card Edit Project -->

<script type="text/javascript">
    // validation start end date
    var $start = "";
    var $end = "";
    
    document.getElementsByName("start_date")[0].addEventListener('change', function(){
        $start = new Date(String(this.value));
    })
    
    document.getElementsByName("end_date")[0].addEventListener('change', function(){
        
        $end = new Date(String(this.value));
        if($start > $end){
            swal({
               title: "Info !",
               text: "Please check start data & end date",
               icon: "info",
               button: "Yes, i'am Understand",
            });
            document.getElementsByName("end_date").value = "yyyy-mm-dd";
        }
    })
</script>
@endsection
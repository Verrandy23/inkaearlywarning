@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header text-center">
    <h1 class="titleC">Form Issue</h1>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <div class="card-body">
                @if ($errors->any())
                <div class="col-md-10">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
                <form id="form" method="POST" enctype="multipart/form-data" action="{{ route('issues.store') }}">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Nama Issue <span class="text-danger">*</span></label>
                            <input type="text" name="name" data-form="input nama issue" class="form-control" placeholder="Masukkan nama issue" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputState">Project <span class="text-danger">*</span></label>
                            <select class="form-control" data-form="input project" name="project_id" id="project"required>
                                <option value="">Pilih Project</option>
                                @foreach($project as $row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputState">Division <span class="text-danger">*</span></label>
                            <select name="division_hidden" data-form="input divisi" class="form-control" id="division" required>
                                <option value="">Pilih Divisi</option>
                            </select>

                            <input hidden type="text" value="Teknologi"  name="division_name" id="division_name">
                        </div>
                        <div class="col-md-6">
                            <label for="inputState">Master Activity <span class="text-danger">*</span></label>
                                <select class="form-control" data-form="input activity" name="master_activity" id="master_activity" required>
                                    <option value="">Pilih Master Activity</option>
                                  
                                </select>
                        </div>
                                   
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputAddress">Permasalahan</label>
                            <textarea style="min-height:  100px" data-form="permasalahan" class="form-control" placeholder="Tindak Lanjut" name="issue"></textarea>
                        </div>
                    </div>  

                        <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Date Issue <span class="text-danger">*</span></label>
                            <input type="date" name="date_issue" data-form="input tanggal issue" class="form-control" required>
                        
                        </div>
              

                        <div class="form-group col-md-6">
                            <label for="inputState">Level <span class="text-danger">*</span></label>
                            <select class="form-control" name="level"   data-form="input level"required>
                                <option value="">Pilih Level</option>
                                <option value="major">Major</option>
                                <option value="moderate">Moderate</option>
                                <option value="minor">Minor</option>
                            </select>
                        </div>
                        </div>
                        <div class="form-row">

                            <div class="form-group col-md-6">
                                <label for="inputState">Status <span class="text-danger">*</span></label>
                                <select class="form-control" data-form="input status" name="status" required>
                                    <option value="">Pilih Status</option>
                                    <option value="open">Open</option>
                                    <option value="closed">Closed</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputState">Files</label>
                                <input type="file"  name="file_data" class="form-control">

                            </div>
                            <div class="form-group col-md-6">
                                <input type="hidden" readonly class="form-control" name="created_by" value="{{Auth::user()->id}}">
                            </div>
                        </div>
                        <div class="form-group mb-0">
                            <input type="button" id="save" class="btn btn-primary" value="Simpan"></input>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>

    var x = {}
    $(document).ready(function () {
        $('#project').on('change', function() {
            $.ajax({
                type: 'GET',
                url: '/'+this.value+'/divisions',
                success: function(data){
                    $.each(data.divisions, function(key, value){
                        $('#division').append($('<option></option>').attr('value', value.id).text(value.name));
                        x[value.id] = value.name;
                    })

                    $.ajax({
                        type: 'GET',
                        url: '/'+$('#division').val()+'/master_activity',
                        success: function(data){
                            $.each(data.master_activity, function(key, value){
                                $('#master_activity').append($('<option></option>').attr('value', value.id).text(value.name).attr('alt', value.id));
                            })
                        }

                    })
                    $('#master_activity').find('option').remove();
                }
            });
            $('#division').find('option').remove();
            x={};
            
        });

        $('#division').on('change', function(){
            $('#division_name').val(x[this.value]);
            console.log($('#division_name').val());
            $.ajax({
                type: 'GET',
                url: '/'+this.value+'/master_activity',
                success: function(data){
                    $.each(data.master_activity, function(key, value){
                        $('#master_activity').append($('<option></option>').attr('value', value.id).text(value.name));
                    })
                }
            })
            $('#master_activity').find('option').remove();
        });
        
    })
</script>
<script>
    $(document).ready(function() {
        $('#master_activity').on('change', function() {
            var id=0;
             $.ajax({
                type:"GET",
                url:"/"+ this.value +"/sub_activity",
                success: function(data){
                $.each(data.sub_activity, function (key, value){
                    //html += "<option value="+value.id+">"+value.name+"</option>";
                    $("#sub_activity").append($('<option></option>').attr("value", value.id). text(value.name));
                    id=value.id;
                }) 

                $.ajax({
                type:"GET",
                url:"/"+ $('#sub_activity').val() +"/work_activity",
                success: function(data){
                    var html = ""
                    console.log(data);
                    $.each(data.work_activity, function (key, value){
                        console.log(value.name);
                        //html += "<option value="+value.id+">"+value.name+"</option>";
                        
                        $("#work_activity").append($('<option></option>').attr("value", value.id). text(value.name));
                    }) 
                }
             });  
             $('#work_activity').find("option").remove();

                
            
            }
             });  
             $('#sub_activity').find("option").remove(); 

      

        })
     });   
    
</script>

<script>
    $(document).ready(function () {
        $('#sub_activity').on('change', function(){
            $.ajax({
                type:"GET",
                url:"create_issue/"+ this.value +"/work_activity",
                success: function(data){
                    var html = ""
                    console.log(data);
                    $.each(data.work_activity, function (key, value){
                        console.log(value.name);
                        //html += "<option value="+value.id+">"+value.name+"</option>";
                        
                        $("#work_activity").append($('<option></option>').attr("value", value.id). text(value.name));
                    }) 
                }
             });  
             $('#work_activity').find("option").remove(); 
        });

    })
</script>



@endsection
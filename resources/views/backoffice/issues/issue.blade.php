@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="text-center">Issue</h1>
    <div style="margin-right:3%" class="section-header-breadcrumb">
        <div class="bottom text-right">
            <a href="/create_issue"><button class="btn btn-sm mb-1 btn-primary"><i class="fa fa-plus"></i> Add Issue</button></a>
        </div>
    </div>
</div>
<div class="card top">
<div class="card-body">
    <h1 class="titleC text-center"> Data Project Issue </h1>
    <br>
    <div class="col-lg-12">
        @include('sweet::alert')
        <!-- Issue By Division -->
        @if (Auth::user()->division_name == 'Teknologi')
        <div class="col-12">
            <div class="row">
                <div class="col-md-6 col-12">
                    <!-- Filter Data -->
                     <table>
                                <form method="get" action="/report_issue_teknologi">

                                    <input type="text" name="project" id="project" hidden="">
                                    <input type="text" name="status" hidden="" id="status">
                                    <input id="level" type="text" name="level" hidden="">
                                <tr>
                                    <label style="margin-left:1%" for="filter">Filter By Projec</label>
                                    <label style="margin-left:2%" for="filter">Level</label>
                                    <label style="margin-left:17%" for="filter">Status</label>
                                    <label style="margin-left:15%" for="filter">Date</label>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th class="filterhead none"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                </tr>
                            </table>
                            <!-- End Filter Data --> <br>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="section-header-breadcrumb">
                                <div class="bottom text-right">
                                    <button class="btn btn-primary" target="_blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            <br>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="exampledr">
                    <thead>
                        <tr>
                            <th width="12%">Project</th>
                            <th width="4%">Level</th>
                            <th class="none">Filter for level</th>
                            <th width="4%">Status</th>
                            <th class="none">Filter for status</th>
                            <th width="10%">Master Activity</th>
                            <th width="15%">Isu</th>
                            <th width="15%">Permasalahan</th>
                            <th width="5%">Tanggal Isu</th>
                            <th class="none">Date Filter Open</th>
                            <th width="20%">Detail Issue</th>
                            <th width="18%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($teknologi as $row)
                        <tr>
                            <td>{{$row->get_project->name}}</td>
                            <td>
                                @if ($row->level == 'major')
                                <strong class="badge badge-danger">Major</strong>
                                @elseif($row->level == 'moderate')
                                <strong class="badge badge-primary">Moderate</strong>
                                @else 
                                <strong class="badge badge-warning">Minor</strong>
                                @endif
                            </td>
                            <td class="none">
                                @if ($row->level == 'major')
                                Major
                                @elseif($row->level == 'moderate')
                                Moderate
                                @else
                                Minor
                                @endif
                            </td>
                            <td>
                                @if ($row->status == 'open')
                                <strong class="badge badge-warning">Open</strong>
                                @else
                                <strong class="badge badge-success">Closed</strong>
                                @endif
                            </td>
                            <td class="none">
                                @if ($row->status == 'open')
                                Open
                                @else
                                Closed
                                @endif
                            </td>
                            <td>{{$row->master_activity}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->issue}}</td>
                            <td>
                                <?php
                                    $date = date_create($row->date_issue);
                                    echo date_format($date, "j F Y");
                                    ?>
                            </td>
                            <td class="none">
                                <?php
                                    date_create($row->date_issue);
                                    $string =  date_format($date, "j F Y");
                                    if (stristr($string, 'January') !== FALSE) {
                                        echo 'January';
                                    } else if (stristr($string, 'February') !== FALSE) {
                                        echo 'February';
                                    } else if (stristr($string, 'March') !== FALSE) {
                                        echo 'March';
                                    } else if (stristr($string, 'April') !== FALSE) {
                                        echo 'April';
                                    } else if (stristr($string, 'May') !== FALSE) {
                                        echo 'May';
                                    } else if (stristr($string, 'June') !== FALSE) {
                                        echo 'June';
                                    } else if (stristr($string, 'July') !== FALSE) {
                                        echo 'July';
                                    } else if (stristr($string, 'August') !== FALSE) {
                                        echo 'August';
                                    } else if (stristr($string, 'September') !== FALSE) {
                                        echo 'September';
                                    } else if (stristr($string, 'October') !== FALSE) {
                                        echo 'October';
                                    } else if (stristr($string, 'November') !== FALSE) {
                                        echo 'November';
                                    } else if (stristr($string, 'December') !== FALSE) {
                                        echo 'December';
                                    }
                                    ?>
                            </td>
                            <td>
                                <a href="{{ route('issues.show', $row->id)}}">
                                <button class="btn btn-icon btn-outline-info btn-sm" type="button">
                                <i class="fas fa-arrow-right"></i> Read More
                                </button>
                                </a>
                            </td>
                            <td>
                                @if($row->status == 'closed')
                                <a href="#">
                                <button class="btn btn-outline-secondary btn-sm" type="button">
                                Issue Closed
                                </button>
                                </a>
                                @elseif(Auth::user()->id == $row->created_by)
                                <form action="{{ route('issues.destroy', $row->id)}}" method="post" id="confirm_delete">
                                    @csrf
                                    @method('DELETE')
                                    <a onclick="return confirm('Apakah anda yakin menghapus data?')">
                                    <button class="btn btn-icon btn-danger" type="submit">
                                    <i class="fas fa-trash"></i>
                                    </button>
                                    </a>
                                    <a href="{{ route('issues.edit', $row->id)}}">
                                    <button class="btn btn-icon btn-info" type="button">
                                    <i class="fas fa-edit"></i>
                                    </button>
                                    </a>
                                </form>
                                @else 
                                <a href="#">
                                <a href="{{ route('issues.edit', $row->id)}}">
                                <button class="btn btn-icon btn-info" type="button">
                                <i class="fas fa-edit"></i>
                                </button>
                                </a>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @if (Auth::user()->division_name == 'Logistik')
        <div class="col-12">
            <div class="row">
                <div class="col-md-6 col-12">
                    <!-- Filter Data -->
                   <table>
                                <form method="get" action="/report_issue_logistik">

                                    <input type="text" name="project" id="project" hidden="">
                                    <input type="text" name="status" hidden="" id="status">
                                    <input id="level" type="text" name="level" hidden="">
                                <tr>
                                    <label style="margin-left:1%" for="filter">Filter By Projec</label>
                                    <label style="margin-left:2%" for="filter">Level</label>
                                    <label style="margin-left:17%" for="filter">Status</label>
                                    <label style="margin-left:15%" for="filter">Date</label>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th class="filterhead none"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                </tr>
                            </table>
                            <!-- End Filter Data --> <br>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="section-header-breadcrumb">
                                <div class="bottom text-right">
                                    <button class="btn btn-primary" target="_blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            <br>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="exampledr">
                    <thead>
                        <tr>
                            <th width="12%">Project</th>
                            <th width="4%">Level</th>
                            <th class="none">Filter for level</th>
                            <th width="4%">Status</th>
                            <th class="none">Filter for status</th>
                            <th width="10%">Master Activity</th>
                            <th width="15%">Isu</th>
                            <th width="15%">Permasalahan</th>
                            <th width="5%">Tanggal Isu</th>
                            <th class="none">Date Filter Open</th>
                            <th width="20%">Detail Issue</th>
                            <th width="18%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($logistik as $row)
                        <tr>
                            <td>{{$row->get_project->name}}</td>
                            <td>
                                @if ($row->level == 'major')
                                <strong class="badge badge-danger">Major</strong>
                                @elseif($row->level == 'moderate')
                                <strong class="badge badge-primary">Moderate</strong>
                                @else 
                                <strong class="badge badge-warning">Minor</strong>
                                @endif
                            </td>
                            <td class="none">
                                @if ($row->level == 'major')
                                Major
                                @elseif($row->level == 'moderate')
                                Moderate
                                @else
                                Minor
                                @endif
                            </td>
                            <td>
                                @if ($row->status == 'open')
                                <strong class="badge badge-warning">Open</strong>
                                @else
                                <strong class="badge badge-success">Closed</strong>
                                @endif
                            </td>
                            <td class="none">
                                @if ($row->status == 'open')
                                Open
                                @else
                                Closed
                                @endif
                            </td>
                            <td>{{$row->master_activity}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->issue}}</td>
                            <td>
                                <?php
                                    $date = date_create($row->date_issue);
                                    echo date_format($date, "j F Y");
                                    ?>
                            </td>
                            <td class="none">
                                <?php
                                    date_create($row->date_issue);
                                    $string =  date_format($date, "j F Y");
                                    if (stristr($string, 'January') !== FALSE) {
                                        echo 'January';
                                    } else if (stristr($string, 'February') !== FALSE) {
                                        echo 'February';
                                    } else if (stristr($string, 'March') !== FALSE) {
                                        echo 'March';
                                    } else if (stristr($string, 'April') !== FALSE) {
                                        echo 'April';
                                    } else if (stristr($string, 'May') !== FALSE) {
                                        echo 'May';
                                    } else if (stristr($string, 'June') !== FALSE) {
                                        echo 'June';
                                    } else if (stristr($string, 'July') !== FALSE) {
                                        echo 'July';
                                    } else if (stristr($string, 'August') !== FALSE) {
                                        echo 'August';
                                    } else if (stristr($string, 'September') !== FALSE) {
                                        echo 'September';
                                    } else if (stristr($string, 'October') !== FALSE) {
                                        echo 'October';
                                    } else if (stristr($string, 'November') !== FALSE) {
                                        echo 'November';
                                    } else if (stristr($string, 'December') !== FALSE) {
                                        echo 'December';
                                    }
                                    ?>
                            </td>
                            <td>
                                <a href="{{ route('issues.show', $row->id)}}">
                                <button class="btn btn-icon btn-outline-info btn-sm" type="button">
                                <i class="fas fa-arrow-right"></i> Read More
                                </button>
                                </a>
                            </td>
                            <td>
                                @if($row->status == 'closed')
                                <a href="#">
                                <button class="btn btn-outline-secondary btn-sm" type="button">
                                Issue Closed
                                </button>
                                </a>
                                @elseif(Auth::user()->id == $row->created_by)
                                <form action="{{ route('issues.destroy', $row->id)}}" method="post" id="confirm_delete">
                                    @csrf
                                    @method('DELETE')
                                    <a onclick="return confirm('Apakah anda yakin menghapus data?')">
                                    <button class="btn btn-icon btn-danger" type="submit">
                                    <i class="fas fa-trash"></i>
                                    </button>
                                    </a>
                                    <a href="{{ route('issues.edit', $row->id)}}">
                                    <button class="btn btn-icon btn-info" type="button">
                                    <i class="fas fa-edit"></i>
                                    </button>
                                    </a>
                                </form>
                                @else 
                                <a href="#">
                                <a href="{{ route('issues.edit', $row->id)}}">
                                <button class="btn btn-icon btn-info" type="button">
                                <i class="fas fa-edit"></i>
                                </button>
                                </a>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @if (Auth::user()->division_name == 'Fabrikasi')
        <div class="col-12">
            <div class="row">
                <div class="col-md-6 col-12">
                    <!-- Filter Data -->
                    <table>
                                <form method="get" action="/report_issue_fabrikasi">

                                    <input type="text" name="project" id="project" hidden="">
                                    <input type="text" name="status" hidden="" id="status">
                                    <input id="level" type="text" name="level" hidden="">
                                <tr>
                                    <label style="margin-left:1%" for="filter">Filter By Projec</label>
                                    <label style="margin-left:2%" for="filter">Level</label>
                                    <label style="margin-left:17%" for="filter">Status</label>
                                    <label style="margin-left:15%" for="filter">Date</label>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th class="filterhead none"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                </tr>
                            </table>
                            <!-- End Filter Data --> <br>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="section-header-breadcrumb">
                                <div class="bottom text-right">
                                    <button class="btn btn-primary" target="_blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            <br>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="exampledr">
                    <thead>
                        <tr>
                            <th width="12%">Project</th>
                            <th width="4%">Level</th>
                            <th class="none">Filter for level</th>
                            <th width="4%">Status</th>
                            <th class="none">Filter for status</th>
                            <th width="10%">Master Activity</th>
                            <th width="15%">Isu</th>
                            <th width="15%">Permasalahan</th>
                            <th width="5%">Tanggal Isu</th>
                            <th class="none">Date Filter Open</th>
                            <th width="20%">Detail Issue</th>
                            <th width="18%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($fabrikasi as $row)
                        <tr>
                            <td>{{$row->get_project->name}}</td>
                            <td>
                                @if ($row->level == 'major')
                                <strong class="badge badge-danger">Major</strong>
                                @elseif($row->level == 'moderate')
                                <strong class="badge badge-primary">Moderate</strong>
                                @else 
                                <strong class="badge badge-warning">Minor</strong>
                                @endif
                            </td>
                            <td class="none">
                                @if ($row->level == 'major')
                                Major
                                @elseif($row->level == 'moderate')
                                Moderate
                                @else
                                Minor
                                @endif
                            </td>
                            <td>
                                @if ($row->status == 'open')
                                <strong class="badge badge-warning">Open</strong>
                                @else
                                <strong class="badge badge-success">Closed</strong>
                                @endif
                            </td>
                            <td class="none">
                                @if ($row->status == 'open')
                                Open
                                @else
                                Closed
                                @endif
                            </td>
                            <td>{{$row->master_activity}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->issue}}</td>
                            <td>
                                <?php
                                    $date = date_create($row->date_issue);
                                    echo date_format($date, "j F Y");
                                    ?>
                            </td>
                            <td class="none">
                                <?php
                                    date_create($row->date_issue);
                                    $string =  date_format($date, "j F Y");
                                    if (stristr($string, 'January') !== FALSE) {
                                        echo 'January';
                                    } else if (stristr($string, 'February') !== FALSE) {
                                        echo 'February';
                                    } else if (stristr($string, 'March') !== FALSE) {
                                        echo 'March';
                                    } else if (stristr($string, 'April') !== FALSE) {
                                        echo 'April';
                                    } else if (stristr($string, 'May') !== FALSE) {
                                        echo 'May';
                                    } else if (stristr($string, 'June') !== FALSE) {
                                        echo 'June';
                                    } else if (stristr($string, 'July') !== FALSE) {
                                        echo 'July';
                                    } else if (stristr($string, 'August') !== FALSE) {
                                        echo 'August';
                                    } else if (stristr($string, 'September') !== FALSE) {
                                        echo 'September';
                                    } else if (stristr($string, 'October') !== FALSE) {
                                        echo 'October';
                                    } else if (stristr($string, 'November') !== FALSE) {
                                        echo 'November';
                                    } else if (stristr($string, 'December') !== FALSE) {
                                        echo 'December';
                                    }
                                    ?>
                            </td>
                            <td>
                                <a href="{{ route('issues.show', $row->id)}}">
                                <button class="btn btn-icon btn-outline-info btn-sm" type="button">
                                <i class="fas fa-arrow-right"></i> Read More
                                </button>
                                </a>
                            </td>
                            <td>
                                @if($row->status == 'closed')
                                <a href="#">
                                <button class="btn btn-outline-secondary btn-sm" type="button">
                                Issue Closed
                                </button>
                                </a>
                                @elseif(Auth::user()->id == $row->created_by)
                                <form action="{{ route('issues.destroy', $row->id)}}" method="post" id="confirm_delete">
                                    @csrf
                                    @method('DELETE')
                                    <a onclick="return confirm('Apakah anda yakin menghapus data?')">
                                    <button class="btn btn-icon btn-danger" type="submit">
                                    <i class="fas fa-trash"></i>
                                    </button>
                                    </a>
                                    <a href="{{ route('issues.edit', $row->id)}}">
                                    <button class="btn btn-icon btn-info" type="button">
                                    <i class="fas fa-edit"></i>
                                    </button>
                                    </a>
                                </form>
                                @else 
                                <a href="#">
                                <a href="{{ route('issues.edit', $row->id)}}">
                                <button class="btn btn-icon btn-info" type="button">
                                <i class="fas fa-edit"></i>
                                </button>
                                </a>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @if (Auth::user()->division_name == 'Finishing')
        <div class="col-12">
            <div class="row">
                <div class="col-md-6 col-12">
                    <!-- Filter Data -->
                    <table>
                                <form method="get" action="/report_issue_finishing">

                                    <input type="text" name="project" id="project" hidden="">
                                    <input type="text" name="status" hidden="" id="status">
                                    <input id="level" type="text" name="level" hidden="">
                                <tr>
                                    <label style="margin-left:1%" for="filter">Filter By Projec</label>
                                    <label style="margin-left:2%" for="filter">Level</label>
                                    <label style="margin-left:17%" for="filter">Status</label>
                                    <label style="margin-left:15%" for="filter">Date</label>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th class="filterhead none"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                </tr>
                            </table>
                            <!-- End Filter Data --> <br>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="section-header-breadcrumb">
                                <div class="bottom text-right">
                                    <button class="btn btn-primary" target="_blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            <br>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="exampledr">
                    <thead>
                        <tr>
                            <th width="12%">Project</th>
                            <th width="4%">Level</th>
                            <th class="none">Filter for level</th>
                            <th width="4%">Status</th>
                            <th class="none">Filter for status</th>
                            <th width="10%">Master Activity</th>
                            <th width="15%">Isu</th>
                            <th width="15%">Permasalahan</th>
                            <th width="5%">Tanggal Isu</th>
                            <th class="none">Date Filter Open</th>
                            <th width="20%">Detail Issue</th>
                            <th width="18%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($finishing as $row)
                        <tr>
                            <td>{{$row->get_project->name}}</td>
                            <td>
                                @if ($row->level == 'major')
                                <strong class="badge badge-danger">Major</strong>
                                @elseif($row->level == 'moderate')
                                <strong class="badge badge-primary">Moderate</strong>
                                @else 
                                <strong class="badge badge-warning">Minor</strong>
                                @endif
                            </td>
                            <td class="none">
                                @if ($row->level == 'major')
                                Major
                                @elseif($row->level == 'moderate')
                                Moderate
                                @else
                                Minor
                                @endif
                            </td>
                            <td>
                                @if ($row->status == 'open')
                                <strong class="badge badge-warning">Open</strong>
                                @else
                                <strong class="badge badge-success">Closed</strong>
                                @endif
                            </td>
                            <td class="none">
                                @if ($row->status == 'open')
                                Open
                                @else
                                Closed
                                @endif
                            </td>
                            <td>{{$row->master_activity}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->issue}}</td>
                            <td>
                                <?php
                                    $date = date_create($row->date_issue);
                                    echo date_format($date, "j F Y");
                                    ?>
                            </td>
                            <td class="none">
                                <?php
                                    date_create($row->date_issue);
                                    $string =  date_format($date, "j F Y");
                                    if (stristr($string, 'January') !== FALSE) {
                                        echo 'January';
                                    } else if (stristr($string, 'February') !== FALSE) {
                                        echo 'February';
                                    } else if (stristr($string, 'March') !== FALSE) {
                                        echo 'March';
                                    } else if (stristr($string, 'April') !== FALSE) {
                                        echo 'April';
                                    } else if (stristr($string, 'May') !== FALSE) {
                                        echo 'May';
                                    } else if (stristr($string, 'June') !== FALSE) {
                                        echo 'June';
                                    } else if (stristr($string, 'July') !== FALSE) {
                                        echo 'July';
                                    } else if (stristr($string, 'August') !== FALSE) {
                                        echo 'August';
                                    } else if (stristr($string, 'September') !== FALSE) {
                                        echo 'September';
                                    } else if (stristr($string, 'October') !== FALSE) {
                                        echo 'October';
                                    } else if (stristr($string, 'November') !== FALSE) {
                                        echo 'November';
                                    } else if (stristr($string, 'December') !== FALSE) {
                                        echo 'December';
                                    }
                                    ?>
                            </td>
                            <td>
                                <a href="{{ route('issues.show', $row->id)}}">
                                <button class="btn btn-icon btn-outline-info btn-sm" type="button">
                                <i class="fas fa-arrow-right"></i> Read More
                                </button>
                                </a>
                            </td>
                            <td>
                                @if($row->status == 'closed')
                                <a href="#">
                                <button class="btn btn-outline-secondary btn-sm" type="button">
                                Issue Closed
                                </button>
                                </a>
                                @elseif(Auth::user()->id == $row->created_by)
                                <form action="{{ route('issues.destroy', $row->id)}}" method="post" id="confirm_delete">
                                    @csrf
                                    @method('DELETE')
                                    <a onclick="return confirm('Apakah anda yakin menghapus data?')">
                                    <button class="btn btn-icon btn-danger" type="submit">
                                    <i class="fas fa-trash"></i>
                                    </button>
                                    </a>
                                    <a href="{{ route('issues.edit', $row->id)}}">
                                    <button class="btn btn-icon btn-info" type="button">
                                    <i class="fas fa-edit"></i>
                                    </button>
                                    </a>
                                </form>
                                @else 
                                <a href="#">
                                <a href="{{ route('issues.edit', $row->id)}}">
                                <button class="btn btn-icon btn-info" type="button">
                                <i class="fas fa-edit"></i>
                                </button>
                                </a>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @if (Auth::user()->division_name == 'Testing')
        <div class="col-12">
            <div class="row">
                <div class="col-md-6 col-12">
                    <!-- Filter Data -->
                    <table>
                                <form method="get" action="/report_issue_testing">

                                    <input type="text" name="project" id="project" hidden="">
                                    <input type="text" name="status" hidden="" id="status">
                                    <input id="level" type="text" name="level" hidden="">
                                <tr>
                                    <label style="margin-left:1%" for="filter">Filter By Projec</label>
                                    <label style="margin-left:2%" for="filter">Level</label>
                                    <label style="margin-left:17%" for="filter">Status</label>
                                    <label style="margin-left:15%" for="filter">Date</label>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th class="filterhead none"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                </tr>
                            </table>
                            <!-- End Filter Data --> <br>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="section-header-breadcrumb">
                                <div class="bottom text-right">
                                    <button class="btn btn-primary" target="_blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            <br>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="exampledr">
                    <thead>
                        <tr>
                            <th width="12%">Project</th>
                            <th width="4%">Level</th>
                            <th class="none">Filter for level</th>
                            <th width="4%">Status</th>
                            <th class="none">Filter for status</th>
                            <th width="10%">Master Activity</th>
                            <th width="15%">Isu</th>
                            <th width="15%">Permasalahan</th>
                            <th width="5%">Tanggal Isu</th>
                            <th class="none">Date Filter Open</th>
                            <th width="20%">Detail Issue</th>
                            <th width="18%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($testing as $row)
                        <tr>
                            <td>{{$row->get_project->name}}</td>
                            <td>
                                @if ($row->level == 'major')
                                <strong class="badge badge-danger">Major</strong>
                                @elseif($row->level == 'moderate')
                                <strong class="badge badge-primary">Moderate</strong>
                                @else 
                                <strong class="badge badge-warning">Minor</strong>
                                @endif
                            </td>
                            <td class="none">
                                @if ($row->level == 'major')
                                Major
                                @elseif($row->level == 'moderate')
                                Moderate
                                @else
                                Minor
                                @endif
                            </td>
                            <td>
                                @if ($row->status == 'open')
                                <strong class="badge badge-warning">Open</strong>
                                @else
                                <strong class="badge badge-success">Closed</strong>
                                @endif
                            </td>
                            <td class="none">
                                @if ($row->status == 'open')
                                Open
                                @else
                                Closed
                                @endif
                            </td>
                            <td>{{$row->master_activity}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->issue}}</td>
                            <td>
                                <?php
                                    $date = date_create($row->date_issue);
                                    echo date_format($date, "j F Y");
                                    ?>
                            </td>
                            <td class="none">
                                <?php
                                    date_create($row->date_issue);
                                    $string =  date_format($date, "j F Y");
                                    if (stristr($string, 'January') !== FALSE) {
                                        echo 'January';
                                    } else if (stristr($string, 'February') !== FALSE) {
                                        echo 'February';
                                    } else if (stristr($string, 'March') !== FALSE) {
                                        echo 'March';
                                    } else if (stristr($string, 'April') !== FALSE) {
                                        echo 'April';
                                    } else if (stristr($string, 'May') !== FALSE) {
                                        echo 'May';
                                    } else if (stristr($string, 'June') !== FALSE) {
                                        echo 'June';
                                    } else if (stristr($string, 'July') !== FALSE) {
                                        echo 'July';
                                    } else if (stristr($string, 'August') !== FALSE) {
                                        echo 'August';
                                    } else if (stristr($string, 'September') !== FALSE) {
                                        echo 'September';
                                    } else if (stristr($string, 'October') !== FALSE) {
                                        echo 'October';
                                    } else if (stristr($string, 'November') !== FALSE) {
                                        echo 'November';
                                    } else if (stristr($string, 'December') !== FALSE) {
                                        echo 'December';
                                    }
                                    ?>
                            </td>
                            <td>
                                <a href="{{ route('issues.show', $row->id)}}">
                                <button class="btn btn-icon btn-outline-info btn-sm" type="button">
                                <i class="fas fa-arrow-right"></i> Read More
                                </button>
                                </a>
                            </td>
                            <td>
                                @if($row->status == 'closed')
                                <a href="#">
                                <button class="btn btn-outline-secondary btn-sm" type="button">
                                Issue Closed
                                </button>
                                </a>
                                @elseif(Auth::user()->id == $row->created_by)
                                <form action="{{ route('issues.destroy', $row->id)}}" method="post" id="confirm_delete">
                                    @csrf
                                    @method('DELETE')
                                    <a onclick="return confirm('Apakah anda yakin menghapus data?')">
                                    <button class="btn btn-icon btn-danger" type="submit">
                                    <i class="fas fa-trash"></i>
                                    </button>
                                    </a>
                                    <a href="{{ route('issues.edit', $row->id)}}">
                                    <button class="btn btn-icon btn-info" type="button">
                                    <i class="fas fa-edit"></i>
                                    </button>
                                    </a>
                                </form>
                                @else 
                                <a href="#">
                                <a href="{{ route('issues.edit', $row->id)}}">
                                <button class="btn btn-icon btn-info" type="button">
                                <i class="fas fa-edit"></i>
                                </button>
                                </a>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        <!-- End Issue By Division-->
        <!-- Issue For Admin , PM-->
        @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3||Auth::user()->role_id == 6 || Auth::user()->role_id == 4)
        <ul class="nav nav-pills" id="myTab3" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="teknologi-tab1" data-toggle="tab" href="#teknologi1" role="tab" aria-controls="teknologi" aria-selected="true">Teknologi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="logistik-tab1" data-toggle="tab" href="#logistik1" role="tab" aria-controls="logistik" aria-selected="false">Logistik</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="fabrikasi-tab1" data-toggle="tab" href="#fabrikasi1" role="tab" aria-controls="fabrikasi" aria-selected="false">Fabrikasi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="finishing-tab1" data-toggle="tab" href="#finishing1" role="tab" aria-controls="finishing" aria-selected="false">Finishing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="testing-tab1" data-toggle="tab" href="#testing1" role="tab" aria-controls="testing" aria-selected="false">Testing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="delivery-tab1" data-toggle="tab" href="#delivery1" role="tab" aria-controls="delivery" aria-selected="false">Delivery</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent2">
            <div class="tab-pane fade show active" id="teknologi1" role="tabpanel" aria-labelledby="teknologi-tab1">
                <div class="col-12 bottom">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <!-- Filter Data -->
                            <table>
                                <form method="get" action="/report_issue_teknologi?name=">

                                    <input type="text" name="project" id="project" hidden="">
                                    <input type="text" name="status" hidden="" id="status">
                                    <input id="level" type="text" name="level" hidden="">
                                <tr>
                                    <label style="margin-left:1%" for="filter">Filter By Projec</label>
                                    <label style="margin-left:2%" for="filter">Level</label>
                                    <label style="margin-left:17%" for="filter">Status</label>
                                    <label style="margin-left:15%" for="filter">Date</label>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                    <th class="filterhead none"></th>
                                    <th class="filterhead none"></th>
                                    <th class="filterhead none"></th>
                                    <th style="width:120px" class="filterhead"></th>
                                </tr>
                            </table>
                            <!-- End Filter Data --> <br>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="section-header-breadcrumb">
                                <div class="bottom text-right">
                                    <button class="btn btn-primary" target="_blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                    <br>
                    <div class="table-responsive">
                        <table class="table col-lg-12 zero-configuration" id="exampledr">
                            <thead>
                                <tr>
                                    <th width="12%">Project</th>
                                    <th width="4%">Level</th>
                                    <th class="none">Filter for level</th>
                                    <th width="4%">Status</th>
                                    <th class="none">Filter for status</th>
                                    <th width="10%">Master Activity</th>
                                    <th width="15%">Isu</th>
                                    <th width="15%">Permasalahan</th>
                                    <th width="5%">Tanggal Isu</th>
                                    <th class="none">Date Filter Open</th>
                                    <th width="20%">Detail Issue</th>
                                    <th width="18%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($teknologi as $row)
                                <tr>
                                    <td>{{$row->get_project->name}}</td>
                                    <td>
                                        @if ($row->level == 'major')
                                        <strong class="badge badge-danger">Major</strong>
                                        @elseif($row->level == 'moderate')
                                        <strong class="badge badge-primary">Moderate</strong>
                                        @else 
                                        <strong class="badge badge-warning">Minor</strong>
                                        @endif
                                    </td>
                                    <td class="none">
                                        @if ($row->level == 'major')
                                        Major
                                        @elseif($row->level == 'moderate')
                                        Moderate
                                        @else
                                        Minor
                                        @endif
                                    </td>
                                    <td>
                                        @if ($row->status == 'open')
                                        <strong class="badge badge-warning">Open</strong>
                                        @else
                                        <strong class="badge badge-success">Closed</strong>
                                        @endif
                                    </td>
                                    <td class="none">
                                        @if ($row->status == 'open')
                                        Open
                                        @else
                                        Closed
                                        @endif
                                    </td>
                                    <td>{{$row->master_activity}}</td>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->issue}}</td>
                                    <td>
                                        <?php
                                            $date = date_create($row->date_issue);
                                            echo date_format($date, "j F Y");
                                            ?>
                                    </td>
                                    <td class="none">
                                        <?php
                                            date_create($row->date_issue);
                                            $string =  date_format($date, "j F Y");
                                            if (stristr($string, 'January') !== FALSE) {
                                                echo 'January';
                                            } else if (stristr($string, 'February') !== FALSE) {
                                                echo 'February';
                                            } else if (stristr($string, 'March') !== FALSE) {
                                                echo 'March';
                                            } else if (stristr($string, 'April') !== FALSE) {
                                                echo 'April';
                                            } else if (stristr($string, 'May') !== FALSE) {
                                                echo 'May';
                                            } else if (stristr($string, 'June') !== FALSE) {
                                                echo 'June';
                                            } else if (stristr($string, 'July') !== FALSE) {
                                                echo 'July';
                                            } else if (stristr($string, 'August') !== FALSE) {
                                                echo 'August';
                                            } else if (stristr($string, 'September') !== FALSE) {
                                                echo 'September';
                                            } else if (stristr($string, 'October') !== FALSE) {
                                                echo 'October';
                                            } else if (stristr($string, 'November') !== FALSE) {
                                                echo 'November';
                                            } else if (stristr($string, 'December') !== FALSE) {
                                                echo 'December';
                                            }
                                            ?>
                                    </td>
                                    <td>
                                        <a href="{{ route('issues.show', $row->id)}}">
                                        <button class="btn btn-icon btn-outline-info btn-sm" type="button">
                                        <i class="fas fa-arrow-right"></i> Read More
                                        </button>
                                        </a>
                                    </td>
                                    <td>
                                        @if($row->status == 'closed')
                                        <a href="#">
                                        <button class="btn btn-outline-secondary btn-sm" type="button">
                                        Issue Closed
                                        </button>
                                        </a>
                                        @elseif(Auth::user()->id == $row->created_by)
                                        <form action="{{ route('issues.destroy', $row->id)}}" method="post" id="confirm_delete">
                                            @csrf
                                            @method('DELETE')
                                            <a onclick="return confirm('Apakah anda yakin menghapus data?')">
                                            <button class="btn btn-icon btn-danger" type="submit">
                                            <i class="fas fa-trash"></i>
                                            </button>
                                            </a>
                                            <a href="{{ route('issues.edit', $row->id)}}">
                                            <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>
                                            </button>
                                            </a>
                                        </form>
                                        @else 
                                        <a href="#">
                                        <a href="{{ route('issues.edit', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                        <i class="fas fa-edit"></i>
                                        </button>
                                        </a>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="logistik1" role="tabpanel" aria-labelledby="logistik-tab1">
                <div class="col-12 bottom">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <!-- Filter Data -->
                           <table>
                                <form method="get" action="/report_issue_logistik">

                                    <input type="text" name="project1" id="project1" hidden="">
                                    <input type="text" name="status1" hidden="" id="status1">
                                    <input id="level1" type="text" name="level1" hidden="">
                                <tr>
                                    <label style="margin-left:1%" for="filter">Filter By Projec</label>
                                    <label style="margin-left:2%" for="filter">Level</label>
                                    <label style="margin-left:17%" for="filter">Status</label>
                                    <label style="margin-left:15%" for="filter">Date</label>
                                    <th style="width:120px" class="filterhead2"></th>
                                    <th class="filterhead2 none"></th>
                                    <th style="width:120px" class="filterhead2"></th>
                                    <th class="filterhead2 none"></th>
                                    <th style="width:120px" class="filterhead2"></th>
                                    <th class="filterhead2 none"></th>
                                    <th class="filterhead2 none"></th>
                                    <th class="filterhead2 none"></th>
                                    <th style="width:120px" class="filterhead2"></th>
                                </tr>
                            </table>
                            <!-- End Filter Data --> <br>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="section-header-breadcrumb">
                                <div class="bottom text-right">
                                    <button class="btn btn-primary" target="_blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                    <br>
                    <div class="table-responsive">
                        <table class="table col-lg-12 zero-configuration" id="exampledr2">
                            <thead>
                                <tr>
                                    <th width="10%">Project</th>
                                    <th width="3%">Level</th>
                                    <th class="none">Filter for level</th>
                                    <th width="2%">Status</th>
                                    <th class="none">Filter for status</th>
                                    <th width="10%">Master Activity</th>
                                    <th width="10%">Isu</th>
                                    <th width="15%">Permasalahan</th>
                                    <th width="5%">Tanggal Isu</th>
                                    <th class="none">Date Filter Open</th>
                                    <th width="25%">Detail Issue</th>
                                    <th width="25%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($logistik as $row)
                                <tr>
                                    <td>{{$row->get_project->name}}</td>
                                    <td>
                                        @if ($row->level == 'major')
                                        <strong class="badge badge-danger">Major</strong>
                                        @elseif($row->level == 'moderate')
                                        <strong class="badge badge-primary">Moderate</strong>
                                        @else 
                                        <strong class="badge badge-warning">Minor</strong>
                                        @endif
                                    </td>
                                    <td class="none">
                                        @if ($row->level == 'major')
                                        Major
                                        @elseif($row->level == 'moderate')
                                        Moderate
                                        @else
                                        Minor
                                        @endif
                                    </td>
                                    <td>
                                        @if ($row->status == 'open')
                                        <strong class="badge badge-warning">Open</strong>
                                        @else
                                        <strong class="badge badge-success">Closed</strong>
                                        @endif
                                    </td>
                                    <td class="none">
                                        @if ($row->status == 'open')
                                        Open
                                        @else
                                        Closed
                                        @endif
                                    </td>
                                    <td>{{$row->master_activity}}</td>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->issue}}</td>
                                    <td>
                                        <?php
                                            $date = date_create($row->date_issue);
                                            echo date_format($date, "j F Y");
                                            ?>
                                    </td>
                                    <td class="none">
                                        <?php
                                            date_create($row->date_issue);
                                            $string =  date_format($date, "j F Y");
                                            if (stristr($string, 'January') !== FALSE) {
                                                echo 'January';
                                            } else if (stristr($string, 'February') !== FALSE) {
                                                echo 'February';
                                            } else if (stristr($string, 'March') !== FALSE) {
                                                echo 'March';
                                            } else if (stristr($string, 'April') !== FALSE) {
                                                echo 'April';
                                            } else if (stristr($string, 'May') !== FALSE) {
                                                echo 'May';
                                            } else if (stristr($string, 'June') !== FALSE) {
                                                echo 'June';
                                            } else if (stristr($string, 'July') !== FALSE) {
                                                echo 'July';
                                            } else if (stristr($string, 'August') !== FALSE) {
                                                echo 'August';
                                            } else if (stristr($string, 'September') !== FALSE) {
                                                echo 'September';
                                            } else if (stristr($string, 'October') !== FALSE) {
                                                echo 'October';
                                            } else if (stristr($string, 'November') !== FALSE) {
                                                echo 'November';
                                            } else if (stristr($string, 'December') !== FALSE) {
                                                echo 'December';
                                            }
                                            ?>
                                    </td>
                                    <td>
                                        <a href="{{ route('issues.show', $row->id)}}">
                                        <button class="btn btn-icon btn-outline-info btn-sm" type="button">
                                        <i class="fas fa-arrow-right"></i> Read More
                                        </button>
                                        </a>
                                    </td>
                                    <td>
                                        @if($row->status == 'closed')
                                        <a href="#">
                                        <button class="btn btn-outline-secondary btn-sm" type="button">
                                        Issue Closed
                                        </button>
                                        </a>
                                        @elseif(Auth::user()->id == $row->created_by)
                                        <form action="{{ route('issues.destroy', $row->id)}}" method="post" id="confirm_delete">
                                            @csrf
                                            @method('DELETE')
                                            <a onclick="return confirm('Apakah anda yakin menghapus data?')">
                                            <button class="btn btn-icon btn-danger" type="submit">
                                            <i class="fas fa-trash"></i>
                                            </button>
                                            </a>
                                            <a href="{{ route('issues.edit', $row->id)}}">
                                            <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>
                                            </button>
                                            </a>
                                        </form>
                                        @else 
                                        <a href="#">
                                        <a href="{{ route('issues.edit', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                        <i class="fas fa-edit"></i>
                                        </button>
                                        </a>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="fabrikasi1" role="tabpanel" aria-labelledby="fabrikasi-tab1">
                <div class="col-12 bottom">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <!-- Filter Data -->
                            <table>
                                <form method="get" action="/report_issue_fabrikasi">

                                    <input type="text" name="project2" id="project2" hidden="">
                                    <input type="text" name="status2" hidden="" id="status2">
                                    <input id="level2" type="text" name="level2" hidden="">
                                <tr>
                                    <label style="margin-left:1%" for="filter">Filter By Projec</label>
                                    <label style="margin-left:2%" for="filter">Level</label>
                                    <label style="margin-left:17%" for="filter">Status</label>
                                    <label style="margin-left:15%" for="filter">Date</label>
                                    <th style="width:120px" class="filterhead3"></th>
                                    <th class="filterhead3 none"></th>
                                    <th style="width:120px" class="filterhead3"></th>
                                    <th class="filterhead3 none"></th>
                                    <th style="width:120px" class="filterhead3"></th>
                                    <th class="filterhead3 none"></th>
                                    <th class="filterhead3 none"></th>
                                    <th class="filterhead3 none"></th>
                                    <th style="width:120px" class="filterhead3"></th>
                                </tr>
                            </table>
                            <!-- End Filter Data --> <br>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="section-header-breadcrumb">
                                <div class="bottom text-right">
                                    <button class="btn btn-primary" target="_blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                    <br>
                    <div class="table-responsive">
                        <table class="table col-lg-12 zero-configuration" id="exampledr3">
                            <thead>
                                <tr>
                                    <th width="12%">Project</th>
                                    <th width="4%">Level</th>
                                    <th class="none">Filter for level</th>
                                    <th width="4%">Status</th>
                                    <th class="none">Filter for status</th>
                                    <th width="10%">Master Activity</th>
                                    <th width="15%">Isu</th>
                                    <th width="15%">Permasalahan</th>
                                    <th width="5%">Tanggal Isu</th>
                                    <th class="none">Date Filter Open</th>
                                    <th width="20%">Detail Issue</th>
                                    <th width="18%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($fabrikasi as $row)
                                <tr>
                                    <td>{{$row->get_project->name}}</td>
                                    <td>
                                        @if ($row->level == 'major')
                                        <strong class="badge badge-danger">Major</strong>
                                        @elseif($row->level == 'moderate')
                                        <strong class="badge badge-primary">Moderate</strong>
                                        @else 
                                        <strong class="badge badge-warning">Minor</strong>
                                        @endif
                                    </td>
                                    <td class="none">
                                        @if ($row->level == 'major')
                                        Major
                                        @elseif($row->level == 'moderate')
                                        Moderate
                                        @else
                                        Minor
                                        @endif
                                    </td>
                                    <td>
                                        @if ($row->status == 'open')
                                        <strong class="badge badge-warning">Open</strong>
                                        @else
                                        <strong class="badge badge-success">Closed</strong>
                                        @endif
                                    </td>
                                    <td class="none">
                                        @if ($row->status == 'open')
                                        Open
                                        @else
                                        Closed
                                        @endif
                                    </td>
                                    <td>{{$row->master_activity}}</td>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->issue}}</td>
                                    <td>
                                        <?php
                                            $date = date_create($row->date_issue);
                                            echo date_format($date, "j F Y");
                                            ?>
                                    </td>
                                    <td class="none">
                                        <?php
                                            date_create($row->date_issue);
                                            $string =  date_format($date, "j F Y");
                                            if (stristr($string, 'January') !== FALSE) {
                                                echo 'January';
                                            } else if (stristr($string, 'February') !== FALSE) {
                                                echo 'February';
                                            } else if (stristr($string, 'March') !== FALSE) {
                                                echo 'March';
                                            } else if (stristr($string, 'April') !== FALSE) {
                                                echo 'April';
                                            } else if (stristr($string, 'May') !== FALSE) {
                                                echo 'May';
                                            } else if (stristr($string, 'June') !== FALSE) {
                                                echo 'June';
                                            } else if (stristr($string, 'July') !== FALSE) {
                                                echo 'July';
                                            } else if (stristr($string, 'August') !== FALSE) {
                                                echo 'August';
                                            } else if (stristr($string, 'September') !== FALSE) {
                                                echo 'September';
                                            } else if (stristr($string, 'October') !== FALSE) {
                                                echo 'October';
                                            } else if (stristr($string, 'November') !== FALSE) {
                                                echo 'November';
                                            } else if (stristr($string, 'December') !== FALSE) {
                                                echo 'December';
                                            }
                                            ?>
                                    </td>
                                    <td>
                                        <a href="{{ route('issues.show', $row->id)}}">
                                        <button class="btn btn-icon btn-outline-info btn-sm" type="button">
                                        <i class="fas fa-arrow-right"></i> Read More
                                        </button>
                                        </a>
                                    </td>
                                    <td>
                                        @if($row->status == 'closed')
                                        <a href="#">
                                        <button class="btn btn-outline-secondary btn-sm" type="button">
                                        Issue Closed
                                        </button>
                                        </a>
                                        @elseif(Auth::user()->id == $row->created_by)
                                        <form action="{{ route('issues.destroy', $row->id)}}" method="post" id="confirm_delete">
                                            @csrf
                                            @method('DELETE')
                                            <a onclick="return confirm('Apakah anda yakin menghapus data?')">
                                            <button class="btn btn-icon btn-danger" type="submit">
                                            <i class="fas fa-trash"></i>
                                            </button>
                                            </a>
                                            <a href="{{ route('issues.edit', $row->id)}}">
                                            <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>
                                            </button>
                                            </a>
                                        </form>
                                        @else 
                                        <a href="#">
                                        <a href="{{ route('issues.edit', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                        <i class="fas fa-edit"></i>
                                        </button>
                                        </a>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="finishing1" role="tabpanel" aria-labelledby="finishing-tab1">
                <div class="col-12 bottom">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <!-- Filter Data -->
                            <table>
                                <form method="get" action="/report_issue_finishing">

                                    <input type="text" name="project3" id="project3" hidden="">
                                    <input type="text" name="status3" hidden="" id="status3">
                                    <input id="level3" type="text" name="level3" hidden="">
                                <tr>
                                    <label style="margin-left:1%" for="filter">Filter By Projec</label>
                                    <label style="margin-left:2%" for="filter">Level</label>
                                    <label style="margin-left:17%" for="filter">Status</label>
                                    <label style="margin-left:15%" for="filter">Date</label>
                                    <th style="width:120px" class="filterhead4"></th>
                                    <th class="filterhead4 none"></th>
                                    <th style="width:120px" class="filterhead4"></th>
                                    <th class="filterhead4 none"></th>
                                    <th style="width:120px" class="filterhead4"></th>
                                    <th class="filterhead4 none"></th>
                                    <th class="filterhead4 none"></th>
                                    <th class="filterhead4 none"></th>
                                    <th style="width:120px" class="filterhead4"></th>
                                </tr>
                            </table>
                            <!-- End Filter Data --> <br>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="section-header-breadcrumb">
                                <div class="bottom text-right">
                                    <button class="btn btn-primary" target="_blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                    <br>
                    <div class="table-responsive">
                        <table class="table col-lg-12 zero-configuration" id="exampledr4">
                            <thead>
                                <tr>
                                    <th width="12%">Project</th>
                                    <th width="4%">Level</th>
                                    <th class="none">Filter for level</th>
                                    <th width="4%">Status</th>
                                    <th class="none">Filter for status</th>
                                    <th width="10%">Master Activity</th>
                                    <th width="15%">Isu</th>
                                    <th width="15%">Permasalahan</th>
                                    <th width="5%">Tanggal Isu</th>
                                    <th class="none">Date Filter Open</th>
                                    <th width="20%">Detail Issue</th>
                                    <th width="18%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($finishing as $row)
                                <tr>
                                    <td>{{$row->get_project->name}}</td>
                                    <td>
                                        @if ($row->level == 'major')
                                        <strong class="badge badge-danger">Major</strong>
                                        @elseif($row->level == 'moderate')
                                        <strong class="badge badge-primary">Moderate</strong>
                                        @else 
                                        <strong class="badge badge-warning">Minor</strong>
                                        @endif
                                    </td>
                                    <td class="none">
                                        @if ($row->level == 'major')
                                        Major
                                        @elseif($row->level == 'moderate')
                                        Moderate
                                        @else
                                        Minor
                                        @endif
                                    </td>
                                    <td>
                                        @if ($row->status == 'open')
                                        <strong class="badge badge-warning">Open</strong>
                                        @else
                                        <strong class="badge badge-success">Closed</strong>
                                        @endif
                                    </td>
                                    <td class="none">
                                        @if ($row->status == 'open')
                                        Open
                                        @else
                                        Closed
                                        @endif
                                    </td>
                                    <td>{{$row->master_activity}}</td>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->issue}}</td>
                                    <td>
                                        <?php
                                            $date = date_create($row->date_issue);
                                            echo date_format($date, "j F Y");
                                            ?>
                                    </td>
                                    <td class="none">
                                        <?php
                                            date_create($row->date_issue);
                                            $string =  date_format($date, "j F Y");
                                            if (stristr($string, 'January') !== FALSE) {
                                                echo 'January';
                                            } else if (stristr($string, 'February') !== FALSE) {
                                                echo 'February';
                                            } else if (stristr($string, 'March') !== FALSE) {
                                                echo 'March';
                                            } else if (stristr($string, 'April') !== FALSE) {
                                                echo 'April';
                                            } else if (stristr($string, 'May') !== FALSE) {
                                                echo 'May';
                                            } else if (stristr($string, 'June') !== FALSE) {
                                                echo 'June';
                                            } else if (stristr($string, 'July') !== FALSE) {
                                                echo 'July';
                                            } else if (stristr($string, 'August') !== FALSE) {
                                                echo 'August';
                                            } else if (stristr($string, 'September') !== FALSE) {
                                                echo 'September';
                                            } else if (stristr($string, 'October') !== FALSE) {
                                                echo 'October';
                                            } else if (stristr($string, 'November') !== FALSE) {
                                                echo 'November';
                                            } else if (stristr($string, 'December') !== FALSE) {
                                                echo 'December';
                                            }
                                            ?>
                                    </td>
                                    <td>
                                        <a href="{{ route('issues.show', $row->id)}}">
                                        <button class="btn btn-icon btn-outline-info btn-sm" type="button">
                                        <i class="fas fa-arrow-right"></i> Read More
                                        </button>
                                        </a>
                                    </td>
                                    <td>
                                        @if($row->status == 'closed')
                                        <a href="#">
                                        <button class="btn btn-outline-secondary btn-sm" type="button">
                                        Issue Closed
                                        </button>
                                        </a>
                                        @elseif(Auth::user()->id == $row->created_by)
                                        <form action="{{ route('issues.destroy', $row->id)}}" method="post" id="confirm_delete">
                                            @csrf
                                            @method('DELETE')
                                            <a onclick="return confirm('Apakah anda yakin menghapus data?')">
                                            <button class="btn btn-icon btn-danger" type="submit">
                                            <i class="fas fa-trash"></i>
                                            </button>
                                            </a>
                                            <a href="{{ route('issues.edit', $row->id)}}">
                                            <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>
                                            </button>
                                            </a>
                                        </form>
                                        @else 
                                        <a href="#">
                                        <a href="{{ route('issues.edit', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                        <i class="fas fa-edit"></i>
                                        </button>
                                        </a>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="testing1" role="tabpanel" aria-labelledby="testing-tab1">
                <div class="col-12 bottom">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <!-- Filter Data -->
                            <table>
                                <form method="get" action="/report_issue_testing">

                                    <input type="text" name="project4" id="project4" hidden="">
                                    <input type="text" name="status4" hidden="" id="status4">
                                    <input id="level4" type="text" name="level4" hidden="">
                                <tr>
                                    <label style="margin-left:1%" for="filter">Filter By Projec</label>
                                    <label style="margin-left:2%" for="filter">Level</label>
                                    <label style="margin-left:17%" for="filter">Status</label>
                                    <label style="margin-left:15%" for="filter">Date</label>
                                    <th style="width:120px" class="filterhead5"></th>
                                    <th class="filterhead5 none"></th>
                                    <th style="width:120px" class="filterhead5"></th>
                                    <th class="filterhead5 none"></th>
                                    <th style="width:120px" class="filterhead5"></th>
                                    <th class="filterhead5 none"></th>
                                    <th class="filterhead5 none"></th>
                                    <th class="filterhead5 none"></th>
                                    <th style="width:120px" class="filterhead5"></th>
                                </tr>
                            </table>
                            <!-- End Filter Data --> <br>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="section-header-breadcrumb">
                                <div class="bottom text-right">
                                    <button class="btn btn-primary" target="_blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                    <br>
                    <div class="table-responsive">
                        <table class="table col-lg-12 zero-configuration" id="exampledr5">
                            <thead>
                                <tr>
                                    <th width="12%">Project</th>
                                    <th width="4%">Level</th>
                                    <th class="none">Filter for level</th>
                                    <th width="4%">Status</th>
                                    <th class="none">Filter for status</th>
                                    <th width="10%">Master Activity</th>
                                    <th width="15%">Isu</th>
                                    <th width="15%">Permasalahan</th>
                                    <th width="5%">Tanggal Isu</th>
                                    <th class="none">Date Filter Open</th>
                                    <th width="20%">Detail Issue</th>
                                    <th width="18%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($testing as $row)
                                <tr>
                                    <td>{{$row->get_project->name}}</td>
                                    <td>
                                        @if ($row->level == 'major')
                                        <strong class="badge badge-danger">Major</strong>
                                        @elseif($row->level == 'moderate')
                                        <strong class="badge badge-primary">Moderate</strong>
                                        @else 
                                        <strong class="badge badge-warning">Minor</strong>
                                        @endif
                                    </td>
                                    <td class="none">
                                        @if ($row->level == 'major')
                                        Major
                                        @elseif($row->level == 'moderate')
                                        Moderate
                                        @else
                                        Minor
                                        @endif
                                    </td>
                                    <td>
                                        @if ($row->status == 'open')
                                        <strong class="badge badge-warning">Open</strong>
                                        @else
                                        <strong class="badge badge-success">Closed</strong>
                                        @endif
                                    </td>
                                    <td class="none">
                                        @if ($row->status == 'open')
                                        Open
                                        @else
                                        Closed
                                        @endif
                                    </td>
                                    <td>{{$row->master_activity}}</td>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->issue}}</td>
                                    <td>
                                        <?php
                                            $date = date_create($row->date_issue);
                                            echo date_format($date, "j F Y");
                                            ?>
                                    </td>
                                    <td class="none">
                                        <?php
                                            date_create($row->date_issue);
                                            $string =  date_format($date, "j F Y");
                                            if (stristr($string, 'January') !== FALSE) {
                                                echo 'January';
                                            } else if (stristr($string, 'February') !== FALSE) {
                                                echo 'February';
                                            } else if (stristr($string, 'March') !== FALSE) {
                                                echo 'March';
                                            } else if (stristr($string, 'April') !== FALSE) {
                                                echo 'April';
                                            } else if (stristr($string, 'May') !== FALSE) {
                                                echo 'May';
                                            } else if (stristr($string, 'June') !== FALSE) {
                                                echo 'June';
                                            } else if (stristr($string, 'July') !== FALSE) {
                                                echo 'July';
                                            } else if (stristr($string, 'August') !== FALSE) {
                                                echo 'August';
                                            } else if (stristr($string, 'September') !== FALSE) {
                                                echo 'September';
                                            } else if (stristr($string, 'October') !== FALSE) {
                                                echo 'October';
                                            } else if (stristr($string, 'November') !== FALSE) {
                                                echo 'November';
                                            } else if (stristr($string, 'December') !== FALSE) {
                                                echo 'December';
                                            }
                                            ?>
                                    </td>
                                    <td>
                                        <a href="{{ route('issues.show', $row->id)}}">
                                        <button class="btn btn-icon btn-outline-info btn-sm" type="button">
                                        <i class="fas fa-arrow-right"></i> Read More
                                        </button>
                                        </a>
                                    </td>
                                    <td>
                                        @if($row->status == 'closed')
                                        <a href="#">
                                        <button class="btn btn-outline-secondary btn-sm" type="button">
                                        Issue Closed
                                        </button>
                                        </a>
                                        @elseif(Auth::user()->id == $row->created_by)
                                        <form action="{{ route('issues.destroy', $row->id)}}" method="post" id="confirm_delete">
                                            @csrf
                                            @method('DELETE')
                                            <a onclick="return confirm('Apakah anda yakin menghapus data?')">
                                            <button class="btn btn-icon btn-danger" type="submit">
                                            <i class="fas fa-trash"></i>
                                            </button>
                                            </a>
                                            <a href="{{ route('issues.edit', $row->id)}}">
                                            <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>
                                            </button>
                                            </a>
                                        </form>
                                        @else 
                                        <a href="#">
                                        <a href="{{ route('issues.edit', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                        <i class="fas fa-edit"></i>
                                        </button>
                                        </a>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="delivery1" role="tabpanel" aria-labelledby="delivery-tab1">
                <div class="col-12 bottom">
                    <!-- Filter Data -->
                    <table>
                        <tr>
                            <label style="margin-left:1%" for="filter">Filter By Project</label>
                            <label style="margin-left:2%" for="filter">Level</label>
                            <label style="margin-left:17%" for="filter">Status</label>
                            <label style="margin-left:15%" for="filter">Date</label>
                            <th style="width:120px" class="filterhead6"></th>
                            <th class="filterhead6 none"></th>
                            <th style="width:120px" class="filterhead6"></th>
                            <th class="filterhead6 none"></th>
                            <th style="width:120px" class="filterhead6"></th>
                            <th class="filterhead6 none"></th>
                            <th class="filterhead6 none"></th>
                            <th class="filterhead6 none"></th>
                            <th style="width:120px" class="filterhead6"></th>
                        </tr>
                    </table>
                    <!-- End Filter Data --> <br>
                    <div class="table-responsive">
                        <table class="table col-lg-12 zero-configuration" id="exampledr6">
                            <thead>
                                <tr>
                                    <th width="12%">Project</th>
                                    <th width="4%">Level</th>
                                    <th class="none">Filter for level</th>
                                    <th width="4%">Status</th>
                                    <th class="none">Filter for status</th>
                                    <th width="10%">Master Activity</th>
                                    <th width="15%">Isu</th>
                                    <th width="15%">Permasalahan</th>
                                    <th width="5%">Tanggal Isu</th>
                                    <th class="none">Date Filter Open</th>
                                    <th width="20%">Detail Issue</th>
                                    <th width="18%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($delivery as $row)
                                <tr>
                                    <td>{{$row->get_project->name}}</td>
                                    <td>
                                        @if ($row->level == 'major')
                                        <strong class="badge badge-danger">Major</strong>
                                        @elseif($row->level == 'moderate')
                                        <strong class="badge badge-primary">Moderate</strong>
                                        @else 
                                        <strong class="badge badge-warning">Minor</strong>
                                        @endif
                                    </td>
                                    <td class="none">
                                        @if ($row->level == 'major')
                                        Major
                                        @elseif($row->level == 'moderate')
                                        Moderate
                                        @else
                                        Minor
                                        @endif
                                    </td>
                                    <td>
                                        @if ($row->status == 'open')
                                        <strong class="badge badge-warning">Open</strong>
                                        @else
                                        <strong class="badge badge-success">Closed</strong>
                                        @endif
                                    </td>
                                    <td class="none">
                                        @if ($row->status == 'open')
                                        Open
                                        @else
                                        Closed
                                        @endif
                                    </td>
                                    <td>{{$row->master_activity}}</td>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->issue}}</td>
                                    <td>
                                        <?php
                                            $date = date_create($row->date_issue);
                                            echo date_format($date, "j F Y");
                                            ?>
                                    </td>
                                    <td class="none">
                                        <?php
                                            date_create($row->date_issue);
                                            $string =  date_format($date, "j F Y");
                                            if (stristr($string, 'January') !== FALSE) {
                                                echo 'January';
                                            } else if (stristr($string, 'February') !== FALSE) {
                                                echo 'February';
                                            } else if (stristr($string, 'March') !== FALSE) {
                                                echo 'March';
                                            } else if (stristr($string, 'April') !== FALSE) {
                                                echo 'April';
                                            } else if (stristr($string, 'May') !== FALSE) {
                                                echo 'May';
                                            } else if (stristr($string, 'June') !== FALSE) {
                                                echo 'June';
                                            } else if (stristr($string, 'July') !== FALSE) {
                                                echo 'July';
                                            } else if (stristr($string, 'August') !== FALSE) {
                                                echo 'August';
                                            } else if (stristr($string, 'September') !== FALSE) {
                                                echo 'September';
                                            } else if (stristr($string, 'October') !== FALSE) {
                                                echo 'October';
                                            } else if (stristr($string, 'November') !== FALSE) {
                                                echo 'November';
                                            } else if (stristr($string, 'December') !== FALSE) {
                                                echo 'December';
                                            }
                                            ?>
                                    </td>
                                    <td>
                                        <a href="{{ route('issues.show', $row->id)}}">
                                        <button class="btn btn-icon btn-outline-info btn-sm" type="button">
                                        <i class="fas fa-arrow-right"></i> Read More
                                        </button>
                                        </a>
                                    </td>
                                    <td>
                                        @if($row->status == 'closed')
                                        <a href="#">
                                        <button class="btn btn-outline-secondary btn-sm" type="button">
                                        Issue Closed
                                        </button>
                                        </a>
                                        @elseif(Auth::user()->id == $row->created_by)
                                        <form action="{{ route('issues.destroy', $row->id)}}" method="post" id="confirm_delete">
                                            @csrf
                                            @method('DELETE')
                                            <a onclick="return confirm('Apakah anda yakin menghapus data?')">
                                            <button class="btn btn-icon btn-danger" type="submit">
                                            <i class="fas fa-trash"></i>
                                            </button>
                                            </a>
                                            <a href="{{ route('issues.edit', $row->id)}}">
                                            <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>
                                            </button>
                                            </a>
                                        </form>
                                        @else 
                                        <a href="#">
                                        <a href="{{ route('issues.edit', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                        <i class="fas fa-edit"></i>
                                        </button>
                                        </a>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End Issue For Admin -->
            @endif
        </div>
    </div>
</div>
<script type="text/javascript">
   
</script>


@endsection
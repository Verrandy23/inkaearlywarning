@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header text-center">
    <a href="/issues"><button class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i> Back To Issue </button> </a>
    @foreach($issue as $row)
    <div class="section-header-breadcrumb">
        <div class="text-right">
            <button class="btn btn-info btn-sm"><i class="fa fa-history"></i> 
            Created By {{$row->pic_name}} At 
            <?php
                $date = date_create($row->date_issue);
                echo date_format($date, "j F Y");
            ?>, Finished At 
            <?php
                if(!empty($row->finished_at)){
                $date = date_create($row->finished_at);
                echo date_format($date, "j F Y");
                }else{
                    echo '-';
                }
            ?> 
            @if(!empty($row->finished_by))
            By {{$row->finished_name}}
            @else
            @endif

            </button>
        </div>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <div class="card-body">
                <h4 class="titleC text-center">{{$row->name}} Project {{$row->project_name}}</h4>
                
                <hr>
                <b>Status :  
                    @if ($row->status == 'open')
                    <strong class="badge badge-warning">Open</strong>
                    @else
                    <strong class="badge badge-success">Closed</strong>
                    @endif
                </b>
                <b style="float:right"> Level :
                    @if ($row->level == 'major')
                    <strong class="badge badge-danger">Major</strong>
                    @elseif($row->level == 'moderate')
                    <strong class="badge badge-primary">Moderate</strong>
                    @else 
                    <strong class="badge badge-secondary">Minor</strong>
                    @endif
                </b>
                <br><br>


            @if(!empty($row->files))
            <center>
                
                 @if(strpos($row->files, ".pdf") || strpos($row->files, ".doc") || strpos($row->files, ".docx"))
                  
                      <a href="/issue/{{$row->files}}" style="font-size:20pt;">
                        Download Files
                        <i class="fa fa-download"></i>
                      </a>
                  @else
                  <a href="/issue/{{$row->files}}" target="_blank">
                    <img width="450px" height="300px" src="{{ asset('issue/' . $row->files) }}"></a>
                  @endif              
            </center>
            <br>
            @else
            @endif
            <b>Permasalahan : </b> <p>{{$row->issue}}</p>  
            <b>Tindak Lanjut :</b> <p>{{$row->action}}</p>
            </div>
        </div>
    @endforeach    
    </div>
</div>
@endsection
<!DOCTYPE html>
<html>
<head>
	<title>Report Issues</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Report Issues Divisi Teknologi</h4>
	</center><br>

    <div class="table-responsive">
        <table class="table col-lg-12 zero-configuration" id="exampledr">
            <thead>
                <tr>
                    <th>Project</th>
                    <th>Level</th>
                    <th>Status</th>
                    <th>Activity</th>
                    <th>Created By</th>
                    <th>Isu</th>
                    <th>Permasalahan</th>
                    <th>Tanggal Isu</th>
                    <th>Tindak Lanjut</th>
                    <th>Finished By</th>
                    
                </tr>
            </thead>
            <tbody>
                @foreach($teknologi as $row)
                <tr>
                    <td>{{$row->project_name}}</td>  
                    <td>
                        @if ($row->level == 'major')
                        <strong class="badge badge-danger">Major</strong>
                        @elseif($row->level == 'moderate')
                        <strong class="badge badge-primary">Moderate</strong>
                        @else 
                        <strong class="badge badge-warning">Minor</strong>
                        @endif
                    </td>      
                    <td>
                        @if ($row->status == 'open')
                        <strong class="badge badge-warning">Open</strong>
                        @else
                        <strong class="badge badge-success">Closed</strong>
                        @endif
                    </td>
                    <td>{{$row->master_activity}}</td>
                    <td>{{$row->pic_name}}</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->issue}}</td>
                    <td>
                        <?php
                        $date = date_create($row->date_issue);
                        echo date_format($date, "j F Y");
                        ?>
                    </td>
                    <td>{{$row->action}}</td>
                    <td>{{$row->finished_name}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</body>
</html>
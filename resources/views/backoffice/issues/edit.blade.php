@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header text-center">
    <h1 class="titleC">Form Issue</h1>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <div class="card-body">
                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ route('issues.update',$issue->id) }}">
                    @method('PATCH')
                    @csrf
                    @if ($errors->any())
                    <div class="col-md-10">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                    @if(Auth::user()->id == $issue->created_by)
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Nama Issue <span class="text-danger">*</span></label>
                            <input type="text" name="name" value="{{$issue->name}}" class="form-control" placeholder="Masukkan nama issue" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputState">Project <span class="text-danger">*</span></label>
                            <select class="form-control" name="project_id" id="project" required>
                                <option selected>Pilih Project</option>
                                @foreach($project as $row)
                                <option value="{{$row->id}}" {{ $issue->get_project->id == $row->id ? 'selected' : '' }}>{{$row->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Permasalahan <span class="text-danger">*</span></label>
                        <textarea style="min-height: 100px" class="form-control" placeholder="Permasalahan" name="issue" required>{{$issue->issue}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Tindak Lanjut</label>
                        <textarea style="min-height: 100px" class="form-control" placeholder="Tindak Lanjut" name="action">{{$issue->action}}</textarea>
                    </div>
      
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Image <span class="text-danger">*</span></label>
                            <input type="file" name="image" value="{{$issue->files}}" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputState">Level <span class="text-danger">*</span></label>
                            <select class="form-control" name="level" required>
                                <option selected>Pilih Level</option>
                                <option value="major" {{$issue->level == 'major' ? 'selected' : ''}}>Major</option>
                                <option value="moderate" {{$issue->level == 'moderate' ? 'selected' : ''}}>Moderate</option>
                                <option value="minor" {{$issue->level == 'minor' ? 'selected' : ''}}>Minor</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        @if (Auth::user()->division_name == 'Keproyekan')
                        <div class="form-group col-md-6">   
                            <label for="inputState">Division <span class="text-danger">*</span></label>
                            <select name="division_hidden" class="form-control" id="division" required>
                                @foreach($division as $row)
                                    @if($row->project_id == $issue->project_id)
                                        

                                        <option value='{{ $row->id }}'
                                            {{ $issue->division_name == $row->name ? 'selected' : ''}}
                                        > {{$row->name}}
                                        </option>
                                    @endif
                                @endforeach
                                <!--
                                <option value="Teknologi" {{$issue->division_name == 'Teknologi' ? 'selected' : ''}}>Teknologi</option>
                                <option value="Logistik" {{$issue->division_name == 'Logistik' ? 'selected' : ''}}>Logistik</option>
                                <option value="Fabrikasi" {{$issue->division_name == 'Fabrikasi' ? 'selected' : ''}}>Fabrikasi</option>
                                <option value="Finishing" {{$issue->division_name == 'Finishing' ? 'selected' : ''}}>Finishing</option>
                                <option value="Testing" {{$issue->division_name == 'Testing' ? 'selected' : ''}}>Testing</option>
                                <option value="Delivery" {{$issue->division_name == 'Delivery' ? 'selected' : ''}}>Delivery</option>
                                -->
                            </select>
                            <input type="hidden" name="division_name" id="division_name" value="{{$issue->division_name}}">
                        </div>
                        @else
                        <input type="hidden" name="division_name" value="{{$issue->division_name}}">
                        <input type="hidden" name="date_issue" value="{{$issue->date_issue}}">
                        <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
                        @endif
                        
                        <div class="form-group col-md-3">
                            <label for="inputState">Master Activity <span class="text-danger">*</span></label>
                            <select class="form-control" name="status" id="master_activity" name="master_activity" required>

                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputState">Status <span class="text-danger">*</span></label>
                            <select class="form-control" name="status" required>
                                <option selected>Pilih Status</option>
                                <option value="open" {{$issue->status == 'open' ? 'selected' : ''}}>Open</option>
                                <option value="closed" {{$issue->status == 'closed' ? 'selected' : ''}}>Closed</option>
                            </select>
                        </div>
                        <input type="hidden" name="finished_by" value="{{Auth::user()->id}}">
                        @else
                        <div class="form-group">
                            <label for="inputState">Status <span class="text-danger">*</span></label>
                            <select class="form-control" name="status" required>
                                <option selected>Pilih Status</option>
                                <option value="open" {{$issue->status == 'open' ? 'selected' : ''}}>Open</option>
                                <option value="closed" {{$issue->status == 'closed' ? 'selected' : ''}}>Closed</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Tindak Lanjut</label>
                            <textarea style="min-height: 100px" class="form-control" placeholder="Tindak Lanjut" name="action">{{$issue->action}}</textarea>
                        </div>
                        <input type="hidden" name="finished_by" value="{{Auth::user()->id}}">

                        @endif
                    </div>
                    <div class="form-group mb-0">
                        <input type="submit" class="btn btn-primary" value="Update"></input>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

    var x = {}
    $(document).ready(function () {
        $.ajax({
                type: 'GET',
                url: '/'+$('#project').val()+'/divisions',
                success: function(data){
                    $.each(data.divisions, function(key, value){
                        x[value.id] = value.name;
                        console.log(value.id);
                    })

                   
                }
            });

                $.ajax({
                        type: 'GET',
                        url: '/'+$('#division').val()+'/master_activity',
                        success: function(data){
                            $.each(data.master_activity, function(key, value){
                                $('#master_activity').append($('<option></option>').attr('value', value.id).text(value.name).attr('alt', value.id));
                            })
                        }

                    });

        $('#project').on('change', function() {
            $.ajax({
                type: 'GET',
                url: '/'+this.value+'/divisions',
                success: function(data){
                    $.each(data.divisions, function(key, value){
                        $('#division').append($('<option></option>').attr('value', value.id).text(value.name));
                        x[value.id] = value.name;
                    })

                    $.ajax({
                        type: 'GET',
                        url: '/'+$('#division').val()+'/master_activity',
                        success: function(data){
                            $.each(data.master_activity, function(key, value){
                                $('#master_activity').append($('<option></option>').attr('value', value.id).text(value.name).attr('alt', value.id));
                            })
                        }

                    })
                    $('#master_activity').find('option').remove();
                }
            });
            $('#division').find('option').remove();
            x = {}
            
        });

        $('#division').on('change', function(){
            $('#division_name').val(x[this.value]);
            console.log(x[this.value]);
            $.ajax({
                type: 'GET',
                url: '/'+this.value+'/master_activity',
                success: function(data){
                    $.each(data.master_activity, function(key, value){
                        $('#master_activity').append($('<option></option>').attr('value', value.id).text(value.name));
                    })
                }
            })
            $('#master_activity').find('option').remove();
        });
        
    })
</script>
@endsection
<!DOCTYPE html>
<html>
<head>
	<title>Report Master Activity</title>
    <br>
</head>
<body>
	<style type="text/css">
	 
    table tr td{
        text-align: center;
        border: 1px solid black;
    },
    table{
        width: 100%;
    }
    table th{
        border:2px solid blue;
      font-size: 12pt;
      background: #fff000;
      
    }
  
	</style>
	<center>
		<h5>Data Report Master Activity</h4>
	</center><br>
    <div class="table-responsive">
        <table class="table col-lg-12 zero-configuration" id="exampledr">
            <thead>
                <tr>
                    
                    <th>Name</th>
                    <th>Division</th>
                </tr>
            </thead>
            <tbody>
                
                @foreach($activities as $index => $row)
                <tr>
                    <td>{{$row->name}}</td>
                    <td>{{$row->get_division->name}}</td>

                </tr>
                @endforeach

            </tbody>
        </table>
    </div>

</body>
</html>
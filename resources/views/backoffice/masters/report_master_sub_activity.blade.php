<!DOCTYPE html>
<html>
<head>
	<title>Report Master Sub Activity</title>
    <br>

</head>
<body>
	<style type="text/css">
	 
    table tr td{
        text-align: center;
        border: 1px solid black;
    },
    table{
        width: 100%;
    }
    table th{
        border:2px solid blue;
      font-size: 12pt;
      
    }

	</style>
	<center>
		<h5>Data Report Master Sub Activity</h4>
	</center><br>
    <div class="table-responsive">
        <table class="table col-lg-12 zero-configuration" id="exampledr">
            <thead>
                <tr>
                   
                    <th>Name</th>
                    <th>Activities</th>
                </tr>
            </thead>
            <tbody>

                  @foreach($sub_activities as $index => $row)
                        <tr>
                          @if($row->get_activity !== null)
                            <td>{{$row->name}}</td>
                            <td>{{$row->get_activity->name}}</td>
                          @endif
                        </tr>
                        @endforeach


            </tbody>
        </table>
    </div>

</body>
</html>
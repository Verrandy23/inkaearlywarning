@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">Master Data Sub Activities </h1>

</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-6 col-12">
                    <!-- Filter Data -->
                <table>
                    <tr>
                        <label style="margin-left:1%" for="filter">Filter By Activities</label>
                        <th class="filterhead none"></th><th class="filterhead none"></th>
                        <th class="filterhead"></th>

                    </tr>
                </table>
                <!-- End Filter Data -->
                </div>

                <div class="col-md-6 col-12">
                    <div class="section-header-breadcrumb">
                        <div class="bottom text-right">
                            <a href="/report_master_sub_activity" class="btn btn-primary" target="_blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</a>
                        </div>
                    </div>
                </div>
            
            </div><br>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="exampledr">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Activities</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($sub_activities as $index => $row)
                        <tr>
                          @if($row->get_activity !== null)
                            <td>{{$row->name}}</td>
                            <td>{{$row->get_activity->name}}</td>
                          @endif
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
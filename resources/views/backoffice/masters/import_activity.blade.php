@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">Import From Excel</h1>

</div>
<div class="card top">
    <div class="card-body">
        @include('sweet::alert')
        <h1 class="titleC text-center">Import Data Activity </h1><br>
        <div class="container">
            <h1 class="titleC text-center">Division Teknologi, Logistik, Fabrikasi, Finishing, Testing  </h1>
            <hr>
            <div class="row">
                <div class="col-md-6 offset-3">
                    <form action="/storeExcelActivity" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">Import Activity With Full Project From Excel (.xls, .xlsx)</label>
                            <input type="file" class="form-control" name="file">
                            <p class="text-danger">{{ $errors->first('file') }}</p>
                            <button style="margin-top:-2%" class="btn btn-primary btn-sm"><i class="fa fa-upload"></i> Import</button></input>
                        </div>
                    </form>
                </div>
            </div>
           
            <hr><br>


        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

@endsection
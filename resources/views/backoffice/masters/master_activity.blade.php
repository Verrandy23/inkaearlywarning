@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="">Master Data Activities </h1>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-6 col-12">
                    <!-- Filter Data -->
                <table>
                    <tr>
                        <label style="margin-left:1%" for="filter">Filter By Division</label>
                        <th class="filterhead none"></th><th class="filterhead none"></th>
                        <th class="filterhead"></th>

                    </tr>
                </table>
                <!-- End Filter Data -->
                </div>

                <div class="col-md-6 col-12">
                    <div class="section-header-breadcrumb">
                        <div class="bottom text-right">
                            <a href="/report_master_activity" class="btn btn-primary" target="_blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</a>
                        </div>
                    </div>
                </div>
            
            </div><br>
                           

            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="exampledr">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Division</th>
                        </tr>
                    </thead>
                    <tbody>
                       
                        @foreach($activities as $index => $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->get_division->name}}</td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
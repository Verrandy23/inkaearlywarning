@extends('layouts.backend.masterPage')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />


<div class="section-header">
    <h1 class="titleC"> {{$work_activity->name}}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
            <a href="{{ URL::previous() }}" class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i> Back </a> </div> <br><br>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <h4 class="titleC text-center">Form Edit Sub Activity</h4><br>
        <div class="basic-form">
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            <form class="form-horizontal" method="post" action="{{ route('work_activities.update',$work_activity->id) }}">
                @method('PATCH')
                @csrf
                @if ($errors->any())
                <div class="col-md-10">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Name <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" placeholder="Input category name" name="name" value="{{$work_activity->name}}">
                    </div><br>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Start Date <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" placeholder="Input category name" name="start_date" value="{{$work_activity->start_date}}">
                    </div><br>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">End Date <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" name="end_date" value="{{$work_activity->end_date}}">
                    </div><br>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Slack <span class="text-danger">* <br> isi jika slack kosong,jika sudah terdapat slack maka tidak perlu mengisi slack sisten akan otomatis mengupdate berdasarkan update tanggal start date dan end date.</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" name="slack" placeholder="{{$work_activity->slack}}">
                    </div><br>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">PIC 
                    </label>
                    <div class="col-sm-9">
                    <select id="select-state" placeholder="Pilih PIC..." name="pic">
                    <option value="">Pilih Pic...</option>
                    @foreach($pic as $key => $name)
                    <option value="{{ $key }}">{{ $name }}</option>
                    @endforeach
                    </select>
                    </div><br>
                </div>

                <div class="form-group row">
                    <div class="col-sm-3">
                        <p></p>
                    </div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn mb-1 btn-primary">Update</span>
                        </button>
                    </div>
                    <input type="hidden" value="{{ URL::previous() }}" name="url">
                </div>

            </form>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {
      $('select').selectize({
          sortField: 'text'
      });
  });
</script>
@endsection
@extends('layouts.backend.masterPage')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

<div class="section-header">
    @foreach($general as $row)
    <h1 class="titleC">{{$row->project_name}}</h1>

    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
            <ol style="background:#38ada9" class="breadcrumb text-white-all">
                <li class="breadcrumb-item"><a href="/backoffice"><i class="fas fa-tachometer-alt"></i> Home</a></li>
                <li class="breadcrumb-item"><a href="/dashboard_divisions/{{$row->project_id}}"> Dashboard Division</a></li>
                <li class="breadcrumb-item"><a href="/divisions/{{$row->division_id}}"> Activity</a></li>
                <li class="breadcrumb-item"><a href="/activities/{{$row->activity_id}}"> Sub Activity</a></li>
            </ol>
        </div><br>
    </div>
</div>
<div class="card top" id="app1">
    <div class="card-body">
        <div class="col-lg-12">
            <h6 class="titleC text-center">{{$sub_activity->name}} - {{$row->project_name}}</h6><br>
            @endforeach

            <div style="margin-right:3%" class="section-header-breadcrumb">
                <div class="bottom">
                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                     + Add Work Activity
                    </button>
                </div>
            </div>


                            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="false" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Activity</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                     <form class="form-class" id="form" method="post" action="{{ route('work_activities.store') }}">
                            @csrf
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" data-form="input nama" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label>Start Date</label>
                        <input type="date" data-form="input tanggal start" class="form-control" id="start" name="start_date">
                    </div>
                    <div class="form-group">
                        <label>End Date</label>
                        <input type="date" data-form="input tanggal selesai" class="form-control" id="end" name="end_date">
                    </div>
                    <div class="form-group">
                        <label>Slack</label>
                        <input type="number" value="0" class="form-control" name="slack">
                    </div>
                    <div class="form-group">
                        <label>PIC</label>
                        <select id="select-state" data-form="input PIC" placeholder="Pilih PIC..." name="pic">
                            <option value="">Pilih Pic...</option>
                            @foreach($pic as $key => $name)
                            <option value="{{ $key }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input hidden type="text" value="{{$sub_activity_id}}" class="form-control" name="sub_activity_id">
                    </div>
                </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="save" class="btn btn-primary">Save changes</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>


            <!-- Form Modal Work Activity 
            <form class="modal-part" id="modal-login-part" method="post" action="{{ route('work_activities.store') }}">
                @csrf
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div class="form-group">
                    <label>Start Date</label>
                    <input type="date" class="form-control" id="start" name="start_date">
                </div>
                <div class="form-group">
                    <label>End Date</label>
                    <input type="date" class="form-control" id="end" name="end_date">
                </div>
                <div class="form-group">
                    <label>Slack</label>
                    <input type="number" class="form-control" name="slack">
                </div>
                <div class="form-group">
                    <label>PIC</label>
                    <select id="select-state" placeholder="Pilih PIC..." name="pic">
                        <option value="">Pilih Pic...</option>
                        @foreach($pic as $key => $name)
                        <option value="{{ $key }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input hidden type="text" value="{{$sub_activity_id}}" class="form-control" name="sub_activity_id">
                </div>         
               
                </form>
             End Form Modal Work Activity -->

            <div class="table-responsive">
                @include('sweet::alert')

                <table class="table col-lg-12 zero-configuration" id="table1">
                    <thead>
                        <tr>
                            @if(Auth::user()->role_id != 1)
                            <th width="1%">No.</th>
                            <th width="29%">Name</th>
                            <th width="5%">Belum</th>
                            <th width="5%">Proses</th>
                            <th width="5%">Finish</th>
                            <th width="15%">Start Date</th>
                            <th width="15%">End Date</th>
                            <th width="20%">PIC</th>
                            <th width="20%">Slack</th>
                            <th width="20%">Add PIC</th>
                            @else
                            <th width="1%">No.</th>
                            <th width="20%">Name</th>
                            <th width="3%">Belum</th>
                            <th width="3%">Proses</th>
                            <th width="3%">Finish</th>
                            <th width="10%">Start Date</th>
                            <th width="10%">End Date</th>
                            <th width="15%">PIC</th>
                            <th width="15%">Slack</th>
                            <th width="20%">Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($progress as $index => $row)
                        @if (!empty($row->id))
                        <tr>
                            <td>{{$index+1}}</td>
                            <td>
                                @if($row->jumlah != null || $row->total != 0)
                                <?php
                                $jumlah = $row->jumlah;
                                $total = $row->total;
                                $hasil = $jumlah / $total * 100;
                                ?>
                                <h6 class="title"> {{$row->name}} <span id="progress{{$index}}" class="pull-right"><?php echo floor($hasil) ?>%</span></h6>
                                <div class="progress mb-3" style="height: 7px">
                                    <div class="progress-bar bg-primary" id="progress_bar{{$index}}" style="width: <?php echo floor($hasil) ?>%;" role="progressbar">
                                    </div>
                                </div>

                                @else>
                                <h6 class="title"> {{$row->name}} <span class="pull-right">0%</span></h6>
                                <div class="progress mb-3" style="height: 7px">
                                    <div class="progress-bar bg-primary" style="width: 0%;" role="progressbar">
                                    </div>
                                </div>
                                @endif
                            </td>
                            <td>
                                <!-- belum -->
                                <input type="hidden" value="{{$row->id}}" name="idwork" id="idwork{{$index}}">
                                        
                                <input type="hidden" name="status_name" id="status_belum{{$index}}" value="belum" data-status="0">

                                <!-- finish -->
                                <input type="hidden" name="status_finish" id="status_finish{{$index}}" data-status="1" data-late="{{$row->late}}" value="finish">


                                @if($row->status == '0')

                                    <input name="belum" id="belum{{$index}}" type="checkbox" v-model="action" class="checkbox-custom checkSingle" data-id="{{$row->id}}" checked>
                                        
                                    </input>

                                    @else

                                        
                                        @csrf

                                        <input type="checkbox" name="belum" class="checkbox-custom checkSingle" data-id="{{$row->id}}" id="belum{{$index}}" value="0"></input>
                               

                                 @endif
                            </td>

                            <td>
                                    @if($row->status >= '0.1' && $row->status <= '0.95' ) 

                                       <input name="finish" id="proses{{$index}}" type="checkbox" v-model="action" class="checkbox-custom" data-index="{{$index}}" data-id="{{$row->id}}" checked></input>
                                    
                                    @else
                                        <input name="proses" data-id="{{$row->id}}"  id="proses{{$index}}" v-model="action" type="checkbox" data-index="{{$index}}" class="checkbox-custom checkSingle"></input>
                                    @endif
                            </td>

                            <td>
                                @if($row->status == '1')

                                <input id="finish{{$index}}" name="finish" type="checkbox" v-model="action" data-id="{{$row->id}}" class="checkbox-custom" checked></input>

                                @else

                               

                                    <input  name="finish" type="checkbox" class="checkbox-custom" name="cek" id="finish{{$index}}" data-id="{{$row->id}}" value="1">
                                        
                                    </input>
                               

                                @endif
                            </td>
                            <td>
                                <?php
                                $date = date_create($row->start_date);
                                echo date_format($date, "j F Y");
                                ?>
                            </td>
                            <td>
                                <?php
                                $date = date_create($row->end_date);
                                echo date_format($date, "j F Y");
                                ?>
                            </td>
                            @if(!empty($row->pic_name))
                            <td>{{$row->pic_name}}</td>
                            @else
                            <td> - </td> 
                            @endif
                            @if(!empty($row->slack))
                            <td>{{$row->slack}} Days</td>
                            @else
                            <td> - </td> 
                            @endif
                            <td>
                                @if(!empty($row->id))
                                <?php if (Auth::user()->role_id != 1) { ?>
                                    <a href="{{ route('work_activities.show', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </a>
                                <?php } else { ?>
                                    <form action="{{ route('work_activities.destroy', $row->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <div class="row">

                                            <a onclick="return confirm('Are you sure to delete this data?')">
                                                <button class="btn btn-icon btn-danger" type="submit">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </a>

                                            <a style="margin-left:5px" href="{{ route('work_activities.edit', $row->id)}}">
                                                <button class="btn btn-icon btn-info" type="button">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </a>

                                          
                                        <?php } ?>
                                        </div>
                                    </form>

                                    <!-- If Data Null-->
                                    @else
                                    <form>
                                        @if(Auth::user()->role_id != 1)
                                        @else
                                        <a href="#">
                                            <button class="btn btn-icon btn-danger" type="submit">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </a>

                                        <a href="#">
                                            <button style="margin-top:5%" class="btn btn-icon btn-info" type="button">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </a>
                                        @endif
                                    </form>
                                    @endif
                            </td>
                        </tr>
                        @else 
                        <tr>
                        <td> - </td>
                        <td> - </td>
                        <td> - </td>
                        <td> - </td>
                        <td> - </td>
                        <td> - </td>
                        <td> - </td>
                        <td> - </td>
                        <td> - </td>
                        <td> - </td>
                        
                        </tr>
                        @endif
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <span> </span>
</div>


<script type="text/javascript">
   
</script>

<script type="text/javascript">

 $('input.belum').on('change', function() {
        $('input.belum').not(this).prop('checked', false);
    })

   /* function onClickBelum(id) {
        var idw = $('#idwork' + id).val();
        $.ajax({
            type: "POST",
            url: "/update",
            data: $('#myForm' + idw).serialize(),
            success: function() {
                Swal.fire({
                        icon: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                });
                setTimeout(() => {location.reload();}, 1000)
            }
        });
    }

     function onClickSelesai(id) {
        var idw = $('#idwork' + id).val();
        $.ajax({
            type: "POST",
            url: "/update",
            data: $('#myFormselesai' + idw).serialize(),
            success: function() {
                Swal.fire({
                        icon: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                });
                setTimeout(() => {location.reload();}, 1000)
            }
        });
    } */

/*
    function onClickTest(id) {
        var Id = id;
        $(".modal-body #workId").val(Id);
        $('#progressModal').modal();
    }
    */

    function klikPic(id) {
        var Id = id;
        $(".form-group #workId").val(Id);
        $('#modal10').modal();
    }

    $(document).ready(function () {
      $('select').selectize({
          sortField: 'text'
      });
  });

    $(document).ready(function(){
    var $start = ''
    var $end = ''
    $('#start').on('change', function(){
        $start = new Date(String($('#start').val()));
    });
    
    $('#end').on('change', function() {
        $end = new Date(String($('#end').val()));
        if ($start>$end){
            
        swal({
          title: "Info !",
          text: "Please check start date & end date",
          icon: "info",
          button: "I'am Understand",
        });
        
        $('#end').val('yyy-mm-dd');
        }
    });
   
    }) 
    
/*var app = new Vue({
    el: "#app1",
    delimiters: ["<<", ">>"],
    data: {
        action: ""
    },
    methods: {
        onBelum: function(event, value){
            console.log(event.target);
        }
    }

})*/


var len = document.getElementsByName("belum");

for(let i=0; i<len.length; i++){
    document.getElementById('belum' + i).addEventListener('click', function(){
        if (this.checked){
            var $belum = document.getElementById('status_belum' + i);
            
            var $status = $belum.dataset.status;
            var $status_name = $belum.value;
            
            axios.post('/update/'+this.dataset.id, {
                 status_name : $status_name,
                status: $status,
              })
              .then(function (response) {
                var $data = response.data;
                $data = parseInt($data.data)*100;

                // untuk progress
                document.getElementById('progress' + i).innerHTML = $data + "%";

                // untuk progress bar
                document.querySelector('#progress_bar' + i).style.width=$data+"%";
                swal({
                    title: "Success!",
                    text: "data telah terupdate",
                    icon: "success",
                });
                document.getElementById('finish' + i).checked = false;
                document.getElementById('proses' + i).checked = false;
              })
              .catch(function (error) {
                console.log(error);
              });
                        
        }
    });

    document.getElementById('finish' + i).addEventListener('click', function(){
        if (this.checked){
            var $finish = document.getElementById('status_finish' + i);
            
            var $late = $finish.dataset.late;
            var $status_name = $finish.value;
            var $status = $finish.dataset.status;
            
            axios.post('/update/'+this.dataset.id, {
                 status_name : $status_name,
                late: $late,
                status: $status,
              })
              .then(function (response) {
                var $data = response.data;
                $data = parseInt($data.data)*100;
                document.getElementById('progress' + i).innerHTML = $data + "%";

                document.querySelector('#progress_bar' + i).style.width=$data+"%";
                
                swal({
                    title: "Success!",
                    text: "data telah terupdate",
                    icon: "success",
                });
                document.getElementById('belum' + i).checked = false;
                document.getElementById('proses' + i).checked = false;

              })
              .catch(function (error) {
                console.log(error);
              });
        }
    });


    document.getElementById('proses' + i).addEventListener('click', function(){
        if(this.checked == true){
            document.getElementById('belum' + i).checked = false;
            document.getElementById('finish' + i).checked = false;
           
            var $proses = $(".modal-body #workId");
            $proses.val(this.dataset.id);
            $proses.attr('data-index', this.dataset.index);

            $('#progressModal').modal();
        }
     });

}

document.getElementById('save').addEventListener('click', event=>{
    var $input = document.querySelectorAll('#form input');
    var $select = document.querySelectorAll('#form select');


    var $errors = []
    var error = false;
    // chek input empty
    for(let i=0; i<$input.length; i++){

        // check input if emtpy
        if($input[i].value == ""){
            var $input_error = $input[i].dataset.form;
            if ($input_error !== undefined){
                $errors.push($input_error);
                error = true;
            }
        }


    }

    //check select empty
    for(let i=0; i<$select.length; i++){
        const $error_name = $select[i].dataset.form;

        if($select[i].value == ""){
            if($error_name !== undefined){
                $errors.push($error_name);
                error =true;
            }
        }
    }

    var text_error = "";

    for(let i=0; i<$errors.length; i++){
        text_error+= "<p class='text-danger'> form " + $errors[i] + " tidak boleh kosong</p>";
    }

    var div_error = document.createElement("div");
    div_error.innerHTML = text_error;
    if (error){
        swal({
            title: 'Error !',
            content: div_error,
            icon: 'error',
        });
    }
    else{
        document.querySelector('#form').submit();
    }
})


</script>



@endsection
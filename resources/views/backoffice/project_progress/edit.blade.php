@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC"></h1>
    <div class="section-header-breadcrumb">
        <a class="btn btn-primary btn-sm" href="{{ URL::previous() }}"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <h6 class="titleC text-center">Form Edit Project Progress Bulanan</h6><br>
            <form method="POST" action="{{ route('projects_progress.update',$project_progress->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Progress Rencana %') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="number" step=".01" min="0" max="100" value="{{$project_progress->progress_rencana}}" class="form-control @error('name') is-invalid @enderror" name="progress_rencana" required>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Progress Realisasi %') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="number" step=".01" min="0" max="100" value="{{$project_progress->progress_realisasi}}" class="form-control @error('name') is-invalid @enderror" name="progress_realisasi" required>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>


                <div class="form-group row">
                    <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('Project') }}</label>
                    <div class="col-md-6">
                        <select class="form-control @error('project_id') is-invalid @enderror" name="project_id">
                            <?php if (empty($project_progress->project_id)) { ?>
                                <option value="" selected>Pilih Project</option>
                            <?php } else { ?>
                                @foreach($project as $row)
                                <option value="{{$row->id}}" {{ $project_progress->get_project->id == $row->id ? 'selected' : '' }}>{{$row->name}}</option>
                                @endforeach
                            <?php } ?>
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="date" value="{{$project_progress->date}}" class="form-control @error('date') is-invalid @enderror" name="date" required>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Update') }}
                        </button>
                    </div>
                </div>

                <span class="text-info">* Data tidak boleh kosong, nilai yang dimasukkan minimal 0 maksimal 100</span>

            </form>

        </div>
    </div>
</div>

@endsection
@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC"></h1>
    <div class="section-header-breadcrumb">
        <a class="btn btn-primary btn-sm" href="{{ URL::previous() }}"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <h6 class="titleC text-center">Form Tambah Project Progress Bulanan</h6><br>
                <form method="post" action="{{ route('projects_progress.store') }}" id="form">
                    @csrf

                    <div class="form-group">
                        <label>Progress Rencana %</label>
                        <input required type="number" data-form="input progress rencana" step=".01" min="0" max="100" name="progress_rencana" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Progress Realisasi %</label>
                        <input required type="number" data-form="input progress realisasi" step=".01" min="0" max="100" name="progress_realisasi" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Project</label>
                        <select id="project_id" data-form="input project" class="form-control @error('project_id') is-invalid @enderror" name="project_id" required>
                        <option value="" selected>Pilih Project</option>
                        @foreach($project as $row)
                        <option value="{{$row->id}}">{{$row->name}}</option>
                        @endforeach
                    </select>
                    </div>

                    <div class="form-group">
                        <label>Date</label>
                        <input required type="date" data-form="input tanggal" name="date" class="form-control">
                    </div>

                    <div class="form-group">
                        <div>
                            <button type="button" id="save" class="btn btn-primary">
                                Save
                            </button>
                        </div>
                    </div>

                    <span class="text-info">* Data tidak boleh kosong, nilai yang dimasukkan minimal 0 maksimal 100</span>

                </form>

        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
$( "#progressForm" ).validate({
  rules: {
    field: {
      required: true,
      integer: true,
      range: [0, 100]
    }
  }
});
</script>

@endsection
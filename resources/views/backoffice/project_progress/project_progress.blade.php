@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">Manage Progress Project Bulanan</h1>
    <div style="margin-top:2%" class="section-header-breadcrumb">
        <div class="bottom text-right">
            <a href="/create_progress"><button class="btn btn-sm mb-1 btn-primary"><i class="fa fa-plus"></i> Add Project Progress</button></a>
        </div>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        @include('sweet::alert')

        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-12 col-12 col-sm-12">
                <!-- Filter Data -->
                <table>
                    <tr>
                        <label style="margin-left:1%" for="filter">Filter By Project</label>
                        <label style="margin-left:11%" for="filter">Filter By Date</label>
                        <label style="margin-left:3%" for="filter">Filter By Year</label>
                        <th class="filterhead none"></th>
                        <th style="width:200px" class="filterhead"></th>
                        <th class="filterhead none"></th>
                        <th class="filterhead none"></th>
                        <th class="filterhead none"></th>
                        <th class="filterhead"></th>
                        <th class="filterhead"></th>
                        
                    </tr>
                </table>
                <!-- End Filter Data -->
                </div>          
            </div><br>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="exampledr">
                    @if ($errors->any())
                    <div class="col-md-10">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Project</th>
                            <th>Progress Rencana</th>
                            <th>Progress Realisasi</th>
                            <th>Month</th>
                            <th>Year</th>
                            <th class="none">Date Filyer</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($project_progress as $index => $row)
                        <tr>
                            <td>{{$index+1}}.</td>
                            <td>{{$row->get_project->name}}</td>
                            <td>{{$row->progress_rencana}} %</td>
                            <td>{{$row->progress_realisasi}} %</td>
                            <td>
                                <?php
                                $date = date_create($row->date);
                                echo date_format($date, "F");
                                ?>
                            </td>
                           
                            <td class="none">
                                <?php
                                    date_create($row->date);
                                    $string =  date_format($date, "j F Y");
                                    if (stristr($string, 'January') !== FALSE) {
                                        echo 'January';
                                    } else if (stristr($string, 'February') !== FALSE) {
                                        echo 'February';
                                    } else if (stristr($string, 'March') !== FALSE) {
                                        echo 'March';
                                    } else if (stristr($string, 'April') !== FALSE) {
                                        echo 'April';
                                    } else if (stristr($string, 'May') !== FALSE) {
                                        echo 'May';
                                    } else if (stristr($string, 'June') !== FALSE) {
                                        echo 'June';
                                    } else if (stristr($string, 'July') !== FALSE) {
                                        echo 'July';
                                    } else if (stristr($string, 'August') !== FALSE) {
                                        echo 'August';
                                    } else if (stristr($string, 'September') !== FALSE) {
                                        echo 'September';
                                    } else if (stristr($string, 'October') !== FALSE) {
                                        echo 'October';
                                    } else if (stristr($string, 'November') !== FALSE) {
                                        echo 'November';
                                    } else if (stristr($string, 'December') !== FALSE) {
                                        echo 'December';
                                    }
                                    ?>
                            </td>
                             <td>
                                <?php
                                $date = date_create($row->date);
                                echo date_format($date, "Y");
                                ?>
                            </td>
                            <td>
                                <form id="delete{{$index}}" action="{{ route('projects_progress.destroy', $row->id)}}" method="post" id="confirm_delete">
                                    @csrf
                                    @method('DELETE')

                                    <a>
                                        <button name="button" class="btn btn-icon btn-danger" type="button">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </a>

                                    <a href="{{ route('projects_progress.edit', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </a>

                                </form>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

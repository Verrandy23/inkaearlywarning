@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">Detail Activities</h1>
    <div class="section-header-breadcrumb">
        <a class="btn btn-primary btn-sm" href="{{ URL::previous() }}"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="table1">
                    <thead>
                        <tr>
                            <th width="15%">Project</th>
                            <th width="10%">Division</th>
                            <th width="20%">Activity</th>
                            <th width="25%">Sub Activity</th>
                            <th width="15%">PIC</th>
                            <th width="10%">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($detailPieChart as $row)
                        <tr>
                            <td>{{$row->project_name}}</td>
                            <td>{{$row->division_name}}</td>
                            <td>{{$row->activity_name}}</td>
                            <td>{{$row->sub_name }} - {{$row->work_name}}</td>
                            <td>{{$row->pic}}</td>
                            <td>
                                <?php if ($row->status == 'belum') { ?>
                                    <strong class="badge badge-primary">Belum</strong>
                                <?php } else if ($row->status == 'proses') { ?>
                                    <strong class="badge badge-warning">Proses</strong>
                                <?php } else { ?>
                                    <strong class="badge badge-success">Selesai</strong>
                                <?php } ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
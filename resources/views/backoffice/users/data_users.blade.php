@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">Manage Data User</h1>
</div>
<div class="card top">
    <div class="card-body">
        @include('sweet::alert')

        <div class="col-lg-12">

            <div class="table-responsive">
                <table class="table col-lg-12" id="table1">
                    <thead>
                        <tr>
                            <th width="20%">NIP</th>
                            <th width="10%">Name</th>
                            <th width="10%">Division</th>
                            <th width="10%">Email</th>
                            <th width="10%">Telephone</th>
                            <th width="10%">Role</th>
                            <th width="30%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <div class="bottom text-right">
                            <a href="/registration">
                                <button class="btn btn-sm mb-1 btn-primary"><i class="fa fa-plus"></i> Add User</button>
                            </a>
                        </div>
                        @foreach($user as $index => $row)
                        <tr>
                            <td>{{$row->nip}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->division_name}}</td>
                            <td>{{$row->email}}</td>
                            <td>{{$row->phone}}</td>
                            <td>{{$row->get_role->name}}</td>
                            <td>
                                <form id="form{{$index}}" action="{{ route('auths.destroy', $row->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')

                                    <a >
                                        <button name="button" class="btn btn-icon btn-danger" type="button">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </a>

                                    <a href="{{ route('auths.edit', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </a>

                                </form>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC"></h1>
    <div class="section-header-breadcrumb">
        <a class="btn btn-primary btn-sm" href="{{ URL::previous() }}"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <h6 class="titleC text-center">Form Edit User</h6><br>
            <form method="POST" action="{{ route('auths.update',$user->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" value="{{$user->name}}" class="form-control @error('name') is-invalid @enderror" name="name" required>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                    <div class="col-md-6">
                        <input id="username" type="text" value="{{$user->username}}" class="form-control @error('username') is-invalid @enderror" name="username" required>
                        @error('username')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nip" class="col-md-4 col-form-label text-md-right">{{ __('NIP') }}</label>

                    <div class="col-md-6">
                        <input id="nip" value="{{$user->nip}}" type="text" class="form-control @error('nip') is-invalid @enderror" name="nip" required>

                        @error('nip')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>
                    <div class="col-md-6">
                        <input id="phone" value="{{$user->phone}}" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" required>

                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>
                    <div class="col-md-6">
                        <select id="role_id" class="form-control @error('role_id') is-invalid @enderror" name="role_id" required>
                            @foreach($role as $row)
                            <option value="{{$row->id}}" {{ $user->get_role->id == $row->id ? 'selected' : '' }}>{{$row->name}}</option>
                            @endforeach
                        </select>
                        @error('role_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('Division') }}</label>
                    <div class="col-md-6">
                        <select id="division_id" class="form-control @error('dvision_id') is-invalid @enderror" name="division_name" required>
                            <option value="Teknologi" {{$user->division_name == 'Teknologi' ? 'selected' : ''}}>Teknologi</option>
                            <option value="Logistik" {{$user->division_name == 'Logistik' ? 'selected' : ''}}>Logistik</option>
                            <option value="Fabrikasi" {{$user->division_name == 'Fabrikasi' ? 'selected' : ''}}>Fabrikasi</option>
                            <option value="Finishing" {{$user->division_name == 'Finishing' ? 'selected' : ''}}>Finishing</option>
                            <option value="Testing" {{$user->division_name == 'Testing' ? 'selected' : ''}}>Testing</option>
                            <option value="Delivery" {{$user->division_name == 'Delivery' ? 'selected' : ''}}>Delivery</option>
                            <option value="Keproyekan" {{$user->division_name == 'Keproyekan' ? 'selected' : ''}}>Keproyekan</option>
                        </select>
                        @error('role_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('Project To PM') }}</label>
                    <div class="col-md-6">
                        <select class="form-control @error('project_id') is-invalid @enderror" name="project_to_pm">
                            <?php if (empty($user->project_to_pm)) { ?>
                                <option value="" selected>Pilih Project</option>
                            <?php } else { ?>
                                @foreach($project as $row)
                                <option value="{{$row->id}}" {{ $user->get_project->id == $row->id ? 'selected' : '' }}>{{$row->name}}</option>
                                @endforeach
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" value="{{$user->email}}" class="form-control @error('email') is-invalid @enderror" name="email" required>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Update') }}
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

@endsection
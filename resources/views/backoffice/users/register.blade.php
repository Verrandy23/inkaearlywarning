@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC"></h1>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <h6 class="titleC text-center">Form Register User</h6><br>
            <form id="form" method="POST" action="{{ route('auths.store') }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" data-form="input nama" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required>

                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                    <div class="col-md-6">
                        <input id="username" data-form="input username"  type="text" class="form-control @error('username') is-invalid @enderror" name="username" required>
                        @error('username')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nip" class="col-md-4 col-form-label text-md-right">{{ __('NIP') }}</label>

                    <div class="col-md-6">
                        <input id="nip" type="text" data-form="input NIP"  class="form-control @error('nip') is-invalid @enderror" name="nip" required>

                        @error('nip')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>
                    <div class="col-md-6">
                        <input  id="phone" type="number" data-form="input nomor telephone"  class="form-control @error('phone') is-invalid @enderror" name="phone" required>

                        @error('phone')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>
                    <div class="col-md-6">
                        <select id="role_id" data-form="input role"  class="form-control @error('role_id') is-invalid @enderror" name="role_id" required>
                            <option value="" selected>Pilih Role</option>
                            @foreach($role as $row)
                            <option value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach
                        </select>
                        @error('role_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('Project To PM') }}</label>
                    <div class="col-md-6">
                        <select class="form-control @error('project_to_pm') is-invalid @enderror" data-form="input project to pm"  name="project_to_pm">
                            <option value="" selected>Pilih Project</option>
                            @foreach($project as $row)
                            <option value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">*untuk register project manager.</span>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('Division') }}</label>
                    <div class="col-md-6">
                        <select id="division_id" data-form="input divisi"  class="form-control @error('dvision_name') is-invalid @enderror" name="division_name" required>
                            <option value="" selected> Pilih Divisi</option>
                            <option value="Teknologi">Teknologi</option>
                            <option value="Logistik">Logistik</option>
                            <option value="Fabrikasi">Fabrikasi</option>
                            <option value="Finishing">Finishing</option>
                            <option value="Testing">Testing</option>
                            <option value="Delivery">Delivery</option>
                            <option value="Keproyekan">Keproyekan</option>
                        </select>
                        @error('role_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" data-form="input email"  type="email" class="form-control @error('email') is-invalid @enderror" name="email" required>

                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" data-form="input password"  type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>

                        @error('password')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="button"  id="save" class="btn btn-primary">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

@endsection
@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">Profile Page</h1>
    <div class="section-header-breadcrumb">
    </div>
</div>
<div class="row mt-sm-4">
    <div class="col-12 col-md-12 col-lg-5">
        <div class="card profile-widget top">
            <div class="profile-widget-header">
                <img alt="image" src="assets/stisla/img/profile/{{Auth::user()->picture}}" class="circular--square2">
                <div class="profile-widget-items">
                    <br>
                    <h6 style="margin-top: -18%;margin-left:30%;">{{Auth::user()->name}}</h6>
                    <div style="margin-top: -18%;" class="text-muted d-inline font-weight-normal">
                        <div class="slash"></div>
                        {{Auth::user()->get_role->name}}
                    </div>
                </div>
            </div>
            <div class="profile-widget-description">
                <b>NIP &nbsp &nbsp : </b>{{Auth::user()->nip}} <br>
                <b>Email &nbsp: </b>{{Auth::user()->email}} <br>
                <b>Phone : </b>{{Auth::user()->phone}}
            </div>
        </div>
    </div>
    <div class="col-12 col-md-12 col-lg-7">
        <div class="card top">
            <div class="card-header">
                <h4>Edit Profile</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('profiles.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-12 col-12">
                            <label>NIP</label>
                            <input type="hidden" value="{{Auth::user()->id}}" name="id"></input>
                            <input type="text" name="nip" class="form-control" value="{{Auth::user()->nip}}">
                            <div class="invalid-feedback">
                                Please fill in the first name
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 col-12">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="{{Auth::user()->name}}">
                            <div class="invalid-feedback">
                                Please fill in the first name
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-12">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control" value="{{Auth::user()->username}}">
                            <div class="invalid-feedback">
                                Please fill in the last name
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-7 col-12">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" value="{{Auth::user()->email}}">
                            <div class="invalid-feedback">
                                Please fill in the email
                            </div>
                        </div>
                        <div class="form-group col-md-5 col-12">
                            <label>Phone</label>
                            <input type="number" name="phone" class="form-control" value="{{Auth::user()->phone}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 col-12">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control">
                            <div class="invalid-feedback">
                                Please fill in the email
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-12">
                            <label>Picture</label>
                            <input type="file" name="picture" class="form-control">
                        </div>
                    </div>
            </div>
            <div class="card-footer text-right">
            <button type="submit" style="margin-top: -15%" class="btn btn-primary">Update Changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
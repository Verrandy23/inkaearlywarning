<!-- Dashboard Admin -->
@if (Auth::user()->division_name == "Keproyekan")
@foreach($progress as $index => $row)

@if ($row->jumlah != null || $row->total != 0)
<?php
$jumlah = $row->jumlah;
$total = $row->total;
$hasil = $jumlah / $total * 100;
?>

<div class="col-lg-4 col-sm-6">
    @if($row->name == "Teknologi")
    <div class="card gradient-1" style="border-radius:5%">
    @elseif($row->name == "Logistik") 
    <div class="card gradient-3" style="border-radius:5%">
    @elseif($row->name == "Fabrikasi") 
    <div class="card gradient-2" style="border-radius:5%">
    @elseif($row->name == "Finishing") 
    <div class="card gradient-4" style="border-radius:5%">
    @elseif($row->name == "Testing") 
    <div class="card gradient-8" style="border-radius:5%">
    @elseif($row->name == "Delivery") 
    <div class="card gradient-6" style="border-radius:5%">
    @endif
        <div class="card-body">
            <h5 class="titleC text-white">{{$row->name}} </h5>
            <div class="d-inline-block" style="width:125px">
                <h4 class="text-white"><?php echo number_format((float) $hasil, 2, '.', ''); ?> %</h4>
                <div class="progress" style="height: 6px">
                 <div role="progressbar" class="progress-bar progress-bar-striped progress-bar-animated bg-success" style="width:<?php echo floor($hasil) ?>%">
                </div>
               
        </div>
        <br>
        <a href="{{ route('divisions.show',$row->id)}}">
            <button class="btn btn-icon btn-sm btn-info" type="button"><i class="fa fa-arrow-right"></i> Show Activity
            </button>
        </a>
    </div>
    <span class="float-right display-5 opacity-5"><i class="fa fa-train"></i></span>
</div>
</div>
</div>
@else
<div class="col-lg-4 col-sm-6">
    @if($row->name == "Teknologi")
    <div class="card gradient-1" style="border-radius:5%">
    @elseif($row->name == "Logistik") 
    <div class="card gradient-3" style="border-radius:5%">
    @elseif($row->name == "Fabrikasi") 
    <div class="card gradient-2" style="border-radius:5%">
    @elseif($row->name == "Finishing") 
    <div class="card gradient-5" style="border-radius:5%">
    @elseif($row->name == "Testing") 
    <div class="card gradient-5" style="border-radius:5%">
    @elseif($row->name == "Delivery") 
    <div class="card gradient-6" style="border-radius:5%">
    @endif
        <div class="card-body">
            <h5 class="titleC text-white">{{$row->name}}</h5>
            <div class="d-inline-block" style="width:125px">
                <h4 class="text-white">0%</h4>
                <div class="progress" style="height: 6px">
                    <div class="progress-bar bg-warning" style="width: 0%"></div>
                </div>
                <br>
                <a href="{{ route('divisions.show',$row->id)}}">
                    <button class="btn btn-icon btn-sm btn-info" type="button"><i class="fa fa-arrow-right"></i> Show Activity
                    </button>
                </a>
            </div>
            <span class="float-right display-5 opacity-5"><i class="fa fa-train"></i></span>
        </div>
    </div>
</div>
@endif
@endforeach


<!-- Progress Delivery -->

<!-- End progress Delivery -->

<ul class="nav nav-pills" id="myTab3" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="teknologi-tab1" data-toggle="tab" href="#teknologi1" role="tab" aria-controls="teknologi" aria-selected="true">Teknologi</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="logistik-tab1" data-toggle="tab" href="#logistik1" role="tab" aria-controls="logistik" aria-selected="false">Logistik</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="fabrikasi-tab1" data-toggle="tab" href="#fabrikasi1" role="tab" aria-controls="fabrikasi" aria-selected="false">Fabrikasi</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="finishing-tab1" data-toggle="tab" href="#finishing1" role="tab" aria-controls="finishing" aria-selected="false">Finishing</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="testing-tab1" data-toggle="tab" href="#testing1" role="tab" aria-controls="testing" aria-selected="false">Testing</a>
    </li>
    <!-- <li class="nav-item">
        <a class="nav-link" id="delivery-tab1" data-toggle="tab" href="#delivery1" role="tab" aria-controls="delivery" aria-selected="false">Delivery</a>
    </li> -->

</ul>
<div class="tab-content" id="myTabContent2">
    <div class="tab-pane fade show active" id="teknologi1" role="tabpanel" aria-labelledby="teknologi-tab1">
        <div class="col-md-12 bottom">
            <div id="container3-1">
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="logistik1" role="tabpanel" aria-labelledby="logistik-tab1">
        <div class="col-md-12 bottom">
            <div id="container2-1">
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="fabrikasi1" role="tabpanel" aria-labelledby="fabrikasi-tab1">
        <div class="col-md-12 bottom">
            <div id="container7-1">
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="finishing1" role="tabpanel" aria-labelledby="finishing-tab1">
        <div class="col-md-12 bottom">
            <div id="container5-1">
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="testing1" role="tabpanel" aria-labelledby="testing-tab1">
        <div class="col-md-12 bottom">
            <div id="container6-1">
            </div>
        </div>
    </div>
    <!-- <div class="tab-pane fade" id="delivery1" role="tabpanel" aria-labelledby="delivery-tab1">
        <div class="col-md-12 bottom">
            <div id="container4-1">
            </div>
        </div>
    </div> -->
</div>
@endif
<script src="{{ asset('assets/stisla/js/jquery-1.10.2.js')}}"></script>
<script type="text/javascript">
    $(function() {
        $('#container3-1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Rekap Progress Aktivitas Kerja'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y} aktivitas</b>'
            },
            plotOptions: {
                pie: {
                    colors: [
                        '#575fcf',
                        '#1abc9c',
                        '#ffb142'
                    ],
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} aktivitas',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Persentase Divisi teknologi',
                data: [
                    <?php
                    $status;
                    foreach ($peichartteknologi as $data) {
                        if ($data->status == "finish") {
                            $status = "Selesai";
                        } else if ($data->status == "proses") {
                            $status = "Progress";
                        } else {
                            $status = "Belum";
                        }

                        echo "['" . $status . "'," . $data->jumlah . "],\n";
                    }

                    ?>
                ],
                point: {
                    events: {
                        click: function(event) {
                            var route;
                            if (this.name == "Progress") {
                                route = "proses"
                            } else if (this.name == "Selesai") {
                                route = "finish"
                            } else {
                                route = "belum"
                            }

                            var currentLocation = window.location;
                            window.location.href = currentLocation + "/" + route + "/" + <?php echo $division_id_teknologi_pie ?>;

                        }
                    }
                }
            }]
        });
    });
</script>
<script type="text/javascript">
    $(function() {
        $('#container2-1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Rekap Progress Aktivitas Kerja'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y} aktivitas</b>'
            },
            plotOptions: {
                pie: {
                    colors: [
                        '#575fcf',
                        '#1abc9c',
                        '#ffb142'
                    ],
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} aktivitas',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Persentase Divisi logistik',
                data: [
                    <?php
                    $status;
                    foreach ($peichartlogistik as $data) {
                        if ($data->status == "finish") {
                            $status = "Selesai";
                        } else if ($data->status == "proses") {
                            $status = "Progress";
                        } else {
                            $status = "Belum";
                        }

                        echo "['" . $status . "'," . $data->jumlah . "],\n";
                    }

                    ?>
                ],
                point: {
                    events: {
                        click: function(event) {
                            var route;
                            if (this.name == "Progress") {
                                route = "proses"
                            } else if (this.name == "Selesai") {
                                route = "finish"
                            } else {
                                route = "belum"
                            }

                            var currentLocation = window.location;
                            window.location.href = currentLocation + "/" + route + "/" + <?php echo $division_id_logistik_pie ?>;
                        }
                    }
                }
            }]
        });
    });
</script>

<script type="text/javascript">
    $(function() {
        $('#container7-1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Rekap Progress Aktivitas Kerja'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y} aktivitas</b>'
            },
            plotOptions: {
                pie: {
                    colors: [
                        '#575fcf',
                        '#1abc9c',
                        '#ffb142'
                    ],
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} aktivitas',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Persentase Divisi fabrikasi',
                data: [
                    <?php
                    $status;
                    foreach ($peichartfabrikasi as $data) {
                        if ($data->status == "finish") {
                            $status = "Selesai";
                        } else if ($data->status == "proses") {
                            $status = "Progress";
                        } else {
                            $status = "Belum";
                        }

                        echo "['" . $status . "'," . $data->jumlah . "],\n";
                    }

                    ?>
                ],
                point: {
                    events: {
                        click: function(event) {
                            var route;
                            if (this.name == "Progress") {
                                route = "proses"
                            } else if (this.name == "Selesai") {
                                route = "finish"
                            } else {
                                route = "belum"
                            }

                            var currentLocation = window.location;
                            window.location.href = currentLocation + "/" + route + "/" + <?php echo $division_id_fabrikasi_pie ?>;
                        }
                    }
                }
            }]
        });
    });
</script>

<script type="text/javascript">
    $(function() {
        $('#container5-1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Rekap Progress Aktivitas Kerja'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y} aktivitas</b>'
            },
            plotOptions: {
                pie: {
                    colors: [
                        '#575fcf',
                        '#1abc9c',
                        '#ffb142'
                    ],
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} aktivitas',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Persentase Divisi Finishing',
                data: [
                    <?php
                    $status;
                    foreach ($peichartfinishing as $data) {
                        if ($data->status == "finish") {
                            $status = "Selesai";
                        } else if ($data->status == "proses") {
                            $status = "Progress";
                        } else {
                            $status = "Belum";
                        }

                        echo "['" . $status . "'," . $data->jumlah . "],\n";
                    }

                    ?>
                ],
                point: {
                    events: {
                        click: function(event) {
                            var route;
                            if (this.name == "Progress") {
                                route = "proses"
                            } else if (this.name == "Selesai") {
                                route = "finish"
                            } else {
                                route = "belum"
                            }

                            var currentLocation = window.location;
                            window.location.href = currentLocation + "/" + route + "/" + <?php echo $division_id_finishing_pie ?>;
                        }
                    }
                }
            }]
        });
    });
</script>
<script type="text/javascript">
    $(function() {
        $('#container6-1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Rekap Progress Aktivitas Kerja'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y} aktivitas</b>'
            },
            plotOptions: {
                pie: {
                    colors: [
                        '#575fcf',
                        '#1abc9c',
                        '#ffb142'
                    ],
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} aktivitas',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Persentase Divisi testing',
                data: [
                    <?php
                    $status;
                    foreach ($peicharttesting as $data) {
                        if ($data->status == "finish") {
                            $status = "Selesai";
                        } else if ($data->status == "proses") {
                            $status = "Progress";
                        } else {
                            $status = "Belum";
                        }

                        echo "['" . $status . "'," . $data->jumlah . "],\n";
                    }

                    ?>
                ],
                point: {
                    events: {
                        click: function(event) {
                            var route;
                            if (this.name == "Progress") {
                                route = "proses"
                            } else if (this.name == "Selesai") {
                                route = "finish"
                            } else {
                                route = "belum"
                            }

                            var currentLocation = window.location;
                            window.location.href = currentLocation + "/" + route + "/" + <?php echo $division_id_testing_pie ?>;
                        }
                    }
                }
            }]
        });
    });
</script>



<div class="container bottom">
    <h3 class="titleC text-center">Division Activities</h3><hr>
    <div class="section-header-breadcrumb">
        <div class="bottom text-right">
            <button id="modal-11" class="btn btn-sm mb-1 btn-primary"><i class="fa fa-plus"></i> Add Activity</button>
        </div>
    </div>


    <!-- Form modal add division activity -->
        <form enctype="multipart/form-data" class="modal-part" id="modal-login-part" method="post" action="{{ route('division_additional.store') }}">
            @csrf
            <div class="form-group">
                <label>Name</label>
                <input required type="text" name="name" placeholder="Input Activity name" class="form-control">
            </div>

            <div class="form-group">
                <label>Information</label>
                <textarea required name="information" class="form-control" cols="50" placeholder="Information"></textarea>
            </div>

            <div class="form-group">
                <label> Date</label>
                <input required type="date" name="date" class="form-control">
            </div>

            <div class="form-group">
                <label>Attachment Files</label>
                <input type="file" name="file" class="form-control">
                <input type="text" name="division_id" class="form-control" value="{{$id_division}}">
            </div>
        </form>

        
    <!-- End Modal Form modal add division activity -->

    <div class="table-responsive">
        <table class="table col-lg-12 zero-configuration" id="tablePicture3">
            <thead style="background:#78e08f">
                <tr>
                    <th style="color:white" width="1%">No.</th>
                    <th style="color:white" width="19%">Activity</th>
                    <th style="color:white" width="20%">Information</th>
                    <th style="color:white" width="20%">Date</th>
                    <th style="color:white" width="20%">Attachment</th>
                    <th style="color:white" width="20%">Action</th>

                </tr>
            </thead>
            <tbody>
                @foreach($division_additional as $index => $row)

                <tr>
                    <td>{{$index+1}}.</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->information}}</td>
                    <td>
                        <?php
                            $date = date_create($row->date);
                            echo date_format($date, "j F Y");
                            ?>
                    </td>
                    <td>
                        @if(stripos($row->file, "png") !== FALSE)

                        <a href="/Division/{{$row->file}}" target="_blank"><img width="250px" height="200px" src="{{ asset('Division/' . $row->file) }}"></a>               
                        @else
                        <a href="/Division/{{$row->file}}" target="_blank">{{$row->file}}</a>
   
                        @endif

                    </td>
                   
                    <td>
                        <form action="{{ route('division_additional.destroy', $row->id)}}" method="post" id="confirm_delete">
                            @csrf
                            @method('DELETE')

                            <a onclick="return confirm('Are you sure to delete this data?')">
                                <button class="btn btn-icon btn-danger" type="submit">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </a>

                            <a href="{{ route('division_additional.edit', $row->id)}}">
                                <button class="btn btn-icon btn-info" type="button">
                                    <i class="fas fa-edit"></i>

                                </button>
                            </a>

                        </form>
                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
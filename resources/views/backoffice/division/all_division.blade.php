  @if(Auth::user()->division_id != '6')
  @foreach($progress as $index => $row)
  <?php $random = mt_rand(1, 5);
    $gradient = 'gradient-' . $random;
    ?>

  @if ($row->jumlah != null || $row->total != 0)
  <?php
    $jumlah = $row->jumlah;
    $total = $row->total;
    $hasil = $jumlah / $total * 100;
    ?>
  <div class="col-lg-4 col-sm-6">
      <div class="card {{$gradient}}" style="border-radius:5%">
          <div class="card-body">
              <h5 class="titleC text-white">{{$row->name}} </h5>
              <div class="d-inline-block" style="width:125px">
                  <h4 class="text-white"><?php echo number_format((float) $hasil, 2, '.', ''); ?> %</h4>
                  <div class="progress" style="height: 6px">
                      @if (floor($hasil) < 50) <div role="progressbar" class="progress-bar bg-light-yellow progress-bar-striped progress-bar-animated bg-danger" style="width:<?php echo floor($hasil) ?>%">
                  </div>
                  @elseif (floor($hasil) >= 50 && floor($hasil) <= 75) <div role="progressbar" class="progress-bar bg-light-yellow progress-bar-striped progress-bar-animated bg-warning" style="width:<?php echo floor($hasil) ?>%">
              </div>
              @elseif (floor($hasil) > 75)
              <div role="progressbar" class="progress-bar bg-light-yellow progress-bar-striped progress-bar-animated bg-success" style="width:<?php echo floor($hasil) ?>%"></div>
              @endif
          </div>
          <br>
          <a href="/divisions/{{$row->id}}">
              <button class="btn btn-icon btn-sm btn-info" type="button"><i class="fa fa-arrow-right"></i> Show Activity
              </button>
          </a>
      </div>
      <span class="float-right display-5 opacity-5"><i class="fa fa-train"></i></span>
        </div>
      </div>
  </div>

  @else
  <div class="col-lg-4 col-sm-6">
      <div class="card {{$gradient}}" style="border-radius:5%">
          <div class="card-body">
              <h5 class="titleC text-white">{{$row->name}} </h5>
              <div class="d-inline-block" style="width:125px">
                  <h4 class="text-white">0%</h4>
                  <div class="progress" style="height: 6px">
                      <div class="progress-bar bg-warning" style="width: 0%"></div>
                  </div>
                  <br>
                  <a href="/divisions/{{$row->id}}">
                      <button class="btn btn-icon btn-sm btn-info" type="button"><i class="fa fa-arrow-right"></i> Show Activity
                      </button>
                  </a>
              </div>
              <span class="float-right display-5 opacity-5"><i class="fa fa-train"></i></span>
          </div>
      </div>
  </div>
  @endif
  @endforeach

  <div class="col-lg-8">
      <h3 class="titleC text-center">End Date
          <?php
            $date = date_create($division_end_date);
            echo date_format($date, "j F Y");
            ?>
      </h3>
      <br>
      @if ($division_end_date < date("Y-m-d")) @else <div class="clock"></div>
  @endif

  </div><br>

  <!-- Dashboard for fabrikasi & finishing -->
  @include('backoffice.division.dashboard_production')
  <!-- End Dashboard for fabrikasi & finishing -->

    <div class="container bottom">  
      <h1 class="titleC text-center bottom">History Activity </h1><hr>
      <ul class="nav nav-pills" id="myTab3" role="tablist">
          <li class="nav-item">
              <a class="nav-link active" id="home-tab3" data-toggle="tab" href="#home3" role="tab" aria-controls="home" aria-selected="true">Selesai</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#profile3" role="tab" aria-controls="profile" aria-selected="false">Proses</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" id="contact-tab3" data-toggle="tab" href="#contact3" role="tab" aria-controls="contact" aria-selected="false">Belum</a>
          </li>
      </ul>
      <div class="tab-content" id="myTabContent2">
          <div class="tab-pane fade show active" id="home3" role="tabpanel" aria-labelledby="home-tab3">
              <div class="table-responsive">
                  <table class="table col-lg-12 zero-configuration" id="table1">
                      <thead style="background-color: #1289A7">
                          <tr>
                              <th style="color:white">No.</th>
                              <th style="color:white">Activity</th>
                              <th style="color:white">Sub Activity</th>
                              <th style="color:white">Work Activity</th>
                              <th style="color:white">Start Date</th>
                              <th style="color:white">End Date</th>
                              <th style="color:white">Status</th>
                              <th style="color:white">PIC</th>
                              <th style="color:white">Add PIC</th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach($historyFinish as $index => $row)

                          <tr>
                              <td>{{$index+1}}.</td>
                              <td>{{$row->activity_name}}</td>
                              <td>{{$row->sub_name}}</td>
                              <td>{{$row->work_name}}</td>
                              <td>
                                  <?php
                                    $date = date_create($row->start_date);
                                    echo date_format($date, "j F Y");
                                    ?>
                              </td>
                              <td>
                                  <?php
                                    $date = date_create($row->end_date);
                                    echo date_format($date, "j F Y");
                                    ?>
                              </td>
                              <td>
                                  @if ($row->status == 0)
                                  <strong class="badge badge-danger">Belum</strong>
                                  @elseif ($row->status == 0.5)
                                  <strong class="badge badge-warning">Proses</strong>
                                  @else
                                  <strong class="badge badge-success">Selesai</strong>
                                  @endif

                              </td>
                              <td>{{$row->user_name}}</td>
                              <td>
                                  @if (Auth::user()->role_id != 1)
                                  <a href="{{ route('work_activities.show', $row->work_activity_id)}}">
                                        <button class="btn btn-icon btn-success" type="button">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                   </a>
                                  @endif

                              </td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="tab-pane fade" id="profile3" role="tabpanel" aria-labelledby="profile-tab3">
              <div class="table-responsive">
                  <table class="table col-lg-12 zero-configuration" id="tableProses">
                      <thead style="background-color: #e1b12c">
                          <tr>
                              <th style="color:white;width:1%">No.</th>
                              <th style="color:white;width:15%">Activity</th>
                              <th style="color:white;width:20%">Sub Activity</th>
                              <th style="color:white;width:10%">Start Date</th>
                              <th style="color:white;width:10%">End Date</th>
                              <th style="color:white;width:3%">Status</th>
                              <th style="color:white;width:10%">PIC</th>
                              <th style="color:white;width:26%">Add PIC</th>

                          </tr>
                      </thead>
                      <tbody>
                          @foreach($historyProses as $index => $row)

                          <tr>
                              <td>{{$index+1}}.</td>
                              <td>{{$row->activity_name}}</td>
                              <td>{{$row->sub_name}} {{$row->work_name}}</td>
                              <td>
                                  <?php
                                    $date = date_create($row->start_date);
                                    echo date_format($date, "j F Y");
                                    ?>
                              </td>
                              <td>
                                  <?php
                                    $date = date_create($row->end_date);
                                    echo date_format($date, "j F Y");
                                    ?>
                              </td>
                              <td>
                                  @if ($row->status == 0)
                                  <strong class="badge badge-danger">Belum</strong>
                                  @else ($row->status >= '0.1' && $row->status <= '0.9' ) <strong class="badge badge-warning">Proses</strong>
                                      @endif

                              </td>
                              <td>{{$row->user_name}}</td>
                              <td>
                                  @if (Auth::user()->role_id != 1)
                                  <a href="{{ route('work_activities.show', $row->work_activity_id)}}">
                                        <button class="btn btn-icon btn-success" type="button">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </a>
                                  @endif

                              </td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="tab-pane fade" id="contact3" role="tabpanel" aria-labelledby="contact-tab3">
              <div class="table-responsive">
                  <table class="table col-lg-12 zero-configuration" id="tableNotYet">
                      <thead style="background-color: #f19066">
                          <tr>
                              <th style="color:white">No.</th>
                              <th style="color:white">Activity</th>
                              <th style="color:white">Sub Activity</th>
                              <th style="color:white">Start Date</th>
                              <th style="color:white">End Date</th>
                              <th style="color:white">Status</th>
                              <th style="color:white">PIC</th>
                              <th style="color:white">Add PIC</th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach($history as $index => $row)

                          <tr>
                              <td>{{$index+1}}.</td>
                              <td>{{$row->activity_name}}</td>
                              <td>{{$row->sub_name}} {{$row->work_name}}</td>
                              <td>
                                  <?php
                                    $date = date_create($row->start_date);
                                    echo date_format($date, "j F Y");
                                    ?>
                              </td>
                              <td>
                                  <?php
                                    $date = date_create($row->end_date);
                                    echo date_format($date, "j F Y");
                                    ?>
                              </td>
                              <td>
                                  @if ($row->status == 0)
                                  <strong class="badge badge-danger">Belum</strong>
                                  @else ($row->status >= '0.1' && $row->status <= '0.9' ) <strong class="badge badge-warning">Proses</strong>
                                      @endif
                              </td>
                              <td>{{$row->user_name}}</td>
                              <td>
                                  @if (Auth::user()->role_id != 1)
                                  <a href="{{ route('work_activities.show', $row->work_activity_id)}}">
                                        <button class="btn btn-icon btn-success" type="button">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </a>
                                  @endif

                              </td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  @endif
  </div>
  
  @if (Auth::user()->division_name != 'Keproyekan')

  <!-- Pie Chart rekap -->
  <div class="col-md-12 bottom">
      <div id="container3">
      </div>
  </div>
  <!-- End Pie Chart rekap -->

  @endif

  

  <script type="text/javascript">
      function klikPic(id) {
          var Id = id;
          $(".modal-body #workId").val(Id);
          $('#picModal').modal();
      }
  </script>

<script src="{{ asset('assets/stisla/js/jquery-1.10.2.js')}}"></script>

  <script type="text/javascript">
      var clock;

      $(document).ready(function() {

          // Grab the current date
          var currentDate = new Date();

          // Set some date in the future. In this case, it's always Jan 1
          var futureDate = new Date('<?php echo $division_end_date ?>');

          // Calculate the difference in seconds between the future and current date
          var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;

          // Instantiate a coutdown FlipClock
          clock = $('.clock').FlipClock(diff, {
              clockFace: 'DailyCounter',
              countdown: true
          });
      });
  </script>

  <?php if (Auth::user()->division_name != null || Auth::user()->division_name != '') { ?>

      <!-- Pie chart rekap total-->
      <script type="text/javascript">
          $(function() {
              $('#container3').highcharts({
                  chart: {
                      plotBackgroundColor: null,
                      plotBorderWidth: null,
                      plotShadow: false
                  },
                  title: {
                      text: 'Rekap Progress Aktivitas Kerja <hr>'
                  },
                  tooltip: {
                      pointFormat: '{series.name}: <b>{point.y} aktivitas</b>'
                  },
                  plotOptions: {
                      pie: {
                          colors: [
                              '#575fcf',
                              '#1abc9c',
                              '#ffb142'
                          ],
                          allowPointSelect: true,
                          cursor: 'pointer',
                          dataLabels: {
                              enabled: true,
                              format: '<b>{point.name}</b>: {point.y} aktivitas',
                              style: {
                                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                              }
                          }
                      }
                  },
                  series: [{
                      type: 'pie',
                      name: 'Persentase Divisi',
                      data: [
                          <?php
                            $status;
                            // data yang diambil dari database
                            if (count($piechart) > 0) {
                                foreach ($piechart as $data) {
                                    if ($data->status == "finish") {
                                        $status = "Selesai";
                                    } else if ($data->status == "proses") {
                                        $status = "Progress";
                                    } else {
                                        $status = "Belum";
                                    }

                                    echo "['" . $status . "'," . $data->jumlah . "],\n";
                                }
                            }
                            ?>
                      ],
                      point: {
                          events: {
                              click: function(event) {
                                  var route;
                                  if (this.name == "Progress") {
                                      route = "proses"
                                  } else if (this.name == "Selesai") {
                                      route = "finish"
                                  } else {
                                      route = "belum"
                                  }

                                  var currentLocation = window.location;
                                  window.location.href = currentLocation + "/" + route;
                              }
                          }
                      }
                  }]
              });
          });
      </script>
      <!-- End Pie chart rekap total-->

  <?php } ?>
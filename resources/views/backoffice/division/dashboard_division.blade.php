@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">Dashboard Division Project {{$project_name}}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
            <ol style="background:#38ada9" class="breadcrumb text-white-all">
                <li class="breadcrumb-item"><a href="/backoffice"><i class="fas fa-tachometer-alt"></i> Home </a></li>
            </ol>
        </div><br>
    </div>

</div>
<div class="card top">
    <div class="card-body">
    @include('sweet::alert')

        <div class="col-lg-12">
            <div class="row">
                <!-- Dashboard page for all division -->
                @if (Auth::user()->division_name != 'Keproyekan')
                @include('backoffice.division.all_division')
                @include('backoffice.division.dashboard_division_additional')
                @include('backoffice.division.dashboard_delivery')
                <!-- End Dashboard page for all division -->
                @else
                
                <!-- Dashboard page for role admin & PM -->
                @include('backoffice.division.dashboard_admin')
                @endif
                <!-- End Dashboard page for role admin & PM -->

                <!-- Dashboard page for division fabrikasi,finishing,role admin & PM  -->
                @if( Auth::user()->division_name == "Keproyekan"|| Auth::user()->division_name == "Fabrikasi" || Auth::user()->division_name == "Finishing")
                @include('backoffice.division.dashboard_picture')
                @endif
                <!-- End Dashboard page for division fabrikasi,finishing,role admin & PM  -->

            </div><br>

        </div>
    </div>
</div>
@endsection
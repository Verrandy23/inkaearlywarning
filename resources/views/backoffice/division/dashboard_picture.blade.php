<!-- Conditional for admin -->
@if(Auth::user()->role_id==1 || Auth::user()->role_id==4)
<div class="container bottom">
    <h3 class="titleC text-center">Production Picture</h3>
    <ul class="nav nav-pills" id="myTab3" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="fabrikasi-tab3" data-toggle="tab" href="#fabrikasi3" role="tab" aria-controls="fabrikasi" aria-selected="true">Fabrikasi</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="finishing-tab3" data-toggle="tab" href="#finishing3" role="tab" aria-controls="finishing" aria-selected="false">Finishing</a>
        </li> 

    </ul>
    <div class="tab-content" id="myTabContent2">
        <div class="tab-pane fade show active" id="fabrikasi3" role="tabpanel" aria-labelledby="fabrikasi-tab3">
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="tablePicture1">
                    <thead>
                        <tr>
                            <th width="10%">No.</th>
                            <th width="30%">Activity</th>
                            <th width="60%">Picture</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($progressFabrikasiPictureAdmin as $index => $row)

                        <tr>
                            <td>{{$index+1}}.</td>
                            <td>{{$row->activity_name}}</td>

                            <td>
                                @if($row->picture == '')
                                @else
                                <a href="/Activity/{{$row->picture}}" target="_blank"><img width="400px" height="250px" src="{{ asset('Activity/' . $row->picture) }}" />
                                </a>
                                @endif
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="tab-pane fade" id="finishing3" role="tabpanel" aria-labelledby="finishing-tab3">
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="tablePicture2">
                    <thead>
                        <tr>
                            <th width="10%">No.</th>
                            <th width="30%">Activity</th>
                            <th width="60%">Picture</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($progressFinishingPictureAdmin as $index => $row)

                        <tr>
                            <td>{{$index+1}}.</td>
                            <td>{{$row->activity_name}}</td>

                            <td>
                                @if($row->picture == '')
                                @else
                                <a href="/Activity/{{$row->picture}}" target="_blank"><img width="400px" height="250px" src="{{ asset('Activity/' . $row->picture) }}" />
                                </a>
                                @endif
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@else

<!-- Conditional for division fabrikasi and finishing -->
<div class="container bottom">
    <h3 class="titleC text-center">Progress Picture</h3>
    <div class="table-responsive">
        <table class="table col-lg-12 zero-configuration" id="tablePicture3">
            <thead>
                <tr>
                    <th width="10%">No.</th>
                    <th width="30%">Activity</th>
                    <th width="50%">Picture</th>
                    <th width="10%">Add Picture</th>

                </tr>
            </thead>
            <tbody>
                @foreach($progressPicture as $index => $row)

                <tr>
                    <td>{{$index+1}}.</td>
                    <td>{{$row->activity_name}}</td>

                    <td>
                        @if($row->picture == '')
                        @else
                        <a href="/Activity/{{$row->picture}}" target="_blank"><img style="padding:2%" width="400px" height="250px" src="{{ asset('Activity/' . $row->picture) }}" />
                        </a>
                        @endif

                    </td>
                    <td>
                        @if (Auth::user()->role_id == 5 || Auth::user()->division_name == 'Fabrikasi' || Auth::user()->division_name == 'Finishing')
                        <a href="{{ route('activities.edit', $row->id)}}">
                            <img width="50px" height="50px" src="{{ asset('assets/stisla/img/products/product-5-50.png') }}">
                        </a>
                        @endif

                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endif
<!-- Dashboard Division Delivery -->
@if (Auth::user()->division_name == 'Delivery')
    @foreach($progressDelivery as $index => $row)
    <?php $random = mt_rand(1, 5);
        $gradient = 'gradient-' . $random;
        ?>

    @if ($row->jumlah != null || $row->total != 0)
        <?php
                $jumlah = $row->jumlah;
                $total = $row->total;
                $hasil = $jumlah / $total * 100;
                ?>
        <div class="col-lg-4 col-sm-6">
            <div class="card {{$gradient}}" style="border-radius:5%">
                <div class="card-body">
                    <h5 class="titleC text-white">{{$row->name}}</h5>
                    <div class="d-inline-block" style="width:125px">
                        <h4 class="text-white"><?php echo number_format((float) $hasil, 2, '.', ''); ?> %</h4>
                        <div class="progress" style="height: 6px">
                            @if (floor($hasil) < 50)
                                <div role="progressbar" class="progress-bar bg-danger" style="width:<?php echo floor($hasil) ?>%"></div>
                            @elseif (floor($hasil) >= 50 && floor($hasil) <= 75)
                                <div role="progressbar" class="progress-bar bg-warning" style="width:<?php echo floor($hasil) ?>%"></div>
                            @elseif (floor($hasil) > 75) 
                                <div role="progressbar" class="progress-bar bg-success" style="width:<?php echo floor($hasil) ?>%"></div>
                            @endif
                        </div>
                        <br>
                        <a href="/delivery/{{$row->id}}">
                            <button class="btn btn-icon btn-sm btn-info" type="button"><i class="fa fa-arrow-right"></i> Show Activity
                            </button>
                        </a>
                    </div>
                    <span class="float-right display-5 opacity-5"><i class="fa fa-train"></i></span>
                </div>
            </div>
        </div>
    @else
        <div class="col-lg-4 col-sm-6">
            <div class="card {{$gradient}}" style="border-radius:5%">
                <div class="card-body">
                    <h5 class="titleC text-white">{{$row->name}}</h5>
                    <div class="d-inline-block" style="width:125px">
                        <h4 class="text-white">0%</h4>
                        <div class="progress" style="height: 6px">
                            <div class="progress-bar bg-warning" style="width: 0%"></div>
                        </div>
                        <br>
                        <a href="/delivery/{{$row->id}}">
                            <button class="btn btn-icon btn-sm btn-info" type="button"><i class="fa fa-arrow-right"></i> Show Activity
                            </button>
                        </a>
                    </div>
                    <span class="float-right display-5 opacity-5"><i class="fa fa-train"></i></span>
                </div>
            </div>
        </div>
    @endif
    <div class="col-lg-8">
        <h3 class="titleC text-center">End Date <?php
                                                    $date = date_create($division_end_date);
                                                    echo date_format($date, "j F Y");
                                                    ?> </h3>
        <br>
        <div class="clock"></div>
    </div>
    @if (Auth::user()->division_id != '')
        <div class="container">
            <br>
            <h1 class="titleC">History Activity </h1>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration btn-sm" id="table1">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Activity</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$index+1}}.</td>
                            <td>{{$row->activity_name}}</td>
                            <td>{{$row->start_date}}</td>
                            <td>{{$row->end_date}}</td>
                            <td>
                                @if ($row->status_delivery == 0)
                                    <strong class="badge badge-primary">Belum</strong>
                                @elseif ($row->status_delivery == 0.5)
                                    <strong class="badge badge-primary">Proses</strong>
                                @else
                                    <strong class="badge badge-primary">Selesai</strong>
                                @endif

                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    @endif
    @endforeach

@endif

<script src="{ asset('assets/stisla/js/jquery.min.js')}}"></script>

<script type="text/javascript">
    var clock;

    $(document).ready(function() {

        // Grab the current date
        var currentDate = new Date();

        // Set some date in the future. In this case, it's always Jan 1
        var futureDate = new Date('<?php echo $division_end_date ?>');

        // Calculate the difference in seconds between the future and current date
        var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;

        // Instantiate a coutdown FlipClock
        clock = $('.clock').FlipClock(diff, {
            clockFace: 'DailyCounter',
            countdown: true
        });
    });
</script>
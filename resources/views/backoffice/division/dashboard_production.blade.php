      @if (Auth::user()->division_name != "Fabrikasi")
      @else
      <div class="col-md-12">
            <div class="table-responsive bottom">
            <div class="section-title">Dashboard Progress Production</div>

            <div class="form-group">
                <form action="" method="GET">
                    <div class="col-md-12">
                        <div class="input-group">
                            <select class="custom-select" id="inputGroupSelect04" name="search">
                                <option value="" selected> Pilih Activity</option>
                                @foreach($fabrikasiActivity as $row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">Filter</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <table class="table col-lg-12 zero-configuration" id="tableProgress">
                <thead style="background-color: #fa983a">
                    <tr>
                        <th style="color: white">Activity</th>
                        @foreach($fabrikasi as $index => $row)
                            <th style="color:white">{{$row->sub_name}}</th>
                        @endforeach                         
                    </tr>
                </thead>

                <tbody>
        
                    <tr>
                        <td>{{$fabrikasiName}}</td>
                        @foreach($fabrikasi as $row)
                        <td>
                            @if ($row->total == $row->total_finish)
                                <img alt="image" src="{{ asset('assets/stisla/img/gif/success.png')}}" width="40px">
                            @elseif ($row->total_finish == 0 && $row->status == 0)
                            @else
                                <img alt="image" src="{{ asset('assets/stisla/img/gif/warning.gif')}}" width="65px" class="--top">
                            @endif
                        </td>
                        @endforeach  
                    </tr>
        
                </tbody>

            </table>
              <b> Information : </b><br>
              &nbsp <img alt="image" src="{{ asset('assets/stisla/img/gif/success.png')}}" width="20px"> &nbsp : Finish <br>
              <img alt="image" src="{{ asset('assets/stisla/img/gif/warning.gif')}}" width="35px"> : On Process

          </div>
      </div><br><br>

  @endif

  @if (Auth::user()->division_name != 'Finishing')
  @else
        <div class="col-md-12">
            <div class="table-responsive bottom">
            <div class="section-title">Dashboard Progress Production</div>

            <div class="form-group">
                <form action="" method="GET">
                    <div class="col-md-12">
                        <div class="input-group">
                            <select class="custom-select" id="inputGroupSelect04" name="search">
                                <option value="" selected> Pilih Takt</option>
                                @foreach($taktActivity as $row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">Filter</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <table class="table col-lg-12 zero-configuration" id="tableProgress">
                <thead style="background-color: #fa983a">
                    <tr>
                        <th style="color: white">Activity</th>
                        @foreach($takt as $index => $row)
                            <th style="color:white">{{$row->sub_name}}</th>
                        @endforeach                         
                    </tr>
                </thead>

                <tbody>
        
                    <tr>
                        <td>{{$taktName}}</td>
                        @foreach($takt as $row)
                        <td>
                            @if ($row->total == $row->total_finish)
                                <img alt="image" src="{{ asset('assets/stisla/img/gif/success.png')}}" width="40px">
                            @elseif ($row->total_finish == 0 && $row->status == 0)
                            @else
                                <img alt="image" src="{{ asset('assets/stisla/img/gif/warning.gif')}}" width="65px" class="--top">
                            @endif
                        </td>
                        @endforeach  
                    </tr>
        
                </tbody>

            </table>
              <b> Information : </b><br>
              &nbsp <img alt="image" src="{{ asset('assets/stisla/img/gif/success.png')}}" width="20px"> &nbsp : Finish <br>
              <img alt="image" src="{{ asset('assets/stisla/img/gif/warning.gif')}}" width="35px"> : On Process

          </div>
      </div><br><br>
      @endif

@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    @foreach($general as $row)
    <h1 class="titleC">{{$row->project_name}}</h1>

    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
            <ol style="background:#38ada9" class="breadcrumb text-white-all">
                <li class="breadcrumb-item"><a href="/backoffice"><i class="fas fa-tachometer-alt"></i> Home</a></li>
                <li class="breadcrumb-item"><a href="/dashboard_divisions/{{$row->project_id}}"> Dashboard Division</a></li>
                <li class="breadcrumb-item"><a href="/divisions/{{$row->division_id}}"> Activity</a></li>
            </ol>
        </div><br>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <h6 class="titleC text-center">{{$activity->name}} - {{$row->project_name}}</h6><br>
            @endforeach


            <div style="margin-right:3%" class="section-header-breadcrumb">
                <div class="bottom">
                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                     + Add Sub Activity
                    </button>
                </div>
            </div>


                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="false" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Activity</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>

                    <form class="form-class" id="modal-login-part" method="post" action="{{ route('sub_activities.store') }}">

                      <div class="modal-body">

                            @csrf
                            
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" required="true" name="name">
                                </div>
                                <div class="form-group">
                                <input type="hidden" value="{{$activity_id}}" class="form-control" name="activity_id">
                                 </div>        
            
                    
                         </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" id="save" disabled="true" class="btn btn-primary">Save changes</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>


            <!-- Form Modal Sub Activity 
            <form class="modal-part" id="modal-login-part" method="post" action="{{ route('sub_activities.store') }}">
                @csrf
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div class="form-group">
                    <input type="hidden" value="{{$activity_id}}" class="form-control" name="activity_id">
                </div>         
               
            </form>
             End Form Modal Sub Activity -->

            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="table1">
                    @include('sweet::alert')

                    <thead>
                        <tr>
                            <th width="10%">No.</th>
                            <th width="50%">Name</th>
                            <th width="40%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($progress as $index => $row)
                        <tr>
                            <td>{{$index+1}}</td>
                            <td>
                                @if ($row->jumlah != null || $row->total != 0)
                                    <?php
                                        $jumlah = $row->jumlah;
                                        $total = $row->total;
                                        $hasil = $jumlah / $total * 100;
                                        ?>
                                    <h6 class="title"> {{$row->name}} <span class="pull-right"><?php echo floor($hasil) ?>%</span></h6>
                                    <div class="progress mb-3" style="height: 7px">
                                        <div class="progress-bar bg-primary" style="width: <?php echo floor($hasil) ?>%;" role="progressbar">
                                        </div>
                                    </div>

                                @else
                                    <h6 class="title"> {{$row->name}} <span class="pull-right">0%</span></h6>
                                    <div class="progress mb-3" style="height: 7px">
                                        <div class="progress-bar bg-primary" style="width: 0%;" role="progressbar">
                                        </div>
                                    </div>
                                @endif
                            </td>
                            <td>
                                @if(!empty($row->id))

                                    <form id="delete{{$index}}" action="{{ route('sub_activities.destroy', $row->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')

                                        @if (Auth::user()->role_id != 1)
                                            <div style="display:none">
                                        @else

                                                <a>
                                                    
                                                <button name="button" class="btn btn-icon btn-danger" type="button">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                                </a>

                                                <a href="{{ route('sub_activities.edit', $row->id)}}">
                                                    <button class="btn btn-icon btn-info" type="button">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </a>
                                        @endif
                                            </div>
                                            <a href="{{ route('sub_activities.show',$row->id)}}">
                                                <button class=" btn btn-icon btn-info" type="button">
                                                    <i class="fa fa-arrow-right"></i> Show Work Activity
                                                </button>
                                            </a>
                                            
                                    </form>
                                @else
                                    <form>
                                        @if (Auth::user()->role_id != 1)
                                            <div style="display:none">
                                        @else
                                                <a href="#">
                                                    <button class="btn btn-icon btn-danger" type="submit">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </a>

                                                <a href="#">
                                                    <button class="btn btn-icon btn-info" type="button">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </a>
                                        @endif
                                            </div>

                                            <a href="#">
                                                <button class=" btn btn-icon btn-info" type="button">
                                                    <i class="fa fa-arrow-right"></i> Show Work Activity
                                                </button>
                                            </a>
                                    </form>
                                @endif

                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
     
    var btn = document.getElementsByName('button');
   

    for (let i=0; i<btn.length; i++){
        

        document.getElementsByName('button')[i].addEventListener('click', function(){
            var id = i;
            
            swal({
                title: "are you sure for deleting this data ?",
                icon: "warning",
                buttons: true,
                dangerMode: true 
            })
            .then((willDelete)=>{
                if(willDelete){
                    document.getElementById('delete' + id).submit();  
                }
            });
        });
    }    


// for check value add of activity
document.getElementsByName('name')[0].addEventListener("keyup", event=> {

    
    if(event.target.value == "" ){
        document.getElementById('save').disabled = true;
    }
    else{
        document.getElementById('save').disabled = false;
    }

});

</script>

@endsection
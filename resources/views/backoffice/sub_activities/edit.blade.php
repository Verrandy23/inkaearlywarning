@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">{{$sub_activity->name}}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
            <a href="{{ URL::previous() }}" class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i> Back </a> </div> <br><br>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <h4 class="titleC text-center">Form Edit Activity</h4><br>
        <div class="basic-form">
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            <form class="form-horizontal" method="post" action="{{ route('sub_activities.update',$sub_activity->id) }}">
                @method('PATCH')
                @csrf
                @if ($errors->any())
                <div class="col-md-10">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Name <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" placeholder="Input category name" name="name" value="{{$sub_activity->name}}">
                    </div><br>
                </div>

                <div class="form-group row">
                    <div class="col-sm-3">
                        <p></p>
                    </div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn mb-1 btn-primary">Update</span>
                        </button>
                    </div>
                    <input type="hidden" value="{{ URL::previous() }}" name="url">
                </div>

            </form>
        </div>
    </div>
</div>
<!-- #/ container -->
@endsection
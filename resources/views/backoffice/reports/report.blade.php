@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">Master Laporan</h1>
    <div class="section-header-breadcrumb">
        <div class="bottom text-right">
            <button id="modal-5" class="btn btn-sm mb-1 btn-primary"><i class="fa fa-plus"></i> Add Laporan</button>
        </div>
    </div>
     @error('project_id')
               <script type="text/javascript">
                     Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'Pilih Project Harus Terisi',
                    })
               </script>
    @enderror
</div>
<div class="card top">
    @include('sweet::alert')

    <div class="card-body">
        <div class="col-lg-12">
            <!-- Form Modal Report -->
            <form enctype="multipart/form-data" class="modal-part" id="modal-login-part" method="post" action="{{ route('reports.store') }}">
                @csrf
                <div class="form-group">
                    <label>Name</label>
                    <input required type="text" name="name" placeholder="Input Laporan name" class="form-control">
                </div>

                <div class="form-group">
                    <label>File</label>
                    <input required type="file" name="file" class="form-control">
                </div>

                <select class="form-control @error('project_id') is-invalid @enderror" name="project_id" required>
                    <option selected value=''>Pilih Project</option>
                    @foreach($project as $row)
                    <option value="{{$row->id}}">{{$row->name}}</option>
                    @endforeach
                </select>
                @error('project_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                    <label>Description</label>
                    <textarea name="description" class="form-control" cols="50" placeholder="Deskripsi Laporan"></textarea>
                </div>

                <div class="form-group">
                    <label> Date</label>
                    <input required type="date" name="date" class="form-control">
                </div>
            </form>
            <!-- End Form Modal Report -->

            <div class="col-md-8">
                <p></p>
            </div>
            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="exampledr">
                    
                    <thead>
                        <tr>
                            <th width="10%">No.</th>
                            <th width="10%">Name</th>
                            <th width="20%">File</th>
                            <th width="10%">Project</th>
                            <th width="20%">Description</th>
                            <th width="10%">Date</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($report as $index => $row)
                        <tr>
                            <td>{{$index+1}}.</td>
                            <td>{{$row->name}}</td>
                            <td>
                                <a href="/laporan/{{$row->file}}" target="_blank">{{$row->file}}</a>
                            </td>
                            <td>{{$row->get_project->name}}</td>
                            <td>{{$row->description}}</td>
                            <td>
                                <?php
                                $date = date_create($row->date);
                                echo date_format($date, "j F Y");
                                ?>
                            </td>
                            <td>
                                <form action="{{ route('reports.destroy', $row->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')

                                    <a onclick="return confirm('Are you sure to delete this data?')">
                                        <button class="btn btn-icon btn-danger" type="submit">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </a>

                                    <a href="{{ route('reports.edit', $row->id)}}">
                                        <button class="btn btn-icon btn-info" type="button">
                                            <i class="fas fa-edit"></i>

                                        </button>
                                    </a>

                                </form>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
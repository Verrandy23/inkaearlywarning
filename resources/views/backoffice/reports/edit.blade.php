@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="title">Edit Laporan</h1>
    <div class="section-header-breadcrumb">
        <a class="btn btn-primary btn-sm" href="{{ URL::previous() }}"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <h4 class="titleC text-center">Form Edit Laporan</h4><br>
        <div class="basic-form">
                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ route('reports.update',$data->id) }}">
                    @method('PATCH')
                    @csrf
                    @if ($errors->any())
                    <div class="col-md-10">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif 
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Name</label>
                        <input required type="text" name="name" value="{{$data->name}}" placeholder="Input Laporan name" class="form-control">
                    </div>

                    <div class="form-group col-md-6">
                        <label>File</label>
                        <input type="file" value="{{$data->file}}" name="file" class="form-control">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputState">Project <span class="text-danger">*</span></label>
                        <select class="form-control" name="project_id" required>
                            <option selected>Pilih Project</option>
                            @foreach($project as $row)
                            <option value="{{$row->id}}" {{ $data->get_project->id == $row->id ? 'selected' : '' }}>{{$row->name}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group col-md-6">
                        <label> Date</label>
                        <input required type="date" name="date" value="{{$data->date}}" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label>Description</label>
                    <textarea name="description" class="form-control" cols="50" placeholder="Deskripsi Laporan">{{$data->description}}</textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Update" class="btn btn-primary"></input>
                </div>
                
            </form>
        </div>
    </div>
 </div>     
 @endsection  
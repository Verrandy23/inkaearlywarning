@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="title">Edit Divsion Activity</h1>
    <div class="section-header-breadcrumb">
        <a class="btn btn-primary btn-sm" href="{{ URL::previous() }}"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
</div>
<div class="card top">
    <div class="card-body">
        <h4 class="titleC text-center">Form Edit Divsion Activity</h4><br>
        <div class="basic-form">
                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ route('division_additional.update',$data->id) }}">
                    @method('PATCH')
                    @csrf
                    @if ($errors->any())
                    <div class="col-md-10">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif 
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Name</label>
                        <input required type="text" name="name" value="{{$data->name}}" placeholder="Input Laporan name" class="form-control">
                    </div>

                    <div class="form-group col-md-6">
                        <label>File</label>
                        <input type="file" name="file" class="form-control">
                    </div>
                </div>

                <div class="form-row">
                 
                    <div class="form-group col-md-8">
                        <label>Information</label>
                        <textarea name="information" class="form-control" cols="50" placeholder="Information">{{$data->information}}</textarea>
                    </div>

                    <div class="form-group col-md-4">
                        <label> Date</label>
                        <input required type="date" name="date" value="{{$data->date}}" class="form-control">
                        <input required type="hidden" name="division_id" value="{{$data->division_id}}">
                        <input type="hidden" value="{{ URL::previous() }}" name="url">

                    </div>
                </div>

                
                <div class="form-group">
                    <input type="submit" value="Update" class="btn btn-primary"></input>
                </div>
                
            </form>
        </div>
    </div>
 </div>     
 @endsection  
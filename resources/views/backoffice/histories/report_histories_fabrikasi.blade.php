<!DOCTYPE html>
<html>
<head>
	<title>Report Histories</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Report Histories Divisi Fabrikasi</h4>
	</center><br>

    <div class="table-responsive">
        <table class="table col-lg-12 zero-configuration" id="exampledr">
            <thead>
                <tr>
                    <th width="20%">Project</th>
                    <th width="10%">Activity</th>
                    <th width="20%">Sub Activity</th>
                    <th width="20%">Work Activity</th>
                    <th width="10%">Finished At</th>
                    <th width="10%">Days Late</th>
                </tr>
            </thead>
            <tbody>
                @foreach($fabrikasi as $index => $row)
                <tr>
                    <td>{{$row->project_name}}</td>
                    <td>{{$row->activity_name}}</td>
                    <td>{{$row->sub_name}}</td>
                    <td>{{$row->work_name}}</td>
                    <td>
                        <?php
                        $date = date_create($row->finished_at);
                        echo date_format($date, "j F Y");
                        ?>
                    </td>
                    <td><strong class="badge badge-danger">{{$row->count_late}} Hari</strong></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</body>
</html>
@extends('layouts.backend.masterPage')
@section('content')

<div id="late">
<div class="section-header">
    <h1 class="text-center">Late Activities </h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
            <a href="/backoffice"><strong> Home</strong></a>
        </div>
        <br>
    </div>
</div>
<div class="card top">
    <br>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12 col-sm-5 col-lg-5">
                <div class="card">
                    <div class="card-body">
                        <!-- Late By Division-->
                        @if (Auth::user()->division_name == 'Teknologi')
                        <div class="col-12">
                            <form method="get" action="/report_latehistories_teknologi">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                            <!-- Filter Data -->
                                            <table>
                                                <tr>
                                                    <label style="margin-left:1%" for="filter">Filter By Project</label><th style="width:200px" class="filterhead"></th>
                                                    <th style="width:200px" class="filterhead"></th>
                                                    <label style="margin-left:24%" for="filter">Filter By Activity</label>
                                                    
                                                </tr>
                                            </table>
                                            <!-- End Filter Data  -->
                                            </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="section-header-breadcrumb">
                                                        <div class="bottom text-right">
                                                            <button class="btn btn-primary" type="submit" target="blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </form><br>
                           
                            <div class="table-responsive">
                                <table class="table col-lg-12 zero-configuration" id="exampledr">
                                    <thead>
                                        <tr>
                                            <th width="20%">Project</th>
                                            <th width="10%">Activity</th>
                                            <th width="20%">Sub Activity</th>
                                            <th width="20%">Work Activity</th>
                                            <th width="10%">End Date</th>
                                            <th width="10%">Hari Keterlambatan</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($teknologi as $index => $row)
                                        <tr>
                                            <td>{{$row->project_name}}</td>
                                            <td>{{$row->activity_name}}</td>
                                            <td>{{$row->sub_name}}</td>
                                            <td>{{$row->work_name}}</td>
                                            <td>
                                                <?php
                                                $date = date_create($row->end_date);
                                                echo date_format($date, "j F Y");
                                                ?>
                                            </td>
                                            <td><strong class="badge badge-danger">{{$row->late}} Hari</strong></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                        @if (Auth::user()->division_name == 'Logistik')
                        <div class="col-12">
                            <form method="get" action="/report_latehistories_logistik">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                            <!-- Filter Data -->
                                            <table>
                                                <tr>
                                                    <label style="margin-left:1%" for="filter">Filter By Project</label><th style="width:200px" class="filterhead"></th>
                                                    <th style="width:200px" class="filterhead"></th>
                                                    <label style="margin-left:24%" for="filter">Filter By Activity</label>
                                                    
                                                </tr>
                                            </table>
                                            <!-- End Filter Data  -->
                                            </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="section-header-breadcrumb">
                                                        <div class="bottom text-right">
                                                            <button class="btn btn-primary" type="submit" target="blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </form><br>

                            <div class="table-responsive">
                                <table class="table col-lg-12 zero-configuration" id="exampledr">
                                    <thead>
                                        <tr>
                                            <th width="20%">Project</th>
                                            <th width="23%">Activity</th>
                                            <th width="20%">Sub Activity</th>
                                            <th width="20%">Work Activity</th>
                                            <th width="10%">End Date</th>
                                            <th width="17%">Slack</th>
                                            <th width="10%">Hari Keterlambatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($logistik as $index => $row)
                                        <tr>
                                            <td>{{$row->project_name}}</td>
                                            <td>{{$row->activity_name}}</td>
                                            <td>{{$row->sub_name}}</td>
                                            <td>{{$row->work_name}}</td>
                                            <td>
                                                <?php
                                                $date = date_create($row->end_date);
                                                echo date_format($date, "j F Y");
                                                ?>
                                            </td>
                                            <td>{{$row->slack}} Days</td>
                                            <td><strong class="badge badge-danger">{{$row->late}} Hari</strong></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                        @if (Auth::user()->division_name == 'Fabrikasi')
                        <div class="col-12">
                            <form method="get" action="/report_latehistories_fabrikasi">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                            <!-- Filter Data -->
                                            <table>
                                                <tr>
                                                    <label style="margin-left:1%" for="filter">Filter By Project</label><th style="width:200px" class="filterhead"></th>
                                                    <th style="width:200px" class="filterhead"></th>
                                                    <label style="margin-left:24%" for="filter">Filter By Activity</label>
                                                    
                                                </tr>
                                            </table>
                                            <!-- End Filter Data  -->
                                            </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="section-header-breadcrumb">
                                                        <div class="bottom text-right">
                                                            <button class="btn btn-primary" type="submit" target="blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </form><br>

                            <div class="table-responsive">
                                <table class="table col-lg-12 zero-configuration" id="exampledr">
                                    <thead>
                                        <tr>
                                            <th width="20%">Project</th>
                                            <th width="10%">Activity</th>
                                            <th width="20%">Sub Activity</th>
                                            <th width="20%">Work Activity</th>
                                            <th width="10%">End Date</th>
                                            <th width="10%">Hari Keterlambatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($fabrikasi as $index => $row)
                                        <tr>
                                            <td>{{$row->project_name}}</td>
                                            <td>{{$row->activity_name}}</td>
                                            <td>{{$row->sub_name}}</td>
                                            <td>{{$row->work_name}}</td>
                                            <td>
                                                <?php
                                                $date = date_create($row->end_date);
                                                echo date_format($date, "j F Y");
                                                ?>
                                            </td>
                                            <td><strong class="badge badge-danger">{{$row->late}} Hari</strong></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                        @if (Auth::user()->division_name == 'Finishing')
                        <div class="col-12">
                             <form method="get" action="/report_latehistories_finishing">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                            <!-- Filter Data -->
                                            <table>
                                                <tr>
                                                    <label style="margin-left:1%" for="filter">Filter By Project</label><th style="width:200px" class="filterhead"></th>
                                                    <th style="width:200px" class="filterhead"></th>
                                                    <label style="margin-left:24%" for="filter">Filter By Activity</label>
                                                    
                                                </tr>
                                            </table>
                                            <!-- End Filter Data  -->
                                            </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="section-header-breadcrumb">
                                                        <div class="bottom text-right">
                                                            <button class="btn btn-primary" type="submit" target="blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </form>
                                        <br>

                            <div class="table-responsive">
                                <table class="table col-lg-12 zero-configuration" id="exampledr">
                                    <thead>
                                        <tr>
                                            <th width="20%">Project</th>
                                            <th width="10%">Activity</th>
                                            <th width="20%">Sub Activity</th>
                                            <th width="20%">Work Activity</th>
                                            <th width="10%">End Date</th>
                                            <th width="10%">Hari Keterlambatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($finishing as $index => $row)
                                        <tr>
                                            <td>{{$row->project_name}}</td>
                                            <td>{{$row->activity_name}}</td>
                                            <td>{{$row->sub_name}}</td>
                                            <td>{{$row->work_name}}</td>
                                            <td>
                                                <?php
                                                $date = date_create($row->end_date);
                                                echo date_format($date, "j F Y");
                                                ?>
                                            </td>
                                            <td><strong class="badge badge-danger">{{$row->late}} Hari</strong></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                        @if (Auth::user()->division_name == 'Testing')
                        <div class="col-12">
                             <form method="get" action="/report_latehistories_testing">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                            <!-- Filter Data -->
                                            <table>
                                                <tr>
                                                    <label style="margin-left:1%" for="filter">Filter By Project</label><th style="width:200px" class="filterhead"></th>
                                                    <th style="width:200px" class="filterhead"></th>
                                                    <label style="margin-left:24%" for="filter">Filter By Activity</label>
                                                    
                                                </tr>
                                            </table>
                                            <!-- End Filter Data  -->
                                            </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="section-header-breadcrumb">
                                                        <div class="bottom text-right">
                                                            <button class="btn btn-primary" type="submit" target="blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </form><br>

                            <div class="table-responsive">
                                <table class="table col-lg-12 zero-configuration" id="exampledr">
                                    <thead>
                                        <tr>
                                            <th width="20%">Project</th>
                                            <th width="10%">Activity</th>
                                            <th width="20%">Sub Activity</th>
                                            <th width="20%">Work Activity</th>
                                            <th width="10%">End Date</th>
                                            <th width="10%">Hari Keterlambatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($testing as $index => $row)
                                        <tr>
                                            <td>{{$row->project_name}}</td>
                                            <td>{{$row->activity_name}}</td>
                                            <td>{{$row->sub_name}}</td>
                                            <td>{{$row->work_name}}</td>
                                            <td>
                                                <?php
                                                $date = date_create($row->end_date);
                                                echo date_format($date, "j F Y");
                                                ?>
                                            </td>
                                            <td><strong class="badge badge-danger">{{$row->late}} Hari</strong></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                        @if (Auth::user()->division_name == 'Delivery')
                        <div class="col-12">
                          <form method="get" action="/report_latehistories_delivery">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                            <!-- Filter Data -->
                                            <table>
                                                <tr>
                                                    <label style="margin-left:1%" for="filter">Filter By Project</label><th style="width:200px" class="filterhead"></th>
                                                    <th style="width:200px" class="filterhead"></th>
                                                    <label style="margin-left:24%" for="filter">Filter By Activity</label>
                                                    
                                                </tr>
                                            </table>
                                            <!-- End Filter Data  -->
                                            </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="section-header-breadcrumb">
                                                        <div class="bottom text-right">
                                                            <button class="btn btn-primary" type="submit" target="blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </form>
                                    <br>

                            <div class="table-responsive">
                                <table class="table col-lg-12 zero-configuration" id="exampledr">
                                    <thead>
                                        <tr>
                                            <th>Project</th>
                                            <th>Activity</th>
                                            <th>End Date</th>
                                            <th>Hari Keterlambatan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($delivery as $index => $row)
                                        <tr>
                                            
                                            <td>{{$row->project_name}}</td>
                                            <td>{{$row->activity_name}}</td>
                                            <td>
                                                <?php
                                                $date = date_create($row->end_date);
                                                echo date_format($date, "j F Y");
                                                ?>
                                            </td>
                                            <td><strong class="badge badge-danger">{{$row->late}} Hari</strong></td>
                                            <td></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                        <!-- End By Division-->
                        
                        <!-- Admin -->
                        @if (Auth::user()->division_name == 'Keproyekan')
                        <ul class="nav nav-pills" id="myTab3" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="teknologi-tab1" data-toggle="tab" href="#teknologi1" role="tab" aria-controls="teknologi" aria-selected="true">Teknologi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="logistik-tab1" data-toggle="tab" href="#logistik1" role="tab" aria-controls="logistik" aria-selected="false">Logistik</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="fabrikasi-tab1" data-toggle="tab" href="#fabrikasi1" role="tab" aria-controls="fabrikasi" aria-selected="false">Fabrikasi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="finishing-tab1" data-toggle="tab" href="#finishing1" role="tab" aria-controls="finishing" aria-selected="false">Finishing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="testing-tab1" data-toggle="tab" href="#testing1" role="tab" aria-controls="testing" aria-selected="false">Testing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="delivery-tab1" data-toggle="tab" href="#delivery1" role="tab" aria-controls="delivery" aria-selected="false">Delivery</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent2">
                            <div class="tab-pane fade show active" id="teknologi1" role="tabpanel" aria-labelledby="teknologi-tab1">
                                <div class="row">
                                    <div class="table-responsive">
                                   <form method="get" action="/report_latehistories_teknologi">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                            <!-- Filter Data -->
                                            <table>
                                                <tr>
                                                    <label style="margin-left:1%" for="filter">Filter By Project</label><th style="width:200px" class="filterhead"></th>
                                                    <th style="width:200px" class="filterhead"></th>
                                                    <label style="margin-left:24%" for="filter">Filter By Activity</label>
                                                    
                                                </tr>
                                            </table>
                                            <!-- End Filter Data  -->
                                            </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="section-header-breadcrumb">
                                                        <div class="bottom text-right">
                                                            <button class="btn btn-primary" type="submit" target="blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </form><br>
                                        <table class="table col-lg-12 zero-configuration" id="exampledr">
                                            <thead>
                                                <tr>
                                                    <th width="15%">Project</th>
                                                    <th width="20%">Activity</th>
                                                    <th width="20%">Sub Activity</th>
                                                    <th width="20%">Work Activity</th>
                                                    <th width="10%">End Date</th>
                                                    <th width="5%">Hari Keterlambatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($teknologi as $index => $row)
                                                <tr>
                                                    <td>{{$row->project_name}}</td>
                                                    <td>{{$row->activity_name}}</td>
                                                    <td>{{$row->sub_name}}</td>
                                                    <td>{{$row->work_name}}</td>
                                                    <td>
                                                        <?php
                                                        $date = date_create($row->end_date);
                                                        echo date_format($date, "j F Y");
                                                        ?>
                                                    </td>
                                                    <td><strong class="badge badge-danger">{{$row->late}} Hari</strong></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                            </div>
                            <div class="tab-pane fade" id="logistik1" role="tabpanel" aria-labelledby="logistik-tab1">
                                <div class="row">
                                    <div class="table-responsive">
                                        <form method="get" action="/report_latehistories_logistik">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                            <!-- Filter Data -->
                                            <table>
                                                <tr>
                                                    <label style="margin-left:1%" for="filter">Filter By Project</label><th style="width:200px" class="filterhead2"></th>
                                                    <th style="width:200px" class="filterhead2"></th>
                                                    <label style="margin-left:24%" for="filter">Filter By Activity</label>
                                                    
                                                </tr>
                                            </table>
                                            <!-- End Filter Data  -->
                                            </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="section-header-breadcrumb">
                                                        <div class="bottom text-right">
                                                            <button class="btn btn-primary" type="submit" target="blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </form><br>
                                        <table class="table col-lg-12 zero-configuration" id="exampledr2">
                                            <thead>
                                                <tr>
                                                    <th width="15%">Project</th>
                                                    <th width="20%">Activity</th>
                                                    <th width="20%">Sub Activity</th>
                                                    <th width="20%">Work Activity</th>
                                                    <th width="10%">End Date</th>
                                                    <th width="5%">Hari Keterlambatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($logistik as $index => $row)
                                                <tr>
                                                    <td>{{$row->project_name}}</td>
                                                    <td>{{$row->activity_name}}</td>
                                                    <td>{{$row->sub_name}}</td>
                                                    <td>{{$row->work_name}}</td>
                                                    <td>
                                                        <?php
                                                        $date = date_create($row->end_date);
                                                        echo date_format($date, "j F Y");
                                                        ?>
                                                    </td>
                                                    <td><strong class="badge badge-danger">{{$row->late}} Hari</strong></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="fabrikasi1" role="tabpanel" aria-labelledby="fabrikasi-tab1">
                                <div class="row">
                                    <div class="table-responsive">
                                        <form method="get" action="/report_latehistories_fabrikasi">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                            <!-- Filter Data -->
                                            <table>
                                                <tr>
                                                    <label style="margin-left:1%" for="filter">Filter By Project</label><th style="width:200px" class="filterhead3"></th>
                                                    <th style="width:200px" class="filterhead3"></th>
                                                    <label style="margin-left:24%" for="filter">Filter By Activity</label>
                                                    
                                                </tr>
                                            </table>
                                            <!-- End Filter Data  -->
                                            </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="section-header-breadcrumb">
                                                        <div class="bottom text-right">
                                                            <button class="btn btn-primary" type="submit" target="blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </form>
                                        <br>
                                        <table class="table col-lg-12 zero-configuration" id="exampledr3">
                                            <thead>
                                                <tr>
                                                    <th width="15%">Project</th>
                                                    <th width="20%">Activity</th>
                                                    <th width="20%">Sub Activity</th>
                                                    <th width="20%">Work Activity</th>
                                                    <th width="10%">End Date</th>
                                                    <th width="5%">Hari Keterlambatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($fabrikasi as $index => $row)
                                                <tr>
                                                    <td>{{$row->project_name}}</td>
                                                    <td>{{$row->activity_name}}</td>
                                                    <td>{{$row->sub_name}}</td>
                                                    <td>{{$row->work_name}}</td>
                                                    <td>
                                                        <?php
                                                        $date = date_create($row->end_date);
                                                        echo date_format($date, "j F Y");
                                                        ?>
                                                    </td>
                                                    <td><strong class="badge badge-danger">{{$row->late}} Hari</strong></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="finishing1" role="tabpanel" aria-labelledby="finishing-tab1">
                                <div class="row">
                                    <div class="table-responsive">
                                        <form method="get" action="/report_latehistories_finishing">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                            <!-- Filter Data -->
                                            <table>
                                                <tr>
                                                    <label style="margin-left:1%" for="filter">Filter By Project</label><th style="width:200px" class="filterhead4"></th>
                                                    <th style="width:200px" class="filterhead4"></th>
                                                    <label style="margin-left:24%" for="filter">Filter By Activity</label>
                                                    
                                                </tr>
                                            </table>
                                            <!-- End Filter Data  -->
                                            </div>
                                            
                                        
                                                <div class="col-md-6 col-12">
                                                    <div class="section-header-breadcrumb">
                                                        <div class="bottom text-right">
                                                            <button class="btn btn-primary" type="submit" target="blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </form>
                                        <br>

                                        <table class="table col-lg-12 zero-configuration" id="exampledr4">
                                            <thead>
                                                <tr>
                                                    <th width="15%">Project</th>
                                                    <th width="20%">Activity</th>
                                                    <th width="20%">Sub Activity</th>
                                                    <th width="20%">Work Activity</th>
                                                    <th width="10%">End Date</th>
                                                    <th width="5%">Hari Keterlambatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($finishing as $index => $row)
                                                <tr>
                                                    <td>{{$row->project_name}}</td>
                                                    <td>{{$row->activity_name}}</td>
                                                    <td>{{$row->sub_name}}</td>
                                                    <td>{{$row->work_name}}</td>
                                                    <td>
                                                        <?php
                                                        $date = date_create($row->end_date);
                                                        echo date_format($date, "j F Y");
                                                        ?>
                                                    </td>
                                                    <td><strong class="badge badge-danger">{{$row->late}} Hari</strong></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="testing1" role="tabpanel" aria-labelledby="testing-tab1">
                                <div class="row">
                                    <div class="table-responsive">
                                        <form method="get" action="/report_latehistories_testing">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                            <!-- Filter Data -->
                                            <table>
                                                <tr>
                                                    <label style="margin-left:1%" for="filter">Filter By Project</label><th style="width:200px" class="filterhead5"></th>
                                                    <th style="width:200px" class="filterhead5"></th>
                                                    <label style="margin-left:24%" for="filter">Filter By Activity</label>
                                                    
                                                </tr>
                                            </table>
                                            <!-- End Filter Data  -->
                                            </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="section-header-breadcrumb">
                                                        <div class="bottom text-right">
                                                            <button class="btn btn-primary" type="submit" target="blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </form>

                                        <br>

                                        <table class="table col-lg-12 zero-configuration" id="exampledr5">
                                            <thead>
                                                <tr>
                                                    <th width="15%">Project</th>
                                                    <th width="20%">Activity</th>
                                                    <th width="20%">Sub Activity</th>
                                                    <th width="20%">Work Activity</th>
                                                    <th width="10%">End Date</th>
                                                    <th width="5%">Hari Keterlambatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($testing as $index => $row)
                                                <tr>
                                                    <td>{{$row->project_name}}</td>
                                                    <td>{{$row->activity_name}}</td>
                                                    <td>{{$row->sub_name}}</td>
                                                    <td>{{$row->work_name}}</td>
                                                    <td>
                                                        <?php
                                                        $date = date_create($row->end_date);
                                                        echo date_format($date, "j F Y");
                                                        ?>
                                                    </td>
                                                    <td><strong class="badge badge-danger">{{$row->late}} Hari</strong></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="delivery1" role="tabpanel" aria-labelledby="delivery-tab1">
                                <div class="row">
                                    
                                    <div class="table-responsive">
                                        <form method="get" action="/report_latehistories_delivery">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                            <!-- Filter Data -->
                                            <table>
                                                <tr>
                                                    <label style="margin-left:1%" for="filter">Filter By Project</label><th style="width:200px" class="filterhead6"></th>
                                                    <th style="width:200px" class="filterhead6"></th>
                                                    <label style="margin-left:24%" for="filter">Filter By Activity</label>
                                                    
                                                </tr>
                                            </table>
                                            <!-- End Filter Data  -->
                                            </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="section-header-breadcrumb">
                                                        <div class="bottom text-right">
                                                            <button class="btn btn-primary" type="submit" target="blank" style="margin-left:4%"><i class="fas fa-file-download"></i> Generate Report</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        </form>
                                        <br>

                                        <table class="table col-lg-12 zero-configuration" id="exampledr6">
                                            <thead>
                                                <tr>
                                                  
                                                    <th width="15%">Project</th>
                                                    <th width="20%">Activity</th>
                                                    <th width="20%">Sub Activity</th>
                                                    <th width="20%">Work Activity</th>
                                                    <th width="10%">End Date</th>
                                                    <th width="5%">Hari Keterlambatan</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($delivery as $index => $row)
                                                <tr>
                                                    
                                                    <td>{{$row->project_name}}</td>
                                                    <td>{{$row->activity_name}}</td>
                                                    <td>{{$row->sub_name}}</td>
                                                    <td>{{$row->work_name}}</td>
                                                    <td>
                                                        <?php
                                                        $date = date_create($row->end_date);
                                                        echo date_format($date, "j F Y");
                                                        ?>
                                                    </td>
                                                    <td><strong class="badge badge-danger">{{$row->late}} Hari</strong></td>
                                                
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
 </div>


</div>
 
 <script type="text/javascript">
    

 </script>   
    @endsection
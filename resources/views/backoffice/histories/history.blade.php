@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="text-center">History Activities </h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
            <a href="/backoffice"><strong> Home</strong></a>
        </div>
        <br>
    </div>
</div>
<div class="card top">
    <br>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12 col-sm-5 col-lg-5">
                <div class="card">
                    <div class="card-body">
                        <!-- late By Division-->
                        @if (Auth::user()->division_name == 'Teknologi')
                        <div class="col-12">
                            <!-- Filter Data -->
                            <table>
                                <tr>
                                    <label for="filter">Filter By Project</label>
                                    <th class="filterhead"></th>
                                    <label style="margin-left:4%" for="filter">Filter By Activity</label>
                                    <th class="filterhead"></th>
                                </tr>
                            </table><br>
                            <!-- End Filter Data -->
                            <div class="table-responsive">
                                <table class="table col-lg-12 zero-configuration" id="exampledr">
                                    <thead>
                                        <tr>
                                            <th width="20%">Project</th>
                                            <th width="10%">Activity</th>
                                            <th width="20%">Sub Activity</th>
                                            <th width="20%">Work Activity</th>
                                            <th width="10%">Finished At</th>
                                            <th width="10%">Days Late</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($teknologi as $index => $row)
                                        <tr>
                                            <td>{{$row->project_name}}</td>
                                            <td>{{$row->activity_name}}</td>
                                            <td>{{$row->sub_name}}</td>
                                            <td>{{$row->work_name}}</td>
                                            <td>
                                                <?php
                                                $date = date_create($row->finished_at);
                                                echo date_format($date, "j F Y");
                                                ?>
                                            </td>
                                            <td><strong class="badge badge-danger">{{$row->count_late}} Hari</strong></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                        @if (Auth::user()->division_name == 'Logistik')
                        <div class="col-12">
                            <!-- Filter Data -->
                            <table>
                                <tr>
                                    <label for="filter">Filter By Project</label>
                                    <th class="filterhead"></th>
                                    <label style="margin-left:4%" for="filter">Filter By Activity</label>
                                    <th class="filterhead"></th>
                                </tr>
                            </table><br>
                            <!-- End Filter Data -->
                            <div class="table-responsive">
                                <table class="table col-lg-12 zero-configuration" id="exampledr">
                                    <thead>
                                        <tr>
                                            <th width="20%">Project</th>
                                            <th width="23%">Activity</th>
                                            <th width="20%">Sub Activity</th>
                                            <th width="20%">Work Activity</th>
                                            <th width="10%">Finished At</th>
                                            <th width="10%">Days Late</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($logistik as $row)
                                        <tr>
                                            <td>{{$row->project_name}}</td>
                                            <td>{{$row->activity_name}}</td>
                                            <td>{{$row->sub_name}}</td>
                                            <td>{{$row->work_name}}</td>
                                            <td>
                                                <?php
                                                $date = date_create($row->finished_at);
                                                echo date_format($date, "j F Y");
                                                ?>
                                            </td>
                                            <td><strong class="badge badge-danger">{{$row->count_late}} Hari</strong></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                        @if (Auth::user()->division_name == 'Fabrikasi')
                        <div class="col-12">
                            <!-- Filter Data -->
                            <table>
                                <tr>
                                    <label for="filter">Filter By Project</label>
                                    <th class="filterhead"></th>
                                    <label style="margin-left:4%" for="filter">Filter By Activity</label>
                                    <th class="filterhead"></th>
                                </tr>
                            </table><br>
                            <!-- End Filter Data -->
                            <div class="table-responsive">
                                <table class="table col-lg-12 zero-configuration" id="exampledr">
                                    <thead>
                                        <tr>
                                            <th width="20%">Project</th>
                                            <th width="10%">Activity</th>
                                            <th width="20%">Sub Activity</th>
                                            <th width="20%">Work Activity</th>
                                            <th width="10%">Finished At</th>
                                            <th width="10%">Days Late</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($fabrikasi as $index => $row)
                                        <tr>
                                            <td>{{$row->project_name}}</td>
                                            <td>{{$row->activity_name}}</td>
                                            <td>{{$row->sub_name}}</td>
                                            <td>{{$row->work_name}}</td>
                                            <td>
                                                <?php
                                                $date = date_create($row->finished_at);
                                                echo date_format($date, "j F Y");
                                                ?>
                                            </td>
                                            <td><strong class="badge badge-danger">{{$row->count_late}} Hari</strong></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                        @if (Auth::user()->division_name == 'Finishing')
                        <div class="col-12">
                            <!-- Filter Data -->
                            <table>
                                <tr>
                                    <label for="filter">Filter By Project</label>
                                    <th class="filterhead"></th>
                                    <label style="margin-left:4%" for="filter">Filter By Activity</label>
                                    <th class="filterhead"></th>
                                </tr>
                            </table><br>
                            <!-- End Filter Data -->
                            <div class="table-responsive">
                                <table class="table col-lg-12 zero-configuration" id="exampledr">
                                    <thead>
                                        <tr>
                                            <th width="20%">Project</th>
                                            <th width="10%">Activity</th>
                                            <th width="20%">Sub Activity</th>
                                            <th width="20%">Work Activity</th>
                                            <th width="10%">Finished At</th>
                                            <th width="10%">Days Late</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($finishing as $index => $row)
                                        <tr>
                                            <td>{{$row->project_name}}</td>
                                            <td>{{$row->activity_name}}</td>
                                            <td>{{$row->sub_name}}</td>
                                            <td>{{$row->work_name}}</td>
                                            <td>
                                                <?php
                                                $date = date_create($row->finished_at);
                                                echo date_format($date, "j F Y");
                                                ?>
                                            </td>
                                            <td><strong class="badge badge-danger">{{$row->count_late}} Hari</strong></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                        @if (Auth::user()->division_name == 'Testing')
                        <div class="col-12">
                            <!-- Filter Data -->
                            <table>
                                <tr>
                                    <label for="filter">Filter By Project</label>
                                    <th class="filterhead"></th>
                                    <label style="margin-left:4%" for="filter">Filter By Activity</label>
                                    <th class="filterhead"></th>
                                </tr>
                            </table><br>
                            <!-- End Filter Data -->
                            <div class="table-responsive">
                                <table class="table col-lg-12 zero-configuration" id="exampledr">
                                    <thead>
                                        <tr>
                                            <th width="20%">Project</th>
                                            <th width="10%">Activity</th>
                                            <th width="20%">Sub Activity</th>
                                            <th width="20%">Work Activity</th>
                                            <th width="10%">Finished At</th>
                                            <th width="10%">Days Late</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($testing as $index => $row)
                                        <tr>
                                            <td>{{$row->project_name}}</td>
                                            <td>{{$row->activity_name}}</td>
                                            <td>{{$row->sub_name}}</td>
                                            <td>{{$row->work_name}}</td>
                                            <td>
                                                <?php
                                                $date = date_create($row->finished_at);
                                                echo date_format($date, "j F Y");
                                                ?>
                                            </td>
                                            <td><strong class="badge badge-danger">{{$row->count_late}} Hari</strong></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                       
                        <!-- End By Division-->
                        
                        <!-- Admin -->
                        @if (Auth::user()->division_name == 'Keproyekan')
                        <ul class="nav nav-pills" id="myTab3" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="teknologi-tab1" data-toggle="tab" href="#teknologi1" role="tab" aria-controls="teknologi" aria-selected="true">Teknologi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="logistik-tab1" data-toggle="tab" href="#logistik1" role="tab" aria-controls="logistik" aria-selected="false">Logistik</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="fabrikasi-tab1" data-toggle="tab" href="#fabrikasi1" role="tab" aria-controls="fabrikasi" aria-selected="false">Fabrikasi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="finishing-tab1" data-toggle="tab" href="#finishing1" role="tab" aria-controls="finishing" aria-selected="false">Finishing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="testing-tab1" data-toggle="tab" href="#testing1" role="tab" aria-controls="testing" aria-selected="false">Testing</a>
                            </li>
                            
                        </ul>
                        <div class="tab-content" id="myTabContent2">

                        <div class="tab-pane fade show active" id="teknologi1" role="tabpanel" aria-labelledby="teknologi-tab1">
                            <div class="col-md-12 bottom">
                                <div class="col-12">
                                    <!-- Filter Data -->
                                    <table>
                                    <tr>
                                        <label style="margin-left:1%" for="filter">Filter By Project</label>
                                        <th class="filterhead"></th>
                
                                        <label style="margin-left:4%" for="filter">Filter By Activity</label>
                                        <th class="filterhead"></th>

                                    </tr>
                                    </table>
                                    <!-- End Filter Data -->
                                    <div class="table-responsive">
                                        <table class="table col-lg-12 zero-configuration" id="exampledr">
                                            <thead>
                                                <tr>
                                                    <th width="15%">Project</th>
                                                    <th width="20%">Activity</th>
                                                    <th width="20%">Sub Activity</th>
                                                    <th width="20%">Work Activity</th>
                                                    <th width="10%">Finished At</th>
                                                    <th width="5%">Days Late</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($teknologi as $index => $row)
                                                <tr>
                                                    <td>{{$row->project_name}}</td>
                                                    <td>{{$row->activity_name}}</td>
                                                    <td>{{$row->sub_name}}</td>
                                                    <td>{{$row->work_name}}</td>
                                                    <td>
                                                        <?php
                                                        $date = date_create($row->finished_at);
                                                        echo date_format($date, "j F Y");
                                                        ?>
                                                    </td>
                                                    <td><strong class="badge badge-danger">{{$row->count_late}} Hari</strong></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="logistik1" role="tabpanel" aria-labelledby="logistik-tab1">
                            <div class="col-md-12 bottom">
                                <div class="col-12">
                                    <!-- Filter Data -->
                                    <table>
                                    <tr>
                                        <label style="margin-left:1%" for="filter">Filter By Project</label>
                                        <th class="filterhead2"></th>
                
                                        <label style="margin-left:4%" for="filter">Filter By Activity</label>
                                        <th class="filterhead2"></th>

                                    </tr>
                                    </table>
                                    <!-- End Filter Data -->
                                    <div class="table-responsive">
                                        <table class="table col-lg-12 zero-configuration" id="exampledr2">
                                            <thead>
                                                <tr>
                                                    <th width="15%">Project</th>
                                                    <th width="20%">Activity</th>
                                                    <th width="20%">Sub Activity</th>
                                                    <th width="20%">Work Activity</th>
                                                    <th width="10%">Finished At</th>
                                                    <th width="5%">Days Late</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($logistik as $index => $row)
                                                <tr>
                                                    <td>{{$row->project_name}}</td>
                                                    <td>{{$row->activity_name}}</td>
                                                    <td>{{$row->sub_name}}</td>
                                                    <td>{{$row->work_name}}</td>
                                                    <td>
                                                        <?php
                                                        $date = date_create($row->finished_at);
                                                        echo date_format($date, "j F Y");
                                                        ?>
                                                    </td>
                                                    <td><strong class="badge badge-danger">{{$row->count_late}} Hari</strong></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="fabrikasi1" role="tabpanel" aria-labelledby="fabrikasi-tab1">
                            <div class="col-md-12 bottom">
                                <div class="col-12">
                                    <!-- Filter Data -->
                                    <table>
                                    <tr>
                                        <label style="margin-left:1%" for="filter">Filter By Project</label>
                                        <th class="filterhead3"></th>
                
                                        <label style="margin-left:4%" for="filter">Filter By Activity</label>
                                        <th class="filterhead3"></th>

                                    </tr>
                                    </table>
                                    <!-- End Filter Data -->
                                    <div class="table-responsive">
                                        <table class="table col-lg-12 zero-configuration" id="exampledr3">
                                            <thead>
                                                <tr>
                                                    <th width="15%">Project</th>
                                                    <th width="20%">Activity</th>
                                                    <th width="20%">Sub Activity</th>
                                                    <th width="20%">Work Activity</th>
                                                    <th width="10%">Finished At</th>
                                                    <th width="5%">Days Late</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($fabrikasi as $index => $row)
                                                <tr>
                                                    <td>{{$row->project_name}}</td>
                                                    <td>{{$row->activity_name}}</td>
                                                    <td>{{$row->sub_name}}</td>
                                                    <td>{{$row->work_name}}</td>
                                                    <td>
                                                        <?php
                                                        $date = date_create($row->finished_at);
                                                        echo date_format($date, "j F Y");
                                                        ?>
                                                    </td>
                                                    <td><strong class="badge badge-danger">{{$row->count_late}} Hari</strong></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="finishing1" role="tabpanel" aria-labelledby="finishing-tab1">
                            <div class="col-md-12 bottom">
                                <div class="col-12">
                                    <!-- Filter Data -->
                                    <table>
                                    <tr>
                                        <label style="margin-left:1%" for="filter">Filter By Project</label>
                                        <th class="filterhead4"></th>
                
                                        <label style="margin-left:4%" for="filter">Filter By Activity</label>
                                        <th class="filterhead4"></th>

                                    </tr>
                                    </table>
                                    <!-- End Filter Data -->
                                    <div class="table-responsive">
                                        <table class="table col-lg-12 zero-configuration" id="exampledr4">
                                            <thead>
                                                <tr>
                                                    <th width="15%">Project</th>
                                                    <th width="20%">Activity</th>
                                                    <th width="20%">Sub Activity</th>
                                                    <th width="20%">Work Activity</th>
                                                    <th width="10%">Finished At</th>
                                                    <th width="5%">Days Late</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($finishing as $index => $row)
                                                <tr>
                                                    <td>{{$row->project_name}}</td>
                                                    <td>{{$row->activity_name}}</td>
                                                    <td>{{$row->sub_name}}</td>
                                                    <td>{{$row->work_name}}</td>
                                                    <td>
                                                        <?php
                                                        $date = date_create($row->finished_at);
                                                        echo date_format($date, "j F Y");
                                                        ?>
                                                    </td>
                                                    <td><strong class="badge badge-danger">{{$row->count_late}} Hari</strong></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="testing1" role="tabpanel" aria-labelledby="testing-tab1">
                        <div class="col-md-12 bottom">
                                <div class="col-12">
                                    <!-- Filter Data -->
                                    <table>
                                    <tr>
                                        <label style="margin-left:1%" for="filter">Filter By Project</label>
                                        <th class="filterhead5"></th>
                
                                        <label style="margin-left:4%" for="filter">Filter By Activity</label>
                                        <th class="filterhead5"></th>

                                    </tr>
                                    </table>
                                    <!-- End Filter Data -->
                                    <div class="table-responsive">
                                        <table class="table col-lg-12 zero-configuration" id="exampledr5">
                                            <thead>
                                                <tr>
                                                    <th width="15%">Project</th>
                                                    <th width="20%">Activity</th>
                                                    <th width="20%">Sub Activity</th>
                                                    <th width="20%">Work Activity</th>
                                                    <th width="10%">Finished At</th>
                                                    <th width="5%">Days Late</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($testing as $index => $row)
                                                <tr>
                                                    <td>{{$row->project_name}}</td>
                                                    <td>{{$row->activity_name}}</td>
                                                    <td>{{$row->sub_name}}</td>
                                                    <td>{{$row->work_name}}</td>
                                                    <td>
                                                        <?php
                                                        $date = date_create($row->finished_at);
                                                        echo date_format($date, "j F Y");
                                                        ?>
                                                    </td>
                                                    <td><strong class="badge badge-danger">{{$row->count_late}} Hari</strong></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
           
                    </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
 </div>
    @endsection
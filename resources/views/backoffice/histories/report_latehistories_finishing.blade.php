<!DOCTYPE html>
<html>
<head>
	<title>Report Late Histories</title>
	
</head>
<body>
	<style type="text/css">
		  table tr td{
        text-align: center;
        border: 1px solid black;
    }
    ,
    table th{
        border:2px solid blue;
         font-size: 13pt; 
      

      
    }
    table{
      width: 100%;
    }
    table .late{
      color: #ffffff;
      background-color: #e8505b;
    }
	</style>
	<center>
		<h5>Report Late Histories Divisi Finishing</h4>
	</center><br>

	<div class="table-responsive">
        <table class="table col-lg-12 zero-configuration" id="exampledr">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="20%">Project</th>
                    <th width="10%">Activity</th>
                    <th width="20%">Sub Activity</th>
                    <th width="20%">Work Activity</th>
                    <th width="10%">End Date</th>
                    <th width="10%">Hari Keterlambatan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($finishing as $index => $row)
                <tr>
                    <td>{{ $index+1 }}</td>
                    <td>{{$row->project_name}}</td>
                    <td>{{$row->activity_name}}</td>
                    <td>{{$row->sub_name}}</td>
                    <td>{{$row->work_name}}</td>
                    <td>
                        <?php
                        $date = date_create($row->end_date);
                        echo date_format($date, "j F Y");
                        ?>
                    </td>
                    <td class="late"><strong class="">{{$row->late}} Hari</strong></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</body>
</html>
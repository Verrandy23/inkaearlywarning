<!-- Count late for admin -->
@if(Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4)
<span>{{$count_late_admin}}</span>

<!-- Count late for division -->
@elseif(Auth::user()->role_id == 4)
<span>{{$count_late_pm}}</span>

<!-- Count late for division -->
@else
<span>{{$count_late_division}}</span>
@endif
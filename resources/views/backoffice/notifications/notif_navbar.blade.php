<!-- Count late for admin -->
@if(Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4)
@foreach($late_admin_navbar as $data)
<?php if (!empty($data->project_name)) { ?>
<a href="#" class="dropdown-item dropdown-item-unread">
    <div class="dropdown-item-icon bg-danger text-white">
        <i class="fas fa-exclamation-triangle"></i>
    </div>
    <div class="dropdown-item-desc">
        &rsaquo; Project {{$data->project_name}} <br>
        &rsaquo; Division {{$data->division_name}} <br>
        &rsaquo; {{$data->activity_name}} <br> 
        &rsaquo; {{$data->work_name}} <strong class="badge badge-warning"> {{$data->late}} Day Late </strong>
    </div>
</a>
<?php } else {?>
<?php } ?>
@endforeach

@elseif(Auth::user()->role_id == 4)
@foreach($late_pm_navbar as $data)
<?php if (!empty($data->project_name)) { ?>
<a href="#" class="dropdown-item dropdown-item-unread">
    <div class="dropdown-item-icon bg-danger text-white">
        <i class="fas fa-exclamation-triangle"></i>
    </div>
    <div class="dropdown-item-desc">
        &rsaquo; Project {{$data->project_name}} <br>
        &rsaquo; Division {{$data->division_name}} <br>
        &rsaquo; {{$data->activity_name}} <br> 
        &rsaquo; {{$data->work_name}} <strong class="badge badge-warning"> {{$data->late}} Day Late </strong>
    </div>
</a>
<?php } else {?>
<?php } ?>
@endforeach

@else
@foreach($late_division_navbar as $data)
<?php if (!empty($data->project_name)) { ?>
<a href="#" class="dropdown-item dropdown-item-unread">
    <div class="dropdown-item-icon bg-danger text-white">
        <i class="fas fa-exclamation-triangle"></i>
    </div>
    <div class="dropdown-item-desc">
        &rsaquo; Project {{$data->project_name}} <br>
        &rsaquo; {{$data->activity_name}} <br> 
        &rsaquo; {{$data->work_name}} <strong class="badge badge-warning"> {{$data->late}} Day Late </strong>
    </div>
</a>
<?php } else {?>
<?php } ?>
@endforeach
@endif


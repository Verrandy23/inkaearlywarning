<!-- Count warning for admin -->
@if(Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4)
@foreach($count_warning as $row)
<span>{{$row->id}}</span>
@endforeach

<!-- Count warning for division -->
@elseif(Auth::user()->role_id == 4)
@foreach($count_warning_pm as $row)
<span>{{$row->id}}</span>
@endforeach

<!-- Count warning for division -->
@else
@foreach($count_warning_division as $row)
<span>{{$row->id}}</span>
@endforeach
@endif
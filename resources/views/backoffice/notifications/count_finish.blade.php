<!-- Count late for admin -->
@if(Auth::user()->division_name == "Keproyekan" && Auth::user()->role_id != 4)
<span>{{$count_finish_admin}}</span>

<!-- Count late for division -->
@elseif(Auth::user()->role_id == 4)
<span>{{$count_finish_pm}}</span>

<!-- Count late for division -->
@else
<span>{{$count_finish_division}}</span>
@endif
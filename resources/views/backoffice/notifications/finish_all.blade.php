@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">Finish Activities</h1>
</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <!-- Late By Division-->
            <?php if (Auth::user()->division_name == 'Teknologi') { ?>
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <!-- Filter Data -->
                        <table>
                            <tr>
                                <label style="margin-left:1%" for="filter">Filter By Project</label>
                                <th style="width:200px" class="filterhead"></th>
                                <label style="margin-left:25%" for="filter">Filter By Activity</label>
                                <th class="filterhead"></th>
                            </tr>
                        </table>
                        <!-- End Filter Data -->
                        </div>          
                    </div><br>
                    <div class="table-responsive">
                        <table class="table col-lg-12 zero-configuration" id="exampledr">
                            <thead>
                                <tr>
                                    <th width="20%">Project</th>
                                    <th width="20%">Activity</th>
                                    <th width="20%">Sub Activity</th>
                                    <th width="30%">Work Activity</th>
                                    <th width="10%">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($teknologi as $row)
                                <tr>
                                    <td>{{$row->project_name}}</td>
                                    <td>{{$row->activity_name}}</td>
                                    <td>{{$row->sub_name}}</td>
                                    <td>{{$row->work_name}}</td>
                                    <td>
                                        <?php if ($row->status == 0) { ?>
                                            <strong class="badge badge-success">Belum</strong>
                                        <?php } else if ($row->status == 0.5) { ?>
                                            <strong class="badge badge-success">Proses</strong>
                                        <?php } else { ?>
                                            <strong class="badge badge-success">Selesai</strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php } ?>
            <?php if (Auth::user()->division_name == 'Logistik') { ?>
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <!-- Filter Data -->
                        <table>
                            <tr>
                                <label style="margin-left:1%" for="filter">Filter By Project</label>
                                <th style="width:200px" class="filterhead"></th>
                                <label style="margin-left:25%" for="filter">Filter By Activity</label>
                                <th class="filterhead"></th>
                            </tr>
                        </table>
                        <!-- End Filter Data -->
                        </div>          
                    </div><br>
                    <div class="table-responsive">
                        <table class="table col-lg-12 zero-configuration" id="exampledr">
                            <thead>
                                <tr>
                                    <th width="20%">Project</th>
                                    <th width="20%">Activity</th>
                                    <th width="20%">Sub Activity</th>
                                    <th width="30%">Work Activity</th>
                                    <th width="10%">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($logistik as $row)
                                <tr>
                                    <td>{{$row->project_name}}</td>
                                    <td>{{$row->activity_name}}</td>
                                    <td>{{$row->sub_name}}</td>
                                    <td>{{$row->work_name}}</td>
                                    <td>
                                        <?php if ($row->status == 0) { ?>
                                            <strong class="badge badge-success">Belum</strong>
                                        <?php } else if ($row->status == 0.5) { ?>
                                            <strong class="badge badge-success">Proses</strong>
                                        <?php } else { ?>
                                            <strong class="badge badge-success">Selesai</strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            <?php } ?>
            <?php if (Auth::user()->division_name == 'Fabrikasi') { ?>
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <!-- Filter Data -->
                        <table>
                            <tr>
                                <label style="margin-left:1%" for="filter">Filter By Project</label>
                                <th style="width:200px" class="filterhead"></th>
                                <label style="margin-left:25%" for="filter">Filter By Activity</label>
                                <th class="filterhead"></th>
                            </tr>
                        </table>
                        <!-- End Filter Data -->
                        </div>          
                    </div><br>
                    <div class="table-responsive">
                        <table class="table col-lg-12 zero-configuration" id="exampledr">
                            <thead>
                                <tr>
                                    <th width="20%">Project</th>
                                    <th width="20%">Activity</th>
                                    <th width="20%">Sub Activity</th>
                                    <th width="30%">Work Activity</th>
                                    <th width="10%">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($fabrikasi as $row)
                                <tr>
                                    <td>{{$row->project_name}}</td>
                                    <td>{{$row->activity_name}}</td>
                                    <td>{{$row->sub_name}}</td>
                                    <td>{{$row->work_name}}</td>
                                    <td>
                                        <?php if ($row->status == 0) { ?>
                                            <strong class="badge badge-success">Belum</strong>
                                        <?php } else if ($row->status == 0.5) { ?>
                                            <strong class="badge badge-success">Proses</strong>
                                        <?php } else { ?>
                                            <strong class="badge badge-success">Selesai</strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            <?php } ?>
            <?php if (Auth::user()->division_name == 'Finishing') { ?>
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6 col-12">
                        <!-- Filter Data -->
                        <table>
                            <tr>
                                <label style="margin-left:1%" for="filter">Filter By Project</label>
                                <th style="width:200px" class="filterhead"></th>
                                <label style="margin-left:25%" for="filter">Filter By Activity</label>
                                <th class="filterhead"></th>
                            </tr>
                        </table>
                        <!-- End Filter Data -->
                        </div>          
                    </div><br>
                    <div class="table-responsive">
                        <table class="table col-lg-12 zero-configuration" id="exampledr">
                            <thead>
                                <tr>
                                    <th width="20%">Project</th>
                                    <th width="20%">Activity</th>
                                    <th width="20%">Sub Activity</th>
                                    <th width="30%">Work Activity</th>
                                    <th width="10%">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($finishing as $row)
                                <tr>
                                    <td>{{$row->project_name}}</td>
                                    <td>{{$row->activity_name}}</td>
                                    <td>{{$row->sub_name}}</td>
                                    <td>{{$row->work_name}}</td>
                                    <td>
                                        <?php if ($row->status == 0) { ?>
                                            <strong class="badge badge-success">Belum</strong>
                                        <?php } else if ($row->status == 0.5) { ?>
                                            <strong class="badge badge-success">Proses</strong>
                                        <?php } else { ?>
                                            <strong class="badge badge-success">Selesai</strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php } ?>
            <?php if (Auth::user()->division_name == 'Testing') { ?>
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <!-- Filter Data -->
                        <table>
                            <tr>
                                <label style="margin-left:1%" for="filter">Filter By Project</label>
                                <th style="width:200px" class="filterhead"></th>
                                <label style="margin-left:25%" for="filter">Filter By Activity</label>
                                <th class="filterhead"></th>
                            </tr>
                        </table>
                        <!-- End Filter Data -->
                        </div>          
                    </div><br>
                    <div class="table-responsive">
                        <table class="table col-lg-12 zero-configuration" id="exampledr">
                            <thead>
                                <tr>
                                    <th width="20%">Project</th>
                                    <th width="20%">Activity</th>
                                    <th width="20%">Sub Activity</th>
                                    <th width="30%">Work Activity</th>
                                    <th width="10%">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($testing as $row)
                                <tr>
                                    <td>{{$row->project_name}}</td>
                                    <td>{{$row->activity_name}}</td>
                                    <td>{{$row->sub_name}}</td>
                                    <td>{{$row->work_name}}</td>
                                    <td>
                                        <?php if ($row->status == 0) { ?>
                                            <strong class="badge badge-success">Belum</strong>
                                        <?php } else if ($row->status == 0.5) { ?>
                                            <strong class="badge badge-success">Proses</strong>
                                        <?php } else { ?>
                                            <strong class="badge badge-success">Selesai</strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php } ?>
            <?php if (Auth::user()->division_name == 'Delivery') { ?>
                <!-- <div class="col-12">
                    <div class="table-responsive">
                        <table class="table col-lg-12 zero-configuration" id="tableDelivery">
                            <thead>
                                <tr>
                                    <th width="30%">Project</th>
                                    <th width="50%">Activity</th>
                                    <th width="20%">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($delivery as $row)
                                <tr>
                                    <td>{{$row->project_name}}</td>
                                    <td>{{$row->activity_name}}</td>
                                    <td>
                                        <?php if ($row->status == 0) { ?>
                                            <strong class="badge badge-success">Belum</strong>
                                        <?php } else if ($row->status == 0.5) { ?>
                                            <strong class="badge badge-success">Proses</strong>
                                        <?php } else { ?>
                                            <strong class="badge badge-success">Selesai</strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div> -->
            <?php } ?>
            <!-- End By Division-->

            <!-- Admin -->
            <?php if (Auth::user()->division_name == 'Keproyekan') { ?>
                <ul class="nav nav-pills" id="myTab3" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="teknologi-tab3" data-toggle="tab" href="#teknologi3" role="tab" aria-controls="home" aria-selected="true">Teknologi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="logistik-tab3" data-toggle="tab" href="#logistik3" role="tab" aria-controls="logistik" aria-selected="false">Logistik</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="fabrikasi-tab3" data-toggle="tab" href="#fabrikasi3" role="tab" aria-controls="fabrikasi" aria-selected="false">Fabrikasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="finishing-tab3" data-toggle="tab" href="#finishing3" role="tab" aria-controls="finishing" aria-selected="false">Finishing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="testing-tab3" data-toggle="tab" href="#testing3" role="tab" aria-controls="testing" aria-selected="false">Testing</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" id="delivery-tab3" data-toggle="tab" href="#delivery3" role="tab" aria-controls="delivery" aria-selected="false">Delivery</a>
                    </li> -->
                </ul>
                <div class="tab-content" id="myTabContent2">
                    <div class="tab-pane fade show active" id="teknologi3" role="tabpanel" aria-labelledby="teknologi-tab3">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <!-- Filter Data -->
                                    <table>
                                        <tr>
                                            <label style="margin-left:1%" for="filter">Filter By Project</label>
                                            <th style="width:200px" class="filterhead"></th>
                                            <label style="margin-left:25%" for="filter">Filter By Activity</label>
                                            <th class="filterhead"></th>
                                        </tr>
                                    </table>
                                    <!-- End Filter Data -->
                                    </div>

                                </div><br>
                                <div class="table-responsive">
                                    <table class="table col-lg-12 zero-configuration" id="exampledr">
                                        <thead>
                                            <tr>
                                                <th width="20%">Project</th>
                                                <th width="20%">Activity</th>
                                                <th width="20%">Sub Activity</th>
                                                <th width="30%">Work Activity</th>
                                                <th width="10%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($teknologi as $row)
                                            <tr>
                                                <td>{{$row->project_name}}</td>
                                                <td>{{$row->activity_name}}</td>
                                                <td>{{$row->sub_name}}</td>
                                                <td>{{$row->work_name}}</td>
                                                <td>
                                                    <?php if ($row->status == 0) { ?>
                                                        <strong class="badge badge-success">Belum</strong>
                                                    <?php } else if ($row->status == 0.5) { ?>
                                                        <strong class="badge badge-success">Proses</strong>
                                                    <?php } else { ?>
                                                        <strong class="badge badge-success">Selesai</strong>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="logistik3" role="tabpanel" aria-labelledby="logistik-tab3">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <!-- Filter Data -->
                                    <table>
                                        <tr>
                                            <label style="margin-left:1%" for="filter">Filter By Project</label>
                                            <th style="width:200px" class="filterhead2"></th>
                                            <label style="margin-left:25%" for="filter">Filter By Activity</label>
                                            <th class="filterhead2"></th>
                                        </tr>
                                    </table>
                                    <!-- End Filter Data -->
                                    </div>
                                </div><br>
                                <div class="table-responsive">
                                    <table class="table col-lg-12 zero-configuration" id="exampledr2">
                                        <thead>
                                            <tr>
                                                <th width="20%">Project</th>
                                                <th width="20%">Activity</th>
                                                <th width="20%">Sub Activity</th>
                                                <th width="30%">Work Activity</th>
                                                <th width="10%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($logistik as $row)
                                            <tr>
                                                <td>{{$row->project_name}}</td>
                                                <td>{{$row->activity_name}}</td>
                                                <td>{{$row->sub_name}}</td>
                                                <td>{{$row->work_name}}</td>
                                                <td>
                                                    <?php if ($row->status == 0) { ?>
                                                        <strong class="badge badge-success">Belum</strong>
                                                    <?php } else if ($row->status == 0.5) { ?>
                                                        <strong class="badge badge-success">Proses</strong>
                                                    <?php } else { ?>
                                                        <strong class="badge badge-success">Selesai</strong>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="fabrikasi3" role="tabpanel" aria-labelledby="fabrikasi-tab3">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <!-- Filter Data -->
                                    <table>
                                        <tr>
                                            <label style="margin-left:1%" for="filter">Filter By Project</label>
                                            <th style="width:200px" class="filterhead3"></th>
                                            <label style="margin-left:25%" for="filter">Filter By Activity</label>
                                            <th class="filterhead3"></th>
                                        </tr>
                                    </table>
                                    <!-- End Filter Data -->
                                    </div>
                                </div><br>
                                <div class="table-responsive">
                                    <table class="table col-lg-12 zero-configuration" id="exampledr3">
                                        <thead>
                                            <tr>
                                                <th width="20%">Project</th>
                                                <th width="20%">Activity</th>
                                                <th width="20%">Sub Activity</th>
                                                <th width="30%">Work Activity</th>
                                                <th width="10%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($fabrikasi as $row)
                                            <tr>
                                                <td>{{$row->project_name}}</td>
                                                <td>{{$row->activity_name}}</td>
                                                <td>{{$row->sub_name}}</td>
                                                <td>{{$row->work_name}}</td>
                                                <td>
                                                    <?php if ($row->status == 0) { ?>
                                                        <strong class="badge badge-success">Belum</strong>
                                                    <?php } else if ($row->status == 0.5) { ?>
                                                        <strong class="badge badge-success">Proses</strong>
                                                    <?php } else { ?>
                                                        <strong class="badge badge-success">Selesai</strong>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="finishing3" role="tabpanel" aria-labelledby="finishing-tab3">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <!-- Filter Data -->
                                    <table>
                                        <tr>
                                            <label style="margin-left:1%" for="filter">Filter By Project</label>
                                            <th style="width:200px" class="filterhead4"></th>
                                            <label style="margin-left:25%" for="filter">Filter By Activity</label>
                                            <th class="filterhead4"></th>
                                        </tr>
                                    </table>
                                    <!-- End Filter Data -->
                                    </div>
                                </div><br>
                                <div class="table-responsive">
                                    <table class="table col-lg-12 zero-configuration" id="exampledr4">
                                        <thead>
                                            <tr>
                                                <th width="20%">Project</th>
                                                <th width="20%">Activity</th>
                                                <th width="20%">Sub Activity</th>
                                                <th width="30%">Work Activity</th>
                                                <th width="10%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($finishing as $row)
                                            <tr>
                                                <td>{{$row->project_name}}</td>
                                                <td>{{$row->activity_name}}</td>
                                                <td>{{$row->sub_name}}</td>
                                                <td>{{$row->work_name}}</td>
                                                <td>
                                                    <?php if ($row->status == 0) { ?>
                                                        <strong class="badge badge-success">Belum</strong>
                                                    <?php } else if ($row->status == 0.5) { ?>
                                                        <strong class="badge badge-success">Proses</strong>
                                                    <?php } else { ?>
                                                        <strong class="badge badge-success">Selesai</strong>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="testing3" role="tabpanel" aria-labelledby="testing-tab3">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <!-- Filter Data -->
                                    <table>
                                        <tr>
                                            <label style="margin-left:1%" for="filter">Filter By Project</label>
                                            <th style="width:200px" class="filterhead5"></th>
                                            <label style="margin-left:25%" for="filter">Filter By Activity</label>
                                            <th class="filterhead5"></th>
                                        </tr>
                                    </table>
                                    <!-- End Filter Data -->
                                    </div>
                                </div><br>
                                <div class="table-responsive">
                                    <table class="table col-lg-12 zero-configuration" id="exampledr5">
                                        <thead>
                                            <tr>
                                                <th width="20%">Project</th>
                                                <th width="20%">Activity</th>
                                                <th width="20%">Sub Activity</th>
                                                <th width="30%">Work Activity</th>
                                                <th width="10%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($testing as $row)
                                            <tr>
                                                <td>{{$row->project_name}}</td>
                                                <td>{{$row->activity_name}}</td>
                                                <td>{{$row->sub_name}}</td>
                                                <td>{{$row->work_name}}</td>
                                                <td>
                                                    <?php if ($row->status == 0) { ?>
                                                        <strong class="badge badge-success">Belum</strong>
                                                    <?php } else if ($row->status == 0.5) { ?>
                                                        <strong class="badge badge-success">Proses</strong>
                                                    <?php } else { ?>
                                                        <strong class="badge badge-success">Selesai</strong>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="tab-pane fade" id="delivery3" role="tabpanel" aria-labelledby="delivery-tab3">
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table class="table col-lg-12 zero-configuration" id="tableDelivery">
                                        <thead>
                                            <tr>
                                                <th width="30%">Project</th>
                                                <th width="50%">Activity</th>
                                                <th width="20%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($delivery as $row)
                                            <tr>
                                                <td>{{$row->project_name}}</td>
                                                <td>{{$row->activity_name}}</td>
                                                <td>
                                                    <?php if ($row->status == 0) { ?>
                                                        <strong class="badge badge-success">Belum</strong>
                                                    <?php } else if ($row->status == 0.5) { ?>
                                                        <strong class="badge badge-success">Proses</strong>
                                                    <?php } else { ?>
                                                        <strong class="badge badge-success">Selesai</strong>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            <?php } ?>
        </div>
    </div>
</div>
@endsection
@extends('layouts.backend.masterPage')
@section('content')
<div class="section-header">
    <h1 class="titleC">{{$division->name}}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
            <ol style="background:#38ada9" class="breadcrumb text-white-all">
                <li class="breadcrumb-item"><a href="/backoffice"><i class="fas fa-tachometer-alt"></i> Home</a></li>
                <li class="breadcrumb-item"><a href="/dashboard_divisions/{{$division->get_project->id}}"> Dashboard Division</a></li>
            </ol>
        </div><br>
    </div>

</div>
<div class="card top">
    <div class="card-body">
        <div class="col-lg-12">
            <h6 class="titleC text-center"> {{$project->get_project->name}}</h6>

            <div class="table-responsive">
                <table class="table col-lg-12 zero-configuration" id="table1">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @endif
                  
                    <thead>
                        <tr>
                            <?php if (Auth::user()->role_id != 1) { ?>
                                <th width="10%">No.</th>
                                <th width="20%">Name</th>
                                <th width="10%">Belum</th>
                                <th width="10%">Proses</th>
                                <th width="10%">Finish</th>
                                <th width="20%">Start Date</th>
                                <th width="20%">End Date</th>
                            <?php } else { ?>
                                <th width="10%">No.</th>
                                <th width="20%">Name</th>
                                <th width="10%">Belum</th>
                                <th width="10%">Proses</th>
                                <th width="10%">Finish</th>
                                <th width="10%">Start Date</th>
                                <th width="10%">End Date</th>
                                <th width="20%">Action</th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($progress as $index => $row)
                        <tr>
                            <td>{{$index+1}}</td>
                            <td>
                                <?php if ($row->jumlah != null || $row->total != 0) { ?>
                                    <?php
                                    $jumlah = $row->jumlah;
                                    $total = $row->total;
                                    $hasil = $jumlah / $total * 100;
                                    ?>
                                    <h6 class="title"> {{$row->name}} <span class="pull-right"><?php echo floor($hasil) ?>%</span></h6>
                                    <div class="progress mb-3" style="height: 7px">
                                        <div class="progress-bar bg-primary" style="width: <?php echo floor($hasil) ?>%;" role="progressbar">
                                        </div>
                                    </div>

                                <?php } else { ?>
                                    <h6 class="title"> {{$row->name}} <span class="pull-right">0%</span></h6>
                                    <div class="progress mb-3" style="height: 7px">
                                        <div class="progress-bar bg-primary" style="width: 0%;" role="progressbar">
                                        </div>
                                    </div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if ($row->status_delivery == '0') { ?>

                                    <input type="checkbox" class="checkbox-custom" checked></input>

                                <?php } else { ?>
                                    <form action="/updateDelivery" value="{{$row->id}}" method="post" id="myForm{{$row->id}}">
                                        @csrf
                                        <input type="hidden" value="{{$row->id}}" name="idwork" id="idwork{{$row->id}}"></input>
                                        <input type="checkbox" class="checkbox-custom" class="belum" onclick="klikbelum('{{$row->id}}')" name="cek" id="cek" value="0"></input>
                                    </form>

                                <?php } ?>
                            </td>

                            <td>
                                <?php if ($row->status_delivery == '0.5') { ?>

                                    <input type="checkbox" class="checkbox-custom" checked></input>

                                <?php } else { ?>

                                    <form action="/updateDelivery" value="{{$row->id}}" method="post" id="myFormprogres{{$row->id}}">
                                        @csrf
                                        <input type="hidden" value="{{$row->id}}" name="idwork" id="idwork{{$row->id}}"></input>
                                        <input type="checkbox" class="checkbox-custom" class="belum" onclick="klikprogres('{{$row->id}}')" name="cek" id="cek" value="0.5"></input>
                                    </form>

                                <?php } ?>
                            </td>

                            <td>
                                <?php if ($row->status_delivery == '1') { ?>

                                    <input type="checkbox" class="checkbox-custom" checked></input>

                                <?php } else { ?>

                                    <form action="/updateDelivery" value="{{$row->id}}" method="post" id="myFormselesai{{$row->id}}">
                                        @csrf
                                        <input type="hidden" value="{{$row->id}}" name="idwork" id="idwork{{$row->id}}"></input>
                                        <input type="checkbox" class="checkbox-custom" class="belum" onclick="klikselesai('{{$row->id}}')" name="cek" id="cek" value="1"></input>
                                    </form>

                                <?php } ?>
                            </td>
                            <td>{{$row->start_delivery}}</td>
                            <td>{{$row->end_delivery}}</td>
                            <td>
                                <?php if (!empty($row->id)) { ?>

                                    <form action="{{ route('activities.destroy', $row->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')

                                        <?php if (Auth::user()->role_id != 1) { ?>
                                            <div style="display:none">
                                            <?php } else { ?>
                                                <a onclick="return confirm('Are you sure to delete this data?')">
                                                    <button class="btn btn-icon btn-danger" type="submit">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </a>

                                                <a href="{{ route('activities.edit', $row->id)}}">
                                                    <button class="btn btn-icon btn-info" type="button">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </a>
                                            <?php } ?>
                                            </div>


                                    </form>
                                <?php } else { ?>
                                    <form>
                                        <?php if (Auth::user()->role_id != 1) { ?>
                                            <div style="display:none">
                                            <?php } else { ?>
                                                <a href="#">
                                                    <button class="btn btn-icon btn-danger" type="submit">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </a>

                                                <a href="#">
                                                    <button class="btn btn-icon btn-info" type="button">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </a>
                                            <?php } ?>
                                            </div>

                                    </form>
                                <?php } ?>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('input.belum').on('change', function() {
        $('input.belum').not(this).prop('checked', false);
    })
</script>

<script type="text/javascript">
    function klikbelum(id) {
        var idw = $('#idwork' + id).val();
        $.ajax({
            type: "POST",
            url: "/updateDelivery",
            data: $('#myForm' + idw).serialize(),
            success: function() {
                location.reload();
            }
        });
    }
</script>
<script type="text/javascript">
    function klikprogres(id) {
        var idw = $('#idwork' + id).val();
        $.ajax({
            type: "POST",
            url: "/updateDelivery",
            data: $('#myFormprogres' + idw).serialize(),
            success: function() {
                location.reload();
            }
        });
    }
</script>
<script type="text/javascript">
    function klikselesai(id) {
        var idw = $('#idwork' + id).val();
        $.ajax({
            type: "POST",
            url: "/updateDelivery",
            data: $('#myFormselesai' + idw).serialize(),
            success: function() {
                location.reload();
            }
        });
    }
</script>
@endsection
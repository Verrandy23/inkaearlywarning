      <div class="main-sidebar">
          <aside id="sidebar-wrapper">
              <div class="sidebar-brand">
                  <img height="30px" src="{{ asset('assets/auth/img/inka-border.png')}}" alt="">
              </div>
              <div class="sidebar-brand sidebar-brand-sm">
                  <a href="index.html">St</a>
              </div>
              <ul class="sidebar-menu">
                  <li class="menu-header">Dashboard</li>
                  <li class="nav-item dropdown">
                      <a href="#" class="nav-link has-dropdown"><i class="fas fa-tachometer-alt"></i><span>Dashboard</span></a>
                      <ul class="dropdown-menu">
                        <li><a class="nav-link" href="/dashboard">Main Dashboard</a></li>
                        <li><a class="nav-link" href="/backoffice">General Dashboard</a></li>
                      </ul>
                  </li>
                  @if(Auth::user()->role_id != 1)
                  @else
                  <li class="menu-header">Master</li>
                  <li class="nav-item dropdown">
                      <a href="#" class="nav-link has-dropdown"><i class="fas fa-columns"></i> <span>Project</span></a>
                      <ul class="dropdown-menu">
                      <li><a class="nav-link" href="/projects"><span>Manage Project</span></a></li>
                      <li><a class="nav-link" href="/projects_progress"><span>Manage Project Progress</span></a></li>                          
                      <li><a class="nav-link" href="/projects_budget"><span>Manage Project Budget</span></a></li>                          
                      </ul>
                  </li>
                  <li><a class="nav-link" href="/reports"><i class="far fa-file-alt"></i> <span>Manage Laporan</span></a></li>
                  <li><a class="nav-link" href="/imports"><i class="fas fa-upload"></i> <span>Import Activities</span></a></li>

                  <li class="nav-item dropdown">
                      <a href="#" class="nav-link has-dropdown"><i class="fas fa-columns"></i> <span>Master Activities</span></a>
                      <ul class="dropdown-menu">
                          <li><a class="nav-link" href="/masters">Master Activity</a></li>
                          <li><a class="nav-link" href="/master_activities">Master Sub Activity</a></li>
                          <li><a class="nav-link" href="/master_sub_activities">Master Work Activity</a></li>
                      </ul>
                  </li>
                  @endif

                  <li class="menu-header">Master Report</li>
                  <li>
                    <a class="nav-link" href="/notifications"><i class="fas fa-check-circle"></i> 
                        <span> Finish Activities </span><strong class="badge badge-success">
                              <div id="count_finish"></div>
                          </strong>
                    </a>
                  </li>
                  <li>
                      <a class="nav-link" href="/warning_notifications"><i class="fas fa-exclamation-triangle"></i>
                          <span> Warning Activities </span><strong class="badge badge-warning">
                              <div id="count_warning"></div>
                          </strong>


                      </a>
                  </li>
                  <li>
                      <a class="nav-link" href="/late_history">
                          <i class="fas fa-bell"></i> <span> Late Activities</span>
                          <strong class="badge badge-danger">
                              <div id="count_late"></div>
                          </strong>
                      </a>
                  </li>

                  <li>
                      <a class="nav-link" href="/issues"><i class="fas fa-list"></i> <span> Issues </span>
                          <strong class="badge badge-primary">
                              <div id="count_issue"></div>
                          </strong>
                      </a>
                  </li>      
                  <li>
                      <a class="nav-link" href="/histories">
                          <i class="fas fa-history"></i> <span> History Activities</span>
                          
                      </a>
                  </li>
                  @if (Auth::user()->role_id != 1)
                  @else
                  <li class="menu-header">Master Role</li>
                  <li><a class="nav-link" href="/auths"><i class="far fa-user"></i> <span>Manage User</span></a></li>
                  @endif

          </aside>
      </div>

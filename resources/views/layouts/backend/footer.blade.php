
<!--**********************************
            Footer start
        ***********************************-->
            <footer class="main-footer">
                <div class="footer-left">
                    Copyright &copy; 2019 <div class="bullet"></div> Made By <a href="https://nauval.in/">Divisi Keproyekan</a>
                </div>
                <div class="footer-right">
                    2.3.0
                </div>
            </footer>
        <!--**********************************
            Footer end
        ***********************************-->
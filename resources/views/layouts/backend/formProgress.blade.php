<!-- Main Content Proses -->
<div class="main-content">
    <section class="section">
        <div class="section-body">
            @yield('content')
        </div>
    </section>
    <div class="modal fade" tabindex="-1" role="dialog" id="progressModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Pilih Proses Presentase</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height:280px">
                    <input type="hidden" name="idwork" id="workId" data-index=""/>
                    <!-- Modal Percentage Process -->

                    <form id="progress_modal" method="post" act>
                        @csrf
                        
                        <input type="hidden" name="status_name" value="proses">
                        <select class="form-control" name="status" required>
                            <option value="0.1">10%</option>
                            <option value="0.15">15%</option>
                            <option value="0.2">20%</option>
                            <option value="0.25">25%</option>
                            <option value="0.3">30%</option>
                            <option value="0.35">35%</option>
                            <option value="0.4">40%</option>
                            <option value="0.45">45%</option>
                            <option value="0.5">50%</option>
                            <option value="0.55">55%</option>
                            <option value="0.6">60%</option>
                            <option value="0.65">65%</option>
                            <option value="0.7">70%</option>
                            <option value="0.75">75%</option>
                            <option value="0.8">80%</option>
                            <option value="0.85">85%</option>
                            <option value="0.9">90%</option>
                            <option value="0.95">95%</option>
                        </select><br>

                        <button type="button" id="button" class="btn mb-1 btn-primary">Simpan</span>
                        </button>

                    </form>
                    </form>
                    <!-- End Modal Percentage Process -->
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var $workValue = document.getElementById('workId');

    document.getElementById('button').addEventListener('click', function(){
            var $status_name = document.getElementsByName('status_name')[0].value;
            var $status = document.getElementsByName('status')[0].value;
        
            axios.post('/update/'+$workValue.value, {
                 status_name : $status_name,
                status: $status,
              })
              .then(function (response) {

                
                var $data = response.data;
                $data = parseFloat($data.data)*100;
                
                document.getElementById('progress' + $workValue.dataset.index).innerHTML = $data + "%";

                document.querySelector('#progress_bar' + $workValue.dataset.index).style.width=$data+"%";
                
                swal({
                    title: "Success!",
                    text: "data telah terupdate",
                    icon: "success",
                });
                document.getElementById('belum' + $workValue.dataset.index).checked = false;
                document.getElementById('finish' + $workValue.dataset.index).checked = false;

                

              })
              .catch(function (error) {
                console.log(error);
              });
    });
    
    
</script>


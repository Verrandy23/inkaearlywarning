<script>
    $(document).ready(function() {
        $('#count_finish').load('/count_finish');
    });

    $(document).ready(function() {
        $('#count_late').load('/count_late');
    });
            
    $(document).ready(function() {
        $('#count_warning').load('/count_warning');
    });
        
    $(document).ready(function() {
        $('#count_issue').load('/count_issue');
    });
    // /* End Count Aside */

    // /* Count Navbar */

    $(document).ready(function() {
        $('#data').load('/notif');
    });   
    
    
    $(document).ready(function() {
        $('#count_late_header').load('/count_late');
    });

    
    //  var auto_refresh = setInterval(
    //  function() {
    //      $('#data').load('http://localhost:8000/notif').fadeIn("slow");
    //  }, 1000);
    // /* End Count Navbar */




    $(document).ready(function() {
        $('#tablePicture1').DataTable();
    });

    $(document).ready(function() {
        $('#tablePicture2').DataTable();
    });

    $(document).ready(function() {
        $('#tablePicture3').DataTable();
    });

    $(document).ready(function() {
    $('#tableProgress').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
    $(document).ready(function() {
        $('#table1').DataTable();
    });

    $(document).ready(function() {
        $('#table2').DataTable();
    });

    $(document).ready(function() {
        $('#table3').DataTable();
    });

    $(document).ready(function() {
        $('#table4').DataTable();
    });

    $(document).ready(function() {
        $('#table5').DataTable();
    });

    $(document).ready(function() {
        $('#table6').DataTable();
    });

    $(document).ready(function() {
        $('#tableProses').DataTable();
    });

    $(document).ready(function() {
        $('#tableNotYet').DataTable();
    });
</script>


<script>
    $(document).ready(function() {
        $('#tableLate thead tr').clone(true).appendTo('#tableLate thead');
        $('#tableLate thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });
        var table = $('#tableLate').DataTable({
            "order": [
                [0, "asc"]
            ],
            orderCellsTop: true,
            fixedHeader: true
        });

    });

    $(document).ready(function() {
        $('#tableLaporan thead tr').clone(true).appendTo('#tableLaporan thead');
        $('#tableLaporan thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });
        var table = $('#tableLaporan').DataTable({
            "order": [
                [0, "asc"]
            ],
            orderCellsTop: true,
            fixedHeader: true
        });

    });


    //Table filter Teknologi
   $(document).ready(function() {
          var table=$('#exampledr').DataTable({
            initComplete: function () {
            }
       });
       $(".filterhead").each(function (i) {
                 if (i != 15  && i != 16 ) {
                     var select = $('<select class="form-control" name="filter'+i+'" id="filter'+i+'"><option value="">All Data</option></select>')
                         .appendTo($(this).empty())
                         .on('change', function () {
                             var term = $(this).val();
                             table.column(i).search(term, false, false).draw();
                         });
                     table.column(i).data().unique().sort().each(function (d, j) {
                         select.append('<option value="' + d + '">' + d + '</option>')
                     });
                 } else {
                    $(this).empty();
                 }
             });

         //filter project issue
         //and filter late issue
         $('#filter0').on('change', function(){
                        $('#project').val($(this).val());
                        });
         $('#filter1').on('change', function() {
                $('#activity').val($(this).val());
                console.log($('#activity').val());
         });
                $('#filter2').on('change', function(){
                        $('#level').val($(this).val());
                        });
                $('#filter4').on('change', function(){
                        $('#status').val($(this).val());
                        });
          
    } );

    

   // filter table Logistiok

    $(document).ready(function() {
          var table=$('#exampledr2').DataTable({
            initComplete: function () {
            }
       });
       $(".filterhead2").each(function (i) {
                 if (i != 15  && i != 16 ) {
                     var select = $('<select class="form-control" name="logistik'+i+'" id="logistik'+i+'"><option value="">All Data</option></select>')
                         .appendTo($(this).empty())
                         .on('change', function () {
                             var term = $(this).val();
                             table.column(i).search(term, false, false).draw();
                         });
                     table.column(i).data().unique().sort().each(function (d, j) {
                         select.append('<option value="' + d + '">' + d + '</option>')
                     });
                 } else {
                    $(this).empty();
                 }
             });

          $('#logistik0').on('change', function(){
                        $('#project1').val($(this).val());
                        });
                $('#logistik2').on('change', function(){
                        $('#level1').val($(this).val());
                        });
                $('#logistik4').on('change', function(){
                        $('#status1').val($(this).val());
                        });
    } );

    $(document).ready(function() {
          var table=$('#exampledr3').DataTable({
            initComplete: function () {
            }
       });
       $(".filterhead3").each(function (i) {
                 if (i != 15  && i != 16 ) {
                     var select = $('<select class="form-control" name="fabrikasi'+i+'" id="fabrikasi'+i+'"><option value="">All Data</option></select>')
                         .appendTo($(this).empty())
                         .on('change', function () {
                             var term = $(this).val();
                             table.column(i).search(term, false, false).draw();
                         });
                     table.column(i).data().unique().sort().each(function (d, j) {
                         select.append('<option value="' + d + '">' + d + '</option>')
                     });
                 } else {
                    $(this).empty();
                 }
             });

         $('#fabrikasi0').on('change', function(){
                        $('#project2').val($(this).val());
                        });
                $('#fabrikasi2').on('change', function(){
                        $('#level2').val($(this).val());
                        });
                $('#fabrikasi4').on('change', function(){
                        $('#status2').val($(this).val());
                        });

    } );

    $(document).ready(function() {
          var table=$('#exampledr4').DataTable({
            initComplete: function () {
            }
       });
       $(".filterhead4").each(function (i) {
                 if (i != 15  && i != 16 ) {
                     var select = $('<select class="form-control" name="finishing'+i+'" name="finishing'+i+'" id="finishing'+i+'"><option value="">All Data</option></select>')
                         .appendTo($(this).empty())
                         .on('change', function () {
                             var term = $(this).val();
                             table.column(i).search(term, false, false).draw();
                         });
                     table.column(i).data().unique().sort().each(function (d, j) {
                         select.append('<option value="' + d + '">' + d + '</option>')
                     });
                 } else {
                    $(this).empty();
                 }
             });

        $('#finishing0').on('change', function(){
                        $('#project3').val($(this).val());
                        });
                $('#finishing2').on('change', function(){
                        $('#level3').val($(this).val());
                        });
                $('#finishing4').on('change', function(){
                        $('#status3').val($(this).val());
                        });
    } );

    $(document).ready(function() {
          var table=$('#exampledr5').DataTable({
            initComplete: function () {
            }
       });
       $(".filterhead5").each(function (i) {
                 if (i != 15  && i != 16 ) {
                     var select = $('<select class="form-control" name="testing'+i+'" id="testing'+i+'"><option value="">All Data</option></select>')
                         .appendTo($(this).empty())
                         .on('change', function () {
                             var term = $(this).val();
                             table.column(i).search(term, false, false).draw();
                         });
                     table.column(i).data().unique().sort().each(function (d, j) {
                         select.append('<option value="' + d + '">' + d + '</option>')
                     });
                 } else {
                    $(this).empty();
                 }
             });
            $('#testing0').on('change', function(){
                        $('#project4').val($(this).val());
                        });
                $('#testing2').on('change', function(){
                        $('#level4').val($(this).val());
                        });
                $('#testing4').on('change', function(){
                        $('#status4').val($(this).val());
                        });

     

     });



    $(document).ready(function() {
          var table=$('#exampledr6').DataTable({
            initComplete: function () {
            }
       });
       $(".filterhead6").each(function (i) {
                 if (i != 15  && i != 16 ) {
                     var select = $('<select class="form-control" id="del'+i+'"><option value="">All Data</option></select>')
                         .appendTo($(this).empty())
                         .on('change', function () {
                             var term = $(this).val();
                             table.column(i).search(term, false, false).draw();
                         });
                     table.column(i).data().unique().sort().each(function (d, j) {
                         select.append('<option value="' + d + '">' + d + '</option>')
                     });
                 } else {
                    $(this).empty();
                 }
             });

       
    } );


    
</script>
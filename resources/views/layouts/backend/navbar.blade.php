 <div class="navbar-bg" style="background-color:#ff6b81"></div>
 <nav class="navbar navbar-expand-lg main-navbar">
     <form class="form-inline mr-auto">
         <ul class="navbar-nav mr-3">
             <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
             <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
         </ul>
         <div class="search-element">
             <div class="search-backdrop"></div>
             <div class="search-result">
                 <div class="search-header">
                     Histories
                 </div>
                 <div class="search-item">
                     <a href="#">How to hack NASA using CSS</a>
                     <a href="#" class="search-close"><i class="fas fa-times"></i></a>
                 </div>
                 <div class="search-item">
                     <a href="#">Kodinger.com</a>
                     <a href="#" class="search-close"><i class="fas fa-times"></i></a>
                 </div>

             </div>
         </div>
     </form>
     <ul class="navbar-nav navbar-right">

         <li class="dropdown dropdown-list-toggle">
             <a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg">
                 <main rel="main">
                     <div class="notification">
                         <svg viewbox="0 0 166 197">
                             <path d="M82.8652955,196.898522 C97.8853137,196.898522 110.154225,184.733014 110.154225,169.792619 L55.4909279,169.792619 C55.4909279,184.733014 67.8452774,196.898522 82.8652955,196.898522 L82.8652955,196.898522 Z" class="notification--bellClapper"></path>
                             <path d="M146.189736,135.093562 L146.189736,82.040478 C146.189736,52.1121695 125.723173,27.9861651 97.4598237,21.2550099 L97.4598237,14.4635396 C97.4598237,6.74321823 90.6498186,0 82.8530327,0 C75.0440643,0 68.2462416,6.74321823 68.2462416,14.4635396 L68.2462416,21.2550099 C39.9707102,27.9861651 19.5163297,52.1121695 19.5163297,82.040478 L19.5163297,135.093562 L0,154.418491 L0,164.080956 L165.706065,164.080956 L165.706065,154.418491 L146.189736,135.093562 Z" class="notification--bell"></path>
                         </svg>
                        <span id="count_late_header" class="notification--num"></span>

                     </div>
                 </main>

             </a>
             <div class="dropdown-menu dropdown-list dropdown-menu-right">
                 <div class="dropdown-header">Notifications
                     <div class="float-right">
                         <a href="#">Mark All As Read</a>
                     </div>
                 </div>
                 <div class="dropdown-list-content dropdown-list-icons">
                     <div id="data"></div>
                 </div>
                 <div class="dropdown-footer text-center">
                     <a href="/late_history">View All <i class="fas fa-chevron-right"></i></a>
                 </div>
             </div>
         </li>

         <li class="dropdown">
             <a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg nav-link-user">
                 <img src="{{ asset('assets/stisla/img/profile/' . Auth::user()->picture)}}" class="circular--square">
                 <div class="d-sm-none d-lg-inline-block">Hi, {{ Auth::user()->name }}</div>

             </a>
             <div class="dropdown-menu dropdown-menu-right">
                 <a href="/profiles" class="dropdown-item has-icon">
                     <i class="far fa-user"></i> Profile
                 </a>

                 <div class="dropdown-divider"></div>
                 <a class="dropdown-item has-icon text-danger" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                     <i class="fas fa-sign-out-alt"></i> Logout
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                         @csrf
                     </form>
                 </a>
             </div>
         </li>
     </ul>
 </nav>

 <!-- <script>
     var auto_refresh = setInterval(
         function() {
             $('#dataCount').load('http://localhost:8000/count_notif').fadeIn("slow");
         }, 1000);
 </script> -->
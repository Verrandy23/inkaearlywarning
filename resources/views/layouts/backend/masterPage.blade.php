<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title> INKA &mdash; Early Warning Monitoring</title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/stisla/css/bootstrap.min.css')}}">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css"> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Libraries -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/stisla/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/stisla/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/stisla/css/components.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/stisla/css/chosen.css')}}">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <link rel="stylesheet" href="{{ asset('assets/compiled/flipclock.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('assets/stisla/js/vue.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/axios.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    
</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            @include('layouts.backend.navbar')
            @include('layouts.backend.aside')
            @include('layouts.backend.formProgress')
            @include('layouts.backend.footer')
        </div>
    </div>
    <!-- General JS Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>

    <script src="{{ asset('assets/compiled/flipclock.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/bootstrap.min.js')}}"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> -->
    <script src="{{ asset('assets/stisla/js/highcharts.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/highcharts-3d.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/data.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/exporting.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/accessibility.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/stisla.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/page/bootstrap-modal.js')}}"></script>
    <!-- JS Libraies -->
    <script src="{{ asset('assets/stisla/js/scripts.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/custom.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/chosen.jquery.js')}}"></script>
    <script src="{{ asset('assets/stisla/js/style.js')}}"></script>
    <!-- FusionCharts -->
    <script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
    <!-- jQuery-FusionCharts -->
    <script type="text/javascript" src="https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js"></script>
    <!-- Fusion Theme -->
    <script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
    @include('layouts.backend.datatable')


</body>

</html>
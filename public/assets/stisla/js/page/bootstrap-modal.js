"use strict";

$("#modal-1").fireModal({
  title: 'Detail Project',
  body: $("#modal-project"),
});
$("#modal-2").fireModal({ body: 'Modal body text goes here.', center: true });

let modal_3_body = '<p>Object to create a button on the modal.</p><pre class="language-javascript"><code>';
modal_3_body += '[\n';
modal_3_body += ' {\n';
modal_3_body += "   text: 'Login',\n";
modal_3_body += "   submit: true,\n";
modal_3_body += "   class: 'btn btn-primary btn-shadow',\n";
modal_3_body += "   handler: function(modal) {\n";
modal_3_body += "     alert('Hello, you clicked me!');\n"
modal_3_body += "   }\n"
modal_3_body += ' }\n';
modal_3_body += ']';
modal_3_body += '</code></pre>';
$("#modal-3").fireModal({
  title: 'Modal with Buttons',
  body: modal_3_body,
  buttons: [
    {
      text: 'Click, me!',
      class: 'btn btn-primary btn-shadow',
      handler: function (modal) {
        alert('Hello, you clicked me!');
      }
    }
  ]
});

$("#modal-4").fireModal({
  footerClass: 'bg-whitesmoke',
  body: 'Add the <code>bg-whitesmoke</code> class to the <code>footerClass</code> option.',
  buttons: [
    {
      text: 'No Action!',
      class: 'btn btn-primary btn-shadow',
      handler: function (modal) {
      }
    }
  ]
});




$("#modal-5").fireModal({
  title: 'Form Division',
  body: $("#modal-login-part"),
  footerClass: 'bg-whitesmoke',
  buttons: [
    {
      text: 'Save',
      submit: true,
      class: 'btn btn-primary btn-shadow',
      handler: function (modal) {
      }
    }
  ]
});



$("#modal-6").fireModal({
  title: 'Form Detail Project',
  body: $("#modal-detail-part"),
  footerClass: 'bg-whitesmoke',
  buttons: [
    {
      text: 'Save',
      submit: true,
      class: 'btn btn-primary btn-shadow',
      handler: function (modal) {
      }
    }
  ]
});

$("#modal-7").fireModal({
  title: 'Form Activity',
  body: $("#modal-login-part"),
  footerClass: 'bg-whitesmoke',
  buttons: [
    {
      text: 'Save',
      submit: true,
      class: 'btn btn-primary btn-shadow',
      handler: function (modal) {
      }
    }
  ]
});

$("#modal-8").fireModal({
  title: 'Form Sub Activity',
  body: $("#modal-login-part"),
  footerClass: 'bg-whitesmoke',
  buttons: [
    {
      text: 'Save',
      submit: true,
      class: 'btn btn-primary btn-shadow',
      handler: function (modal) {
      }
    }
  ]
});


$("#modal-9").fireModal({
  title: 'Form Work Activity',
  body: $("#modal-login-part"),
  footerClass: 'bg-whitesmoke',
  buttons: [
    {
      text: 'Save',
      submit: true,
      class: 'btn btn-primary btn-shadow',
      handler: function (modal) {
      }
    }
  ]
});

$("#modal-10").fireModal({
  title: 'Form Add PIC',
  body: $("#modal-login-part"),
  footerClass: 'bg-whitesmoke',
  buttons: [
    {
      text: 'Save',
      submit: true,
      class: 'btn btn-primary btn-shadow',
      handler: function (modal) {
      }
    }
  ]
});

$("#modal-11").fireModal({
  title: 'Form Add Division Activity',
  body: $("#modal-login-part"),
  footerClass: 'bg-whitesmoke',
  buttons: [
    {
      text: 'Save',
      submit: true,
      class: 'btn btn-primary btn-shadow',
      handler: function (modal) {
      }
    }
  ]
});


$('#oh-my-modal').fireModal({
  title: 'Form Project Progress Bulanan',
  body: $("#modal-projectprogress"),
  buttons: [
    {
      text: 'Save',
      submit: true,
      class: 'btn btn-primary btn-shadow',
      handler: function (modal) {
      }
    }
  ]
});
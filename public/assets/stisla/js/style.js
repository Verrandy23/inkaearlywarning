 
// for delete button with sweetalert
 var btn = document.getElementsByName('button');
    for (let i=0; i<btn.length; i++){
        

        document.getElementsByName('button')[i].addEventListener('click', function(){
            var id = i;
            
            swal({
                title: "are you sure for deleting this data ?",
                icon: "warning",
                buttons: true,
                dangerMode: true 
            })
            .then((willDelete)=>{
                if(willDelete){
                    document.getElementById('delete' + id).submit();  
                }
            });
        });
  }    


// validation add data 
   document.getElementById('save').addEventListener('click', event=>{
    var $input = document.querySelectorAll('#form input');
    var $select = document.querySelectorAll('#form select');

    var $errors = []
    var error = false;
    // chek input empty
    for(let i=0; i<$input.length; i++){

        // check input if emtpy
        if($input[i].value == ""){
            var $input_error = $input[i].dataset.form;
            if ($input_error !== undefined){
                $errors.push($input_error);
                error = true;
            }
        }


    }

    //check select empty
    for(let i=0; i<$select.length; i++){
        const $error_name = $select[i].dataset.form;
        if($select[i].value == ""){
            if($error_name !== undefined){
                $errors.push($error_name);
                error =true;
            }
        }
    }

    var text_error = "";

    for(let i=0; i<$errors.length; i++){
        text_error+= "<p class='text-danger'> form " + $errors[i] + " tidak boleh kosong</p>";
    }

    var div_error = document.createElement("div");
    div_error.innerHTML = text_error;
    if (error){
        swal({
            title: 'Error !',
            content: div_error,
            icon: 'error',
        });
    }
    else{
        document.querySelector('#form').submit();
    }
});

// end validation


//validation input number
 var $input = document.querySelectorAll('#form input.form-control');

    for (let i = 0; i<$input.length; i++){
        if($input[i].type === "number"){
            $input[i].addEventListener('keyup', event=>{
                event.target.value = event.target.value.split(/[^0-9]/).join('');
            });
        }
       
    }

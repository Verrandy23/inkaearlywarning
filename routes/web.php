<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
	return redirect('login');
});



Route::group(['middleware' => 'cekstatus'], function() {
	/* --------------------- Dashboard --------------------- */
Route::resource('dashboards', 'V1\Backoffice\DashboardController');
Route::get('backoffice', 'V1\Backoffice\DashboardController@index');
Route::get('backoffice', 'V1\Backoffice\DashboardController@index');
Route::get('dashboard', 'V1\Backoffice\DashboardController@showDashboard');
Route::get('detail_project', 'V1\Backoffice\DashboardController@detail_project');
Route::get('imports', 'V1\Backoffice\DashboardController@import');

/* --------------------- End Project --------------------- */


/* --------------------- Project --------------------- */
Route::resource('projects', 'V1\Backoffice\ProjectController');
Route::get('dashboard_divisions/{id}', 'V1\Backoffice\ProjectController@showDivision');

/* --------------------- End Project --------------------- */


/* --------------------- Project Progress--------------------- */
Route::resource('projects_progress', 'V1\Backoffice\ProjectProgressController');
Route::get(
	'create_progress',
	'V1\Backoffice\ProjectProgressController@create_progress'
);
/* --------------------- End Project --------------------- */

/* --------------------- Project Budget--------------------- */
Route::resource('projects_budget', 'V1\Backoffice\ProjectBudgetController');
Route::get(
	'create_budget',
	'V1\Backoffice\ProjectBudgetController@create_budget'
);
/* --------------------- End Project Budget --------------------- */

/* --------------------- Detail Project --------------------- */
Route::resource('detail_projects', 'V1\Backoffice\DetailProjectController');
/* --------------------- End Detail Project --------------------- */

/* --------------------- Detail Project --------------------- */
Route::resource('divisions', 'V1\Backoffice\DivisionController');
/* --------------------- End Detail Project --------------------- */



/* --------------------- Activity --------------------- */
Route::resource('activities', 'V1\Backoffice\ActivityController');
Route::post('/storeExcelActivity', 'V1\Backoffice\ActivityController@storeExcelActivity');
Route::post('/storeExcelActivityDelivery', 'V1\Backoffice\ActivityController@storeExcelActivityDelivery');
Route::get('activity_delivery/{id}', 'V1\Backoffice\ActivityController@showDelivery');

/* --------------------- End Activity ---------------------*/

/* --------------------- Sub Activity --------------------- */
Route::resource('sub_activities', 'V1\Backoffice\SubActivityController');
Route::post('/storeExcelSubActivity', 'V1\Backoffice\SubActivityController@storeExcelSubActivity');
Route::get('sub_technology/{id}', 'V1\Backoffice\SubActivityController@showTechnology');
Route::get('sub_delivery/{id}', 'V1\Backoffice\SubActivityController@showDelivery');
/* --------------------- End Sub Activity ---------------------*/

/* --------------------- Work Activity --------------------- */
Route::resource('work_activities', 'V1\Backoffice\WorkActivityController');
Route::post('/storeExcelWorkActivity', 'V1\Backoffice\WorkActivityController@storeExcelWorkActivity');
Route::get('type_technology/{id}', 'V1\Backoffice\WorkActivityController@showTechnology');

/* --------------------- End Work Activity ---------------------*/

/* Update for checklist activity */
Route::post('/updateDelivery', 'V1\Backoffice\ActivityController@updateTaskDelivery');
Route::post('/update/{id}', 'V1\Backoffice\WorkActivityController@updateTask');
Route::post('/updatePic', 'V1\Backoffice\WorkActivityController@updatePic');

/* ---------------------End Detail Work Activity --------------------- */

/* --------------------- Late Report --------------------- */
Route::resource('late_history', 'V1\Backoffice\LateHistoryController');
Route::get('report_latehistories_teknologi', 'V1\Backoffice\LateHistoryController@cetak_laporan_late_teknologi');
Route::get('report_latehistories_logistik', 'V1\Backoffice\LateHistoryController@cetak_laporan_late_logistik');
Route::get('report_latehistories_fabrikasi', 'V1\Backoffice\LateHistoryController@cetak_laporan_late_fabrikasi');
Route::get('report_latehistories_finishing', 'V1\Backoffice\LateHistoryController@cetak_laporan_late_finishing');
Route::get('report_latehistories_testing', 'V1\Backoffice\LateHistoryController@cetak_laporan_late_testing');
Route::get('report_latehistories_delivery', 'V1\Backoffice\LateHistoryController@cetak_laporan_late_delivery');
/* --------------------- End Late Report --------------------- */


/* --------------------- Master Data --------------------- */
Route::resource('masters', 'V1\Backoffice\MasterDataController');
Route::get('master_activities', 'V1\Backoffice\MasterDataController@showSubActivity');
Route::get('master_sub_activities', 'V1\Backoffice\MasterDataController@showWorkActivity');
Route::get('report_master_activity', 'V1\Backoffice\MasterDataController@cetak_laporan_activity');
Route::get('report_master_sub_activity', 'V1\Backoffice\MasterDataController@cetak_laporan_sub_activity');
Route::get('report_master_work_activity', 'V1\Backoffice\MasterDataController@cetak_laporan_work_activity');

/* --------------------- End Master Data --------------------- */

/* --------------------- Manage User Data --------------------- */
Route::resource('auths', 'V1\Backoffice\AuthController');
Route::resource('profiles', 'V1\Backoffice\ProfileController');
Route::get('registration', 'V1\Backoffice\AuthController@showFormRegister');
/* --------------------- End Master Data --------------------- */


/* --------------------- Manage Issues  --------------------- */
Route::resource('issues', 'V1\Backoffice\IssueController');
Route::get(
	'create_issue',
	'V1\Backoffice\IssueController@create_issue'
);


Route::get(
	'{id}/master_activity',
	'V1\Backoffice\IssueController@get_master_activity'
);

Route::get(
	'{id}/divisions',
	'V1\Backoffice\IssueController@get_divisions'
);



Route::get(
	'create_issue',
	'V1\Backoffice\IssueController@create_issue'
);
Route::get('create_issue/{id}/sub_activity', 'V1\Backoffice\IssueController@get_sub_activity');
Route::get('create_issue/{id}/work_activity', 'V1\Backoffice\IssueController@get_work_activity');
Route::get(
	'data_issue',
	'V1\Backoffice\IssueController@issue'
);

// Cetak isu report
Route::get('report_issue_teknologi/', 'V1\Backoffice\IssueController@cetak_laporan_isu_teknologi');
Route::get('report_issue_logistik', 'V1\Backoffice\IssueController@cetak_laporan_isu_logistik');
Route::get('report_issue_fabrikasi', 'V1\Backoffice\IssueController@cetak_laporan_isu_fabrikasi');
Route::get('report_issue_finishing', 'V1\Backoffice\IssueController@cetak_laporan_isu_finishing');
Route::get('report_issue_testing', 'V1\Backoffice\IssueController@cetak_laporan_isu_testing');

/* --------------------- End Issues --------------------- */

/* --------------------- Data Notification History --------------------- */
Route::resource('notifications', 'V1\Backoffice\NotificationController');
Route::get(
	'warning_notifications',
	'V1\Backoffice\NotificationController@showWarning'
);
/* --------------------- ENd Notification History --------------------- */


/* --------------------- Reports Data --------------------- */
Route::resource('reports', 'V1\Backoffice\ReportController');
/* --------------------- End Report Data --------------------- */


/* --------------------- Histories Data --------------------- */
Route::resource('histories', 'V1\Backoffice\HistoryController');
Route::get('report_histories_teknologi', 'V1\Backoffice\HistoryController@cetak_laporan_teknologi');
Route::get('report_histories_logistik', 'V1\Backoffice\HistoryController@cetak_laporan_logistik');
Route::get('report_histories_fabrikasi', 'V1\Backoffice\HistoryController@cetak_laporan_fabrikasi');
Route::get('report_histories_finishing', 'V1\Backoffice\HistoryController@cetak_laporan_finishing');
Route::get('report_histories_testing', 'V1\Backoffice\HistoryController@cetak_laporan_testing');
/* --------------------- End Report Data --------------------- *

Route::get('/home', 'HomeController@index')->name('home');

/* --------------------- Pie chart detail --------------------- */

Route::get('
	dashboard_divisions/{id}/{status_name}/{division_id}', 'V1\Backoffice\ProjectController@showDetailPieChartAdmin');

Route::get('
    dashboard_divisions/{id}/{status_name}', 'V1\Backoffice\ProjectController@showDetailPieChartDivision');

/* --------------------- End Pie chart detail --------------------- */

/* --------------------- Count At Sidebar --------------------- */

Route::get(
	'count_finish',
	'V1\Backoffice\NotificationController@countFinish'
);

Route::get(
	'count_warning',
	'V1\Backoffice\NotificationController@countWarning'
);

Route::get(
	'count_late',
	'V1\Backoffice\NotificationController@countLate'
);

Route::get(
	'count_issue',
	'V1\Backoffice\NotificationController@countIssue'
);
/* --------------------- End Count At Sidebar --------------------- */

/* --------------------- Count At Navbar --------------------- */
Route::get(
	'notif',
	'V1\Backoffice\NotificationController@show'
);
/* --------------------- End Count At Navbar --------------------- */


/* ---------------------  Division Additional --------------------- */
Route::resource('division_additional', 'V1\Backoffice\DivisionAdditionalController');

/* --------------------- End Division Additional --------------------- */

Route::get('/home', 'HomeController@index')->name('home');

});

